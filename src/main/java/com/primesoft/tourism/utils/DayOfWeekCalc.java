package com.primesoft.tourism.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class DayOfWeekCalc {


    public HashMap<String, String> getDayOfWeekSearch(String f, String t){

        String today = null;
        HashMap<String, String> map = new HashMap<>();

        Calendar calendar = Calendar.getInstance();

        if(f==null){
            f= Util.getCurrentDateAddDays(0, "Asia/Yerevan");
        }

        if(t==null){
            t = "2040-01-01";
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String returnString = ",";

        Date from;
        Date to;
        try {
            from = dateFormat.parse(f);
            to = dateFormat.parse(t);
        } catch (ParseException e) {
            map.put("dayOfWeeks",returnString);
            return map;
        }

        long difDayCount = getDifferenceDays(from, to);


        if(difDayCount<0){
            String xFrom = f;
            Date x = from;

            f = t;
            from = to;

            to = x;
            t = xFrom;
            difDayCount = getDifferenceDays(from, to);
        }


        if(getDifferenceDays(from, Util.getCurrentDateAddDay(0,"Asia/Yerevan",Constants.DATE_FORMAT))==0l ){
            today = f;
        }else if(getDifferenceDays(to, Util.getCurrentDateAddDay(0,"Asia/Yerevan",Constants.DATE_FORMAT))==0l){
            today = t;
        }



        if(difDayCount<6){

            calendar.setTime(from);
            Integer dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

            if(dayOfWeek>1){
                dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)-1;
            }else{
                dayOfWeek = 7;
            }

            for(int i=0;i<=difDayCount;i++){

                if(i==0){
                    returnString = dayOfWeek.toString();
                }else{
                    if(dayOfWeek+i<=7){
                        returnString +=(dayOfWeek+i);
                    }else{
                        returnString += (dayOfWeek+i-7);
                    }
                }

                if(i!=difDayCount){
                    returnString+= "|";
                }

            }
        }else {
            map.put("today",null);
            map.put("dayOfWeeks",returnString);
            return map;
        }

        if(today==null) {
            map.put("today",null);
            map.put("dayOfWeeks",returnString);
        }else if(returnString.length()>1){
            map.put("today",returnString.substring(0,1));
            map.put("dayOfWeeks",returnString.substring(2));
        }else {
            map.put("today",returnString);
            map.put("dayOfWeeks",null);
        }

        return map;
    }


    public  long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }
}
