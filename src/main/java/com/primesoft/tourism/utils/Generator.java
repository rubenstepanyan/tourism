package com.primesoft.tourism.utils;

import java.security.MessageDigest;

public class Generator {

    public static String stringToSha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    public static Integer getConfirmCode(){
        Integer confirmCode = (int) (Math.random() * 900000 + 100000);
        return confirmCode;
    }
}
