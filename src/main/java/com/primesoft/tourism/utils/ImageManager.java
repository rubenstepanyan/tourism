package com.primesoft.tourism.utils;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ValidationException;
import java.io.*;

public class ImageManager {

    private static final Logger log = LoggerFactory.getLogger(ImageManager.class);

    public void saveImage(MultipartFile file,String imagePath) throws IOException {
        File image = new File(imagePath);
        file.transferTo(image);
        image.setReadable(true,false);
    }

    public void deleteImage(String imagePath) {
        File image = new File(imagePath);
        image.delete();
    }


    public String getImageFormatType(String fileFormat) throws ValidationException {
        //String fileFormat = fileContentType.split("image/")[1];
        if (fileFormat.equalsIgnoreCase(Constants.JPEG)) {
            fileFormat = ".jpeg";
        } else if (fileFormat.equalsIgnoreCase(Constants.JPG)) {
            fileFormat = ".jpg";
        } else if (fileFormat.equalsIgnoreCase(Constants.PNG)) {
            fileFormat = ".png";
        } else {
            throw new ValidationException("Unsupported image format "+fileFormat);
        }
        return fileFormat;
    }


    public static String decodeAndSaveBase64(String base64, String urlPrefix , String folderName, String imageName) {

        imageName = imageName +"__"+ Util.getRandomString((short)10) + ".jpg";
        String url = Constants.SERVER_NAME +urlPrefix + imageName;
        String localPath = folderName+"/" + imageName;
        byte[] data = Base64.decodeBase64(base64);
        try (OutputStream stream = new FileOutputStream(localPath)) {
            stream.write(data);
            final File file = new File(localPath);
            file.setReadable(true, false);
            file.setExecutable(true, false);
            file.setWritable(true, false);

        } catch (FileNotFoundException e) {
            log.info("FileNotFoundException: " + e);
            url = Constants.SERVER_NAME +urlPrefix + "problem.jpg";
        } catch (IOException e) {
            log.info("IOException" + e);
            url = Constants.SERVER_NAME +urlPrefix + "problem.jpg";
        }
        return url;
    }



}
