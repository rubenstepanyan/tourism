package com.primesoft.tourism.utils;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class Constants {

    public final static Type LIST_TYPE = new TypeToken<List<Integer>>() {}.getType();

    public final static String SERVER_NAME = "https://api.visitmyarmenia.com/";
    public final static String YEREVAN_TIMEZONE = "GMT+04:00";

    public final static String FIREBASE_NOTIFICATION_API_URL = "https://fcm.googleapis.com/fcm/send";
    public final static String FIREBASE_AUTHORIZATION = "key=AIzaSyC-uAfmF2s7b7MhMrQQpXoNnNcx_P3gpqA";


    public final static String DRIVER_IMAGE_PATH = "/var/www/tourism/photos/driver/";
    public final static String CAR_IMAGE_PATH = "/var/www/tourism/photos/car/";
    public final static String AGENCY_IMAGE_PATH = "/var/www/tourism/photos/agency/";
    public final static String TOUR_IMAGE_PATH = "/var/www/tourism/photos/tour/";
    public final static String ENTERTAINMENT_PLACE_IMAGE_PATH = "/var/www/tourism/photos/entertainment_place/";
    public final static String CUSTOMER_IMAGE_PATH = "/var/www/tourism/photos/customer/";
    public final static String GUIDE_IMAGE_PATH = "/var/www/tourism/photos/guide/";
    public final static String DYNAMIC_PAGE_IMAGE_PATH = "/var/www/tourism/photos/dynamic_page/";
    public final static String SLIDE_SHOW_IMAGE_PATH = "/var/www/tourism/photos/slide_show/";
    public final static String PARTNER_IMAGE_PATH = "/var/www/tourism/photos/partner/";
    public final static String VIDEO_IMAGE_PATH = "/var/www/tourism/photos/video/";
    public final static String BLOG_IMAGE_PATH = "/var/www/tourism/photos/blog/";

    public final static String DRIVER_IMAGE_PREFIX = "driver/";
    public final static String CAR_IMAGE_PREFIX = "car/";
    public final static String AGENCY_IMAGE_PREFIX = "agency/";
    public final static String TOUR_IMAGE_PREFIX = "tour/";
    public final static String ENTERTAINMENT_PLACE_IMAGE_PREFIX = "entertainment_place/";
    public final static String CUSTOMER_IMAGE_PREFIX = "customer/";
    public final static String GUIDE_IMAGE_PREFIX = "guide/";
    public final static String DYNAMIC_PAGE_IMAGE_PREFIX  = "dynamic_page/";
    public final static String SLIDE_SHOW_IMAGE_PREFIX  = "slide_show/";
    public final static String PARTNER_IMAGE_PREFIX  = "partner/";
    public final static String VIDEO_IMAGE_PREFIX  = "video/";
    public final static String BLOG_IMAGE_PREFIX  = "blog/";

    public final static String PNG = "png";
    public final static String JPG = "jpg";
    public final static String JPEG = "jpeg";
    public final static String APPLICATION_JSON = "application/json";

    public final static String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";
    public final static String DATE_FORMAT = "yyyy-MM-dd";
    public final static String TIME_FORMAT = "HH:mm";

    //paths
    public final static String ADMIN_PATH = "/admin/";
    public final static String DRIVER_PATH = "/driver/";
    public final static String AGENCY_PATH = "/agency/";
    public final static String CUSTOMER_PATH = "/customer/";
    public final static String GUIDE_PATH = "/guide/";

    public final static Long ONE_DAY_IN_MILLIS = 86400000l;
    public final static Long ONE_HOUR_IN_MILLIS = 3600000l;
    public final static Long ONE_MINUTE_IN_MILLIS = 60000l;



    public final static Integer AM_LANGUAGE = 1;
    public final static Integer RU_LANGUAGE = 2;
    public final static Integer EN_LANGUAGE = 3;

    public final static Integer DRIVER_TYPE = 1;
    public final static Integer AGENCY_TYPE = 2;
    public final static Integer CUSTOMER_TYPE = 3;
    public final static Integer GUIDE_TYPE = 4;

    public final static Integer TOUR_ORDER_TYPE = 1;
    public final static Integer GUIDE_ORDER_TYPE = 2;

    public final static Integer SEASONS_TYPE = 1;
    public final static Integer MY_ARMENIA_TYPE = 2;
    public final static Integer USEFUL_INFORMATION_TYPE = 3;


    public final static Integer MUSEUM_TYPE = 1;
    public final static Integer CAFFE_TYPE = 2;
    public final static Integer ENTERTAINMENT_TYPE = 3;
    public final static Integer HOTEL_TYPE = 4;
    public final static Integer ATRAKCION_TYPE = 5;
    public final static Integer TRAVEL_AGENCYES_TYPE = 6;
    public final static Integer NIGHT_CLUB_TYPE = 7;
    public final static Integer EXTREME_TYPE = 8;
    public final static Integer CLINIC_TYPE = 9;
    public final static Integer OTHER_TYPE = 10;


    //Notification
    public final static String NOTIFICATION_TITLE = "Visit my Armenia";
    //Notification actions
    public final static String NOTIFICATION_CANCEL_ORDER = "canceledOrder";
    public final static String NOTIFICATION_NEW_ORDER = "newOrder";
    public final static String NOTIFICATION_QR_SCAN_TOUR_ORDER = "finishTourOrder";
    public final static String NOTIFICATION_QR_SCAN_GUIDE_ORDER = "finishGuideOrder";
    public final static String TOUR_ORDER_STATUS_CHANGED = "tourOrderStatusChanged";
    public final static String GUIDE_ORDER_STATUS_CHANGED = "guideOrderStatusChanged";

    public final static Integer OS_TYPE_ANDROID_AND_WEB = 1;
    public final static Integer OS_TYPE_IOS = 2;

    public final static Integer GLOBAL_TOUR_TYPE = 1;
    public final static Integer PERSONAL_TOUR_TYPE = 2;

    public final static Integer PENDING = 1;
    public final static Integer CANCELED= 2;
    public final static Integer CONFIRM = 3;
    public final static Integer CUSTOMER_PAYED = 4;
    public final static Integer CUSTOMER_CANCELED = 5;
    public final static Integer FINISHED = 6;

    //arca
    public final static Double REFUND_PERCENT = 0.9;
    public final static Double ARCA_PERCENT = 0.025;


    public final static Integer ENTERTAINMENT_PLACE_ITEMS_COUNT = 12;
    public final static Integer VIDEO_ITEMS_COUNT = 12;
    public final static Integer BLOG_ITEMS_COUNT = 15;

    public final static Integer GUIDE_TYPE_1_7 = 1;
    public final static Integer GUIDE_TYPE_8_20 = 2;
    public final static Integer GUIDE_TYPE_21_50 = 3;

    public final static Integer BUY_COUPON = 1;
    public final static Integer BUY_GUIDE= 2;
    public final static Integer BUY_TOUR = 3;
    public final static Integer BUY_TRANSFER = 4;


    //tour type
    public final static Integer ALL = 0;
    public final static Integer JOIN_TO_THE_TOUR = 1;
    public final static Integer PERSONAL_TOUR = 2;

    //order type
    public final static Integer TOUR_ORDER = 0;
    public final static Integer GUIDE_ORDER = 1;

    //sex
    public final static Integer MALE = 1;
    public final static Integer FEMALE = 2;

}
