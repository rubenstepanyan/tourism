package com.primesoft.tourism.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.primesoft.tourism.beans.responseBase.Response;

public class BaseController {

    private static final String stringBracketLeft = "\"\\[";
    private static final String stringBracketRight = "]\"";
    private static final String getArrayBracketLeft = "[";
    private static final String arrayBracketRight = "]";
    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();


    public String createGson(Response response) {
        return gson.toJson(response)
                .replaceAll(stringBracketLeft, getArrayBracketLeft)
                .replaceAll(stringBracketRight, arrayBracketRight);
    }


}
