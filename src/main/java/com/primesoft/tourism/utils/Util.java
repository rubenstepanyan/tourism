package com.primesoft.tourism.utils;



import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Util {

    public static Integer getLanguageId(String language){
        switch (language.toUpperCase()){
            case "HY": return Constants.AM_LANGUAGE;
            case "RU": return Constants.RU_LANGUAGE;
            case "EN": return Constants.EN_LANGUAGE;

            default: return Constants.AM_LANGUAGE;
        }
    }

    public static StringBuilder replaceAll(StringBuilder builder, String from, String to)
    {
        int index = builder.indexOf(from);
        while (index != -1)
        {
            builder.replace(index, index + from.length(), to);
            index += to.length(); // Move to the end of the replacement
            index = builder.indexOf(from, index);
        }
        return builder;
    }

    public static String getImageURL(String prefix,String imageName){
        return Constants.SERVER_NAME +prefix+imageName;
    }


    public static String getTime() {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Yerevan"));
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        String output = sdf.format(c.getTime());
        return output;
    }

    public static String getCurrentDateAddDays(int dayCount, String timeZone) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, dayCount);
        String output = sdf.format(c.getTime());
        return output;
    }

    public static Date getCurrentDateAddDays(int dayCount) {

        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, dayCount);

        return c.getTime();
    }

    public static Date getCurrentDateAddDay(int dayCount, String timeZone,String dateFormat) {

        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

        Calendar c = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
        c.setTime(new Date());
        c.add(Calendar.DATE, dayCount);


        sdf.setTimeZone(TimeZone.getTimeZone(timeZone));

        return parseDate(sdf, c, dateFormat);
    }


    private static Date parseDate(SimpleDateFormat sdf, Calendar c, String dateFormat) {

        try {
            return new SimpleDateFormat(dateFormat).parse(sdf.format(c.getTime()));
        } catch (ParseException e) {
            return new Date();
        }
    }

    public static String formattedDate(String oldDate, Locale locale){
        Date actualDate;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm yyyy-MM-dd");
        SimpleDateFormat newSdf = new SimpleDateFormat("HH:mm yyyy-MMM-dd",locale);
        String newDate = null;
        try {
            actualDate = sdf.parse(oldDate);
            newDate =  newSdf.format(actualDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate == null ? oldDate : newDate;

    }

    public static String getCurrentDateAndTime() {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd-MM-yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+04:00"));
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        String output = sdf.format(c.getTime());
        return output;
    }

    public static String getCurrentDateAndTimeSeconds() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+04:00"));
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        String output = sdf.format(c.getTime());
        return output;
    }



    public static Date addMinutesToDate(int minutes, Date beforeTime){
        long curTimeInMs = beforeTime.getTime();
        Date afterAddingMins = new Date(curTimeInMs + (minutes * Constants.ONE_MINUTE_IN_MILLIS));

        return afterAddingMins;
    }

    public static String getRandomString(short size) {
        SecureRandom random = new SecureRandom();
        String returnString = new BigInteger(130, random).toString(16);

        return returnString.substring(0,size);
    }



    public static int getDateDiffInDay(String startDate, String endDate) {
        return getDateDiffInDay(startDate, endDate, "yyyy-MM-dd");
    }

    public static int getDateDiffInDay(String startDate, String endDate, String dateFormat) {
        return (int)(getDateDiffInMillis(startDate, endDate, dateFormat)/Constants.ONE_DAY_IN_MILLIS);
    }


    public static long getDateDiffInMillis(String startDate, String endDate) {
        return getDateDiffInMillis(startDate, endDate, "yyyy-MM-dd HH:mm:ss");
    }

    public static long getDateDiffInMillis(String startDate, String endDate, String dateFormat) {

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = new SimpleDateFormat(dateFormat).parse(startDate);
            date2=new SimpleDateFormat(dateFormat).parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long diffInMillis = date2.getTime() - date1.getTime();
        System.out.printf("diffInMillis : "+diffInMillis);
        return diffInMillis;
    }




    public static long minuteToMillis(int minutes){
        return minutes*60000;
    }


    public static Double getTrueDouble(Double varable){
        DecimalFormat df = new DecimalFormat(".##");
        return Double.parseDouble(df.format(varable));
    }


    public static long calculatePageCount(long itemsCount, Integer pageItemsCount){
        if(itemsCount>0) {
            if (itemsCount % pageItemsCount == 0) {
                return itemsCount / pageItemsCount;
            } else {
                return (itemsCount / pageItemsCount) + 1;
            }
        }else {
            return 1;
        }
    }

//
//    public static List<VoteResult> getVoteResult(List<Answer> answers){
//        Integer totalQuantity = 0;
//        List<VoteResult> voteResults = new ArrayList<>();
//
//        for (Answer answer:answers) {
//            totalQuantity+=answer.getSelectedQuantity();
//        }
//
//        for (Answer answer:answers) {
//            VoteResult voteResult = new VoteResult();
//            voteResult.setPercent(Util.getTrueDouble((answer.getSelectedQuantity().doubleValue()/totalQuantity)*100 ));
//            voteResult.setPosition(answer.getAnswerPosition());
//            voteResult.setMessage(answer.getAnswer()+" | "+voteResult.getPercent()+"% "+answer.getSelectedQuantity());
//            voteResults.add(voteResult);
//        }
//        return voteResults;
//    }



    public static Integer getRandomNumber(){
        Integer random = (int )(Math.random() * 899999) + 100000;

        return random;

    }



    public static String generateTags(List<String> tags){


        if(tags!=null){
            String tagString = "";
            for (String tag:tags) {
                tagString += tag+",";
            }
            return tagString;
        }else {
            return null;
        }

    }

    public static String replaceAllShitsSymbols(String text){
        return text;
    }

//    public static String formattedStringToTime(String startTime) {
//
//        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
//        String dateInString = startTime;
//
//        try {
//
//            Date date = formatter.parse(dateInString);
//            startTime =  formatter.format(date);
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        return startTime;
//    }


}
