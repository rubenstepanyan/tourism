package com.primesoft.tourism.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadFile {

    private static final Logger log = LoggerFactory.getLogger(ReadFile.class);

    public static String readFile(String filePath) {
        Scanner scanner = null;
        try {
            scanner = new Scanner( new File(filePath), "UTF-8" );
        } catch (FileNotFoundException e) {
            log.error(e.toString());
        }
        String text = scanner.useDelimiter("\\A").next();
        scanner.close();
        return text;
    }


}
