package com.primesoft.tourism.controllers.rest.client.driver.put;

import com.primesoft.tourism.beans.request.DriverRegistrationRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.AlertManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class DriverRegistration extends BaseController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private AgencyService agencyService;


    @RequestMapping(value = Constants.DRIVER_PATH+"registration",method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String driverRegistration(@Valid @RequestBody DriverRegistrationRequest request,
                                     @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Driver existingDriver = driverService.getDriverByPhone(request.getPhone());


        AlertManager smsManager = new AlertManager();
        Integer confirmationCode = Util.getRandomNumber();


        if(existingDriver == null) {

            Driver driver = request.getDriverObject();


            if(request.getAgencyCode() != null){
                Agency agency = agencyService.getAgencyByAgencyCode(request.getAgencyCode());

                if(agency != null){
                    driver.setTourismAgencyId(agency.getId());
                }else {
                    response.setError(language.getAgencyNotFound(),LanguageManager.getMessageKey().getReject());
                    return super.createGson(response);
                }
            }

            driver.setConfirmationCode(confirmationCode.toString());
            driverService.save(driver);

            smsManager.sendSms(driver.getPhone(), confirmationCode.toString());
            response.setMessage(language.getVerificationCodeSentToPhone(), LanguageManager.getMessageKey().getSuccess());

        }else if(!existingDriver.getConfirmed()){

            existingDriver = request.updateDriverObject(existingDriver);
            existingDriver.setConfirmationCode(confirmationCode.toString());
            driverService.save(existingDriver);

            smsManager.sendSms(existingDriver.getPhone(), confirmationCode.toString());
            response.setMessage(language.getVerificationCodeSentToPhone(), LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getAlreadyRegistered(),LanguageManager.getMessageKey().getReject());
        }

        return super.createGson(response);
    }

}
