package com.primesoft.tourism.controllers.rest.client.customer.post;

import com.primesoft.tourism.beans.request.CustomerLoginRequest;
import com.primesoft.tourism.beans.response.CustomerLoginResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Generator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class CustomerLogin extends BaseController {


    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = Constants.CUSTOMER_PATH+"login", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String login(@Valid @RequestBody CustomerLoginRequest request,
                        @RequestHeader(value = "os") Integer os,
                        @RequestHeader("language") String lang) {

        Response<CustomerLoginResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer customer = customerService.getCustomerByMailAndPassword(request.getMail(), Generator.stringToSha256(request.getPassword()));

        if (customer != null){
            if(customer.getConfirmed()) {
                customer.setOsType(os);
                if(customer.getTopicIos() == null || customer.getTopicAndroidAndWeb() == null){
                    customer.setTopicAndroidAndWeb(Constants.CUSTOMER_TYPE+"a"+customer.getId()+"a"+1);
                    customer.setTopicIos(Constants.CUSTOMER_TYPE+"a"+customer.getId()+"a"+2);
                }
                customerService.save(customer);
                response.setData(new CustomerLoginResponse(customer.getAuthorization()));
            }else{
                response.setError(language.getYourAccountIsNotVerifiedPleaseConfirmIt(),LanguageManager.getMessageKey().getNotConfirmed());
            }
        }else{
            response.setError(language.getWrongLoginOrPassword(), LanguageManager.getMessageKey().getReject());
        }

        return super.createGson(response);
    }
}
