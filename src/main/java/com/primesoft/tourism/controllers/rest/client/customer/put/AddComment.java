package com.primesoft.tourism.controllers.rest.client.customer.put;

import com.primesoft.tourism.beans.request.AddCommentRequest;
import com.primesoft.tourism.beans.response.AddCommentResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Blog;
import com.primesoft.tourism.beans.tableBeans.Comment;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.BlogService;
import com.primesoft.tourism.service.interfaces.CommentService;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AddComment extends BaseController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private BlogService blogService;



    @RequestMapping(value = Constants.CUSTOMER_PATH+"addComment",method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String addComment(@Valid @RequestBody AddCommentRequest request,
                             @RequestHeader("language") String lang,
                             @RequestHeader("authorization") String authorization){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        if(customer != null){
            Comment comment = request.getComment(customer);
            commentService.save(comment);
            Blog blog = blogService.getBlog(request.getBlogId());
            blog.setCommentCount(blog.getCommentCount()+1);
            blogService.save(blog);
            Comment newComment = commentService.getOneComment(comment.getId());
            customer.cleanShowFields();
            response.setData(new AddCommentResponse(newComment,customer));
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }
}
