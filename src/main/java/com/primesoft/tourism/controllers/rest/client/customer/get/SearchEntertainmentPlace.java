package com.primesoft.tourism.controllers.rest.client.customer.get;

import com.primesoft.tourism.beans.response.SearchEntertainmentPlacesResponse;
import com.primesoft.tourism.beans.response.getEntertainmentPlaces.Content;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class SearchEntertainmentPlace extends BaseController {

    @Autowired
    private EntertainmentPlaceService entertainmentPlaceService;


    @RequestMapping(value = Constants.CUSTOMER_PATH+"searchEntertainmentPlace", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String searchEntertainmentPlace(@RequestHeader("language") String lang,
                                           @RequestParam(value = "name") String name) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        System.out.println("search Entertainment Place");
        List<Content> contents = entertainmentPlaceService.search(Util.getLanguageId(lang), name);
        response.setData(new SearchEntertainmentPlacesResponse(contents));

        return super.createGson(response);
    }
}
