package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.RejectOrderRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.*;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.NotificationManager;
import com.primesoft.tourism.service.interfaces.*;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class RejectOrder extends BaseController {

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private GuideService guideService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private GuideOrderService guideOrderService;

    @Autowired
    private CustomNotificationService customNotificationService;

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "rejectOrder", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String rejectOrder(@Valid @RequestBody RejectOrderRequest request,
                              @RequestHeader("language") String lang,
                              @RequestHeader("authorization") String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency agency = agencyService.getAgency(authorization);
        Driver driver = driverService.getDriver(authorization);
        Guide guide = guideService.getGuidByAuthorization(authorization);


        if(agency !=null) {
            if(request.getType().equals(Constants.TOUR_ORDER)){
                TourOrder tourOrder = orderService.getTourOrder(request.getOrderId());
                tourOrder.setStatus(Constants.CANCELED);
                tourOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());
                orderService.save(tourOrder);
                response.setMessage(language.getYourOrderRejected(),LanguageManager.getMessageKey().getReject());
                Boolean isTransfer = false;

                if(tourOrder.getTransfer()){
                    isTransfer = true;
                }

                rejectOrderCustomNotification(tourOrder.getCustomerId(),Constants.TOUR_ORDER_STATUS_CHANGED,tourOrder.getStatus(),isTransfer,tourOrder.getId());

            }else if(request.getType().equals( Constants.GUIDE_ORDER)){
                GuideOrder guideOrder = guideOrderService.getGuideOrder(request.getOrderId(),request.getQuantityType(),request.getLanguage());
                response.setMessage(language.getYourOrderRejected(),LanguageManager.getMessageKey().getReject());
                guideOrder.setStatus(Constants.CANCELED);
                guideOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());
                guideOrderService.save(guideOrder);

                rejectOrderCustomNotification(guideOrder.getCustomerId(),Constants.GUIDE_ORDER_STATUS_CHANGED,guideOrder.getStatus(),null,guideOrder.getId());
            }
        }else  if(driver != null){
            if(request.getType().equals(Constants.TOUR_ORDER)){
                TourOrder tourOrder = orderService.getTourOrder(request.getOrderId());
                tourOrder.setStatus(Constants.CANCELED);
                tourOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());
                response.setMessage(language.getYourOrderRejected(),LanguageManager.getMessageKey().getReject());
                orderService.save(tourOrder);

                Boolean isTransfer = false;;

                if(tourOrder.getTransfer()){
                    isTransfer = true;
                }

                rejectOrderCustomNotification(tourOrder.getCustomerId(),Constants.TOUR_ORDER_STATUS_CHANGED,tourOrder.getStatus(),isTransfer, tourOrder.getId());
            }
        }else if(guide != null){
            if(request.getType().equals(Constants.GUIDE_ORDER)){
                response.setMessage(language.getYourOrderRejected(),LanguageManager.getMessageKey().getReject());
                GuideOrder guideOrder = guideOrderService.getGuideOrder(request.getOrderId(),request.getQuantityType(),request.getLanguage());
                guideOrder.setStatus(Constants.CANCELED);
                guideOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());
                guideOrderService.save(guideOrder);
                rejectOrderCustomNotification(guideOrder.getCustomerId(),Constants.GUIDE_ORDER_STATUS_CHANGED,guideOrder.getStatus(),null,guideOrder.getId());

            }
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

    private void rejectOrderCustomNotification(Integer customerId, String action, Integer status, Boolean isTransfer, Integer orderId){
        CustomNotification notification = new CustomNotification();
        NotificationManager notificationManager = new NotificationManager();

        notification.setCustomerId(customerId);
        notification.setAction(Constants.CANCELED);
        notification.setMessageAM("Ձեր պատվերը մերժվեց");
        notification.setMessageRU("Ваш заказ был отклонен");
        notification.setMessageEN("Your order rejected");

        customNotificationService.save(notification);

        Customer customer = customerService.getCustomer(customerId);
        notificationManager.sendNotif(action,LanguageManager.getLanguageInstance(customer.getDashboardLanguage()).getYouHaveCanceledOrder(),customer.getTopicAndroidAndWeb(),customer.getTopicIos(),status,isTransfer,orderId);
    }
}
