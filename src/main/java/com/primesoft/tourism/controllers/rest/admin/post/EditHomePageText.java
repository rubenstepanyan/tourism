package com.primesoft.tourism.controllers.rest.admin.post;

import com.primesoft.tourism.beans.request.editHomePageTextRequest.EditHomePageRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.HomePageText;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.HomePageTextService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class EditHomePageText extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private HomePageTextService homePageTextService;

    @RequestMapping(value = Constants.ADMIN_PATH+"editHomePageText",method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String editHomePageText(@Valid @RequestBody EditHomePageRequest request,
                                   @RequestHeader("authorization") String authorization,
                                   @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator !=null) {

            List<HomePageText> homePageTexts = request.updateHomePageText();
            homePageTextService.save(homePageTexts);

            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());

        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
