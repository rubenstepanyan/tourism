package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.getAllBlogResponse.BlogContentResponse;
import com.primesoft.tourism.beans.response.getAllBlogResponse.GetAllBlogResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.service.interfaces.BlogService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class GetAllBlog extends BaseController {



    @Autowired
    private BlogService blogService;


    @RequestMapping(value = "getAllBlog", method = RequestMethod.GET,produces = Constants.APPLICATION_JSON)
    public String getAllBlog(@RequestHeader("language") String lang,
                             @RequestParam(value = "page", defaultValue = "1")Integer page){

        Response<Object> response = new Response<>();

        System.out.println("Blog");

        Long pageCount = Util.calculatePageCount(blogService.getCount(), Constants.BLOG_ITEMS_COUNT);
        Integer firstNewsId = ((page - 1) * Constants.BLOG_ITEMS_COUNT);
        List<BlogContentResponse> blog = blogService.getAllBlog(Util.getLanguageId(lang), firstNewsId, Constants.BLOG_ITEMS_COUNT);


        response.setData(new GetAllBlogResponse(blog,pageCount));

        return super.createGson(response);
    }
}
