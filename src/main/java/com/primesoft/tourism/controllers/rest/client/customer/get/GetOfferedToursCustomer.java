package com.primesoft.tourism.controllers.rest.client.customer.get;

import com.primesoft.tourism.beans.response.getOfferedTourCustomerResponse.Content;
import com.primesoft.tourism.beans.response.getOfferedTourCustomerResponse.GetOfferedToursCustomerRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.service.interfaces.OfferedTourService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetOfferedToursCustomer extends BaseController {

    @Autowired
    private OfferedTourService offeredTourService;

    @RequestMapping(value = Constants.CUSTOMER_PATH+"getOfferedTours", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getOfferedToursCustomer(@RequestHeader("language") String lang,
                                          @RequestParam(value = "transfer", required = false) Boolean transfer) {

        Response<Object> response = new Response<>();

        List<Content> content = offeredTourService.getAllOfferedTour(Util.getLanguageId(lang), transfer);

        response.setData(new GetOfferedToursCustomerRequest(content));

        return super.createGson(response);
    }
}
