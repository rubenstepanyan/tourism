package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.Currency;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.manager.CurrencyManager;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetCurrency extends BaseController {


    @RequestMapping(value = "getCurrency",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getCurrency(){

        System.out.println("Barev Hayko");
        Response<Currency> response = new Response<>();

        response.setData(CurrencyManager.getCurrency());

        return super.createGson(response);
    }

}
