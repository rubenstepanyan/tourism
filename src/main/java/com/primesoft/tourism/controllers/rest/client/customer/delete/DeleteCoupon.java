package com.primesoft.tourism.controllers.rest.client.customer.delete;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.CouponService;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DeleteCoupon extends BaseController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CouponService couponService;


    @RequestMapping(value = Constants.CUSTOMER_PATH+"deleteCoupon", method = RequestMethod.DELETE, produces = Constants.APPLICATION_JSON)
    public String deleteCoupon(@RequestHeader("language") String lang,
                               @RequestHeader("authorization") String authorization,
                               @RequestParam(value = "id") Integer couponId) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        if(customer!=null){
            couponService.delete(customer.getId(), couponId);
            response.setMessage(language.getSuccessfullyDeleted(), LanguageManager.getMessageKey().getSuccess());
        }else {
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }

}
