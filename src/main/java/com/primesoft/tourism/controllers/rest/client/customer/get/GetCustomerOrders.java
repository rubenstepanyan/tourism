package com.primesoft.tourism.controllers.rest.client.customer.get;

import com.primesoft.tourism.beans.response.getCustomerOrders.GetCustomerOrderResponse;
import com.primesoft.tourism.beans.response.getCustomerOrders.GuideOrderContent;
import com.primesoft.tourism.beans.response.getCustomerOrders.TourOrderContent;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.ArcaManager;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.service.interfaces.GuideOrderService;
import com.primesoft.tourism.service.interfaces.OrderService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class GetCustomerOrders extends BaseController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private GuideOrderService guideOrderService;

    @Autowired
    private ArcaManager arcaManager;

    @RequestMapping(value = Constants.CUSTOMER_PATH+"getOrders", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getOrders(@RequestHeader("language") String lang,
                            @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        if(customer != null){
            arcaManager.checkOrder(customer);

            List<TourOrderContent> tourOrderContents = orderService.getTourOrderByCustomerId(customer.getId(), Util.getLanguageId(lang));

            List<GuideOrderContent> guideOrderContents = guideOrderService.getGuideOrderByCustomerId(customer.getId());

            response.setData(new GetCustomerOrderResponse(tourOrderContents,guideOrderContents));

        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
