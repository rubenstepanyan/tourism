package com.primesoft.tourism.controllers.rest.admin.post;

import com.primesoft.tourism.beans.request.EditCustomerRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditCustomer extends BaseController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private AdministratorService administratorService;


    @RequestMapping(value = Constants.ADMIN_PATH+"editCustomer", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String editCustomer(@Valid @RequestBody EditCustomerRequest request,
                                          @RequestHeader("language") String lang,
                                          @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){
            Customer customer = customerService.getCustomer(request.getId());
            request.updateCustomer(customer);
            customerService.save(customer);
            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
