package com.primesoft.tourism.controllers.rest.client.customer.get;

import com.primesoft.tourism.beans.response.getAllGuideResponse.GetAllGuideContentResponse;
import com.primesoft.tourism.beans.response.getAllGuideResponse.GetAllGuideResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetAllGuide extends BaseController {


    @Autowired
    private GuideService guideService;


    @RequestMapping(value = Constants.CUSTOMER_PATH+"getAllGuide", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getAllGuide(@RequestHeader("language") String lang,
                              @RequestParam(value = "from", required = false) String from,
                              @RequestParam(value = "to", required = false) String to,
                              @RequestParam(value = "language", required = false) String guideLanguage,
                              @RequestParam(value = "type", required = false,defaultValue = "1") Integer type,
                              @RequestParam(value = "page", required = false) Integer page,
                              @RequestParam(value = "city", required = false) Integer city) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        List<GetAllGuideContentResponse> guideContents = guideService.getAll(from,to,guideLanguage,type,city);

        response.setData(new GetAllGuideResponse(guideContents,page));


        return super.createGson(response);
    }
}
