package com.primesoft.tourism.controllers.rest.admin.get;

import com.primesoft.tourism.beans.response.GetAdminEntertainmentPlaceResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlace;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceInfo;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlacePhoto;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceWorkingDays;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceInfoService;
import com.primesoft.tourism.service.interfaces.EntertainmentPlacePhotoService;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceService;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceWorkingDaysService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetAdminEntertainmentPlace extends BaseController {

    @Autowired
    private EntertainmentPlaceService entertainmentPlaceService;

    @Autowired
    private EntertainmentPlaceInfoService entertainmentPlaceInfoService;

    @Autowired
    private EntertainmentPlaceWorkingDaysService entertainmentPlaceWorkingDaysService;

    @Autowired
    private EntertainmentPlacePhotoService entertainmentPlacePhotoService;

    @RequestMapping(value = Constants.ADMIN_PATH+"getEntertainmentPlace", method = RequestMethod.GET,produces = Constants.APPLICATION_JSON)
    public String getEntertainmentPlace(@RequestParam(value = "id") Integer id,
                                        @RequestHeader(value = "language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        EntertainmentPlace place = entertainmentPlaceService.getById(id);
        List<EntertainmentPlaceInfo> info = entertainmentPlaceInfoService.getAllInfo(id);
        List<EntertainmentPlaceWorkingDays> workingDays = entertainmentPlaceWorkingDaysService.findAll(id);
        List<EntertainmentPlacePhoto> photos = entertainmentPlacePhotoService.getAll(place.getId());

        response.setData(new GetAdminEntertainmentPlaceResponse(place, info, workingDays, photos));

        return super.createGson(response);
    }


}
