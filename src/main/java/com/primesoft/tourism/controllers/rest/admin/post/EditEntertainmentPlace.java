package com.primesoft.tourism.controllers.rest.admin.post;

import com.primesoft.tourism.beans.request.editEntertainmentPlace.EditEntertainmentPlaceRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlace;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceInfo;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceWorkingDays;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.*;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class EditEntertainmentPlace extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private EntertainmentPlaceService entertainmentPlaceService;

    @Autowired
    private EntertainmentPlaceInfoService entertainmentPlaceInfoService;

    @Autowired
    private EntertainmentPlaceWorkingDaysService entertainmentPlaceWorkingDaysService;

    @Autowired
    private EntertainmentPlacePhotoService entertainmentPlacePhotoService;


    @RequestMapping(value = Constants.ADMIN_PATH+"editEntertainmentPlace",method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String editEntertainmentPlace(@Valid @RequestBody EditEntertainmentPlaceRequest request,
                                         @RequestHeader("language")String lang,
                                         @RequestHeader("authorization")String authorization){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){

            EntertainmentPlace entertainmentPlace = entertainmentPlaceService.getById(request.getId());
            request.updateEntertainmentPlace(entertainmentPlace);
            entertainmentPlaceService.save(entertainmentPlace);

            List<EntertainmentPlaceInfo> info = entertainmentPlaceInfoService.getAllInfo(request.getId());
            request.updateEntertainmentPlaceInfo(info);
            entertainmentPlaceInfoService.save(info);

            List<EntertainmentPlaceWorkingDays> workingDays = entertainmentPlaceWorkingDaysService.findAll(request.getId());
            request.updateWorkingDays(workingDays);
            entertainmentPlaceWorkingDaysService.save(workingDays);


            if(request.getDeletedPhotos()!=null && request.getDeletedPhotos().size()>0) {
                entertainmentPlacePhotoService.deleteById(request.getDeletedPhotos());
            }

            response.setData(entertainmentPlace);
        }else {
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

}
