package com.primesoft.tourism.controllers.rest.client.guide.get;

import com.primesoft.tourism.beans.response.getGuidePriceList.GetAllGuidePriceListResponse;
import com.primesoft.tourism.beans.response.getGuidePriceList.PriceListGroup;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.beans.tableBeans.GuidePriceList;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.GuidePriceListService;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class GetAllGuidePriceList extends BaseController {

    @Autowired
    private GuideService guideService;

    @Autowired
    private GuidePriceListService guidePriceListService;


    @RequestMapping(value = Constants.GUIDE_PATH+"getAllGuidePriceList", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getAllGuidePriceList(@RequestHeader("language") String lang,
                                       @RequestHeader("authorization")String authorization) {

        Response<GetAllGuidePriceListResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Guide guide = guideService.getGuidByAuthorization(authorization);

        if(guide != null){

            List<GuidePriceList> guidePriceLists = guidePriceListService.getAll(guide.getId());

            GetAllGuidePriceListResponse getAllGuidePriceListResponse = new GetAllGuidePriceListResponse();
            List<PriceListGroup> priceListGroups = new ArrayList<>();

            List<Long> groupsIds = guidePriceListService.getGroupIds(guide.getId());

            System.out.println("groupsIds :"+groupsIds);

            for (int i = 0; i < groupsIds.size(); i++) {
                PriceListGroup group = new PriceListGroup();
                for (int j = 0; j < guidePriceLists.size(); j++) {

                    if(groupsIds.get(i).equals(guidePriceLists.get(j).getGroupId())){
                        group.getGuidePriceLists().add(guidePriceLists.get(j));
                    }
                }
                priceListGroups.add(group);
            }

            getAllGuidePriceListResponse.setPriceListGroups(priceListGroups);

            response.setData(getAllGuidePriceListResponse);
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }
}
