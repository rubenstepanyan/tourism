package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.service.interfaces.PartnerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetPartners extends BaseController {


    @Autowired
    private PartnerService partnerService;

    @RequestMapping(value = "getPartners",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getPartners(){

        Response<Object> response = new Response<>();

        response.setData(partnerService.getAll());

        return super.createGson(response);
    }
}
