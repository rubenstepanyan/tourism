package com.primesoft.tourism.controllers.rest.admin.get;

import com.primesoft.tourism.beans.response.getOfferedTourResponse.GetOfferedTourResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.OfferedTour;
import com.primesoft.tourism.beans.tableBeans.OfferedTourInfo;
import com.primesoft.tourism.service.interfaces.OfferedTourInfoService;
import com.primesoft.tourism.service.interfaces.OfferedTourService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GetOfferedTour extends BaseController {

    @Autowired
    private OfferedTourService offeredTourService;

    @Autowired
    private OfferedTourInfoService offeredTourInfoService;

    @RequestMapping(value = Constants.ADMIN_PATH+"getOfferedTour", method = RequestMethod.GET,produces = Constants.APPLICATION_JSON)
    public String getOfferedTour(@RequestParam("id") Integer id){

        Response<Object> response = new Response<>();

        OfferedTour offeredTour = offeredTourService.getOfferedTour(id);
        List<OfferedTourInfo> contents = offeredTourInfoService.getInfo(offeredTour.getId());
        GetOfferedTourResponse getOfferedTourResponse = new GetOfferedTourResponse(offeredTour, contents);
        response.setData(getOfferedTourResponse);

        return super.createGson(response);
    }

}
