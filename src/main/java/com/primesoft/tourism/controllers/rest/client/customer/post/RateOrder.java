package com.primesoft.tourism.controllers.rest.client.customer.post;

import com.primesoft.tourism.beans.request.RateOrderRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.*;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.*;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class RateOrder extends BaseController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private GuideOrderService guideOrderService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private GuideService guideService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private TourService tourService;

    @Autowired
    private GuidePriceListService  guidePriceListService;



    @RequestMapping(value = Constants.CUSTOMER_PATH+"rateOrder", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String rateOrder(@Valid @RequestBody RateOrderRequest request,
                            @RequestHeader("language") String lang,
                            @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        if(customer != null) {
            if (request.getType() == Constants.TOUR_ORDER) {
                TourOrder tourOrder = orderService.getTourOrder(request.getId(),customer.getId());

                if(tourOrder!=null && customer.getId().equals(tourOrder.getCustomerId())) {
                    tourOrder.setRating(request.getRating());
                    orderService.save(tourOrder);
                    if(tourOrder.getReferenceType().equals(Constants.AGENCY_TYPE)){
                        Agency agency = agencyService.getAgency(tourOrder.getReference());

                        System.out.println("request.getRating()"+ request.getRating());
                        System.out.println("agency.getTotalRating()"+ agency.getTotalRating());

                        agency.setCurrentRating(calculateRating(request.getRating(), agency.getTotalRating(), agency.getCurrentRating()));
                        agency.setTotalRating(agency.getTotalRating()+request.getRating());

                        System.out.println("agency.getTotalRating()"+ agency.getTotalRating());
                        agencyService.save(agency);

                        tourService.updateTourRating(agency.getId(), agency.getCurrentRating(), Constants.AGENCY_TYPE);


                    }else if(tourOrder.getReferenceType().equals(Constants.DRIVER_TYPE)) {
                        Driver driver = driverService.getDriver(tourOrder.getReference());

                        driver.setCurrentRating(calculateRating(request.getRating(), driver.getTotalRating(), driver.getCurrentRating()));
                        driver.setTotalRating(driver.getTotalRating()+request.getRating());

                        driverService.save(driver);
                        tourService.updateTourRating(driver.getId(), driver.getCurrentRating(), Constants.DRIVER_TYPE);
                    }
                    response.setMessage(language.getThankYouForYourRating(), LanguageManager.getMessageKey().getSuccess());
                }else {
                    response.setError(language.getTourNotFound(), LanguageManager.getMessageKey().getReject());
                }
            } else if (request.getType() == Constants.GUIDE_ORDER) {

                GuideOrder guideOrder = guideOrderService.getGuideOrderByIdAndCustomerId(request.getId(),customer.getId());
                guideOrder.setRating(request.getRating());



                if(guideOrder.getReference()!= null){
                    Agency agency = agencyService.getAgency(guideOrder.getReference());

                    agency.setCurrentRating(calculateRating(request.getRating(), agency.getTotalRating(), agency.getCurrentRating()));
                    agency.setTotalRating(agency.getTotalRating()+request.getRating());

                    agencyService.save(agency);

                    guideOrder.setPriceRating(agency.getCurrentRating());
                    guideOrderService.save(guideOrder);
                    guidePriceListService.updateGuideRating(guideOrder.getGuideId(),agency.getCurrentRating());

                }else{
                    Guide guide = guideService.getGuidById(guideOrder.getGuideId());

                    guide.setCurrentRating(calculateRating(request.getRating(), guide.getTotalRating(), guide.getCurrentRating()));
                    guide.setTotalRating(guide.getTotalRating()+request.getRating());

                    guideService.save(guide);

                    guideOrder.setPriceRating(guide.getCurrentRating());
                    guidePriceListService.updateGuideRating(guideOrder.getGuideId(),guide.getCurrentRating());
                }

                guideOrderService.save(guideOrder);


                response.setMessage(language.getThankYouForYourRating(),LanguageManager.getMessageKey().getSuccess());
            }
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }



        return super.createGson(response);
    }


    private Double calculateRating(Integer rating, Integer totalRating, Double currentRating){
        Integer ratingCount = (int)Math.round(totalRating/currentRating)+1;
        totalRating += rating;
        return Util.getTrueDouble(totalRating.doubleValue() /ratingCount);
    }
}
