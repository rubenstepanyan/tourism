package com.primesoft.tourism.controllers.rest.client.customer.get;

import com.primesoft.tourism.beans.response.GetCustomerOptionsResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.beans.tableBeans.GuideOrder;
import com.primesoft.tourism.beans.tableBeans.TourOrder;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.service.interfaces.GuideOrderService;
import com.primesoft.tourism.service.interfaces.OrderService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GetCustomerOptions extends BaseController {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private OrderService orderService;

    @Autowired
    private GuideOrderService guideOrderService;

    @RequestMapping(value = Constants.CUSTOMER_PATH+"getOptions", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String GetCustomerOptions(@RequestHeader("language") String lang,
                                  @RequestHeader("authorization")String authorization) {

        Response<GetCustomerOptionsResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer customer  = customerService.getCustomerByAuthorization(authorization);

        if(customer != null){
            List<TourOrder> tourOrder = orderService.getTourOrderByCustomerIdAndStatus(customer.getId(),Constants.CONFIRM);
            List<GuideOrder> guideOrders = guideOrderService.getGuideOrderByCustomerIdAndStatus(customer.getId(),Constants.CONFIRM);

            GetCustomerOptionsResponse getCustomerOptionsResponse = new GetCustomerOptionsResponse();
            getCustomerOptionsResponse.setTopicAndroid(customer.getTopicAndroidAndWeb());
            getCustomerOptionsResponse.setTopicIos(customer.getTopicIos());
            if((tourOrder != null || guideOrders != null) && (tourOrder.size()>0 || guideOrders.size()>0)){
                getCustomerOptionsResponse.setNew(true);
            }else{
                getCustomerOptionsResponse.setNew(false);
            }
            getCustomerOptionsResponse.setTutorialUrl(getTutorialUrl(lang));
            response.setData(getCustomerOptionsResponse);
        }else{
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

    private String getTutorialUrl(String lang){

        String tutorialUrl;
        if(lang.toUpperCase().equals("HY")){
            tutorialUrl = "https://www.youtube.com/watch?v=NEqHPxnxrFc&list=RDMMNEqHPxnxrFc&start_radio=1";
        }else if(lang.toUpperCase().equals("RU")){
            tutorialUrl = "https://www.youtube.com/watch?v=vtdwnDjN9GY&list=RDMMNEqHPxnxrFc&index=4";
        }else{
            tutorialUrl = "https://www.youtube.com/watch?v=9dcVOmEQzKA&list=RDGMEMHDXYb1_DDSgDsobPsOFxpAVM9dcVOmEQzKA&index=1";
        }

        return tutorialUrl;

    }
}
