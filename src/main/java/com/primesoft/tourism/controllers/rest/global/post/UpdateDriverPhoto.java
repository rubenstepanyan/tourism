package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.response.UpdateDriverPhotoRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class UpdateDriverPhoto extends BaseController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private AdministratorService administratorService;


    @RequestMapping(value = "updateDriverPhoto", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String updateDriverPhoto(@Valid @RequestBody  UpdateDriverPhotoRequest request,
                              @RequestHeader("authorization") String authorization,
                              @RequestHeader("language") String lang) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);
        Driver existingDriver = driverService.getDriver(authorization);

        if(administrator != null){
            Driver driver = driverService.getDriver(request.getId());
            request.updateDriverPhoto(driver);
            driverService.save(driver);
            response.setMessage(language.getPhotoSuccessfullyChanged(),LanguageManager.getMessageKey().getSuccess());
        }else if(existingDriver != null){
            request.updateDriverPhoto(existingDriver);
            driverService.save(existingDriver);
            response.setMessage(language.getPhotoSuccessfullyChanged(),LanguageManager.getMessageKey().getSuccess());
        }
        else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

}
