package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.EditGuideRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditGuide extends BaseController {

    @Autowired
    private GuideService guideService;

    @Autowired
    private AdministratorService administratorService;


    @RequestMapping(value = "editGuide",method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String editGuide(@Valid @RequestBody EditGuideRequest request,
                             @RequestHeader("authorization") String authorization,
                             @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);
        Guide existingGuide = guideService.getGuidByAuthorization(authorization);

        if(administrator !=null) {
            Guide guide = guideService.getGuidById(request.getId());
            request.updateGuide(guide);
            guide.setApproved(request.getApproved());
            guideService.save(guide);
            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());

        }else if(existingGuide != null){
            request.editGuide(existingGuide);
            guideService.save(existingGuide);
            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
