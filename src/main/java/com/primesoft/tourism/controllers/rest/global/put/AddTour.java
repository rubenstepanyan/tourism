package com.primesoft.tourism.controllers.rest.global.put;

import com.primesoft.tourism.beans.request.AddTourRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.*;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.*;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AddTour extends BaseController {


    @Autowired
    private DriverService driverService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private CarService carService;

    @Autowired
    private TourService tourService;

    @Autowired
    private OfferedTourService offeredTourService;


    @RequestMapping(value = "addTour", method = RequestMethod.PUT, produces = Constants.APPLICATION_JSON)
    public String addTour(@Valid @RequestBody AddTourRequest request,
                          @RequestHeader("authorization") String authorization,
                          @RequestHeader("language") String lang) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Car car = null;
        Driver driver = driverService.getDriver(authorization);
        Agency agency = agencyService.getAgency(authorization);

        OfferedTour offeredTour = offeredTourService.getOfferedTour(request.getOfferedTourId());
        if (request.getDaysOfWeek() == null && request.getStartDate() == null) {
            response.setError(language.getPleaseAddStartDate(),LanguageManager.getMessageKey().getReject());
            return super.createGson(response);
        }
            if (driver != null) {
                if (driver != null) {
                    car = carService.getDriverCar(driver.getId());
                }

                Tour tour = request.getTourObject(driver, null, car, offeredTour);
                tourService.save(tour);

                response.setMessage(language.getSuccessfullyAdded(), LanguageManager.getMessageKey().getSuccess());
            } else if (agency != null) {
                car = carService.getCar(request.getCarId());
                Tour tour = request.getTourObject(null, agency, car, offeredTour);
                tourService.save(tour);
                response.setMessage(language.getSuccessfullyAdded(), LanguageManager.getMessageKey().getSuccess());
            } else {
                response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
            }

        return super.createGson(response);
    }

}
