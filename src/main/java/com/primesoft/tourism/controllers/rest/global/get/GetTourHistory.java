package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.getHistoryResponse.GetTourOrderHistoryContentResponse;
import com.primesoft.tourism.beans.response.getHistoryResponse.GetTourOrderHistoryResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.service.interfaces.OrderService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetTourHistory extends BaseController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private OrderService orderService;



    @RequestMapping(value = "getTourHistory", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getTourHistory(@RequestHeader("language") String lang,
                                 @RequestHeader("authorization")String authorization,
                                 @RequestParam(value = "page", required = false) Integer page,
                                 @RequestParam(value = "tourId", required = false) Integer tourId) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency agency = agencyService.getAgency(authorization);
        Driver driver = driverService.getDriver(authorization);

        if(agency != null){

            List<GetTourOrderHistoryContentResponse> tourOrderContents = orderService.getOrderHistory(agency.getId(),null, Util.getLanguageId(lang),tourId);

            response.setData(new GetTourOrderHistoryResponse(tourOrderContents,page));

        }else if(driver != null){

            List<GetTourOrderHistoryContentResponse> tourOrderContents = orderService.getOrderHistory(null,driver.getId(), Util.getLanguageId(lang),0);

            response.setData(new GetTourOrderHistoryResponse(tourOrderContents,page));
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }



        return super.createGson(response);
    }
}
