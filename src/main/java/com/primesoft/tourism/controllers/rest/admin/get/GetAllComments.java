package com.primesoft.tourism.controllers.rest.admin.get;

import com.primesoft.tourism.beans.response.GetAllCommentsResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.CommentService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class GetAllComments extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = Constants.ADMIN_PATH+"getAllComments",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getAllComments(@RequestHeader("authorization") String authorization,
                                 @RequestHeader("language") String lang,
                                 @RequestParam("from") String from,
                                 @RequestParam("to") String to){

        Response<List<GetAllCommentsResponse>> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);
        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){
            response.setData(commentService.getAllComments(from,to));
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
