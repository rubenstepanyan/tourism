package com.primesoft.tourism.controllers.rest.admin.delete;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceInfoService;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceService;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceWorkingDaysService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DeleteEntertainmentPlace extends BaseController {


    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private EntertainmentPlaceService entertainmentPlaceService;

    @Autowired
    private EntertainmentPlaceInfoService entertainmentPlaceInfoService;

    @Autowired
    private EntertainmentPlaceWorkingDaysService entertainmentPlaceWorkingDaysService;



    @RequestMapping(value = Constants.ADMIN_PATH+"deleteEntertainmentPlace",method = RequestMethod.DELETE,produces = Constants.APPLICATION_JSON)
    public String deleteEntertainmentPlace(@RequestHeader("language")String lang,
                                           @RequestHeader("authorization")String authorization,
                                           @RequestParam(value = "id") Integer id){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){

            entertainmentPlaceService.delete(id);
            entertainmentPlaceInfoService.delete(id);
            entertainmentPlaceWorkingDaysService.delete(id);

            response.setMessage(language.getSuccessfullyDeleted(),LanguageManager.getMessageKey().getSuccess());
        }else {
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

}
