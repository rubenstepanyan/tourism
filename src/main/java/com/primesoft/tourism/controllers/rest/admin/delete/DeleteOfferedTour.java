package com.primesoft.tourism.controllers.rest.admin.delete;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.OfferedTourInfoService;
import com.primesoft.tourism.service.interfaces.OfferedTourService;
import com.primesoft.tourism.service.interfaces.TourService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DeleteOfferedTour extends BaseController {


    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private OfferedTourService offeredTourService;

    @Autowired
    private OfferedTourInfoService offeredTourInfoService;

    @Autowired
    private TourService tourService;

    @RequestMapping(value = Constants.ADMIN_PATH+"deleteOfferedTour", method = RequestMethod.DELETE,produces = Constants.APPLICATION_JSON)
    public String deleteOfferedTour(@RequestParam("id")Integer id,
                                 @RequestHeader("authorization") String authorization,
                                 @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){
            offeredTourService.delete(id);
            offeredTourInfoService.deleteByOfferedTourId(id);
            tourService.deleteAllByOfferedTorId(id);

            response.setMessage(language.getSuccessfullyDeleted(), LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }


}
