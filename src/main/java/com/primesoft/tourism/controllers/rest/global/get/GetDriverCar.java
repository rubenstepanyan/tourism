package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.GetDriverCarResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Car;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.CarService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetDriverCar extends BaseController {



    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private CarService carService;

    @RequestMapping(value = "getDriverCar",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getDriverCar(@RequestHeader("language") String lang,
                               @RequestHeader("authorization")String authorization,
                               @RequestParam(value = "driverId", required = false) Integer driverId){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        if(driverId != null){
            Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);
            if(administrator != null){
                List<Car> cars = carService.getCars(driverId,Constants.DRIVER_TYPE);
                response.setData(new GetDriverCarResponse(cars));
            }else {
                response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
            }
        }else{
            Driver driver = driverService.getDriver(authorization);
            if(driver != null){
                List<Car> cars = carService.getCars(driver.getId(),Constants.DRIVER_TYPE);
                response.setData(new GetDriverCarResponse(cars));
            }else {
                response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
            }
        }

        return super.createGson(response);
    }


}
