package com.primesoft.tourism.controllers.rest.client.guide.delete;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.GuidePriceListService;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DeleteGuidePriceList extends BaseController {

    @Autowired
    private GuideService guideService;

    @Autowired
    private GuidePriceListService guidePriceListService;

    @RequestMapping(value = Constants.GUIDE_PATH+"deleteGuidePriceList",method = RequestMethod.DELETE,produces = Constants.APPLICATION_JSON)
    public String deleteGuidePriceList(@RequestHeader("language")String lang,
                                       @RequestHeader("authorization")String authorization,
                                       @RequestParam(value = "groupId") Long groupId){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Guide guide = guideService.getGuidByAuthorization(authorization);

        System.out.println("groupId: "+groupId);

        if(guide != null){
            guidePriceListService.deleteByGroupId(groupId);
            response.setMessage(language.getSuccessfullyDeleted(),LanguageManager.getMessageKey().getSuccess());
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }
}
