package com.primesoft.tourism.controllers.rest.client.driver.post;

import com.primesoft.tourism.beans.request.ConfirmDriverRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ConfirmDriver extends BaseController {

    @Autowired
    private DriverService driverService;

    @RequestMapping(value = Constants.DRIVER_PATH+"confirm", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String confirm(@Valid @RequestBody ConfirmDriverRequest request,
                          @RequestHeader("language") String lang) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Driver driver = driverService.getDriverByPhoneAndConfirmationCode(request.getPhone(),request.getConfirmationCode());

        if(driver != null){
            driver.setConfirmed(true);
            response.setMessage(language.getSuccessfullyConfirmed(), LanguageManager.getMessageKey().getSuccess());
            driverService.save(driver);
        }else{
            response.setError(language.getWrongConfirmationCode(), LanguageManager.getMessageKey().getReject());
        }


        return super.createGson(response);
    }
}
