package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.GetTourReservationResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.beans.tableBeans.TourReservation;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.service.interfaces.TourReservationService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetTourReservation extends BaseController {

    @Autowired
    private TourReservationService tourReservationService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private AgencyService agencyService;


    @RequestMapping(value = "getTourReservation", method = RequestMethod.GET,produces = Constants.APPLICATION_JSON)
    public String getTourReservation(@RequestHeader("language") String lang,
                                     @RequestHeader("authorization") String authorization,
                                     @RequestParam("tourId") Integer tourId,
                                     @RequestParam(value = "reservationDate", required = false) String reservationDate){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Driver driver = driverService.getDriver(authorization);
        Agency agency = agencyService.getAgency(authorization);

        if(agency != null){
            List<TourReservation> tourReservations;
            if(reservationDate != null){
               tourReservations = tourReservationService.getTourReservation(agency.getId(),Constants.AGENCY_TYPE, tourId,reservationDate);
            }else{
               tourReservations = tourReservationService.getAllTourReservation(agency.getId(),Constants.AGENCY_TYPE, tourId);
            }
            response.setData(new GetTourReservationResponse(tourReservations));
        }else if(driver != null){
            List<TourReservation> tourReservations;
            if(reservationDate != null){
               tourReservations = tourReservationService.getTourReservation(agency.getId(),Constants.AGENCY_TYPE, tourId,reservationDate);
            }else{
                tourReservations = tourReservationService.getAllTourReservation(driver.getId(),Constants.DRIVER_TYPE, tourId);
            }
            response.setData(new GetTourReservationResponse(tourReservations));
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }



        return super.createGson(response);
    }
}
