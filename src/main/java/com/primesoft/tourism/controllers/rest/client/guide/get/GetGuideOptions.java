package com.primesoft.tourism.controllers.rest.client.guide.get;

import com.primesoft.tourism.beans.response.GetGuideOptionsResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.beans.tableBeans.GuideOrder;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.GuideOrderService;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GetGuideOptions extends BaseController {


    @Autowired
    private GuideService guideService;

    @Autowired
    private GuideOrderService guideOrderService;

    @RequestMapping(value = Constants.GUIDE_PATH+"getOptions", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getGuideOptions(@RequestHeader("language") String lang,
                                  @RequestHeader("authorization")String authorization) {

        Response<GetGuideOptionsResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Guide guide = guideService.getGuidByAuthorization(authorization);

        if(guide != null){
            GetGuideOptionsResponse getGuideOptionsResponse = new GetGuideOptionsResponse();
            List<GuideOrder> guideOrders = guideOrderService.getGuideOrderByGuideIdAndStatus(guide.getId(),Constants.PENDING);
            if(guide.getTourismAgencyId()!=null){
                getGuideOptionsResponse.setAgencyGuide(true);
            }else {
                getGuideOptionsResponse.setAgencyGuide(false);
            }

            if(guideOrders != null && guideOrders.size()>0){
                getGuideOptionsResponse.setNew(true);
            }else{
                getGuideOptionsResponse.setNew(false);
            }
            getGuideOptionsResponse.setTopicAndroid(guide.getTopicAndroidAndWeb());
            getGuideOptionsResponse.setTopicIos(guide.getTopicIos());
            getGuideOptionsResponse.setTutorialUrl(getTutorialUrl(lang));
            response.setData(getGuideOptionsResponse);
        }else{
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

    private String getTutorialUrl(String lang){

        String tutorialUrl;
        if(lang.toUpperCase().equals("HY")){
            tutorialUrl = "https://www.youtube.com/watch?v=NEqHPxnxrFc&list=RDMMNEqHPxnxrFc&start_radio=1";
        }else if(lang.toUpperCase().equals("RU")){
            tutorialUrl = "https://www.youtube.com/watch?v=vtdwnDjN9GY&list=RDMMNEqHPxnxrFc&index=4";
        }else{
            tutorialUrl = "https://www.youtube.com/watch?v=9dcVOmEQzKA&list=RDGMEMHDXYb1_DDSgDsobPsOFxpAVM9dcVOmEQzKA&index=1";
        }

        return tutorialUrl;

    }

}
