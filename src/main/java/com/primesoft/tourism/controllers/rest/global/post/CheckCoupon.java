package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.CheckCouponRequest;
import com.primesoft.tourism.beans.response.checkCouponResponse.CouponData;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.CouponService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class CheckCoupon extends BaseController {

    @Autowired
    private CouponService couponService;

    @RequestMapping(value = "checkCoupon", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String checkCoupon(@Valid @RequestBody CheckCouponRequest request,
                             @RequestHeader("language") String lang) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        CouponData coupon = couponService.getCouponByNumber(request.getCouponNumber(), false, true, Util.getLanguageId(lang));

        if(coupon != null){
            response.setData(coupon);
        }else{
            response.setError(language.getCouponNotFound(),LanguageManager.getMessageKey().getReject());
        }
        return super.createGson(response);
    }
}
