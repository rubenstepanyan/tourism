package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.GetAgencyCarsResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.CarService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class GetAgencyCars extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private CarService carService;

    @RequestMapping(value = "getAgencyCars",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getAgencyCars(@RequestHeader("language") String lang,
                                @RequestHeader("authorization")String authorization,
                                @RequestParam(value = "agencyId", required = false) Integer agencyId){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        if(agencyId !=null){
            Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);
            if(administrator != null){
                response.setData(new GetAgencyCarsResponse(carService.getCars(agencyId,Constants.AGENCY_TYPE)));
            }else {
                response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
            }
        }else{
            Agency agency = agencyService.getAgency(authorization);
            if(agency != null){
                response.setData(new GetAgencyCarsResponse(carService.getCars(agency.getId(),Constants.AGENCY_TYPE)));
            }else {
                response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
            }
        }


        return super.createGson(response);
    }

}
