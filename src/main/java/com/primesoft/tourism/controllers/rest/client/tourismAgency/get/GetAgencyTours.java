package com.primesoft.tourism.controllers.rest.client.tourismAgency.get;

import com.primesoft.tourism.beans.response.GetAgencyToursResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.TourService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class GetAgencyTours extends BaseController {

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private TourService tourService;


    @RequestMapping(value = "getAgencyTours", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getAgencyTours(@RequestHeader("language") String lang,
                                 @RequestHeader("authorization")String authorization,
                                 @RequestParam(value = "agencyId", required = false) Integer agencyId) {

        Response<GetAgencyToursResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);
        Agency agency = agencyService.getAgency(authorization);

        if(administrator != null){
            response.setData(new GetAgencyToursResponse(tourService.getAll(agencyId, Constants.AGENCY_TYPE, Util.getLanguageId(lang))));
        }else if(agency != null){
            response.setData(new GetAgencyToursResponse(tourService.getAll(agency.getId(), Constants.AGENCY_TYPE, Util.getLanguageId(lang))));
        }else{
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }



        return super.createGson(response);
    }
}
