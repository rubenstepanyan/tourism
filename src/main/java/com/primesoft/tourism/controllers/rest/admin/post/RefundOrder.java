package com.primesoft.tourism.controllers.rest.admin.post;

import com.primesoft.tourism.beans.request.RefundOrderRequest;
import com.primesoft.tourism.beans.response.ArcaRefundResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Arca;
import com.primesoft.tourism.beans.tableBeans.GuideOrder;
import com.primesoft.tourism.beans.tableBeans.TourOrder;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.ArcaManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.ArcaService;
import com.primesoft.tourism.service.interfaces.GuideOrderService;
import com.primesoft.tourism.service.interfaces.OrderService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class RefundOrder extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private GuideOrderService guideOrderService;

    @Autowired
    private ArcaService arcaService;


    @RequestMapping(value = Constants.ADMIN_PATH+"refundOrder",method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String refundOrder(@Valid @RequestBody RefundOrderRequest request,
                            @RequestHeader("authorization") String authorization,
                            @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator !=null) {
            refund(request);
            response.setMessage(language.getSuccessfullyRefunded(), LanguageManager.getMessageKey().getSuccess());

        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

    private void refund(RefundOrderRequest request){

        TourOrder tourOrder = null;
        GuideOrder guideOrder = null;
        String orderId = null;
        Integer amount = null;
        if(request.getType() == Constants.TOUR_ORDER_TYPE){
          tourOrder =  orderService.getTourOrder(request.getOrderId());
          orderId = tourOrder.getArcaOrderId();
          amount= tourOrder.getPrice();
        }else if(request.getType() == Constants.GUIDE_ORDER_TYPE){
           guideOrder = guideOrderService.getGuideOrder(request.getOrderId());
            orderId = guideOrder.getArcaOrderId();
            amount= guideOrder.getPrice();
        }
        if(orderId!=null) {
            ArcaRefundResponse arcaRefundResponse = new ArcaManager().refund(orderId, amount);

            if (arcaRefundResponse.getErrorCode().equals("0")) {
                Arca arca = arcaService.getArca(orderId);

                if (request.getType() == 0) {
                    tourOrder.setRefund(true);
                    orderService.save(tourOrder);

                } else if (request.getType() == 1) {
                    guideOrder.setRefund(true);
                    guideOrderService.save(guideOrder);

                }

                arca.setRefunded(true);
                arcaService.save(arca);
            }
        }
    }
}
