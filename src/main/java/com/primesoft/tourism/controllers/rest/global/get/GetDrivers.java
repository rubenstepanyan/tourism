package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.getAllDriverObjects.DriverObject;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetDrivers extends BaseController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private AgencyService agencyService;


    @RequestMapping(value = "getDrivers",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getDrivers(@RequestHeader("authorization") String authorization,
                             @RequestHeader("language") String lang,
                             @RequestParam(value = "type", required = false) Integer type,
                             @RequestParam(value = "key", required = false) String key){

        Response<List<DriverObject>> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);
        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        Agency agency = agencyService.getAgency(authorization);

        if(administrator != null){
            response.setData(driverService.getDrivers(type, key));
        }else if(agency != null){
            response.setData(driverService.getDrivers(type, key));
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
