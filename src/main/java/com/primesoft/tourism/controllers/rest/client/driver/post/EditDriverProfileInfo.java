package com.primesoft.tourism.controllers.rest.client.driver.post;

import com.primesoft.tourism.beans.request.EditDriverProfileInfoRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.service.interfaces.TourService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditDriverProfileInfo extends BaseController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private TourService tourService;


    @RequestMapping(value = Constants.DRIVER_PATH+"editProfileInfo", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String editDriverProfileInfo(@Valid @RequestBody EditDriverProfileInfoRequest request,
                                        @RequestHeader("language") String lang,
                                        @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Driver driver  = driverService.getDriver(authorization);

        if(driver != null){
            driver = request.updateDriver(driver);
            driverService.save(driver);

            if(!driver.getFirstName().equals(request.getFirstName()) || !driver.getLastName().equals(request.getLastName())){
                tourService.updateTours(Constants.DRIVER_TYPE, driver.getId(), request.getFirstName()+" "+request.getLastName());
            }


            if(!request.getPhotoUrl().startsWith("https://")){
                tourService.updateToursLogoUrl(Constants.DRIVER_TYPE, driver.getId(), driver.getPhotoUrl());
            }

            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());
        }else{

            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
