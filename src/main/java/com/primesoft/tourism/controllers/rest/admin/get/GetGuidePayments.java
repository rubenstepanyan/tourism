package com.primesoft.tourism.controllers.rest.admin.get;

import com.primesoft.tourism.beans.response.getGuidePaymentsResponse.GetGuidePaymentsResponse;
import com.primesoft.tourism.beans.response.getGuidePaymentsResponse.GuidePaymentContent;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.GuideOrderService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetGuidePayments extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private GuideOrderService guideOrderService;

    @RequestMapping(value = Constants.ADMIN_PATH+"getGuidePayments",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getGuidePayments(@RequestHeader("authorization") String authorization,
                                   @RequestHeader("language") String lang,
                                   @RequestParam(value = "agencyId") Integer agencyId,
                                   @RequestParam(value = "from") String from,
                                   @RequestParam(value = "to") String to,
                                   @RequestParam(value = "guideId") Integer guideId){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);
        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        Integer type;
        if(administrator != null){
            List<GuidePaymentContent> paymentsContents = null;
            if(agencyId != 0){
                type = 1;
                paymentsContents  = guideOrderService.getAllPayments(agencyId,type,from,to);
            }else if(guideId != 0){
                type = 0;
                paymentsContents = guideOrderService.getAllPayments(guideId,type,from,to);
            }else {
                paymentsContents = guideOrderService.getAllPayments(0,null,from,to);
            }

            response.setData(new GetGuidePaymentsResponse(paymentsContents));
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }
}
