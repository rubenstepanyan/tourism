package com.primesoft.tourism.controllers.rest.client.guide.get;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class GetGuideProfileInfo extends BaseController {

    @Autowired
    private GuideService guideService;


    @RequestMapping(value = Constants.GUIDE_PATH+"getGuideProfileInfo", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getGuideProfileInfo(@RequestHeader("language") String lang,
                                      @RequestHeader("authorization")String authorization) {

        Response<Guide> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Guide guide = guideService.getGuidByAuthorization(authorization);

        if(guide != null){
            guide.cleanNonShowFields();
            response.setData(guide);
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }
}
