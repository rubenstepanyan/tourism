package com.primesoft.tourism.controllers.rest.client.tourismAgency.put;

import com.primesoft.tourism.beans.request.AgencyRegistrationRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.AlertManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AgencyRegistration extends BaseController {

    @Autowired
    private AgencyService agencyService;



    @RequestMapping(value = Constants.AGENCY_PATH+"registration",method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String registration(@Valid @RequestBody AgencyRegistrationRequest request,
                                     @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency existingAgency= agencyService.getAgencyByPhone(request.getPhone());

        AlertManager smsManager = new AlertManager();
        Integer confirmationCode = Util.getRandomNumber();

        if(existingAgency == null) {

            Agency agency = request.getAgencyObject();
            agency.setConfirmationCode(confirmationCode.toString());
            agencyService.save(agency);

            smsManager.sendSms(agency.getPhone(), confirmationCode.toString());
            response.setMessage(language.getVerificationCodeSentToPhone(), LanguageManager.getMessageKey().getSuccess());

        }else if(!existingAgency.getConfirmed()){
            request.updateAgencyObject(existingAgency);
            existingAgency.setConfirmationCode(confirmationCode.toString());
            agencyService.save(existingAgency);
            smsManager.sendSms(existingAgency.getPhone(), confirmationCode.toString());
            response.setMessage(language.getVerificationCodeSentToPhone(), LanguageManager.getMessageKey().getSuccess());
        } else{
                response.setError(language.getAlreadyRegistered(),LanguageManager.getMessageKey().getReject());
            }

        return super.createGson(response);
    }
}
