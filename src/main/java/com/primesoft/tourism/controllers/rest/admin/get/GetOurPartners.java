package com.primesoft.tourism.controllers.rest.admin.get;

import com.primesoft.tourism.beans.response.getOurPartnersResponse.GetOurPartnersContent;
import com.primesoft.tourism.beans.response.getOurPartnersResponse.GetOurPartnersContentEntertainmentPlace;
import com.primesoft.tourism.beans.response.getOurPartnersResponse.GetOurPartnersResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.*;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class GetOurPartners extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private GuideService guideService;

    @Autowired
    private EntertainmentPlaceInfoService entertainmentPlaceInfoService;

    @RequestMapping(value = Constants.ADMIN_PATH+"getOurPartners",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getOurPartners(@RequestHeader("authorization") String authorization,
                                 @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);
        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){
            List<GetOurPartnersContent> agenciesList = agencyService.getAllAgencies();
            List<GetOurPartnersContent> driverList = driverService.getAllDrivers();
            List<GetOurPartnersContent> guideList = guideService.getAllGuides();
            List<GetOurPartnersContentEntertainmentPlace> entertainmentPlaceList = entertainmentPlaceInfoService.getAllEntertainmentPlace(Util.getLanguageId(lang));

            response.setData(new GetOurPartnersResponse(agenciesList,driverList,guideList,entertainmentPlaceList));
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
