package com.primesoft.tourism.controllers.rest.admin.get;

import com.primesoft.tourism.beans.response.GetVideoResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.service.interfaces.VideoContentService;
import com.primesoft.tourism.service.interfaces.VideoService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class GetVideo extends BaseController {

    @Autowired
    private VideoService videoService;

    @Autowired
    private VideoContentService videoContentService;


    @RequestMapping(value = Constants.ADMIN_PATH+"getVideo", method = RequestMethod.GET,produces = Constants.APPLICATION_JSON)
    public String getVideo(@RequestParam("videoId") Integer videoId){

        Response<Object> response = new Response<>();
        response.setData(new GetVideoResponse(videoService.getVideo(videoId), videoContentService.getVideoContent(videoId)));
        return super.createGson(response);
    }
}
