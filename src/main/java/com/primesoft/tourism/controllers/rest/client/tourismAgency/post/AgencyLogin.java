package com.primesoft.tourism.controllers.rest.client.tourismAgency.post;

import com.primesoft.tourism.beans.request.AgencyLoginRequest;
import com.primesoft.tourism.beans.response.AgencyLoginResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Generator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AgencyLogin extends BaseController {

    @Autowired
    private AgencyService agencyService;

    @RequestMapping(value = Constants.AGENCY_PATH+"login", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String login(@Valid @RequestBody AgencyLoginRequest request,
                        @RequestHeader("language") String lang) {

        Response<AgencyLoginResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        if(!request.getPhone().startsWith("+")){
            request.setPhone("+"+request.getPhone());
        }

        Agency agency = agencyService.getDriverByPhoneAndPassword(request.getPhone(), Generator.stringToSha256(request.getPassword()));


        if (agency != null){
            if(agency.getConfirmed() && !agency.getSuspended()) {
                if(agency.getTopicAndroidAndWeb() == null || agency.getTopicIos() == null){
                    agency.setTopicAndroidAndWeb(Constants.AGENCY_TYPE+"a"+agency.getId()+"a"+1);
                    agency.setTopicIos(Constants.AGENCY_TYPE+"a"+agency.getId()+"a"+2);
                }
                agencyService.save(agency);
                response.setData(new AgencyLoginResponse(agency.getAuthorization(),agency.getTopicAndroidAndWeb(),agency.getId()));
            }else if(agency.getSuspended()){
                response.setError(language.getYourAccountIsSuspended(),LanguageManager.getMessageKey().getReject());
            }else{
                response.setError(language.getYourAccountIsNotVerifiedPleaseConfirmIt(),LanguageManager.getMessageKey().getReject());
            }
        }else{
            response.setError(language.getWrongLoginOrPassword(), LanguageManager.getMessageKey().getReject());
        }

        return super.createGson(response);
    }
}
