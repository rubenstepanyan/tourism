package com.primesoft.tourism.controllers.rest.client.customer.put;

import com.google.gson.Gson;
import com.primesoft.tourism.beans.request.OrderRequest.OrderRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.*;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.NotificationManager;
import com.primesoft.tourism.service.interfaces.*;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.DayOfWeekCalc;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
public class AddOrder extends BaseController {

    @Autowired
    private TourService tourService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private TourReservationService tourReservationService;

    @Autowired
    private OfferedTourService offeredTourService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private AgencyService agencyService;


    @RequestMapping(value =Constants.CUSTOMER_PATH+"addOrder", method = RequestMethod.PUT, produces = Constants.APPLICATION_JSON)
    public String addOrder(@Valid @RequestBody OrderRequest request,
                           @RequestHeader("language") String lang,
                           @RequestHeader("authorization") String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        NotificationManager notificationManager = new NotificationManager();

        Tour tour;

        String today;
        String dayOfWeek;
        if(customer !=null){

            Tour trueDateTour = tourService.getTour(request.getId());
            if(trueDateTour.getStartDate() == null && request.getDate()==null && request.getDate().length()<=6){
                response.setError(language.getInThisDayTourIsNotPerformed(), LanguageManager.getMessageKey().getTourIsNotPerformed());
                return super.createGson(response);
            }

            DayOfWeekCalc calc = new DayOfWeekCalc();
            HashMap<String, String> map = calc.getDayOfWeekSearch(request.getDate(), request.getDate());
            dayOfWeek = "%" + map.get("dayOfWeeks") + "%";
            today = "%" + map.get("today") + "%";
            System.out.println("dayOfWeek: " + dayOfWeek);
            System.out.println("today: " + today);


            if (map.get("dayOfWeeks") != null) {
                tour = tourService.getTour(request.getId(), request.getDate(), dayOfWeek);
            } else {
                tour = tourService.getTour(request.getId(), request.getDate(), today);
            }


            if (tour != null) {

                if(tour.getPickMeUp() && request.getLat()==null){
                    response.setError(language.getPleaseAddAddress(), LanguageManager.getMessageKey().getReject());
                }

                String tourDate = request.getDate()+" "+tour.getStartTime().split(" ")[1];

                if(Util.getDateDiffInMillis(tourDate, Util.getCurrentDateAndTimeSeconds()) > Constants.ONE_HOUR_IN_MILLIS){
                    response.setError(language.getThisTourNotAvailable(),LanguageManager.getMessageKey().getReject());
                    return super.createGson(response);
                }

                TourReservation tourReservation = tourReservationService.getTourReservation(tour.getId(), request.getDate());

                System.out.println(""+request.getDate());
                System.out.println("tourReservation: "+new Gson().toJson(tourReservation));
                if((tourReservation == null && tour.getFreeSeats()>=request.getQuantity())  || (tourReservation != null && tourReservation.getFreeSeats()>=request.getQuantity())) {

                    OfferedTour offeredTour = offeredTourService.getOfferedTour(tour.getOfferedTourId());
                    Driver driver = null;
                    Agency agency = null;

                    if(tour.getType().equals(Constants.DRIVER_TYPE)){
                        driver = driverService.getDriver(tour.getReference());
                    }else if(tour.getType().equals(Constants.AGENCY_TYPE)){
                        agency = agencyService.getAgency(tour.getReference());
                    }

                    TourOrder tourOrder = new TourOrder();

                    if(tour.getTourType().equals(Constants.JOIN_TO_THE_TOUR)){
                        tourOrder.setPrice(tour.getPrice() * request.getQuantity());
                    }else{
                        tourOrder.setPrice(tour.getPrice());
                    }


                    tourOrder.setQuantity(request.getQuantity());
                    tourOrder.setStatus(Constants.PENDING);
                    tourOrder.setCustomerId(customer.getId());
                    tourOrder.setTourId(request.getId());
                    tourOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());
                    tourOrder.setReference(tour.getReference());
                    tourOrder.setReferenceType(tour.getType());
                    tourOrder.setTourDate(tourDate);
                    tourOrder.setOfferedTourId(offeredTour.getId());
                    tourOrder.setTransfer(offeredTour.getTransfer());
                    tourOrder.setQr("VP-" + Util.getRandomNumber().toString());
                    tourOrder.setPayed(false);
                    tourOrder.setMyTripsPaid(false);
                    tourOrder.setTouristAddress(request.getTouristAddress());
                    tourOrder.setLat(request.getLat());
                    tourOrder.setLng(request.getLng());
                    tourOrder.setRefund(false);

                    if(tour.getType().equals(Constants.DRIVER_TYPE)){
                        tourOrder.setPhoneNumber(driver.getPhone());
                    }else if(tour.getType().equals(Constants.AGENCY_TYPE)){
                        tourOrder.setPhoneNumber(agency.getPhone());
                    }

                    orderService.save(tourOrder);

                    Boolean isTransfer;

                    if(tourOrder.getTransfer()){
                        isTransfer=true;
                    }else{
                        isTransfer=false;
                    }
                    if(tour.getType().equals(Constants.DRIVER_TYPE)){
                        notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED, LanguageManager.getLanguageInstance(driver.getDashboardLanguage()).getYouHaveNewOrder(), driver.getTopicAndroidAndWeb(), driver.getTopicIos(), tourOrder.getStatus(),isTransfer,tourOrder.getId());
                    }else if(tour.getType().equals(Constants.AGENCY_TYPE)){
                        notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED, LanguageManager.getLanguageInstance(agency.getDashboardLanguage()).getYouHaveNewOrder(), agency.getTopicAndroidAndWeb(), agency.getTopicIos(), tourOrder.getStatus(),isTransfer,tourOrder.getId());
                    }

                    response.setDataAndMessage(tourOrder.getId(),language.getPleaseWaitForTheOrganizerToAcceptYourOrder(),LanguageManager.getMessageKey().getOrderSuccessfullyAdded());

                }else{
                    response.setError(language.getThisTourDoesNotHaveSoManyFreeSeats(), LanguageManager.getMessageKey().getReject());
                    return super.createGson(response);
                }

            } else {

                tour = tourService.getTour(request.getId());

                if(tour!=null){
                    response.setError(language.getInThisDayTourIsNotPerformed(), LanguageManager.getMessageKey().getTourIsNotPerformed());
                    return super.createGson(response);
                }else {
                    response.setError(language.getTourNotFound(), LanguageManager.getMessageKey().getTourNotFound());
                    return super.createGson(response);
                }
            }

        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

}
