package com.primesoft.tourism.controllers.rest.admin.get;

import com.primesoft.tourism.beans.response.getCouponPeynetsResponse.GetCouponPaymentContent;
import com.primesoft.tourism.beans.response.getCouponPeynetsResponse.GetCouponPaymentsResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.CouponService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetCouponPayments extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private CouponService couponService;

    @RequestMapping(value = Constants.ADMIN_PATH+"getCouponPayments",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getCouponPayments(@RequestHeader("authorization") String authorization,
                                    @RequestHeader("language") String lang,
                                    @RequestParam(value = "entertainmentPlaceId") Integer entertainmentPlaceId){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);
        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);


        if(administrator != null){
            List<GetCouponPaymentContent> paymentsContents = null;
            if(entertainmentPlaceId != 0){
                paymentsContents  = couponService.getAllPayments(entertainmentPlaceId, Util.getLanguageId(lang));
            }else {
                paymentsContents = couponService.getAllPayments(0, Util.getLanguageId(lang));
            }

            response.setData(new GetCouponPaymentsResponse(paymentsContents));
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
