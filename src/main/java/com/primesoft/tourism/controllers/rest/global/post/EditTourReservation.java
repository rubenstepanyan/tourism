package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.EditTourReservationRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.beans.tableBeans.TourReservation;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.service.interfaces.TourReservationService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditTourReservation extends BaseController {


    @Autowired
    private TourReservationService tourReservationService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private AgencyService agencyService;


    @RequestMapping(value = "editTourReservation",method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String editTourReservation(@Valid @RequestBody EditTourReservationRequest request,
                                      @RequestHeader("authorization") String authorization,
                                      @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency agency = agencyService.getAgency(authorization);
        Driver driver = driverService.getDriver(authorization);

        if(agency != null){
            TourReservation tourReservation = tourReservationService.getTourReservation(request.getTourId());
            tourReservation.setFreeSeats(request.getFreeSeats());
            tourReservationService.save(tourReservation);
            response.setMessage(language.getSuccessfullyEdited(),LanguageManager.getMessageKey().getSuccess());
        }else if(driver != null){
            TourReservation tourReservation = tourReservationService.getTourReservation(request.getTourId());
            tourReservation.setFreeSeats(request.getFreeSeats());
            tourReservationService.save(tourReservation);
            response.setMessage(language.getSuccessfullyEdited(),LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

}
