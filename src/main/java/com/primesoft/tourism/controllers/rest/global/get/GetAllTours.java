package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.getAllTouesResponse.GetAllTourContentResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.TourService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetAllTours extends BaseController {


    @Autowired
    private TourService tourService;


    @RequestMapping(value = "getAllTours", method = RequestMethod.GET,produces = Constants.APPLICATION_JSON)
    public String getAllTours(@RequestHeader("language") String lang,
                              @RequestParam("id")Integer id,
                              @RequestParam("type")Integer type){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        List<GetAllTourContentResponse> data = null;


        if(id.equals(0) && type.equals(0)){
            data = tourService.getAll(Util.getLanguageId(lang));
        }else if(Constants.DRIVER_TYPE.equals(type)){
            if(id.equals(0)){
                data = tourService.getAll(Constants.DRIVER_TYPE,Util.getLanguageId(lang));
            }else {
                data = tourService.getAll(id, Constants.DRIVER_TYPE,Util.getLanguageId(lang));
            }
        }else if(Constants.AGENCY_TYPE.equals(type)){
            if(id.equals(0)){
                data = tourService.getAll(Constants.AGENCY_TYPE,Util.getLanguageId(lang));
            }else {
                data = tourService.getAll(id, Constants.AGENCY_TYPE,Util.getLanguageId(lang));
            }
        }

        response.setData(data);

        return super.createGson(response);
    }

}
