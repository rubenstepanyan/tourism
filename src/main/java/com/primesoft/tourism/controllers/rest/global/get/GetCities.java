package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.CityService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class GetCities extends BaseController {


    @Autowired
    private CityService cityService;

    @Autowired
    private AdministratorService administratorService;

    @RequestMapping(value = "getCities",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getCities(@RequestHeader("language") String lang,
                            @RequestHeader(value = "authorization", required = false) String authorization,
                            @RequestParam(value = "notShowInTours", required = false, defaultValue = "false") Boolean notShowInTours ){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);
        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){
            response.setData(cityService.getAllByLanguage(Util.getLanguageId(lang)));
        }else if(notShowInTours){
            response.setData(cityService.getAllByLanguage(Util.getLanguageId(lang)));
        }else {
            response.setData(cityService.getAll(Util.getLanguageId(lang), notShowInTours));
        }



        return super.createGson(response);
    }

}
