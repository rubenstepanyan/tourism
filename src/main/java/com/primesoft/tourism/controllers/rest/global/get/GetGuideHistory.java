package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.getHistoryResponse.GetGuideOrderHistoryContentResponse;
import com.primesoft.tourism.beans.response.getHistoryResponse.GetGuideOrderHistoryResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.GuideOrderService;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetGuideHistory extends BaseController {

    @Autowired
    private GuideService guideService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private GuideOrderService guideOrderService;


    @RequestMapping(value = "getGuideHistory", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getGuideHistory(@RequestHeader("language") String lang,
                                  @RequestHeader("authorization")String authorization,
                                  @RequestParam(value = "page", required = false) Integer page) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency agency = agencyService.getAgency(authorization);
        Guide guide = guideService.getGuidByAuthorization(authorization);

        if(agency != null){

            List<GetGuideOrderHistoryContentResponse> guideOrderContents = guideOrderService.getGuideOrderHistory(agency.getId(),null);

            response.setData(new GetGuideOrderHistoryResponse(guideOrderContents,page));

        }else if(guide != null){

            List<GetGuideOrderHistoryContentResponse> guideOrderContents =guideOrderService.getGuideOrderHistory(null,guide.getId());

            response.setData(new GetGuideOrderHistoryResponse(guideOrderContents,page));

        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }



        return super.createGson(response);
    }
}
