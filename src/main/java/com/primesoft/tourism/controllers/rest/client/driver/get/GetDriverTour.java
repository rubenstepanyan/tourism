package com.primesoft.tourism.controllers.rest.client.driver.get;

import com.primesoft.tourism.beans.response.GetAgencyToursResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.service.interfaces.TourService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class GetDriverTour extends BaseController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private TourService tourService;


    @RequestMapping(value = "getDriverTour", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getDriverTour(@RequestHeader("language") String lang,
                                @RequestHeader("authorization")String authorization,
                                @RequestParam(value = "driverId", required = false) Integer driverId) {

        Response<GetAgencyToursResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);
        Driver driver = driverService.getDriver(authorization);

        if(administrator != null){
            response.setData(new GetAgencyToursResponse(tourService.getAll(driverId, Constants.DRIVER_TYPE, Util.getLanguageId(lang))));
        }else if(driver != null){
            response.setData(new GetAgencyToursResponse(tourService.getAll(driverId, Constants.DRIVER_TYPE, Util.getLanguageId(lang))));
        }else{
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }



        return super.createGson(response);
    }
}
