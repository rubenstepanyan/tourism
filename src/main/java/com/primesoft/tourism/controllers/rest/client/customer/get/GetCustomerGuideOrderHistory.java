package com.primesoft.tourism.controllers.rest.client.customer.get;

import com.primesoft.tourism.beans.response.getCustomerHistory.GetCustomerGuideOrderHistoryContent;
import com.primesoft.tourism.beans.response.getCustomerHistory.GetCustomerGuideOrderHistoryResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.service.interfaces.GuideOrderService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetCustomerGuideOrderHistory extends BaseController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private GuideOrderService guideOrderService;



    @RequestMapping(value = Constants.CUSTOMER_PATH+"getGuideOrderHistory", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getCustomerGuideOrderHistory(@RequestHeader("language") String lang,
                                               @RequestHeader("authorization")String authorization,
                                               @RequestParam(value = "page", required = false) Integer page) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        if(customer != null){
            List<GetCustomerGuideOrderHistoryContent> guideOrderContents = guideOrderService.getCustomerGuideOrderHistory(customer.getId());

            response.setData(new GetCustomerGuideOrderHistoryResponse(guideOrderContents,page));

        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

}
