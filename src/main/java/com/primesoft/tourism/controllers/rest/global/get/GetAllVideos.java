package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.getAllVideoResponse.GetAllVideosResponse;
import com.primesoft.tourism.beans.response.getAllVideoResponse.VideoContentResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.service.interfaces.VideoService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetAllVideos extends BaseController {

    @Autowired
    private VideoService videoService;


    @RequestMapping(value = "getAllVideos", method = RequestMethod.GET,produces = Constants.APPLICATION_JSON)
    public String getAllVideos(@RequestHeader("language") String lang,
                               @RequestParam(value = "page", required = false, defaultValue = "1") Integer page){

        Response<Object> response = new Response<>();

        System.out.println("video");
        Long pageCount = Util.calculatePageCount(videoService.getCount(), Constants.VIDEO_ITEMS_COUNT);
        Integer firstNewsId = ((page - 1) * Constants.VIDEO_ITEMS_COUNT);
        List<VideoContentResponse> videos = videoService.getAllVideos(Util.getLanguageId(lang), firstNewsId, Constants.VIDEO_ITEMS_COUNT);


        response.setData(new GetAllVideosResponse(videos,pageCount));

        return super.createGson(response);
    }
}
