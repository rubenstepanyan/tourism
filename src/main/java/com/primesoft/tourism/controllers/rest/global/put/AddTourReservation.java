package com.primesoft.tourism.controllers.rest.global.put;

import com.primesoft.tourism.beans.request.AddTourReservationRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.beans.tableBeans.Tour;
import com.primesoft.tourism.beans.tableBeans.TourReservation;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.service.interfaces.TourReservationService;
import com.primesoft.tourism.service.interfaces.TourService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
@RestController
public class AddTourReservation extends BaseController {

    @Autowired
    private TourReservationService tourReservationService;

    @Autowired
    private TourService tourService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private  AgencyService agencyService;


    @RequestMapping(value ="addTourReservation",method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String addTourReservation(@Valid @RequestBody AddTourReservationRequest request,
                                     @RequestHeader("language") String lang,
                                     @RequestHeader("authorization") String authorization){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Driver driver = driverService.getDriver(authorization);
        Agency agency = agencyService.getAgency(authorization);

        Tour tour = tourService.getTour(request.getTourId());

        TourReservation reservation = tourReservationService.getTourReservation(request.getTourId(),request.getReservationDate());

        if(agency != null){
            addReservation(reservation,request,response,language,null,agency,tour.getFreeSeats());
        }else if(driver != null){
            addReservation(reservation,request,response,language,driver,null,tour.getFreeSeats());
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

    private void addReservation(TourReservation reservation,AddTourReservationRequest request,Response<Object> response,Language language,Driver driver, Agency agency,Integer seats){
        if(reservation != null){
            if(request.getFreeSeats() <= seats){
                reservation.setFreeSeats(request.getFreeSeats());
                tourReservationService.save(reservation);
                response.setMessage(language.getSuccessfullyAdded(), LanguageManager.getMessageKey().getSuccess());
            }else{
                response.setError(language.getThisTourDoesNotHaveSoManyFreeSeats(),LanguageManager.getMessageKey().getReject());
            }
        }else{
            if(request.getFreeSeats() <= seats) {
                TourReservation tourReservation = request.getTourReservation(driver, agency);
                tourReservationService.save(tourReservation);
                response.setMessage(language.getSuccessfullyAdded(), LanguageManager.getMessageKey().getSuccess());
            }else{
                response.setError(language.getThisTourDoesNotHaveSoManyFreeSeats(),LanguageManager.getMessageKey().getReject());
            }
        }
    }
}
