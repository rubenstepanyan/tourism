package com.primesoft.tourism.controllers.rest.client.driver.post;

import com.primesoft.tourism.beans.request.EditAgencyPasswordRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Generator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditDriverPassword extends BaseController {

    @Autowired
    private DriverService driverService;


    @RequestMapping(value = Constants.DRIVER_PATH+"editPassword", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String editDriverPassword(@Valid @RequestBody EditAgencyPasswordRequest request,
                                     @RequestHeader("language") String lang,
                                     @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

       Driver driver = driverService.getDriver(authorization);

        if(driver != null){
            if(Generator.stringToSha256(request.getOldPassword()).equals(driver.getPassword())){
                driver.setPassword(Generator.stringToSha256(request.getNewPassword()));
                response.setMessage(language.getSuccessfullyEdited(),LanguageManager.getMessageKey().getSuccess());
                driverService.save(driver);

            }else{
                response.setError(language.getIncorrectPassword(),LanguageManager.getMessageKey().getReject());
            }
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
