package com.primesoft.tourism.controllers.rest.admin.post;

import com.primesoft.tourism.beans.request.UpdatePhotoRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlacePhoto;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.EntertainmentPlacePhotoService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
public class UpdatePhotoEP extends BaseController {


    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private EntertainmentPlacePhotoService entertainmentPlacePhotoService;

    @RequestMapping(value = Constants.ADMIN_PATH+"updatePhotoEP", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String updatePhotoEP(@ModelAttribute("uploadFile") UpdatePhotoRequest request,
                                @RequestHeader("authorization") String authorization,
                                @RequestHeader("language") String lang) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if (administrator != null){

            List<MultipartFile> images = request.getImages();

            if(images != null && images.size()>0) {

                ImageManager imageManager = new ImageManager();

                for (int i = 0; i < images.size(); i++) {

                    EntertainmentPlacePhoto photoUrl = new EntertainmentPlacePhoto();
                    photoUrl.setEntertainmentPlaceId(request.getId());
                    String[] splittedName = images.get(i).getOriginalFilename().split("\\.");
                    String imageFormat = imageManager.getImageFormatType(splittedName[splittedName.length - 1]);
                    String imageName = Util.getRandomString((short) 15) + imageFormat;

                    try {
                        imageManager.saveImage(images.get(i), Constants.ENTERTAINMENT_PLACE_IMAGE_PATH + imageName);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    photoUrl.setUrl(Constants.SERVER_NAME + Constants.ENTERTAINMENT_PLACE_IMAGE_PREFIX + imageName);
                    entertainmentPlacePhotoService.save(photoUrl);
                }

            }

            if(request.getMode().equalsIgnoreCase("add")) {
                response.setMessage(language.getSuccessfullyAdded(), LanguageManager.getMessageKey().getSuccess());
            }else {
                response.setMessage(language.getSuccessfullyChanged(), LanguageManager.getMessageKey().getSuccess());
            }
        }else {
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }
        return super.createGson(response);
    }

}
