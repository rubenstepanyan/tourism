package com.primesoft.tourism.controllers.rest.admin.get;

import com.primesoft.tourism.beans.response.getBlogResponse.GetBlogResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Blog;
import com.primesoft.tourism.beans.tableBeans.BlogContent;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.BlogContentService;
import com.primesoft.tourism.service.interfaces.BlogService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetBlog extends BaseController {

    @Autowired
    private BlogService blogService;

    @Autowired
    private BlogContentService blogContentService;

    @Autowired
    private AdministratorService administratorService;

    @RequestMapping(value = Constants.ADMIN_PATH+"getBlog",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getBlog(@RequestHeader("authorization") String authorization,
                          @RequestHeader("language") String lang,
                          @RequestParam("blogId") Integer blogId){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);
        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){
            Blog blog = blogService.getBlog(blogId);
            List<BlogContent> blogContents = blogContentService.getBlogContent(blogId);
            response.setData(new GetBlogResponse(blog,blogContents));
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
