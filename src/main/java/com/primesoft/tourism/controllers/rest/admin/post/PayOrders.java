package com.primesoft.tourism.controllers.rest.admin.post;

import com.primesoft.tourism.beans.request.PayOrdersRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.GuideOrderService;
import com.primesoft.tourism.service.interfaces.OrderService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class PayOrders extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private GuideOrderService  guideOrderService;


    @RequestMapping(value = Constants.ADMIN_PATH+"payOrders",method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String payOrders(@Valid @RequestBody PayOrdersRequest request,
                             @RequestHeader("authorization") String authorization,
                             @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator !=null) {

            if(request.getType() == Constants.TOUR_ORDER_TYPE){
                orderService.updateTourOrders(request.getIds());
            }else if(request.getType() == Constants.GUIDE_ORDER_TYPE){
                guideOrderService.updateGuideOrders(request.getIds());
            }
            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());

        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
