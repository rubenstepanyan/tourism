package com.primesoft.tourism.controllers.rest.client.tourismAgency.get;

import com.primesoft.tourism.beans.response.GetAgencyOptionsResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.TourOrder;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.OrderService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GetAgencyOptions extends BaseController {

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private OrderService orderService;


    @RequestMapping(value = Constants.AGENCY_PATH+"getOptions", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String GetCustomerOptions(@RequestHeader("language") String lang,
                                     @RequestHeader("authorization")String authorization) {

        Response<GetAgencyOptionsResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency agency = agencyService.getAgency(authorization);

        if(agency != null){
            GetAgencyOptionsResponse getAgencyOptionsResponse = new GetAgencyOptionsResponse();
            List<TourOrder> tourOrder = orderService.getTourOrderByDriverIdAndStatus(agency.getId(),Constants.AGENCY_TYPE,Constants.PENDING);

            if(tourOrder != null && tourOrder.size()>0){
                getAgencyOptionsResponse.setNew(true);
            }else{
                getAgencyOptionsResponse.setNew(false);
            }
            getAgencyOptionsResponse.setTopicAndroid(agency.getTopicAndroidAndWeb());
            getAgencyOptionsResponse.setTopicIos(agency.getTopicIos());
            getAgencyOptionsResponse.setTutorialUrl(getTutorialUrl(lang));
            response.setData(getAgencyOptionsResponse);
        }else{
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

    private String getTutorialUrl(String lang){

        String tutorialUrl;
        if(lang.toUpperCase().equals("HY")){
            tutorialUrl = "https://www.youtube.com/watch?v=NEqHPxnxrFc&list=RDMMNEqHPxnxrFc&start_radio=1";
        }else if(lang.toUpperCase().equals("RU")){
            tutorialUrl = "https://www.youtube.com/watch?v=vtdwnDjN9GY&list=RDMMNEqHPxnxrFc&index=4";
        }else{
            tutorialUrl = "https://www.youtube.com/watch?v=9dcVOmEQzKA&list=RDGMEMHDXYb1_DDSgDsobPsOFxpAVM9dcVOmEQzKA&index=1";
        }

        return tutorialUrl;

    }
}
