package com.primesoft.tourism.controllers.rest.client.tourismAgency.post;

import com.primesoft.tourism.beans.request.EditAgencyProfileInfoRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.TourService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditAgencyProfileInfo extends BaseController {

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private TourService tourService;

    @RequestMapping(value = Constants.AGENCY_PATH+"editProfileInfo", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String editAgencyProfileInfo(@Valid @RequestBody EditAgencyProfileInfoRequest request,
                                          @RequestHeader("language") String lang,
                                          @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency agency = agencyService.getAgency(authorization);

        if(agency != null){

            if(!agency.getName().equals(request.getName())){
                tourService.updateTours(Constants.AGENCY_TYPE, agency.getId(), request.getName());
            }

            agency = request.updateAgency(agency);

            if(!request.getLogoUrl().startsWith("https://")){
                tourService.updateToursLogoUrl(Constants.AGENCY_TYPE, agency.getId(), agency.getLogoUrl());
            }
            agencyService.save(agency);
            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());
        }else{

            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
