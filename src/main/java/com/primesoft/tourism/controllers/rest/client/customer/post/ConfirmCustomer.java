package com.primesoft.tourism.controllers.rest.client.customer.post;

import com.primesoft.tourism.beans.request.ConfirmCustomerRequest;
import com.primesoft.tourism.beans.response.ConfirmCustomerResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ConfirmCustomer extends BaseController {


    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = Constants.CUSTOMER_PATH+"confirm", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String confirm(@Valid @RequestBody ConfirmCustomerRequest request,
                          @RequestHeader("language") String lang) {

        Response<ConfirmCustomerResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer customer = customerService.getCustomerByMailAndConfirmationCode(request.getMail(),request.getConfirmationCode());

        if(customer != null){
            customer.setConfirmed(true);
            customerService.save(customer);
            response.setMessage(language.getSuccessfullyConfirmed(), LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getWrongConfirmationCode(), LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }
}
