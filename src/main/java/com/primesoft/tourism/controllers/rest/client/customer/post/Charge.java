package com.primesoft.tourism.controllers.rest.client.customer.post;

import com.primesoft.tourism.beans.request.ChargeRequest;
import com.primesoft.tourism.beans.response.ArcaResponse;
import com.primesoft.tourism.beans.response.ChargeResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.*;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.ArcaManager;
import com.primesoft.tourism.service.interfaces.*;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class Charge extends BaseController {



    @Autowired
    private CustomerService customerService;

    @Autowired
    private EntertainmentPlaceService entertainmentPlaceService;

    @Autowired
    private EntertainmentPlaceInfoService entertainmentPlaceInfoService;

    @Autowired
    private ArcaService arcaService;

    @Autowired
    private GuideOrderService guideOrderService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private CouponService couponService;

    @RequestMapping(value = Constants.CUSTOMER_PATH+"charge", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String charge(@Valid @RequestBody ChargeRequest request,
                         @RequestHeader("language") String lang,
                         @RequestHeader("authorization")String authorization) {

        Response<Object> resp = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);
        EntertainmentPlace entertainmentPlace = null;

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        if(customer != null){
            Long orderNumber = System.currentTimeMillis();
            Integer price = 0;
            Integer reference = 0;
            String description = "";
            TourOrder order = orderService.getTourOrder(request.getId());

            if(request.getType().equals(Constants.BUY_COUPON)) {
                entertainmentPlace = entertainmentPlaceService.getById(request.getId());
                EntertainmentPlaceInfo info = entertainmentPlaceInfoService.getInfo(entertainmentPlace.getId(), Constants.EN_LANGUAGE);
                price = entertainmentPlace.getPrice().intValue();
                description = info.getName() + " " + entertainmentPlace.getDiscount() + " Discount";
            }else if(request.getType().equals(Constants.BUY_GUIDE)){
                GuideOrder guideOrder = guideOrderService.getGuideOrder(request.getId());
                price = guideOrder.getPrice();
                reference = guideOrder.getId();
                System.out.println("guideOrder.getId(): "+guideOrder.getId());
                System.out.println("reference: "+reference);
                description = guideOrder.getFromDate()+" - "+guideOrder.getToDate()+" "+" Guide";
            }else if(request.getType().equals(Constants.BUY_TOUR)){
                price = order.getPrice();
                reference = order.getId();

                if(order.getTransfer()) {
                    description = order.getTourDate() + " - Transfer";
                    request.setType(Constants.BUY_TRANSFER);
                }else {
                    description = order.getTourDate() + " - Tour";
                }
            }else if(request.getType().equals(Constants.BUY_TRANSFER)){
                price = order.getPrice();
                reference = order.getId();
                description = order.getTourDate() + " - Transfer";
            }


            //Arca arca = arcaService.getArca(customer.getId(), request.getType(), reference, false);

            //if(arca==null) {

            ArcaResponse arcaResponse = new ArcaManager().charge(lang, request.getPageView(), description, price, orderNumber, request.getUrl());

            if (arcaResponse.getErrorCode().equals("0")) {

                if(request.getType().equals(Constants.BUY_COUPON)) {
                    Coupon coupon = addCoupon(entertainmentPlace, orderNumber, customer.getId(), arcaResponse.getOrderId());
                    reference = coupon.getId();
                }
                Arca arca = new Arca();
                arca.setUrl(arcaResponse.getFormUrl());
                arca.setCustomerId(customer.getId());
                arca.setType(request.getType());
                System.out.println("reference: "+reference);
                arca.setReference(reference);
                arca.setOrderId(arcaResponse.getOrderId());
                arca.setOrderNumber(orderNumber);
                arca.setCharged(false);
                arca.setChargeDate(Util.getCurrentDateAndTimeSeconds());
                arca.setRefunded(false);
                arcaService.save(arca);

                resp.setData(new ChargeResponse(arca.getUrl(), arcaResponse.getOrderId()));
            } else {
                resp.setError(arcaResponse.getErrorMessage(), LanguageManager.getMessageKey().getReject());
            }

            //}else {
            //    resp.setData(new ChargeResponse(arca.getUrl()));
            //}
        }

        return super.createGson(resp);
    }



    private Coupon addCoupon(EntertainmentPlace entertainmentPlace, Long orderNumber, Integer customerId, String orderId){

        Coupon existCoupon = couponService.getCoupon(entertainmentPlace.getId(),customerId,false);

        if(existCoupon!=null) {
            return existCoupon;
        }else {
            Coupon coupon = new Coupon();
            coupon.setEntertainmentPlaceId(entertainmentPlace.getId());
            coupon.setPrice(entertainmentPlace.getPrice());
            coupon.setDiscount(entertainmentPlace.getDiscount());
            coupon.setNumber("CP" + ((int) (Math.random() * 89999999) + 10000000));
            coupon.setActivated(false);
            coupon.setCustomerId(customerId);
            coupon.setOrderId(orderId);
            coupon.setOrderNumber(orderNumber);
            coupon.setPayed(false);
            couponService.save(coupon);

            return coupon;
        }
    }


}
