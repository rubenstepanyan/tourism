package com.primesoft.tourism.controllers.rest.client.tourismAgency.post;

import com.primesoft.tourism.beans.request.EditAgencyPhoneRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.OrderService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditAgencyPhone extends BaseController {


    @Autowired
    private AgencyService agencyService;

    @Autowired
    private OrderService orderService;


    @RequestMapping(value = Constants.AGENCY_PATH+"editPhone", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String editAgencyPhone(@Valid @RequestBody EditAgencyPhoneRequest request,
                                    @RequestHeader("language") String lang,
                                    @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);


        if(!request.getPhone().startsWith("+")){
            request.setPhone("+"+request.getPhone());
        }

        Agency agency = agencyService.getAgency(authorization);

        if(agency != null){
            if(request.getConfirmationCode().equals(agency.getConfirmationCode())){
                response.setMessage(language.getSuccessfullyEdited(),LanguageManager.getMessageKey().getSuccess());
                agency.setPhone(request.getPhone());
                agencyService.save(agency);

                orderService.updateTourOrders(agency.getId(),agency.getPhone(),Constants.AGENCY_TYPE);
            }else{
                response.setError(language.getWrongConfirmationCode(),LanguageManager.getMessageKey().getReject());
            }
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
