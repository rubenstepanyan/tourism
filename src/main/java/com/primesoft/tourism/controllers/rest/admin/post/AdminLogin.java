package com.primesoft.tourism.controllers.rest.admin.post;

import com.primesoft.tourism.beans.request.AdministratorLoginRequest;
import com.primesoft.tourism.beans.response.AdministratorLoginResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Generator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
@RestController
public class AdminLogin extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @RequestMapping(value = Constants.ADMIN_PATH+"login", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String administratorLoginController(@Valid @RequestBody AdministratorLoginRequest request,
                                               @RequestHeader("language") String lang) {

        Response<AdministratorLoginResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByUserNameAndPassword(request.getUserName(), Generator.stringToSha256(request.getPassword()));

        if (administrator != null){
            response.setData(new AdministratorLoginResponse(administrator.getAuthorization()));
        }else{
            response.setError(language.getWrongLoginOrPassword(), LanguageManager.getMessageKey().getReject());
        }
        return super.createGson(response);
    }

}
