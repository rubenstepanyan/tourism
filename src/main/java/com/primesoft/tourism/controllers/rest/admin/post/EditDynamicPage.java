package com.primesoft.tourism.controllers.rest.admin.post;

import com.primesoft.tourism.beans.request.editDynamicPage.EditDynamicPageRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.DynamicPage;
import com.primesoft.tourism.beans.tableBeans.DynamicPageContent;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.DynamicPageContentService;
import com.primesoft.tourism.service.interfaces.DynamicPagePhotoService;
import com.primesoft.tourism.service.interfaces.DynamicPageService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class EditDynamicPage extends BaseController {


    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private DynamicPageService dynamicPageService;

    @Autowired
    private DynamicPageContentService dynamicPageContentService;

    @Autowired
    private DynamicPagePhotoService dynamicPagePhotoService;


    @RequestMapping(value = Constants.ADMIN_PATH+"editDynamicPage",method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String editDynamicPage(@Valid @RequestBody EditDynamicPageRequest request,
                                         @RequestHeader("language")String lang,
                                         @RequestHeader("authorization")String authorization){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){

            DynamicPage dynamicPage= dynamicPageService.getDynamicPage(request.getId());
            request.updatePhoto(dynamicPage);
            dynamicPageService.save(dynamicPage);

            List<DynamicPageContent> dynamicPageContents = dynamicPageContentService.getDynamicPageContents(dynamicPage.getId());
            request.updateDynamicPageContents(dynamicPageContents);
            dynamicPageContentService.save(dynamicPageContents);



            if(request.getDeletedPhotos()!=null && request.getDeletedPhotos().size()>0) {
                dynamicPagePhotoService.deleteById(request.getDeletedPhotos());
            }

            response.setData(dynamicPage);

        }else {
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
