package com.primesoft.tourism.controllers.rest.client.tourismAgency.post;

import com.primesoft.tourism.beans.request.EditAgencyPasswordRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Generator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditAgencyPassword extends BaseController {

    @Autowired
    private AgencyService agencyService;


    @RequestMapping(value = Constants.AGENCY_PATH+"editPassword", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String editAgencyPassword(@Valid @RequestBody EditAgencyPasswordRequest request,
                                       @RequestHeader("language") String lang,
                                       @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency agency = agencyService.getAgency(authorization);

        if(agency != null){
            if(Generator.stringToSha256(request.getOldPassword()).equals(agency.getPassword())){
                response.setMessage(language.getSuccessfullyEdited(),LanguageManager.getMessageKey().getSuccess());
                agency.setPassword(Generator.stringToSha256(request.getNewPassword()));
                agencyService.save(agency);
            }else{
                response.setError(language.getIncorrectPassword(),LanguageManager.getMessageKey().getReject());
            }
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
