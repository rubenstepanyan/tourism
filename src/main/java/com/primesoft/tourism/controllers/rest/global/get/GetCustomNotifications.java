package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.GetCustomNotificationResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.CustomNotification;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.CustomNotificationService;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class GetCustomNotifications extends BaseController {


    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomNotificationService customNotificationService;



    @RequestMapping(value = "getCustomNotifications", method = RequestMethod.GET,produces = Constants.APPLICATION_JSON)
    public String getCustomNotifications(@RequestHeader("language") String lang,
                                         @RequestHeader("authorization") String authorization){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        if(customer != null){
            List<CustomNotification> contents = customNotificationService.getAll(customer.getId());

            if(contents.size() >= 1){
                if(contents.size() == 1){
                    GetCustomNotificationResponse resp;
                    System.out.println("lang.toUpperCase" + lang + " | " + lang.toUpperCase());
                    switch (lang.toUpperCase()){
                        case "EN": resp = new GetCustomNotificationResponse(contents.get(0).getAction(), contents.get(0).getMessageEN());
                            break;
                        case "RU": resp = new GetCustomNotificationResponse(contents.get(0).getAction(), contents.get(0).getMessageRU());
                            break;
                        case "HY": resp = new GetCustomNotificationResponse(contents.get(0).getAction(), contents.get(0).getMessageAM());
                            break;
                        default: resp = new GetCustomNotificationResponse(contents.get(0).getAction(), contents.get(0).getMessageAM());
                    }
                    response.setData(resp);
                }else {
                    response.setData(new GetCustomNotificationResponse(1,language.getManyChangesCustomNotification()));
                }
                customNotificationService.delete(customer.getId());

            }else{
                response.setData(new Object());
            }
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }
}
