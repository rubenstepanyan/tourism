package com.primesoft.tourism.controllers.rest.admin.put;

import com.primesoft.tourism.beans.request.AddPartnerRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Partners;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.PartnerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
@RestController
public class AddPartner extends BaseController {

    @Autowired
    private PartnerService partnerService;

    @Autowired
    private AdministratorService administratorService;

    @RequestMapping(value = Constants.ADMIN_PATH+"addPartner",method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String addPartner(@Valid @RequestBody AddPartnerRequest request,
                         @RequestHeader("authorization") String authorization,
                         @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);
        if (administrator != null) {

            Partners partners = request.getPartnersObject();
            partnerService.save(partners);
            response.setMessage(language.getSuccessfullyAdded(),LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }
        return super.createGson(response);
    }
}
