package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.getAllDynamicPagesResponse.Content;
import com.primesoft.tourism.beans.response.getAllDynamicPagesResponse.GetAllDynamicPagesResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.DynamicPageService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class GetAllDynamicPages extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private DynamicPageService dynamicPageService;

    @RequestMapping(value = "getAllDynamicPages", method = RequestMethod.GET,produces = Constants.APPLICATION_JSON)
    public String getAllDynamicPages(@RequestParam(value = "type", required = false, defaultValue = "0") Integer type,
                                     @RequestHeader("language") String lang,
                                     @RequestHeader(value = "authorization", required = false) String authorization){


        Response<GetAllDynamicPagesResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);
        if(administrator != null){
           List<Content> dynamicPages = dynamicPageService.getAll(type, Util.getLanguageId(lang));
           response.setData(new GetAllDynamicPagesResponse(dynamicPages));
        }else{
            List<Content> dynamicPages = dynamicPageService.getAll(type, Util.getLanguageId(lang));
            response.setData(new GetAllDynamicPagesResponse(dynamicPages));
        }



        return super.createGson(response);
    }
}
