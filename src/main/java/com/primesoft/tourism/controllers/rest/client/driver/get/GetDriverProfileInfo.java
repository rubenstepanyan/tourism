package com.primesoft.tourism.controllers.rest.client.driver.get;


import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetDriverProfileInfo extends BaseController {


    @Autowired
    private DriverService driverService;


    @RequestMapping(value = Constants.DRIVER_PATH+"getProfileInfo", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getDriverTour(@RequestHeader("language") String lang,
                                @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Driver driver = driverService.getDriver(authorization);

        if(driver != null){
            driver.cleanNonShowFields();
            response.setData(driver);
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }
}
