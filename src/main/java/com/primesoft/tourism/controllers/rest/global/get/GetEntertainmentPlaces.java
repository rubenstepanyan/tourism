package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.getEntertainmentPlaces.Content;
import com.primesoft.tourism.beans.response.getEntertainmentPlaces.GetEntertainmentPlacesResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetEntertainmentPlaces extends BaseController {

    @Autowired
    private EntertainmentPlaceService entertainmentPlaceService;

    @RequestMapping(value = "getEntertainmentPlaces", method = RequestMethod.GET,produces = Constants.APPLICATION_JSON)
    public String getEntertainmentPlaces(@RequestParam(value = "type", required = false, defaultValue = "0") Integer type,
                                         @RequestParam(value = "city", required = false, defaultValue = "0") Integer city,
                                         @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                         @RequestHeader("language") String lang){


        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        List<Content> list;
        if(page>0) {
            Integer firstId = ((page-1)*Constants.ENTERTAINMENT_PLACE_ITEMS_COUNT);

            Long itemCount = entertainmentPlaceService.getCount(type, city);
            Long pageCount = Util.calculatePageCount(itemCount, Constants.ENTERTAINMENT_PLACE_ITEMS_COUNT);

            list = entertainmentPlaceService.getAll(Util.getLanguageId(lang), type, city, firstId, Constants.ENTERTAINMENT_PLACE_ITEMS_COUNT);
            response.setData(new GetEntertainmentPlacesResponse(itemCount,pageCount, list));
        }else {
            list = entertainmentPlaceService.getAll(Util.getLanguageId(lang), type, city, 0, 1000000);
            response.setData(new GetEntertainmentPlacesResponse(1l, list));
        }

        return super.createGson(response);
    }

}
