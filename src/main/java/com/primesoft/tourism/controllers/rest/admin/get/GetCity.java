package com.primesoft.tourism.controllers.rest.admin.get;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.CityService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class GetCity extends BaseController {

    @Autowired
    private CityService cityService;

    @RequestMapping(value = Constants.ADMIN_PATH+"getCity",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getCity(@RequestParam("parentId") Integer parentId,
                            @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        response.setData(cityService.getAllByParentId(parentId));

        return super.createGson(response);
    }



}
