package com.primesoft.tourism.controllers.rest.client.driver.post;

import com.primesoft.tourism.beans.request.DriverLoginRequest;
import com.primesoft.tourism.beans.response.DriverLoginResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Generator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class DriverLogin extends BaseController {


    @Autowired
    private DriverService driverService;

    @RequestMapping(value = Constants.DRIVER_PATH+"login", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String login(@Valid @RequestBody DriverLoginRequest request,
                        @RequestHeader(value = "os") Integer os,
                        @RequestHeader("language") String lang) {

        Response<DriverLoginResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        if(!request.getPhone().startsWith("+")){
            request.setPhone("+"+request.getPhone());
        }

        Driver driver = driverService.getDriverByPhoneAndPassword(request.getPhone(), Generator.stringToSha256(request.getPassword()));

        if (driver != null){
            if(driver.getConfirmed() && !driver.getSuspended()) {
                driver.setOsType(os);
                if(driver.getTopicAndroidAndWeb() == null || driver.getTopicIos() == null){
                    driver.setTopicAndroidAndWeb(Constants.DRIVER_TYPE+"a"+driver.getId()+"a"+1);
                    driver.setTopicIos(Constants.DRIVER_TYPE+"a"+driver.getId()+"a"+2);
                }
                driverService.save(driver);
                Boolean personalDriver = false;
                if(driver.getTourismAgencyId() != null){
                    personalDriver = true;
                }
                response.setData(new DriverLoginResponse(driver.getAuthorization(), driver.getNotificationToken(), personalDriver));
            }else if(!driver.getConfirmed()){
                response.setError(language.getYourAccountIsNotVerifiedPleaseConfirmIt(),LanguageManager.getMessageKey().getReject());
            }else {
                response.setError(language.getYourAccountIsSuspended(),LanguageManager.getMessageKey().getReject());
            }
        }else{
            response.setError(language.getWrongLoginOrPassword(), LanguageManager.getMessageKey().getReject());
        }
        return super.createGson(response);
    }

}
