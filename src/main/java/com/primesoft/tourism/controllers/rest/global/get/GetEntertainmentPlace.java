package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.GetEntertainmentPlaceResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlace;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceInfo;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlacePhoto;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceWorkingDays;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceInfoService;
import com.primesoft.tourism.service.interfaces.EntertainmentPlacePhotoService;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceService;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceWorkingDaysService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class GetEntertainmentPlace extends BaseController {

    @Autowired
    private EntertainmentPlaceService entertainmentPlaceService;

    @Autowired
    private EntertainmentPlaceInfoService entertainmentPlaceInfoService;

    @Autowired
    private EntertainmentPlaceWorkingDaysService entertainmentPlaceWorkingDaysService;

    @Autowired
    private EntertainmentPlacePhotoService entertainmentPlacePhotoService;

    @RequestMapping(value = "getEntertainmentPlace", method = RequestMethod.GET,produces = Constants.APPLICATION_JSON)
    public String getEntertainmentPlace(@RequestParam(value = "id") Integer id,
                                        @RequestHeader(value = "language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        EntertainmentPlace place = entertainmentPlaceService.getById(id);
        if(place!=null) {
            EntertainmentPlaceInfo info = entertainmentPlaceInfoService.getInfo(id, Util.getLanguageId(lang));
            List<EntertainmentPlaceWorkingDays> workingDays = entertainmentPlaceWorkingDaysService.findAll(id);
            List<EntertainmentPlacePhoto> photos = entertainmentPlacePhotoService.getAll(place.getId());
            response.setData(new GetEntertainmentPlaceResponse(place, info, workingDays, photos));
        }else {
            response.setError(language.getEntertainmentPlaceNotFound(),LanguageManager.getMessageKey().getEntertainmentPlaceNotFound());
        }

        return super.createGson(response);
    }


}
