package com.primesoft.tourism.controllers.rest.global.post;

import com.google.gson.Gson;
import com.primesoft.tourism.beans.request.SubscribeRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;

@RestController
public class Subscribe extends BaseController {

    @RequestMapping(value = "subscribe",method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String subscribe(@Valid @RequestBody SubscribeRequest request,
                            @RequestHeader("authorization") String authorization,
                            @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        response.setData(subscribe(request));

        return super.createGson(response);
    }

    private Object subscribe(SubscribeRequest request) {

        Object response = null;
        try {

            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", Constants.FIREBASE_AUTHORIZATION);
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<>(headers);
            response = new RestTemplate().postForObject("https://iid.googleapis.com/iid/v1/"+request.getToken()+"/rel/topics/"+request.getTopic(), entity, String.class);

            System.out.println(new Gson().toJson(response));

            return response;
        } catch (Exception e) {
            System.out.println(e);
        }

        return response;
    }
}
