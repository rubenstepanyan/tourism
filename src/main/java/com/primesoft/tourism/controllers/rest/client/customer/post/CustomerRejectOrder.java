package com.primesoft.tourism.controllers.rest.client.customer.post;

import com.primesoft.tourism.beans.request.CustomerRejectOrderRequest;
import com.primesoft.tourism.beans.response.ArcaRefundResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.*;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.AlertManager;
import com.primesoft.tourism.manager.ArcaManager;
import com.primesoft.tourism.manager.NotificationManager;
import com.primesoft.tourism.service.interfaces.*;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class CustomerRejectOrder extends BaseController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private GuideOrderService guideOrderService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private GuideService guideService;

    @Autowired
    private TourReservationService tourReservationService;

    @Autowired
    private TourService tourService;

    @Autowired
    private ArcaService arcaService;


    @RequestMapping(value =Constants.CUSTOMER_PATH+"rejectOrder", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String customerRejectOrder(@Valid @RequestBody CustomerRejectOrderRequest request,
                                      @RequestHeader("language") String lang,
                                      @RequestHeader("authorization") String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);
        Boolean refund = false;

        AlertManager alertManager = new AlertManager();

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        NotificationManager notificationManager = new NotificationManager();

        TourOrder tourOrder = orderService.getTourOrder(request.getOrderId(), customer.getId());

        GuideOrder guideOrder = guideOrderService.getGuideOrderByIdAndCustomerId(request.getOrderId(), customer.getId());

        Integer canceledDate = null;
        Integer status = null;

        if(request.getType().equals(Constants.TOUR_ORDER)){
            canceledDate = Util.getDateDiffInDay(Util.getCurrentDateAndTimeSeconds(),tourOrder.getTourDate(),"yyyy-MM-dd HH:mm:ss");
            status = tourOrder.getStatus();
        }else if(request.getType().equals(Constants.GUIDE_ORDER)){
            canceledDate = Util.getDateDiffInDay(Util.getCurrentDateAndTimeSeconds(),guideOrder.getFromDate(),"yyyy-MM-dd HH:mm:ss");
            status = guideOrder.getStatus();
        }

        System.out.println("canceledDate: "+canceledDate);

        if(canceledDate >= 1 || status<Constants.CUSTOMER_PAYED) {

            if (customer != null) {
                if (request.getType() == 0) {
                    if(tourOrder.getStatus()==Constants.CUSTOMER_PAYED){
                        refund=true;
                    }
                    tourOrder.setStatus(Constants.CUSTOMER_CANCELED);
                    tourOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());
                    orderService.save(tourOrder);


                    System.out.println("tourOrder.getTourId(): "+tourOrder.getTourId() );
                    System.out.println("tourOrder.getTourDate(): "+tourOrder.getTourDate() );

                    response.setMessage(language.getYourOrderHasBeenCanceled(), LanguageManager.getMessageKey().getSuccess());

                    Boolean isTransfer;
                    if(tourOrder.getTransfer()){
                        isTransfer = true;
                    }else{
                        isTransfer = false;
                    }
                    if (tourOrder.getReferenceType() == 2) {
                        Agency agency = agencyService.getAgency(tourOrder.getReference());
                        notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED, LanguageManager.getLanguageInstance(agency.getDashboardLanguage()).getYouHaveCanceledOrder(), agency.getTopicAndroidAndWeb(), agency.getTopicIos(),tourOrder.getStatus(),isTransfer, tourOrder.getId());
                    } else if (tourOrder.getReferenceType() == 1) {
                        Driver driver = driverService.getDriver(tourOrder.getReference());
                        notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED, LanguageManager.getLanguageInstance(driver.getDashboardLanguage()).getYouHaveCanceledOrder(), driver.getTopicAndroidAndWeb(), driver.getTopicIos(),tourOrder.getStatus(),isTransfer, tourOrder.getId());
                    }

                } else if (request.getType() == 1) {
                    if(guideOrder.getStatus()==Constants.CUSTOMER_PAYED){
                        refund=true;
                    }
                    guideOrder.setStatus(Constants.CUSTOMER_CANCELED);
                    guideOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());
                    guideOrderService.save(guideOrder);
                    response.setMessage(language.getYourOrderHasBeenCanceled(), LanguageManager.getMessageKey().getSuccess());

                    Guide guide = guideService.getGuidById(guideOrder.getGuideId());
                    notificationManager.sendNotif(Constants.GUIDE_ORDER_STATUS_CHANGED, LanguageManager.getLanguageInstance(guide.getDashboardLanguage()).getYouHaveCanceledOrder(), guide.getTopicAndroidAndWeb(), guide.getTopicIos(),guideOrder.getStatus(),null, guideOrder.getId());
                }

                if(refund){
                    String orderId = null;
                    Integer amount= null;

                    if(request.getType() == 0){
                        orderId = tourOrder.getArcaOrderId();
                        amount = tourOrder.getPrice();
                    }else if(request.getType() == 1){
                        orderId = guideOrder.getArcaOrderId();
                        amount = guideOrder.getPrice();
                    }

                    ArcaRefundResponse arcaRefundResponse = new ArcaManager().refund(orderId,(int)(amount*Constants.REFUND_PERCENT));

                    if(arcaRefundResponse.getErrorCode().equals("0")){
                        Arca arca = arcaService.getArca(orderId);

                        if(request.getType() == 0){
                            tourOrder.setStatus(Constants.CUSTOMER_CANCELED);
                            tourOrder.setReasonComment("Չեղարկվել է վճարումից հետո");
                            orderService.save(tourOrder);

                            TourReservation tourReservation = tourReservationService.getTourReservation(tourOrder.getTourId(),tourOrder.getTourDate());
                            Tour tour = tourService.getTour(tourOrder.getTourId());

                            if(tourReservation!=null && (tourReservation.getFreeSeats() + tourOrder.getQuantity()) >= tour.getFreeSeats()){
                                tourReservationService.delete(tourReservation.getId());
                            }else if(tourReservation!=null){
                                tourReservation.setFreeSeats(tourReservation.getFreeSeats() + tourOrder.getQuantity());
                                tourReservationService.save(tourReservation);
                            }

                        }else if(request.getType() == 1){
                            guideOrder.setStatus(Constants.CUSTOMER_CANCELED);
                            guideOrder.setReasonComment("Չեղարկվել է վճարումից հետո");
                            guideOrderService.save(guideOrder);


                        }


                        alertManager.sendCancelMail(customer.getMail(),"Cancel order");
                        arca.setRefunded(true);
                        arcaService.save(arca);
                    }
                }

            } else {
                response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
            }
        }else{
            response.setError(language.getYouDontCanceledThisOrder(),LanguageManager.getMessageKey().getReject());
        }



        return super.createGson(response);
    }
}
