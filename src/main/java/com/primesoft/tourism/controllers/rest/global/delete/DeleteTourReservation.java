package com.primesoft.tourism.controllers.rest.global.delete;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.service.interfaces.TourReservationService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DeleteTourReservation extends BaseController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private TourReservationService tourReservationService;

    @RequestMapping(value = "deleteTourReservation",method = RequestMethod.DELETE,produces = Constants.APPLICATION_JSON)
    public String deleteTourReservation(@RequestHeader("language")String lang,
                                        @RequestHeader("authorization")String authorization,
                                        @RequestParam(value = "id") Integer id){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency agency = agencyService.getAgency(authorization);
        Driver driver = driverService.getDriver(authorization);

        if(agency != null){
            tourReservationService.delete(id);
            response.setMessage(language.getSuccessfullyDeleted(),LanguageManager.getMessageKey().getSuccess());
        }else if(driver != null){
            tourReservationService.delete(id);
            response.setMessage(language.getSuccessfullyDeleted(),LanguageManager.getMessageKey().getSuccess());
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }
}
