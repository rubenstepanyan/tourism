package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.EditCarRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Car;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.CarService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditCar extends BaseController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private CarService carService;


    @RequestMapping(value = "editCar",method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String editCar(@Valid @RequestBody EditCarRequest request,
                             @RequestHeader("authorization") String authorization,
                             @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);
        Agency agency = agencyService.getAgency(authorization);
        Driver driver = driverService.getDriver(authorization);

        if(administrator !=null) {
            Car car = carService.getCar(request.getId());
            request.updateCarObject(car);
            carService.saveCar(car);
            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());

        }else  if(agency != null){
            Car car = carService.getCar(request.getId());
            request.updateCarObject(car);
            carService.saveCar(car);
            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());
        }else if(driver != null){
            Car car = carService.getCar(request.getId());
            request.updateCarObject(car);
            carService.saveCar(car);
            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());

        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
