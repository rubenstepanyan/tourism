package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.UpdateCarPhotoRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Car;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.CarService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class UpdateCarPhoto extends BaseController {

    @Autowired
    private CarService carService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private AgencyService agencyService;


    @RequestMapping(value = "updateCarPhoto", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String updateCarPhoto(@Valid @RequestBody UpdateCarPhotoRequest request,
                                    @RequestHeader("authorization") String authorization,
                                    @RequestHeader("language") String lang) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Driver driver = driverService.getDriver(authorization);

        Agency  agency = agencyService.getAgency(authorization);

        Car car = carService.getCar(request.getId());

        if(driver != null || agency != null){
            if(car != null) {
                request.updateCarPhoto(car);
                carService.saveCar(car);
                response.setMessage(language.getPhotoSuccessfullyChanged(), LanguageManager.getMessageKey().getSuccess());
            }else{
                response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());

            }
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }


}
