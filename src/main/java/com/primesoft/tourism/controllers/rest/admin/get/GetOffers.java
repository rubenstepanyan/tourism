package com.primesoft.tourism.controllers.rest.admin.get;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.OfferService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetOffers extends BaseController {


    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private OfferService offerService;

    @RequestMapping(value = Constants.ADMIN_PATH+"getOffers",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getOffers(@RequestHeader("language") String lang,
                            @RequestHeader("authorization")String authorization){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator  = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){
            response.setData(offerService.getAll());
        } else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }
}
