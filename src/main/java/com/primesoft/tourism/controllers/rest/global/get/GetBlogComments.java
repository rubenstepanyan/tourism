package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.getBlogCommentResponse.BlogCommentContent;
import com.primesoft.tourism.beans.response.getBlogCommentResponse.GetBlogCommentsResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.service.interfaces.CommentService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GetBlogComments extends BaseController {

    @Autowired
    private CommentService commentService;


    @RequestMapping(value = "getBlogComments", method = RequestMethod.GET,produces = Constants.APPLICATION_JSON)
    public String getBlogComments(@RequestParam(value = "blogId") Integer blogId,
                                  @RequestParam(value = "lastCommentId",required = false, defaultValue = "2147483647") Integer lastId){

        Response<Object> response = new Response<>();


        List<BlogCommentContent> comments = commentService.getAllBlogComment(blogId,lastId);

        response.setData(new GetBlogCommentsResponse(comments));

        return super.createGson(response);
    }
}
