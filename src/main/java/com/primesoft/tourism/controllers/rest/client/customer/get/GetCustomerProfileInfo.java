package com.primesoft.tourism.controllers.rest.client.customer.get;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetCustomerProfileInfo extends BaseController {


    @Autowired
    private CustomerService customerService;


    @RequestMapping(value = Constants.CUSTOMER_PATH+"getProfileInfo", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getCustomerProfileInfo(@RequestHeader("language") String lang,
                                         @RequestHeader("authorization")String authorization) {

        Response<Customer> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        if(customer != null){
            customer.cleanNonShowFields();
            response.setData(customer);
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }
        return super.createGson(response);
    }

}
