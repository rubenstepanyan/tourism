package com.primesoft.tourism.controllers.rest.client.guide.post;


import com.primesoft.tourism.beans.request.ConfirmGuideRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ConfirmGuide extends BaseController {

    @Autowired
    private GuideService guideService;

    @RequestMapping(value = Constants.GUIDE_PATH +"confirm", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String confirm(@Valid @RequestBody ConfirmGuideRequest request,
                          @RequestHeader("language") String lang) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Guide guide = guideService.getGuidByPhoneAndConfirmationCode(request.getPhone(),request.getConfirmationCode());

        if(guide != null){
            guide.setConfirmed(true);
            guideService.save(guide);
            response.setMessage(language.getSuccessfullyConfirmed(), LanguageManager.getMessageKey().getSuccess());

        }else{
            response.setError(language.getWrongConfirmationCode(), LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }
}
