package com.primesoft.tourism.controllers.rest.client.guide.put;

import com.primesoft.tourism.beans.request.GuideRegistrationRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.AlertManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class GuideRegistration extends BaseController {

    @Autowired
    private GuideService guideService;

    @Autowired
    private AgencyService agencyService;


    @RequestMapping(value = Constants.GUIDE_PATH+"registration",method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String guidRegistration(@Valid @RequestBody GuideRegistrationRequest request,
                                   @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Guide existingGuide = guideService.getGuidByPhone(request.getPhone());

        AlertManager smsManager = new AlertManager();
        Integer confirmationCode = Util.getRandomNumber();


        if(existingGuide == null) {

            Guide guide = request.getGuidObject();

            if(request.getAgencyCode() != null){
                Agency agency = agencyService.getAgencyByAgencyCode(request.getAgencyCode());

                if(agency != null){
                    guide.setTourismAgencyId(agency.getId());
                }else {
                    response.setError(language.getAgencyNotFound(),LanguageManager.getMessageKey().getReject());
                }
            }

            guide.setConfirmationCode(confirmationCode.toString());
            guideService.save(guide);

            smsManager.sendSms(guide.getPhone(), confirmationCode.toString());
            response.setMessage(language.getVerificationCodeSentToPhone(), LanguageManager.getMessageKey().getSuccess());

        }else if(!existingGuide.getConfirmed()){

            existingGuide = request.updateGuidObject(existingGuide);
            existingGuide.setConfirmationCode(confirmationCode.toString());
            guideService.save(existingGuide);

            smsManager.sendSms(existingGuide.getPhone(), confirmationCode.toString());
            response.setMessage(language.getVerificationCodeSentToPhone(), LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getAlreadyRegistered(),LanguageManager.getMessageKey().getReject());
        }

        return super.createGson(response);
    }
}
