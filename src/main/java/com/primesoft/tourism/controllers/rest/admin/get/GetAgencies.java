package com.primesoft.tourism.controllers.rest.admin.get;

import com.primesoft.tourism.beans.response.GetAgenciesResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GetAgencies extends BaseController {

    @Autowired
    private  AgencyService agencyService;

    @Autowired
    private AdministratorService administratorService;

    @RequestMapping(value = Constants.ADMIN_PATH+"getAgencies",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getAgencies(@RequestHeader("authorization") String authorization,
                             @RequestHeader("language") String lang){

        Response<List<GetAgenciesResponse>> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);
        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){
            response.setData(agencyService.getAgencies());
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
