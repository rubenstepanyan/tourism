package com.primesoft.tourism.controllers.rest.admin.delete;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.FollowerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DeleteFollower extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private FollowerService followerService;



    @RequestMapping(value = Constants.ADMIN_PATH+"deleteFollower",method = RequestMethod.DELETE,produces = Constants.APPLICATION_JSON)
    public String deleteFollower(@RequestHeader("language")String lang,
                                 @RequestHeader("authorization")String authorization,
                                 @RequestParam(value = "id") Integer id){

        Response<Object> response = new Response<>();
        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);
        Language language = LanguageManager.getLanguageInstance(lang);


        if(administrator != null){

            followerService.delete(id);

            response.setMessage(language.getSuccessfullyDeleted(),LanguageManager.getMessageKey().getSuccess());

        }else {
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
