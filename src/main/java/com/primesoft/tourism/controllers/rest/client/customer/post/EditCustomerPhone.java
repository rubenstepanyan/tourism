package com.primesoft.tourism.controllers.rest.client.customer.post;

import com.primesoft.tourism.beans.request.EditCustomerPhoneRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditCustomerPhone extends BaseController {


    @Autowired
    private CustomerService customerService;


    @RequestMapping(value = Constants.CUSTOMER_PATH+"editPhone", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String editCustomerPhone(@Valid @RequestBody EditCustomerPhoneRequest request,
                                   @RequestHeader("language") String lang,
                                   @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);


        if(!request.getPhone().startsWith("+")){
            request.setPhone("+"+request.getPhone());
        }

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        if(customer != null){
            if(request.getConfirmationCode().equals(customer.getConfirmationCode())){
                customer.setPhone(request.getPhone());
                customerService.save(customer);
                response.setMessage(language.getSuccessfullyEdited(),LanguageManager.getMessageKey().getSuccess());
            }else{
                response.setError(language.getWrongConfirmationCode(),LanguageManager.getMessageKey().getReject());
            }
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

}
