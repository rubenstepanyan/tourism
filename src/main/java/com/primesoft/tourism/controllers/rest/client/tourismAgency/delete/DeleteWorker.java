package com.primesoft.tourism.controllers.rest.client.tourismAgency.delete;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class DeleteWorker extends BaseController {



    @Autowired
    private AgencyService agencyService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private GuideService guideService;


    @RequestMapping(value =Constants.AGENCY_PATH+"deleteWorker",method = RequestMethod.DELETE,produces = Constants.APPLICATION_JSON)
    public String deleteWorker(@RequestHeader("language")String lang,
                               @RequestHeader("authorization")String authorization,
                               @RequestParam(value = "id") Integer id,
                               @RequestParam(value = "type") Integer type){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency agency = agencyService.getAgency(authorization);

        if(agency != null){
            if(type.equals(Constants.DRIVER_TYPE)){
                Driver driver = driverService.getDriver(id);
                if(agency.getId().equals(driver.getTourismAgencyId())){
                    driver.setTourismAgencyId(null);
                    driver.setAuthorization(UUID.randomUUID().toString());
                    driverService.save(driver);
                    response.setMessage(language.getSuccessfullyDeleted(), LanguageManager.getMessageKey().getSuccess());
                }else {
                    response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
                }

            }else if(type.equals(Constants.GUIDE_TYPE)){
                Guide guide = guideService.getGuidById(id);
                if(agency.getId().equals(guide.getTourismAgencyId())){
                    guide.setAuthorization(UUID.randomUUID().toString());
                    guide.setTourismAgencyId(null);
                    guideService.save(guide);
                    response.setMessage(language.getSuccessfullyDeleted(), LanguageManager.getMessageKey().getSuccess());
                }else {
                    response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
                }
            }
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
