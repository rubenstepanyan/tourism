package com.primesoft.tourism.controllers.rest.admin.post;

import com.primesoft.tourism.beans.request.editVideo.EditVideoRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Video;
import com.primesoft.tourism.beans.tableBeans.VideoContent;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.VideoContentService;
import com.primesoft.tourism.service.interfaces.VideoService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class EditVideo extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private VideoService videoService;

    @Autowired
    private VideoContentService videoContentService;


    @RequestMapping(value = Constants.ADMIN_PATH+"editVideo",method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String editVideo(@Valid @RequestBody EditVideoRequest request,
                             @RequestHeader("authorization") String authorization,
                             @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator !=null) {
            Video video = videoService.getVideo(request.getId());
            request.updateVideo(video);
            videoService.save(video);

            List<VideoContent> videoContents = videoContentService.getVideoContent(video.getId());
            request.updateVideoContent(videoContents);
            videoContentService.save(videoContents);

            response.setMessage(language.getSuccessfullyEdited(),LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
