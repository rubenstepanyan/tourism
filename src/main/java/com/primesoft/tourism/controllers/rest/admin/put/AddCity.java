package com.primesoft.tourism.controllers.rest.admin.put;

import com.primesoft.tourism.beans.request.addCityRequest.AddCityRequest;
import com.primesoft.tourism.beans.request.addCityRequest.Cities;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.City;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.CityService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class AddCity extends BaseController {


    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private CityService cityService;

    @RequestMapping(value = Constants.ADMIN_PATH+"addCity",method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String addCity(@Valid @RequestBody AddCityRequest request,
                                   @RequestHeader("authorization") String authorization,
                                   @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator !=null) {

            City cityAm = new City();
            for (Cities city :request.getCities()) {
                if(city.getLanguage().equals(Constants.AM_LANGUAGE)){
                    cityAm = city.getCity(0);
                    break;
                }
            }
            cityService.save(cityAm);


            List<City> cities = new ArrayList<>();
            for (Cities city :request.getCities()) {
                if(!city.getLanguage().equals(Constants.AM_LANGUAGE)) {
                    cities.add(city.getCity(cityAm.getId()));
                }
            }
            cityAm.setParentId(cityAm.getId());
            cityService.save(cityAm);
            cityService.save(cities);
            response.setMessage(language.getSuccessfullyAdded(), LanguageManager.getMessageKey().getSuccess());

        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

}
