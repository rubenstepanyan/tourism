package com.primesoft.tourism.controllers.rest.admin.post;

import com.primesoft.tourism.beans.request.editCity.Cities;
import com.primesoft.tourism.beans.request.editCity.EditCityRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.CityService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditCity extends BaseController {


    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private CityService cityService;

    @RequestMapping(value = Constants.ADMIN_PATH+"editCity",method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String editCity(@Valid @RequestBody EditCityRequest request,
                           @RequestHeader("authorization") String authorization,
                           @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator !=null) {

            for (Cities city : request.getCities()) {
                cityService.update(city.getId(), city.getName(),city.getPriority(),city.getNotShowInTours());
            }

            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());

        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

}
