package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.service.interfaces.OfferedTourService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class GetOfferedTours extends BaseController {


    @Autowired
    private OfferedTourService offeredTourService;

    @Autowired
    private CustomerService  customerService;


    @RequestMapping(value = "getOfferedTours", method = RequestMethod.GET,produces = Constants.APPLICATION_JSON)
    public String getOfferedTours(@RequestParam(required = false, name = "guideMode", defaultValue = "false") Boolean guideMode,
                                  @RequestHeader("language") String lang,
                                  @RequestHeader(value = "authorization", required = false) String authorization){

        Response<Object> response = new Response<>();

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        if(guideMode) {
            response.setData(offeredTourService.getAll(Util.getLanguageId(lang)));
        }else {
            if(customer != null){
                response.setData(offeredTourService.getAll(false, Util.getLanguageId(lang) ));
            }else {
                response.setData(offeredTourService.getAll(Util.getLanguageId(lang)));
            }
        }

        return super.createGson(response);
    }

}
