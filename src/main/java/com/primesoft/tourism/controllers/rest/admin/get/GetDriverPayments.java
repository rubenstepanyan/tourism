package com.primesoft.tourism.controllers.rest.admin.get;

import com.primesoft.tourism.beans.response.getTourPaymentsResponse.GetTourPaymentsContent;
import com.primesoft.tourism.beans.response.getTourPaymentsResponse.GetTourPaymentsResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.OrderService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetDriverPayments extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = Constants.ADMIN_PATH + "getDriverPayments", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getDriverPayments(@RequestHeader("authorization") String authorization,
                                    @RequestHeader("language") String lang,
                                    @RequestParam(value = "from") String from,
                                    @RequestParam(value = "to") String to,
                                    @RequestParam(value = "driverId") Integer driverId) {

        Response<Object> response = new Response<>();

        Language language = LanguageManager.getLanguageInstance(lang);
        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if (administrator != null) {
            List<GetTourPaymentsContent> paymentsContents;
            if (driverId != 0) {
                paymentsContents = orderService.getAllDriverPayments(Constants.DRIVER_TYPE, driverId, Util.getLanguageId(lang), from, to);
            } else {
                paymentsContents = orderService.getAllDriverPayments(Constants.DRIVER_TYPE, 0, Util.getLanguageId(lang), from, to);
            }

            response.setData(new GetTourPaymentsResponse(paymentsContents));
        } else {
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
