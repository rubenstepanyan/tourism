package com.primesoft.tourism.controllers.rest.admin.put;

import com.primesoft.tourism.beans.request.addVideo.AddVideoRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Video;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.VideoContentService;
import com.primesoft.tourism.service.interfaces.VideoService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AddVideo extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private VideoService videoService;

    @Autowired
    private VideoContentService videoContentService;

    @RequestMapping(value = Constants.ADMIN_PATH+"addVideo",method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String addVideo(@Valid @RequestBody AddVideoRequest request,
                          @RequestHeader("authorization") String authorization,
                          @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator !=null) {
            Video video = request.getVideo();
            videoService.save(video);
            videoContentService.save(request.getVideoContent(video.getId()));
            response.setMessage(language.getSuccessfullyAdded(),LanguageManager.getMessageKey().getSuccess());
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
