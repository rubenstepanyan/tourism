package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.CheckArcaStatusRequest;
import com.primesoft.tourism.beans.response.ArcaStatusResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.ArcaManager;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class CheckArcaStatus extends BaseController {


    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "checkArcaStatus", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String checkArcaStatus(@RequestBody CheckArcaStatusRequest request,
                                 @RequestHeader("authorization") String authorization,
                                 @RequestHeader("language") String lang) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        if (customer != null) {

            ArcaManager arcaManager = new ArcaManager();
            ArcaStatusResponse arcaStatusResponse = arcaManager.getOrderStatus(request.getOrderId());
            response.setData(arcaStatusResponse);

            return super.createGson(response);
        } else {
            response.setError(language.getWrongCouponNumber(), LanguageManager.getMessageKey().getReject());
        }


        return super.createGson(response);
    }

}
