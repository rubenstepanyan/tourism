package com.primesoft.tourism.controllers.rest.client.driver.post;

import com.primesoft.tourism.beans.request.EditAgencyPhoneRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.service.interfaces.OrderService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditDriverPhone extends BaseController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private OrderService orderService;


    @RequestMapping(value = Constants.DRIVER_PATH+"editPhone", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String editDriverPhone(@Valid @RequestBody EditAgencyPhoneRequest request,
                                  @RequestHeader("language") String lang,
                                  @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);


        if(!request.getPhone().startsWith("+")){
            request.setPhone("+"+request.getPhone());
        }

        Driver driver = driverService.getDriver(authorization);

        if(driver != null){
            if(request.getConfirmationCode().equals(driver.getConfirmationCode())){
                driver.setPhone(request.getPhone());
                response.setMessage(language.getSuccessfullyEdited(),LanguageManager.getMessageKey().getSuccess());
                driverService.save(driver);
                orderService.updateTourOrders(driver.getId(),driver.getPhone(),Constants.DRIVER_TYPE);
            }else{
                response.setError(language.getWrongConfirmationCode(),LanguageManager.getMessageKey().getReject());
            }
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
