package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.getOrdersResponse.GetGuideOrderContentsResponse;
import com.primesoft.tourism.beans.response.getOrdersResponse.GetTourOrderContentResponse;
import com.primesoft.tourism.beans.response.getOrdersResponse.GetTourOrdersResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.*;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class GetOrders extends BaseController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private GuideService guideService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private GuideOrderService guideOrderService;

    @RequestMapping(value = "getOrders", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getOrders(@RequestHeader("language") String lang,
                            @RequestHeader("authorization") String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency agency = agencyService.getAgency(authorization);
        Driver driver = driverService.getDriver(authorization);
        Guide guide = guideService.getGuidByAuthorization(authorization);

        if(agency != null){

            List<GetTourOrderContentResponse> tourOrderContents = orderService.getOrder(agency.getId(),null, Util.getLanguageId(lang));
            List<GetGuideOrderContentsResponse> guideOrderContents = guideOrderService.getGuideOrder(agency.getId(),null);

            response.setData(new GetTourOrdersResponse(tourOrderContents,guideOrderContents));

        }else if(driver != null){

            List<GetTourOrderContentResponse> tourOrderContents = orderService.getOrder(null,driver.getId(), Util.getLanguageId(lang));

            response.setData(new GetTourOrdersResponse(tourOrderContents,null));
        }else if(guide != null){

           List<GetGuideOrderContentsResponse> guideOrderContents =guideOrderService.getGuideOrder(null,guide.getId());

            response.setData(new GetTourOrdersResponse(null,guideOrderContents));

        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }



        return super.createGson(response);
    }
}
