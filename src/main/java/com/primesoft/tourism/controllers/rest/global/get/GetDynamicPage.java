package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.GetDynamicPageResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.DynamicPage;
import com.primesoft.tourism.beans.tableBeans.DynamicPageContent;
import com.primesoft.tourism.beans.tableBeans.DynamicPagePhoto;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.DynamicPageContentService;
import com.primesoft.tourism.service.interfaces.DynamicPagePhotoService;
import com.primesoft.tourism.service.interfaces.DynamicPageService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class GetDynamicPage extends BaseController {
    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private DynamicPageService dynamicPageService;

    @Autowired
    private DynamicPageContentService dynamicPageContentService;

    @Autowired
    private DynamicPagePhotoService dynamicPagePhotoService;

    @RequestMapping(value = "getDynamicPage", method = RequestMethod.GET,produces = Constants.APPLICATION_JSON)
    public String getDynamicPage(@RequestParam(value = "id") Integer id,
                                 @RequestHeader(value = "language") String lang,
                                 @RequestHeader(value = "authorization",required = false) String authorization){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        System.out.println("language "+lang);
        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);
        DynamicPage dynamicPage = dynamicPageService.getDynamicPage(id);

        if(administrator != null && dynamicPage != null){
            List<DynamicPageContent> dynamicPageContents = dynamicPageContentService.getDynamicPageContents(id);
            List<DynamicPagePhoto> dynamicPagePhotos = dynamicPagePhotoService.getDynamicPagePhoto(id);
            GetDynamicPageResponse getDynamicPageResponse = new GetDynamicPageResponse(dynamicPage,dynamicPageContents,dynamicPagePhotos);
            response.setData(getDynamicPageResponse);
        }else{
            DynamicPageContent dynamicPageContent = dynamicPageContentService.getDynamicPageContent(id, Util.getLanguageId(lang));
            List<DynamicPagePhoto> dynamicPagePhoto = dynamicPagePhotoService.getDynamicPagePhoto(id);
            response.setData(new GetDynamicPageResponse(dynamicPage,dynamicPageContent,dynamicPagePhoto));
        }



        return super.createGson(response);
    }
}
