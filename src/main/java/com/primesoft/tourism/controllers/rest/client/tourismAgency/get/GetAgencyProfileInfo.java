package com.primesoft.tourism.controllers.rest.client.tourismAgency.get;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetAgencyProfileInfo extends BaseController {

    @Autowired
    private AgencyService agencyService;


    @RequestMapping(value = Constants.AGENCY_PATH+"getProfileInfo", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getAgencyProfileInfo(@RequestHeader("language") String lang,
                                       @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        System.out.println("profile info");
        Agency agency = agencyService.getAgency(authorization);

        if(agency != null){
            agency.cleanNonShowFields();
            response.setData(agency);
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }



        return super.createGson(response);
    }

}
