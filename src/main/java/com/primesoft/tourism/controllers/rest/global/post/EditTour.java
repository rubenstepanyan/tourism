package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.EditTourRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.*;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.*;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditTour extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private  TourService tourService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private CarService carService;


    @RequestMapping(value = "editTour",method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String editTour(@Valid @RequestBody EditTourRequest request,
                           @RequestHeader("authorization") String authorization,
                           @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);
        Agency agency = agencyService.getAgency(authorization);
        Driver driver = driverService.getDriver(authorization);
        Tour tour = tourService.getTour(request.getId());
        Car car = null;

        if(tour!=null){
            car = carService.getCar(request.getCarId());
        }

        if(administrator != null){
            request.updateTourObject(tour, car);
            tourService.save(tour);
            response.setMessage(language.getSuccessfullyEdited(),LanguageManager.getMessageKey().getSuccess());
        }else if(agency != null){
            request.updateTourObject(tour, car);
            tourService.save(tour);
            response.setMessage(language.getSuccessfullyEdited(),LanguageManager.getMessageKey().getSuccess());
        }else if(driver != null){
            request.updateTourObject(tour, car);
            tourService.save(tour);
            response.setMessage(language.getSuccessfullyEdited(),LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
