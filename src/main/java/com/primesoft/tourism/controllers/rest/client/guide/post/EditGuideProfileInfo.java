package com.primesoft.tourism.controllers.rest.client.guide.post;

import com.primesoft.tourism.beans.request.EditGuideProfileInfoRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditGuideProfileInfo extends BaseController {

    @Autowired
    private GuideService guideService;


    @RequestMapping(value = Constants.GUIDE_PATH+"editProfileInfo", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String EditGuideProfileInfo(@Valid @RequestBody EditGuideProfileInfoRequest request,
                                        @RequestHeader("language") String lang,
                                        @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Guide guide  = guideService.getGuidByAuthorization(authorization);

        if(guide != null){
            guide = request.updateGuide(guide);
            guideService.save(guide);
            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
