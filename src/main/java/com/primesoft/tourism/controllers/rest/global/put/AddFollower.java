package com.primesoft.tourism.controllers.rest.global.put;

import com.primesoft.tourism.beans.request.AddFollowerRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Follower;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.FollowerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AddFollower extends BaseController {

    @Autowired
    private FollowerService followerService;

    @RequestMapping(value = "addFollower",method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String addFollower(@Valid @RequestBody AddFollowerRequest request,
                              @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Follower existingFollower = followerService.getFollower(request.getMail());

        if(existingFollower != null){
            response.setMessage(language.getSuccessfullyFollowed(), LanguageManager.getMessageKey().getSuccess());
        }else{
            Follower follower = new Follower();
            follower.setMail(request.getMail());
            followerService.save(follower);
            response.setMessage(language.getSuccessfullyFollowed(), LanguageManager.getMessageKey().getSuccess());
        }



        return super.createGson(response);
    }
}
