package com.primesoft.tourism.controllers.rest.admin.post;

import com.primesoft.tourism.beans.request.editBlog.EditBlogRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Blog;
import com.primesoft.tourism.beans.tableBeans.BlogContent;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.BlogContentService;
import com.primesoft.tourism.service.interfaces.BlogService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class EditBlog extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private BlogService blogService;

    @Autowired
    private BlogContentService blogContentService;


    @RequestMapping(value = Constants.ADMIN_PATH+"editBlog",method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String editBlog(@Valid @RequestBody EditBlogRequest request,
                            @RequestHeader("authorization") String authorization,
                            @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator !=null) {
            Blog blog = blogService.getBlog(request.getId());
            request.updateBlog(blog);
            blogService.save(blog);

            List<BlogContent> blogContents = blogContentService.getBlogContent(blog.getId());
            request.updateBlogContent(blogContents);
            blogContentService.save(blogContents);

            response.setMessage(language.getSuccessfullyEdited(),LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
