package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.ChangePasswordRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Generator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ChangePassword extends BaseController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private GuideService guideService;


    @RequestMapping(value = "changePassword", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String changePassword(@Valid @RequestBody ChangePasswordRequest request,
                                 @RequestHeader("language") String lang) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);


        if(request.getType().equals(Constants.CUSTOMER_TYPE)) {

            Customer customer = customerService.getCustomerByMail(request.getMailOrPhone());
            if(customer!=null) {
                if (request.getConfirmationCode().equals(customer.getConfirmationCode())) {
                    customer.setPassword(Generator.stringToSha256(request.getPassword()));
                    customerService.save(customer);
                } else {
                    response.setError(language.getWrongConfirmationCode(), LanguageManager.getMessageKey().getReject());
                }
            }else{
                response.setMessage(language.getCustomerNotFound(),LanguageManager.getMessageKey().getReject());
            }
        }else if(request.getType().equals(Constants.DRIVER_TYPE)){

            Driver driver = driverService.getDriverByPhone(request.getMailOrPhone());
            if(driver!=null) {
                if (request.getConfirmationCode().equals(driver.getConfirmationCode())) {
                    driver.setPassword(Generator.stringToSha256(request.getPassword()));
                    driverService.save(driver);

                } else {
                    response.setError(language.getWrongConfirmationCode(), LanguageManager.getMessageKey().getReject());
                }
            }else{
                response.setMessage(language.getDriverNotFound(),LanguageManager.getMessageKey().getReject());
            }

        }else if(request.getType().equals(Constants.AGENCY_TYPE)){

            Agency agency = agencyService.getAgencyByPhone(request.getMailOrPhone());
            if(agency != null) {
                if (request.getConfirmationCode().equals(agency.getConfirmationCode())) {
                    agency.setPassword(Generator.stringToSha256(request.getPassword()));
                    agencyService.save(agency);
                } else {
                    response.setError(language.getWrongConfirmationCode(), LanguageManager.getMessageKey().getReject());
                }
            }else{
                response.setMessage(language.getAgencyNotFound(),LanguageManager.getMessageKey().getReject());
            }
        }else if(request.getType().equals(Constants.GUIDE_TYPE)){
            Guide guide = guideService.getGuidByPhone(request.getMailOrPhone());
            if(guide != null) {
                if (request.getConfirmationCode().equals(guide.getConfirmationCode())) {
                    guide.setPassword(Generator.stringToSha256(request.getPassword()));
                    guideService.save(guide);
                } else {
                    response.setError(language.getWrongConfirmationCode(), LanguageManager.getMessageKey().getReject());
                }
            }else{
                response.setMessage(language.getGuideNotFound(),LanguageManager.getMessageKey().getReject());
            }
        }

        if(response.getError()==null) {
            response.setMessage(language.getYourPasswordSuccessfullyChanged(), LanguageManager.getMessageKey().getSuccess());
        }

        return super.createGson(response);
    }
}
