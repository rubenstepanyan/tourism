package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.SendDriverConfirmationCodeMailRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.AlertManager;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class SendConfirmationCodeMail extends BaseController {

    @Autowired
    private CustomerService customerService;


    @RequestMapping(value = "sendConfirmationCodeMail", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String sendConfirmationCodeMail(@Valid @RequestBody SendDriverConfirmationCodeMailRequest request,
                                           @RequestHeader("language") String lang,
                                           @RequestHeader (value = "authorization", required = false) String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);
        AlertManager alertManager = new AlertManager();
        Integer confirmationCode = Util.getRandomNumber();

        Customer customer = customerService.getCustomerByAuthorization(authorization);
        Customer existCustomer = customerService.getCustomerByMail(request.getMail());

        if(customer != null){
            Customer existingCustomer = customerService.getCustomerByMail(request.getMail());
            if(existingCustomer != null){
                response.setError(language.getAlreadyRegistered(),LanguageManager.getMessageKey().getReject());
            }else {
                customer.setConfirmationCode(confirmationCode.toString());
                alertManager.sendConfirmationMail(request.getMail(), confirmationCode.toString());
                customerService.save(customer);
                response.setMessage(language.getSuccessfullySent(), LanguageManager.getMessageKey().getSuccess());
            }
        }else  if(existCustomer != null){
            existCustomer.setConfirmationCode(confirmationCode.toString());
            alertManager.sendConfirmationMail(request.getMail(), confirmationCode.toString());
            customerService.save(existCustomer);
            response.setMessage(language.getSuccessfullySent(), LanguageManager.getMessageKey().getSuccess());
        }
        else{
            response.setError(language.getCustomerNotFound(),LanguageManager.getMessageKey().getCustomerNotFound());
        }



        return super.createGson(response);
    }



}
