package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.EditDriverRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.service.interfaces.TourService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditDriver extends BaseController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private TourService tourService;


    @RequestMapping(value = "editDriver",method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String editDriver(@Valid @RequestBody EditDriverRequest request,
                           @RequestHeader("authorization") String authorization,
                           @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);
        Driver existingDriver = driverService.getDriver(authorization);

        if(administrator !=null) {
            Driver driver = driverService.getDriver(request.getId());

            if(!(driver.getFirstName()+" "+driver.getLastName()).equals(request.getLastName()+" "+request.getLastName())){
                tourService.updateTours(Constants.DRIVER_TYPE, driver.getId(), request.getFirstName()+" "+request.getLastName());
            }

            if(!driver.getCurrentRating().equals(request.getCurrentRating())){
                tourService.updateTourRating(driver.getId(),request.getCurrentRating(),Constants.DRIVER_TYPE);
            }


            if(!driver.getApproved().equals(request.getApproved())){
                tourService.updateApproved(driver.getId(), Constants.DRIVER_TYPE,request.getApproved());
            }

            if(!driver.getSuspended().equals(request.getSuspended())){
                tourService.updateBlocked(driver.getId(), Constants.DRIVER_TYPE,request.getApproved());
            }

            request.updateDriver(driver);
            driver.setCurrentRating(request.getCurrentRating());
            driverService.save(driver);
            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());
        }else if(existingDriver != null){
            request.updateDriver(existingDriver);
            driverService.save(existingDriver);
            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }


}
