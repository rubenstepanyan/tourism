package com.primesoft.tourism.controllers.rest.client.customer.post;

import com.primesoft.tourism.beans.request.EditCommentRequest;
import com.primesoft.tourism.beans.response.AddCommentResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Comment;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.CommentService;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditComment extends BaseController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CommentService commentService;


    @RequestMapping(value = Constants.CUSTOMER_PATH+"editComment", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String editComment(@Valid @RequestBody EditCommentRequest request,
                              @RequestHeader("language") String lang,
                              @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        if(customer != null){
            Comment comment = commentService.getComment(request.getId(),customer.getId());
            comment.setCommentDate(Util.getCurrentDateAndTime());
            comment.setComment(request.getComment());
            commentService.save(comment);
            Comment newComment = commentService.getOneComment(request.getId());
            customer.cleanShowFields();
            response.setData(new AddCommentResponse(newComment,customer));

        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
