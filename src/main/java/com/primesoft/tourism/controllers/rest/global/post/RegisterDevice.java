package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegisterDevice extends BaseController {

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private GuideService guideService;

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "changeLanguage", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String changeLanguage(@RequestHeader("language") String lang,
                                 @RequestHeader("authorization") String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency agency = agencyService.getAgency(authorization);
        Driver driver = driverService.getDriver(authorization);
        Guide guide = guideService.getGuidByAuthorization(authorization);
        Customer customer = customerService.getCustomerByAuthorization(authorization);

        if(agency != null){
            agency.setDashboardLanguage(Util.getLanguageId(lang));
            agencyService.save(agency);
        }else if(driver != null){
            driver.setDashboardLanguage(Util.getLanguageId(lang));
            driverService.save(driver);
        }else if(guide != null){
            guide.setDashboardLanguage(Util.getLanguageId(lang));
            guideService.save(guide);
        }else if(customer != null){
            customer.setDashboardLanguage(Util.getLanguageId(lang));
            customerService.save(customer);
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
