package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.GetPhotosResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.service.interfaces.SlideShowService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class GetPhotos extends BaseController {

    @Autowired
    private SlideShowService slideShowService;


    @RequestMapping(value = "getPhotos",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getPhotos(){

        System.out.println("home Page");
        Response<GetPhotosResponse> response = new Response<>();

        response.setData(new GetPhotosResponse(slideShowService.getPhotos()));


        return super.createGson(response);
    }
}
