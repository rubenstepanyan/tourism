package com.primesoft.tourism.controllers.rest.client.tourismAgency.post;

import com.primesoft.tourism.beans.request.ConfirmAgencyRequest;
import com.primesoft.tourism.beans.response.ConfirmAgencyResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ConfirmAgency extends BaseController {

    @Autowired
    private AgencyService agencyService;

    @RequestMapping(value = Constants.AGENCY_PATH+"confirm", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String confirm(@Valid @RequestBody ConfirmAgencyRequest request,
                          @RequestHeader("language") String lang) {

        Response<ConfirmAgencyResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency agency = agencyService.getAgencyByPhoneAndConfirmationCode(request.getPhone(),request.getConfirmationCode());

        if(agency != null){
            response.setMessage(language.getSuccessfullyConfirmed(),LanguageManager.getMessageKey().getSuccess());
            agency.setConfirmed(true);
            agencyService.save(agency);
        }else{
            response.setError(language.getWrongConfirmationCode(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
