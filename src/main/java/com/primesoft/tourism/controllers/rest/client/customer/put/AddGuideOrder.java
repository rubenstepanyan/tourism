package com.primesoft.tourism.controllers.rest.client.customer.put;

import com.primesoft.tourism.beans.request.AddGuideOrderRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.*;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.NotificationManager;
import com.primesoft.tourism.service.interfaces.*;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AddGuideOrder extends BaseController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private GuideService guideService;

    @Autowired
    private GuidePriceListService guidePriceListService;

    @Autowired
    private GuideOrderService guideOrderService;

    @Autowired
    private AgencyService agencyService;


    @RequestMapping(value = Constants.CUSTOMER_PATH+"addGuideOrder", method = RequestMethod.PUT, produces = Constants.APPLICATION_JSON)
    public String addGuideOrder(@Valid @RequestBody AddGuideOrderRequest request,
                           @RequestHeader("language") String lang,
                           @RequestHeader("authorization") String authorization) {


        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        NotificationManager  notificationManager = new NotificationManager();

        if(customer !=null){

            Guide guide = guideService.getGuidById(request.getId());


            if(Util.getDateDiffInMillis(request.getFrom(),request.getTo())>= 0) {
                if (guide != null) {
                    GuidePriceList guidePriceList = guidePriceListService.getGuidePriceList(request.getPriceListId());

                    GuideOrder guideOrder = new GuideOrder();

                    guideOrder.setGuideId(request.getId());
                    guideOrder.setGuidePriceListId(request.getPriceListId());
                    guideOrder.setLanguage(request.getLanguage());
                    guideOrder.setQuantityType(request.getQuantityType());
                    guideOrder.setPrice((Util.getDateDiffInDay(request.getFrom(), request.getTo()) + 1) * guidePriceList.getPrice());
                    guideOrder.setStatus(Constants.PENDING);
                    guideOrder.setCustomerId(customer.getId());
                    guideOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());
                    guideOrder.setReference(guide.getTourismAgencyId());
                    guideOrder.setFromDate(request.getFrom());
                    guideOrder.setToDate(request.getTo());
                    guideOrder.setQr("VP-" + Util.getRandomNumber().toString());
                    guideOrder.setPayed(false);
                    guideOrder.setMyTripsPaid(false);
                    guideOrder.setDirection(request.getDirection());
                    guideOrder.setPricePerDay(guidePriceList.getPrice());
                    guideOrder.setPriceRating(guidePriceList.getRating());
                    guideOrder.setCity(guidePriceList.getCity());
                    guideOrder.setRefund(false);

                    guideOrderService.save(guideOrder);

                    Agency agency = agencyService.getAgency(guide.getTourismAgencyId());
                    if(agency != null){
                        notificationManager.sendNotif(Constants.GUIDE_ORDER_STATUS_CHANGED, LanguageManager.getLanguageInstance(agency.getDashboardLanguage()).getYouHaveNewOrder(), agency.getTopicAndroidAndWeb(), agency.getTopicIos(),guideOrder.getStatus(),null,guideOrder.getId());
                    }else {
                        notificationManager.sendNotif(Constants.GUIDE_ORDER_STATUS_CHANGED, LanguageManager.getLanguageInstance(guide.getDashboardLanguage()).getYouHaveNewOrder(), guide.getTopicAndroidAndWeb(), guide.getTopicIos(),guideOrder.getStatus(),null, guideOrder.getId());
                    }


                    response.setMessage(language.getPleaseWaitForTheGuideToAcceptYourOrder(), LanguageManager.getMessageKey().getSuccess());
                } else {
                    response.setError(request.getId().toString(), LanguageManager.getMessageKey().getGuideNotFound());
                    return super.createGson(response);
                }
            }else{
                response.setError(language.getIncorrectDate(), LanguageManager.getMessageKey().getReject());
                return super.createGson(response);
            }
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

}

