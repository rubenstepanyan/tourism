package com.primesoft.tourism.controllers.rest.client.guide.get;

import com.primesoft.tourism.beans.response.GetAllReservationResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.beans.tableBeans.GuideReservation;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.GuideReservationService;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GetAllReservations extends BaseController {

    @Autowired
    private GuideService guideService;

    @Autowired
    private GuideReservationService guideReservationService;


    @RequestMapping(value = Constants.GUIDE_PATH+"getAllReservations", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getAllReservations(@RequestHeader("language") String lang,
                                     @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Guide guide = guideService.getGuidByAuthorization(authorization);

        if(guide != null){
            List<GuideReservation> guideReservations = guideReservationService.getAll(guide.getId());
            response.setData(new GetAllReservationResponse(guideReservations));
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }
}
