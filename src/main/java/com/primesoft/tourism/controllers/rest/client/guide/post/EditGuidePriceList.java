package com.primesoft.tourism.controllers.rest.client.guide.post;

import com.google.gson.Gson;
import com.primesoft.tourism.beans.request.editGuidePriceList.EditGuidePriceListRequest;
import com.primesoft.tourism.beans.request.editGuidePriceList.GuidePriceLists;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.GuidePriceListService;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditGuidePriceList extends BaseController {


    @Autowired
    private GuideService guideService;

    @Autowired
    private GuidePriceListService guidePriceListService;

    @RequestMapping(value = Constants.GUIDE_PATH +"editGuidePriceLIst", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String editGuidePriceLIst(@Valid @RequestBody EditGuidePriceListRequest request,
                                     @RequestHeader("language") String lang,
                                     @RequestHeader("authorization") String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Guide guide = guideService.getGuidByAuthorization(authorization);

        if(guide !=null){

            for (GuidePriceLists priceList : request.getPriceLists()) {

                System.out.println(new Gson().toJson(request));

                guidePriceListService.update(priceList.getId(), priceList.getLanguage(),priceList.getPrice(),priceList.getType());
            }

            response.setMessage(language.getSuccessfullyEdited(),LanguageManager.getMessageKey().getSuccess());
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }
}
