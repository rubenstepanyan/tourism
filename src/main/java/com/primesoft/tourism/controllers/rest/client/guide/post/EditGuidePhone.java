package com.primesoft.tourism.controllers.rest.client.guide.post;

import com.primesoft.tourism.beans.request.EditAgencyPhoneRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditGuidePhone extends BaseController {

    @Autowired
    private GuideService guideService;


    @RequestMapping(value = Constants.GUIDE_PATH+"editPhone", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String editGuidePhone(@Valid @RequestBody EditAgencyPhoneRequest request,
                                  @RequestHeader("language") String lang,
                                  @RequestHeader("authorization")String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);


        if(!request.getPhone().startsWith("+")){
            request.setPhone("+"+request.getPhone());
        }

        Guide guide = guideService.getGuidByAuthorization(authorization);

        if(guide != null){
            if(request.getConfirmationCode().equals(guide.getConfirmationCode())){
                guide.setPhone(request.getPhone());
                guideService.save(guide);
                response.setMessage(language.getSuccessfullyEdited(),LanguageManager.getMessageKey().getSuccess());
            }else{
                response.setError(language.getWrongConfirmationCode(),LanguageManager.getMessageKey().getReject());
            }
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
