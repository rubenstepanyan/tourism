package com.primesoft.tourism.controllers.rest.admin.put;

import com.primesoft.tourism.beans.request.AddAdministratorRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AddAdministrator extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @RequestMapping(value = Constants.ADMIN_PATH+"addAdministrator",method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String addAdministrator(@Valid @RequestBody AddAdministratorRequest request,
                                   @RequestHeader("authorization") String authorization,
                                   @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator thisAdministrator = administratorService.getAdministratorByAuthorization(authorization);

        if(thisAdministrator !=null) {

            Administrator existingAdministrator = administratorService.getAdministratorByUserName(request.getUserName());

            if (existingAdministrator == null) {

                Administrator administrator = request.getAdministratorObject();

                administratorService.saveAdministrator(administrator);
                response.setMessage(language.getSuccessfullyAdded(), LanguageManager.getMessageKey().getSuccess());

            } else {
                response.setError(language.getExistingAdministrator(), LanguageManager.getMessageKey().getReject());
            }
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
