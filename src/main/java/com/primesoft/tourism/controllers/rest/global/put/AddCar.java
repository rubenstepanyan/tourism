package com.primesoft.tourism.controllers.rest.global.put;

import com.primesoft.tourism.beans.request.AddCarRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Car;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.CarService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AddCar extends BaseController{

    @Autowired
    private CarService carService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private AgencyService agencyService;

    @RequestMapping(value = "addCar",method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String addCar(@Valid @RequestBody AddCarRequest request,
                         @RequestHeader("authorization") String authorization,
                         @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Driver driver = driverService.getDriver(authorization);
        Agency agency = agencyService.getAgency(authorization);

        if(driver!=null){
            Car car = request.getCarObject(driver.getId(), Constants.DRIVER_TYPE);
            carService.saveCar(car);
            response.setDataAndMessage(car,language.getCarSuccessfullyAdded(), LanguageManager.getMessageKey().getSuccess());
        }else if(agency != null){
            Car car = request.getCarObject(agency.getId(), Constants.AGENCY_TYPE);
            carService.saveCar(car);
            response.setDataAndMessage(car,language.getCarSuccessfullyAdded(), LanguageManager.getMessageKey().getSuccess());

        }else{
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
