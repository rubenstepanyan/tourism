package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.ActivateCouponRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Coupon;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.CouponService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ActivateCoupon extends BaseController {


    @Autowired
    private CouponService couponService;

    @RequestMapping(value = "activateCoupon", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String activateCoupon(@Valid @RequestBody ActivateCouponRequest request,
                              @RequestHeader("language") String lang) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);


            Coupon coupon = couponService.getCouponByNumber(request.getCouponNumber(),false,true);

            if (coupon != null) {
                coupon.setActivated(true);
                couponService.save(coupon);
                response.setMessage(language.getCouponActivated(), LanguageManager.getMessageKey().getSuccess());
            } else {
                response.setError(language.getWrongCouponNumber(), LanguageManager.getMessageKey().getReject());
            }


        return super.createGson(response);
    }
}
