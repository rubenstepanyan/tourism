package com.primesoft.tourism.controllers.rest.client.driver.get;

import com.primesoft.tourism.beans.response.GetDriverOptionsResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.beans.tableBeans.TourOrder;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.service.interfaces.OrderService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GetDriverOptions extends BaseController {


    @Autowired
    private DriverService driverService;

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = Constants.DRIVER_PATH+"getOptions", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getDriverOptions(@RequestHeader("language") String lang,
                                   @RequestHeader("authorization")String authorization) {

        Response<GetDriverOptionsResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Driver driver = driverService.getDriver(authorization);

        if(driver != null){
            GetDriverOptionsResponse getDriverOptionsResponse = new GetDriverOptionsResponse();

            List<TourOrder> tourOrder = orderService.getTourOrderByDriverIdAndStatus(driver.getId(),Constants.DRIVER_TYPE,Constants.PENDING);

            if(tourOrder != null && tourOrder.size()>0){
                getDriverOptionsResponse.setNew(true);
            }else {
                getDriverOptionsResponse.setNew(false);
            }

            if(driver.getTourismAgencyId()!=null){
                getDriverOptionsResponse.setAgencyDriver(true);

            }else {
                getDriverOptionsResponse.setAgencyDriver(false);
            }
            getDriverOptionsResponse.setTopicAndroid(driver.getTopicAndroidAndWeb());
            getDriverOptionsResponse.setTopicIos(driver.getTopicIos());
            getDriverOptionsResponse.setTutorialUrl(getTutorialUrl(lang));

            response.setData(getDriverOptionsResponse);
        }else{
            response.setError(language.getSessionExpired(), LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }

    private String getTutorialUrl(String lang){

        String tutorialUrl;
        if(lang.toUpperCase().equals("HY")){
            tutorialUrl = "https://www.youtube.com/watch?v=NEqHPxnxrFc&list=RDMMNEqHPxnxrFc&start_radio=1";
        }else if(lang.toUpperCase().equals("RU")){
            tutorialUrl = "https://www.youtube.com/watch?v=vtdwnDjN9GY&list=RDMMNEqHPxnxrFc&index=4";
        }else{
            tutorialUrl = "https://www.youtube.com/watch?v=9dcVOmEQzKA&list=RDGMEMHDXYb1_DDSgDsobPsOFxpAVM9dcVOmEQzKA&index=1";
        }

        return tutorialUrl;

    }

}
