package com.primesoft.tourism.controllers.rest.client.guide.post;

import com.primesoft.tourism.beans.request.GuideLoginRequest;
import com.primesoft.tourism.beans.response.GuideLoginResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Generator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class GuideLogin extends BaseController {

    @Autowired
    private GuideService guideService;

    @RequestMapping(value = Constants.GUIDE_PATH +"login", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String guideLogin(@Valid @RequestBody GuideLoginRequest request,
                        @RequestHeader(value = "os") Integer os,
                        @RequestHeader("language") String lang) {

        Response<GuideLoginResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        if(!request.getPhone().startsWith("+")){
            request.setPhone("+"+request.getPhone());
        }

        Guide guide = guideService.getGuid(request.getPhone(), Generator.stringToSha256(request.getPassword()));


        if (guide != null){
            if(guide.getConfirmed() && !guide.getSuspended()) {
                guide.setOsType(os);
                if(guide.getTopicIos() == null || guide.getTopicAndroidAndWeb() == null){
                    guide.setTopicAndroidAndWeb(Constants.GUIDE_TYPE+"a"+guide.getId()+"a"+1);
                    guide.setTopicIos(Constants.GUIDE_TYPE+"a"+guide.getId()+"a"+2);
                }
                guideService.save(guide);
                Boolean personalGuide = false;
                if(guide.getTourismAgencyId() != null){
                    personalGuide = true;
                }
                response.setData(new GuideLoginResponse(guide.getAuthorization(), guide.getNotificationToken(), personalGuide));
            }else if(!guide.getConfirmed()){
                response.setError(language.getYourAccountIsNotVerifiedPleaseConfirmIt(),LanguageManager.getMessageKey().getNotConfirmed());
            }else{
                response.setError(language.getYourAccountIsSuspended(),LanguageManager.getMessageKey().getReject());
            }
        }else{
            response.setError(language.getWrongLoginOrPassword(), LanguageManager.getMessageKey().getReject());
        }

        return super.createGson(response);
    }
}
