package com.primesoft.tourism.controllers.rest.admin.get;

import com.primesoft.tourism.beans.response.getAllCustomerObjects.GetAllCustomerObjectsResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetCustomers  extends BaseController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private AdministratorService administratorService;


    @RequestMapping(value = Constants.ADMIN_PATH+"getCustomers",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getCustomers(@RequestHeader("authorization") String authorization,
                               @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);
        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){
            response.setData(new GetAllCustomerObjectsResponse(customerService.getCustomerObjects()));
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }



        return super.createGson(response);
    }

}
