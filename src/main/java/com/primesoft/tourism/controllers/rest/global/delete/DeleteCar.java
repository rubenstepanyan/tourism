package com.primesoft.tourism.controllers.rest.global.delete;

import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.CarService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DeleteCar extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private CarService carService;


    @RequestMapping(value = "deleteCar",method = RequestMethod.DELETE,produces = Constants.APPLICATION_JSON)
    public String deleteCar(@RequestHeader("language")String lang,
                            @RequestHeader("authorization")String authorization,
                            @RequestParam(value = "id") Integer id){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);
        Agency agency = agencyService.getAgency(authorization);
        Driver driver = driverService.getDriver(authorization);

        if(administrator != null){
            carService.delete(id);
            response.setMessage(language.getSuccessfullyDeleted(), LanguageManager.getMessageKey().getSuccess());
        }else if(agency != null){
            carService.delete(id);
            response.setMessage(language.getSuccessfullyDeleted(),LanguageManager.getMessageKey().getSuccess());
        }else  if(driver != null){
            carService.delete(id);
            response.setMessage(language.getSuccessfullyDeleted(),LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
