package com.primesoft.tourism.controllers.rest.client.customer.put;

import com.primesoft.tourism.beans.request.CustomerRegistrationRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.AlertManager;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class CustomerRegistration extends BaseController {


    @Autowired
    private CustomerService customerService;



    @RequestMapping(value = Constants.CUSTOMER_PATH+"registration",method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String registration(@Valid @RequestBody CustomerRegistrationRequest request,
                               @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer existingCustomer = customerService.getCustomerByMail(request.getMail());


        AlertManager alertManager = new AlertManager();
        Integer confirmationCode = Util.getRandomNumber();

        if(existingCustomer ==null) {
            Customer customer = request.getCustomerObject();
            customer.setConfirmationCode(confirmationCode.toString());
            customerService.save(customer);
//            if(customer.getPhone().startsWith("+374")){
//                alertManager.sendSms(customer.getPhone(), confirmationCode.toString());
//            }

            alertManager.sendConfirmationMail(customer.getMail(), confirmationCode.toString());
            response.setMessage(language.getVerificationCodeSentToMail(), LanguageManager.getMessageKey().getSuccess());

        }else if(!existingCustomer.getConfirmed()){
            Integer id = existingCustomer.getId();
            existingCustomer = request.getCustomerObject();
            existingCustomer.setConfirmationCode(confirmationCode.toString());
            existingCustomer.setId(id);
            customerService.save(existingCustomer);

            alertManager.sendConfirmationMail(existingCustomer.getMail(), confirmationCode.toString());
            response.setMessage(language.getVerificationCodeSentToMail(), LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getAlreadyRegistered(),LanguageManager.getMessageKey().getReject());
        }

        return super.createGson(response);
    }
}
