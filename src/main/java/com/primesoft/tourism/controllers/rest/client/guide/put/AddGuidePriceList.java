package com.primesoft.tourism.controllers.rest.client.guide.put;

import com.primesoft.tourism.beans.request.guidePriseLIstRequest.AddGuidePriceListRequest;
import com.primesoft.tourism.beans.request.guidePriseLIstRequest.GuidePriceLists;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.GuidePriceListService;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AddGuidePriceList extends BaseController {

    @Autowired
    private GuideService guideService;

    @Autowired
    private GuidePriceListService guidePriceListService;

    @Autowired
    private AgencyService agencyService;


    @RequestMapping(value = Constants.GUIDE_PATH+"addGuidePriceList",method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String addGuidePriceList(@Valid @RequestBody AddGuidePriceListRequest request,
                                   @RequestHeader("language") String lang,
                                   @RequestHeader("authorization") String authorization){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency agency = null;
        Guide guide = guideService.getGuidByAuthorization(authorization);
        Long groupId = System.currentTimeMillis();
        if(guide != null){

            if(guide.getTourismAgencyId()!=null){
                agency = agencyService.getAgency(guide.getTourismAgencyId());
            }

            for(GuidePriceLists priceLists : request.getGuidePriceLists()){

                guidePriceListService.save(priceLists.getGuidPriceListObject(guide,agency,groupId));
            }

            response.setMessage(language.getSuccessfullyAdded(),LanguageManager.getMessageKey().getSuccess());
        }else {
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }



        return super.createGson(response);
    }
}
