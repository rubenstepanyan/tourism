package com.primesoft.tourism.controllers.rest.admin.put;

import com.primesoft.tourism.beans.request.addEntertainmentPlace.AddEntertainmentPlaceRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlace;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceInfoService;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceService;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceWorkingDaysService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AddEntertainmentPlace extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private EntertainmentPlaceService entertainmentPlaceService;

    @Autowired
    private EntertainmentPlaceInfoService entertainmentPlaceInfoService;

    @Autowired
    private EntertainmentPlaceWorkingDaysService entertainmentPlaceWorkingDaysService;


    @RequestMapping(value = Constants.ADMIN_PATH+"addEntertainmentPlace", method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String addEntertainmentPlace(@Valid @RequestBody AddEntertainmentPlaceRequest request,
                                 @RequestHeader("authorization") String authorization,
                                 @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){
            EntertainmentPlace entertainmentPlace = request.getEntertainmentPlace();
            entertainmentPlaceService.save(entertainmentPlace);
            entertainmentPlaceInfoService.save(request.getEntertainmentPlaceInfo(entertainmentPlace.getId()));
            entertainmentPlaceWorkingDaysService.save(request.getWorkingDays(entertainmentPlace.getId()));
            response.setData(entertainmentPlace);
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }
}
