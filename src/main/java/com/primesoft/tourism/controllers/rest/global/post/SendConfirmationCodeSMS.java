package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.SendDriverConfirmationCodeSMSRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.AlertManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class SendConfirmationCodeSMS extends BaseController {

    @Autowired
    private DriverService  driverService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private GuideService guideService;

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "sendConfirmationCodeSMS", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String sendDriverConfirmationCode(@Valid @RequestBody SendDriverConfirmationCodeSMSRequest request,
                                             @RequestHeader("language") String lang,
                                             @RequestHeader (value = "authorization",required = false) String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);
        AlertManager smsManager = new AlertManager();
        Integer confirmationCode = Util.getRandomNumber();

        if(authorization != null){

            Agency agency = agencyService.getAgency(authorization);
            Driver driver = driverService.getDriver(authorization);
            Guide guide = guideService.getGuidByAuthorization(authorization);
            Customer customer = customerService.getCustomerByAuthorization(authorization);

            if(agency != null) {
                Agency existingAgency = agencyService.getAgencyByPhone(request.getPhone());
                if(existingAgency != null){
                    response.setError(language.getAlreadyRegistered(),LanguageManager.getMessageKey().getReject());
                }else{
                    agency.setConfirmationCode(confirmationCode.toString());
                    response.setMessage(language.getVerificationCodeSentToPhone(),LanguageManager.getMessageKey().getSuccess());
                    smsManager.sendSms(request.getPhone(), confirmationCode.toString());
                    agencyService.save(agency);
                }
            }else if(driver != null){
                Driver existingDriver = driverService.getDriverByPhone(request.getPhone());
                if(existingDriver != null){
                    response.setError(language.getAlreadyRegistered(),LanguageManager.getMessageKey().getReject());
                }else{
                    response.setMessage(language.getVerificationCodeSentToPhone(),LanguageManager.getMessageKey().getSuccess());
                    driver.setConfirmationCode(confirmationCode.toString());
                    smsManager.sendSms(request.getPhone(),confirmationCode.toString());
                    driverService.save(driver);
                }
            }else if(guide != null){
                Guide existingGuide = guideService.getGuidByPhone(request.getPhone());

                if(existingGuide != null){
                    response.setError(language.getAlreadyRegistered(),LanguageManager.getMessageKey().getReject());
                }else {
                    guide.setConfirmationCode(confirmationCode.toString());
                    smsManager.sendSms(request.getPhone(),confirmationCode.toString());
                    guideService.save(guide);
                    response.setMessage(language.getVerificationCodeSentToPhone(),LanguageManager.getMessageKey().getSuccess());
                }
            }else if(customer != null){
                Customer existingCustomer = customerService.getCustomerByPhone(request.getPhone());
                if(existingCustomer != null){
                    response.setError(language.getAlreadyRegistered(),LanguageManager.getMessageKey().getReject());
                }else{
                    customer.setConfirmationCode(confirmationCode.toString());
                    smsManager.sendSms(request.getPhone(),confirmationCode.toString());
                    response.setMessage(language.getVerificationCodeSentToPhone(),LanguageManager.getMessageKey().getSuccess());
                    customerService.save(customer);
                }
            }else{
                response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
            }
        }else{

            Agency existAgency = agencyService.getAgencyByPhone(request.getPhone());
            Driver existDriver = driverService.getDriverByPhone(request.getPhone());
            Guide existGuide = guideService.getGuidByPhone(request.getPhone());

            if(request.getType().equals(Constants.AGENCY_TYPE) && existAgency != null){
                existAgency.setConfirmationCode(confirmationCode.toString());
                smsManager.sendSms(request.getPhone(),confirmationCode.toString());
                agencyService.save(existAgency);
                response.setMessage(language.getVerificationCodeSentToPhone(),LanguageManager.getMessageKey().getSuccess());
            }else if(request.getType().equals(Constants.DRIVER_TYPE) && existDriver != null){
                existDriver.setConfirmationCode(confirmationCode.toString());
                smsManager.sendSms(request.getPhone(),confirmationCode.toString());
                driverService.save(existDriver);
                response.setMessage(language.getVerificationCodeSentToPhone(),LanguageManager.getMessageKey().getSuccess());
            }else if(request.getType().equals(Constants.GUIDE_TYPE) && existGuide != null){
                existGuide.setConfirmationCode(confirmationCode.toString());
                smsManager.sendSms(request.getPhone(),confirmationCode.toString());
                guideService.save(existGuide);
                response.setMessage(language.getVerificationCodeSentToPhone(),LanguageManager.getMessageKey().getSuccess());
            }else{
                response.setError(language.getNotFound(),LanguageManager.getMessageKey().getReject());
            }

        }

        return super.createGson(response);
    }
}
