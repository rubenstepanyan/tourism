package com.primesoft.tourism.controllers.rest.client.customer.get;

import com.primesoft.tourism.beans.response.getTourResponse.GetToursResponse;
import com.primesoft.tourism.beans.response.getTourResponse.TourContent;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.OfferedTourInfo;
import com.primesoft.tourism.service.interfaces.OfferedTourInfoService;
import com.primesoft.tourism.service.interfaces.TourService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.DayOfWeekCalc;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;


@RestController
public class SearchTours extends BaseController {

    @Autowired
    private TourService tourService;

    @Autowired
    private OfferedTourInfoService offeredTourInfoService;

    @RequestMapping(value = Constants.CUSTOMER_PATH+"getTours", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String searchTours(@RequestHeader("language") String lang,
                              @RequestParam(value = "startCity", required = false) Integer startCity,
                              @RequestParam(value = "offeredTourId",required = false) Integer offeredTourId,
                              @RequestParam(value = "budget", required = false) Double budget,
                              @RequestParam(value = "quantity",required = false) Integer quantity,
                              @RequestParam(value = "from", required = false) String from,
                              @RequestParam(value = "to", required = false) String to,
                              @RequestParam(value = "guided", required = false) Integer guided,
                              @RequestParam(value = "language", required = false) String tourLanguage,
                              @RequestParam(value = "tourType", required = false) Integer tourType,
                              @RequestParam(value = "page", required = false) Integer page,
                              @RequestParam(value = "transfer") Boolean transfer,
                              @RequestParam(value = "pickMeUp", required = false) Boolean pickMeUp) {

        String today = null;

        if(from ==null && to!=null){
            from = Util.getCurrentDateAddDays(0,Constants.YEREVAN_TIMEZONE);
        }

        Response<Object> response = new Response<>();
        String dayOfWeeks = null;


        String offeredTourIds= null;
        if (offeredTourId != null){
            offeredTourIds = generateOfferedTourIds(offeredTourId, lang);
        }



        if(from!=null || to!=null) {
            DayOfWeekCalc calc = new DayOfWeekCalc();
            HashMap<String, String> map = calc.getDayOfWeekSearch(from, to);
            today = map.get("today");
            dayOfWeeks = map.get("dayOfWeeks");
        }



        List<TourContent> tourContents = tourService.searchTour(offeredTourId, offeredTourIds, startCity, Util.getLanguageId(lang),budget,quantity,from,to,dayOfWeeks, today,guided,tourLanguage,tourType,transfer,pickMeUp);

        response.setData(new GetToursResponse(tourContents, page));


        return super.createGson(response);
    }


    private String generateOfferedTourIds(Integer offeredTourId, String lang){

        if(offeredTourId!=0 && offeredTourId!=null){
            String offeredTourIds = "(";
            String tourNamesRegexp = "";
            OfferedTourInfo info = offeredTourInfoService.getOfferedTourInfo(offeredTourId, Util.getLanguageId(lang));
            String[] tourNames = info.getName().split(",|\\(|\\)|\"");
            for (int i=0; i<tourNames.length; i++){

                if(tourNames[i].length()>3){
                    if(i<tourNames.length-1){
                        tourNamesRegexp += Util.replaceAllShitsSymbols(tourNames[i].trim())+"|";
                    }else {
                        tourNamesRegexp += Util.replaceAllShitsSymbols(tourNames[i].trim());
                    }
                }

            }


            List<OfferedTourInfo> ids = offeredTourInfoService.getOfferedTourIds(tourNamesRegexp);

            for (int i=0; i<ids.size(); i++){

                if(i<ids.size()-1){
                    offeredTourIds += ids.get(i).getOfferedTourId()+",";
                }else {
                    offeredTourIds += ids.get(i).getOfferedTourId()+")";
                }
            }
            if(ids.size()>=1){
                return offeredTourIds;
            }else {
                return null;
            }
        }

        return null;
    }

}
