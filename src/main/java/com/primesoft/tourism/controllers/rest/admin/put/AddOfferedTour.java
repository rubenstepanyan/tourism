package com.primesoft.tourism.controllers.rest.admin.put;

import com.primesoft.tourism.beans.request.addOfferedTour.AddOfferedTourRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.OfferedTour;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.OfferedTourInfoService;
import com.primesoft.tourism.service.interfaces.OfferedTourService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AddOfferedTour extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private OfferedTourService offeredTourService;

    @Autowired
    private OfferedTourInfoService offeredTourInfoService;

    @RequestMapping(value = Constants.ADMIN_PATH+"addOfferedTour", method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String addOfferedTour(@Valid @RequestBody AddOfferedTourRequest request,
                                 @RequestHeader("authorization") String authorization,
                                 @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){
            OfferedTour offeredTour = request.getOfferedTour();
            offeredTourService.save(offeredTour);
            offeredTourInfoService.save(request.getOfferedTourInfo(offeredTour.getId()));
            response.setMessage(language.getSuccessfullyAdded(), LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }
}
