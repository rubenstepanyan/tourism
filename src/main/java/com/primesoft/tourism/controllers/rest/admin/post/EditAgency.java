package com.primesoft.tourism.controllers.rest.admin.post;

import com.primesoft.tourism.beans.request.EditAgencyRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.TourService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EditAgency extends BaseController {

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private TourService tourService;


    @RequestMapping(value = Constants.ADMIN_PATH+"editAgency",method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String editAgency(@Valid @RequestBody EditAgencyRequest request,
                             @RequestHeader("authorization") String authorization,
                             @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator !=null) {
            Agency agency = agencyService.getAgency(request.getId());

            if(!agency.getName().equals(request.getName())){
                tourService.updateTours(Constants.AGENCY_TYPE, agency.getId(), request.getName());
            }

            if(!agency.getCurrentRating().equals(request.getCurrentRating())){
                tourService.updateTourRating(agency.getId(),request.getCurrentRating(),Constants.AGENCY_TYPE);
            }

            if(!agency.getApproved().equals(request.getApproved())){
                tourService.updateApproved(agency.getId(), Constants.AGENCY_TYPE,request.getApproved());
            }

            if(!agency.getSuspended().equals(request.getSuspended())){
                tourService.updateBlocked(agency.getId(), Constants.AGENCY_TYPE,request.getSuspended());
            }

            request.updateAgency(agency);
            agencyService.save(agency);
            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());

        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
