package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.FinishOrderRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.*;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.NotificationManager;
import com.primesoft.tourism.service.interfaces.*;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class FinishOrder extends BaseController {

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private GuideService guideService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private GuideOrderService guideOrderService;

    @Autowired
    private CustomerService customerService;


    @RequestMapping(value = "finishOrder", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String finishOrder(@Valid @RequestBody FinishOrderRequest request,
                              @RequestHeader("language") String lang,
                              @RequestHeader("authorization") String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency agency = agencyService.getAgency(authorization);
        Driver driver = driverService.getDriver(authorization);
        Guide guide = guideService.getGuidByAuthorization(authorization);

        NotificationManager notificationManager = new NotificationManager();

        if (agency != null) {
            if (request.getType() == Constants.TOUR_ORDER_TYPE) {
                TourOrder tourOrder = orderService.getTourOrderByQr(request.getQr(), true, Constants.CUSTOMER_PAYED, agency.getId(), Constants.AGENCY_TYPE);

                if (tourOrder != null) {
                    tourOrder.setStatus(Constants.FINISHED);
                    tourOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());
                    orderService.save(tourOrder);
                    response.setData(orderService.getTourOrderInfoByQr(request.getQr(), Util.getLanguageId(lang)));

                    Customer customer = customerService.getCustomer(tourOrder.getCustomerId());
                    if (!tourOrder.getTransfer()) {
                        finishOrder(notificationManager, Constants.NOTIFICATION_QR_SCAN_TOUR_ORDER, customer.getDashboardLanguage(), customer.getTopicAndroidAndWeb(), customer.getTopicIos(), Constants.FINISHED, false, tourOrder.getId());
                    } else {
                        finishOrder(notificationManager, Constants.NOTIFICATION_QR_SCAN_TOUR_ORDER, customer.getDashboardLanguage(), customer.getTopicAndroidAndWeb(), customer.getTopicIos(), Constants.FINISHED, true, tourOrder.getId());
                    }
                    //response.setMessage(language.getSuccessfullyFinished(),LanguageManager.getMessageKey().getSuccess());
                } else {
                    response.setError(language.getNotFound(), LanguageManager.getMessageKey().getReject());
                }
            } else if (request.getType() == Constants.GUIDE_ORDER_TYPE) {
                GuideOrder guideOrder = guideOrderService.getGuideOrderByQr(request.getQr(), true, Constants.CUSTOMER_PAYED, agency.getId());
                if (guideOrder != null) {
                    guideOrder.setStatus(Constants.FINISHED);
                    guideOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());

                    //guide info
                    //response.setMessage(language.getSuccessfullyFinished(),LanguageManager.getMessageKey().getSuccess());
                    response.setData(guideOrderService.getGuideOrderByQrInfo(request.getQr()));
                    guideOrderService.save(guideOrder);

                    Customer customer = customerService.getCustomer(guideOrder.getCustomerId());

                    finishOrder(notificationManager, Constants.NOTIFICATION_QR_SCAN_GUIDE_ORDER, customer.getDashboardLanguage(), customer.getTopicAndroidAndWeb(), customer.getTopicIos(), Constants.FINISHED, false, guideOrder.getId());

                } else {
                    response.setError(language.getNotFound(), LanguageManager.getMessageKey().getReject());
                }
            }
        } else if (driver != null) {
            TourOrder tourOrder;
            if (driver.getTourismAgencyId() != null) {
                tourOrder = orderService.getTourOrderByQr(request.getQr(), true, Constants.CUSTOMER_PAYED, driver.getTourismAgencyId(), Constants.AGENCY_TYPE);
            } else {
                tourOrder = orderService.getTourOrderByQr(request.getQr(), true, Constants.CUSTOMER_PAYED, driver.getId(), Constants.DRIVER_TYPE);
            }

            if (tourOrder != null) {
                tourOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());
                tourOrder.setStatus(Constants.FINISHED);
                orderService.save(tourOrder);
                response.setData(orderService.getTourOrderInfoByQr(request.getQr(), Util.getLanguageId(lang)));

                Customer customer = customerService.getCustomer(tourOrder.getCustomerId());
                if (!tourOrder.getTransfer()) {
                    finishOrder(notificationManager, Constants.NOTIFICATION_QR_SCAN_TOUR_ORDER, customer.getDashboardLanguage(), customer.getTopicAndroidAndWeb(), customer.getTopicIos(), Constants.FINISHED, false, tourOrder.getId());
                } else {
                    finishOrder(notificationManager, Constants.NOTIFICATION_QR_SCAN_TOUR_ORDER, customer.getDashboardLanguage(), customer.getTopicAndroidAndWeb(), customer.getTopicIos(), Constants.FINISHED, true, tourOrder.getId());
                }

            } else {
                response.setError(language.getNotFound(), LanguageManager.getMessageKey().getReject());
            }
        } else if (guide != null) {
            GuideOrder guideOrder;
            if (guide.getTourismAgencyId() != null) {
                guideOrder = guideOrderService.getGuideOrderByQr(request.getQr(), true, Constants.CUSTOMER_PAYED, guide.getTourismAgencyId());
            } else {
                guideOrder = guideOrderService.getGuideOrderByQrAndGuideId(request.getQr(), true, Constants.CUSTOMER_PAYED, guide.getId());
            }

            if (guideOrder != null) {
                guideOrder.setStatus(Constants.FINISHED);

                //guide info
                //response.setMessage(language.getSuccessfullyFinished(),LanguageManager.getMessageKey().getSuccess());
                guideOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());
                guideOrderService.save(guideOrder);
                response.setData(guideOrderService.getGuideOrderByQrInfo(request.getQr()));

                Customer customer = customerService.getCustomer(guideOrder.getCustomerId());

                finishOrder(notificationManager, Constants.NOTIFICATION_QR_SCAN_GUIDE_ORDER, customer.getDashboardLanguage(), customer.getTopicAndroidAndWeb(), customer.getTopicIos(), Constants.FINISHED, false, guideOrder.getId());
            } else {
                response.setError(language.getNotFound(), LanguageManager.getMessageKey().getReject());
            }
        }


        return super.createGson(response);
    }

    private void finishOrder(NotificationManager notificationManager,String action, Integer lang, String topicAndroid, String topicIos, Integer status, Boolean isTransfer, Integer orderId) {

        notificationManager.sendNotif(action, LanguageManager.getLanguageInstance(lang).getYourOrderAccepted(), topicAndroid, topicIos, status, isTransfer,orderId);

    }

}
