package com.primesoft.tourism.controllers.rest.client.customer.get;

import com.primesoft.tourism.beans.response.GetCouponsResponse;
import com.primesoft.tourism.beans.response.PurchasedCoupons;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.ArcaManager;
import com.primesoft.tourism.service.interfaces.CouponService;
import com.primesoft.tourism.service.interfaces.CustomerService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;


@RestController
public class GetCoupons extends BaseController {

    @Autowired
    private ArcaManager arcaManager;


    @Autowired
    private CustomerService customerService;

    @Autowired
    private CouponService couponService;

    @RequestMapping(value = Constants.CUSTOMER_PATH+"getCoupons", method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getCoupons(@RequestHeader("language") String lang,
                             @RequestHeader("authorization")String authorization) {

        Response<GetCouponsResponse> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Customer customer = customerService.getCustomerByAuthorization(authorization);

        if(customer != null){

            arcaManager.checkOrder(customer);

            ArrayList<PurchasedCoupons> coupons = couponService.getCouponsByCustomerId(customer.getId(), Util.getLanguageId(lang));
            response.setData(new GetCouponsResponse(coupons));
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }



        return super.createGson(response);
    }
}
