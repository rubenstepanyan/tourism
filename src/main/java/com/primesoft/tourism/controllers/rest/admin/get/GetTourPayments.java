package com.primesoft.tourism.controllers.rest.admin.get;

import com.primesoft.tourism.beans.response.getTourPaymentsResponse.GetTourPaymentsContent;
import com.primesoft.tourism.beans.response.getTourPaymentsResponse.GetTourPaymentsResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.OrderService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetTourPayments extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = Constants.ADMIN_PATH+"getTourPayments",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getTourPayments(@RequestHeader("authorization") String authorization,
                                  @RequestHeader("language") String lang,
                                  @RequestParam(value = "agencyId") Integer agencyId,
                                  @RequestParam(value = "from") String from,
                                  @RequestParam(value = "to") String to){

        Response<Object> response = new Response<>();

        Language language = LanguageManager.getLanguageInstance(lang);
        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){
            List<GetTourPaymentsContent> paymentsContents;
            if(agencyId != 0){
                paymentsContents  = orderService.getAllPayments(Constants.AGENCY_TYPE, agencyId, Util.getLanguageId(lang),from,to);
            }else {
                paymentsContents = orderService.getAllPayments(Constants.AGENCY_TYPE, 0,Util.getLanguageId(lang),from, to);
            }

            response.setData(new GetTourPaymentsResponse(paymentsContents));
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }

        return super.createGson(response);
    }
}
