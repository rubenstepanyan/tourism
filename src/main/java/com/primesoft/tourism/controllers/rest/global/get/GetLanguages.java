package com.primesoft.tourism.controllers.rest.global.get;

import com.primesoft.tourism.beans.response.getLanguagesResponse.GetLanguagesResponse;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.service.interfaces.LanguagesService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetLanguages extends BaseController {


    @Autowired
    private LanguagesService languagesService;

    @RequestMapping(value = "getLanguages",method = RequestMethod.GET, produces = Constants.APPLICATION_JSON)
    public String getLanguages(@RequestHeader("language") String lang){

        Response<Object> response = new Response<>();

        response.setData(new GetLanguagesResponse(languagesService.getAll()));

        return super.createGson(response);
    }
}
