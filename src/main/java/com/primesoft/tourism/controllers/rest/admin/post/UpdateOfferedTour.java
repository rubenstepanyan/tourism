package com.primesoft.tourism.controllers.rest.admin.post;

import com.primesoft.tourism.beans.request.UpdateOfferedTourRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.beans.tableBeans.OfferedTour;
import com.primesoft.tourism.beans.tableBeans.OfferedTourInfo;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import com.primesoft.tourism.service.interfaces.OfferedTourInfoService;
import com.primesoft.tourism.service.interfaces.OfferedTourService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UpdateOfferedTour extends BaseController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private OfferedTourService offeredTourService;

    @Autowired
    private OfferedTourInfoService offeredTourInfoService;


    @RequestMapping(value = Constants.ADMIN_PATH+"updateOfferedTour", method = RequestMethod.POST,produces = Constants.APPLICATION_JSON)
    public String updateOfferedTour(@Valid @RequestBody UpdateOfferedTourRequest request,
                                    @RequestHeader("authorization") String authorization,
                                    @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Administrator administrator = administratorService.getAdministratorByAuthorization(authorization);

        if(administrator != null){
            OfferedTour offeredTour = offeredTourService.getOfferedTour(request.getId());
            List<OfferedTourInfo> infoList = offeredTourInfoService.getInfo(request.getId());
            request.updateOfferedTour(offeredTour, infoList);

            offeredTourService.save(offeredTour);
            offeredTourInfoService.save(infoList);

            response.setMessage(language.getSuccessfullyEdited(), LanguageManager.getMessageKey().getSuccess());
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }


        return super.createGson(response);
    }

}
