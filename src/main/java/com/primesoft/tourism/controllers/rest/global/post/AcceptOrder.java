package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.AcceptOrderRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.*;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.manager.NotificationManager;
import com.primesoft.tourism.service.interfaces.*;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AcceptOrder extends BaseController {

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private GuideService guideService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private GuideOrderService guideOrderService;

    @Autowired
    private CustomNotificationService customNotificationService;

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "acceptOrder", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String acceptOrder(@Valid @RequestBody AcceptOrderRequest request,
                              @RequestHeader("language") String lang,
                              @RequestHeader("authorization") String authorization) {

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Agency agency = agencyService.getAgency(authorization);
        Driver driver = driverService.getDriver(authorization);
        Guide guide = guideService.getGuidByAuthorization(authorization);

        TourOrder tourOrder = null;
        GuideOrder guideOrder = null;

        NotificationManager notificationManager = new NotificationManager();


        if(request.getType().equals(Constants.TOUR_ORDER)){
            tourOrder = orderService.getTourOrder(request.getOrderId());
        }else if(request.getType().equals(Constants.GUIDE_ORDER)) {
            guideOrder = guideOrderService.getGuideOrder(request.getOrderId(), request.getQuantityType(), request.getLanguage());
        }

        if((request.getType().equals(Constants.TOUR_ORDER) && (tourOrder==null || tourOrder.getStatus().equals(Constants.CUSTOMER_CANCELED) || tourOrder.getStatus().equals(Constants.CANCELED))) ||
                request.getType().equals(Constants.GUIDE_ORDER) && (guideOrder==null || guideOrder.getStatus().equals(Constants.CUSTOMER_CANCELED) || guideOrder.getStatus().equals(Constants.CANCELED))){
            response.setError(language.getOrderOutOffTime(), LanguageManager.getMessageKey().getReject());
            return super.createGson(response);
        }

        if(agency !=null) {

            if(request.getType().equals(Constants.TOUR_ORDER)){
                acceptTourOrder(tourOrder,notificationManager);
                response.setMessage(language.getYouWillReceiveAnAdditionalNoticeAboutTheEndOfTheClientsPaymentIfClientDoesntPayWithin15MinutesThOrderWillBeCancelAutomaticallyThankYou(),LanguageManager.getMessageKey().getSuccess());
            }else if(request.getType().equals(Constants.GUIDE_ORDER)){
                acceptGuideOrder(guideOrder, notificationManager);
                response.setMessage(language.getYourOrderAccepted(),LanguageManager.getMessageKey().getSuccess());
            }
        }else  if(driver != null && tourOrder!=null){
            if(request.getType().equals(Constants.TOUR_ORDER)){
                acceptTourOrder(tourOrder,notificationManager);
                response.setMessage(language.getYouWillReceiveAnAdditionalNoticeAboutTheEndOfTheClientsPaymentIfClientDoesntPayWithin15MinutesThOrderWillBeCancelAutomaticallyThankYou(),LanguageManager.getMessageKey().getSuccess());
            }
        }else if(guide != null){
            if(request.getType().equals(Constants.GUIDE_ORDER)){
                acceptGuideOrder(guideOrder, notificationManager);
                response.setMessage(language.getYouWillReceiveAnAdditionalNoticeAboutTheEndOfTheClientsPaymentIfClientDoesntPayWithin15MinutesThOrderWillBeCancelAutomaticallyThankYou(),LanguageManager.getMessageKey().getSuccess());
            }
        }else{
            response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
        }



        return super.createGson(response);
    }

    private void addCustomNotification(Integer customerId){
        CustomNotification notification = new CustomNotification();

        notification.setCustomerId(customerId);
        notification.setAction(Constants.CONFIRM);
        notification.setMessageAM(LanguageManager.getLanguageInstance(Constants.AM_LANGUAGE).getYourOrderAccepted());
        notification.setMessageRU(LanguageManager.getLanguageInstance(Constants.AM_LANGUAGE).getYourOrderAccepted());
        notification.setMessageEN(LanguageManager.getLanguageInstance(Constants.AM_LANGUAGE).getYourOrderAccepted());

        customNotificationService.save(notification);

   }

    private void acceptTourOrder(TourOrder tourOrder,NotificationManager notificationManager){
        tourOrder.setStatus(Constants.CONFIRM);
        tourOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());
        orderService.save(tourOrder);
        addCustomNotification(tourOrder.getCustomerId());

        Customer customer = customerService.getCustomer(tourOrder.getCustomerId());

        Boolean isTransfer;

        if(tourOrder.getTransfer()){
            isTransfer = true;
        }else{
            isTransfer = false;
        }
        notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(customer.getDashboardLanguage()).getYourOrderAccepted(),customer.getTopicAndroidAndWeb(), customer.getTopicIos(),tourOrder.getStatus(),isTransfer, tourOrder.getId());

    }

    private void acceptGuideOrder(GuideOrder guideOrder, NotificationManager notificationManager){
        guideOrder.setStatus(Constants.CONFIRM);
        guideOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());
        guideOrderService.save(guideOrder);
        addCustomNotification(guideOrder.getCustomerId());

        Customer customer = customerService.getCustomer(guideOrder.getCustomerId());
        notificationManager.sendNotif(Constants.GUIDE_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(customer.getDashboardLanguage()).getYourOrderAccepted(),customer.getTopicAndroidAndWeb(), customer.getTopicIos(),guideOrder.getStatus(),null, guideOrder.getId());
    }

}
