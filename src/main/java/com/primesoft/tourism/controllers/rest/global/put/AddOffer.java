package com.primesoft.tourism.controllers.rest.global.put;

import com.primesoft.tourism.beans.request.AddOffersRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Offers;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.OfferService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AddOffer extends BaseController {

    @Autowired
    private OfferService offerService;

    @RequestMapping(value = "addOffer",method = RequestMethod.PUT,produces = Constants.APPLICATION_JSON)
    public String addOffers(@Valid @RequestBody AddOffersRequest request,
                            @RequestHeader("language") String lang){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Offers offers = request.getOffersObject();
        offerService.save(offers);
        response.setMessage(language.getSuccessfullyAdded(),LanguageManager.getMessageKey().getSuccess());

        return super.createGson(response);
    }
}
