package com.primesoft.tourism.controllers.rest.global.post;

import com.primesoft.tourism.beans.request.ChangeCompanyRequest;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.language.Language;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.AgencyService;
import com.primesoft.tourism.service.interfaces.DriverService;
import com.primesoft.tourism.service.interfaces.GuideService;
import com.primesoft.tourism.utils.BaseController;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ChangeCompany extends BaseController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private GuideService guideService;


    @RequestMapping(value = "changeCompany", method = RequestMethod.POST, produces = Constants.APPLICATION_JSON)
    public String changeCompany(@Valid @RequestBody ChangeCompanyRequest request,
                                 @RequestHeader("language") String lang,
                                 @RequestHeader("authorization") String authorization){

        Response<Object> response = new Response<>();
        Language language = LanguageManager.getLanguageInstance(lang);

        Driver driver = driverService.getDriver(authorization);
        Guide guide = guideService.getGuidByAuthorization(authorization);
        Agency agency = agencyService.getAgencyByAgencyCode(request.getAgencyCode());

        if(agency !=null){
            if(driver != null){
                driver.setTourismAgencyId(agency.getId());
                driverService.save(driver);
                response.setMessage(language.getYourCompanyChanged(),LanguageManager.getMessageKey().getSuccess());
            }else if(guide != null){
                guide.setTourismAgencyId(agency.getId());
                guideService.save(guide);
                response.setMessage(language.getYourCompanyChanged(),LanguageManager.getMessageKey().getSuccess());
            }else{
                response.setError(language.getSessionExpired(),LanguageManager.getMessageKey().getSessionExpired());
            }
        }else{
            response.setError(language.getWrongAgencyCode(),LanguageManager.getMessageKey().getReject());
        }

        return super.createGson(response);
    }
}
