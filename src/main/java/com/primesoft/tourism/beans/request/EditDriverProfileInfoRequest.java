package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

public class EditDriverProfileInfoRequest {


    private String firstName;
    private String lastName;
    private String languageSkills;
    private String mail;
    private String photoUrl;
    private String accountNumber;
    private String city;


    public Driver updateDriver(Driver driver) {

        if (this.photoUrl != null && !this.photoUrl.startsWith("https://")) {
            ImageManager imageManager = new ImageManager();
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            driver.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.DRIVER_IMAGE_PREFIX, Constants.DRIVER_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        driver.setFirstName(this.firstName);
        driver.setLastName(this.lastName);


        driver.setLanguageSkills(this.languageSkills);
        driver.setMail(this.mail);
        driver.setAccountNumber(this.accountNumber);
        driver.setCity(this.city);

        return driver;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(String languageSkills) {
        this.languageSkills = languageSkills;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }


    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
