package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Generator;
import com.primesoft.tourism.utils.Util;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Size;
import java.util.UUID;

public class CustomerRegistrationRequest {

    @Size(min = 2, max = 100)
    private String lastName;

    @Size(min = 2, max = 100)
    private String firstName;

    @Size(min = 6, max = 100)
    private String password;

    @Email
    private String mail;

    @Size(min = 4, max = 40)
    private String phone;

    private String birthDay;

    private String nationality;

    public Customer getCustomerObject(){

        Customer customer = new Customer();

        customer.setConfirmed(false);
        customer.setLastName(this.lastName);
        customer.setFirstName(this.firstName);
        customer.setPassword(Generator.stringToSha256(this.password));
        customer.setAuthorization(UUID.randomUUID().toString());
        customer.setMail(this.mail);
        if(!this.phone.startsWith("+")){
            this.phone = "+"+this.phone;
        }

        customer.setPhone(this.phone);
        customer.setRegistrationDate(Util.getCurrentDateAddDays(0));
        customer.setBirthDay(this.birthDay);
        customer.setPhotoUrl("https://api.visitmyarmenia.com/customer/customer.png");
        customer.setNationality(this.nationality);
        customer.setDashboardLanguage(Constants.EN_LANGUAGE);

        return customer;

    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
}
