package com.primesoft.tourism.beans.request.addVideo;

public class AddVideoContent {

    private String title;

    private Integer language;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }
}
