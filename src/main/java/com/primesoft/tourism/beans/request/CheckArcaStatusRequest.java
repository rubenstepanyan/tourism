package com.primesoft.tourism.beans.request;

public class CheckArcaStatusRequest {

    private String orderId;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
