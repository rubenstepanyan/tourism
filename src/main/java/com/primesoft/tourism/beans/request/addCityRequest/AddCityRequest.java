package com.primesoft.tourism.beans.request.addCityRequest;


import java.util.List;

public class AddCityRequest {

    private List<Cities> cities;

    public List<Cities> getCities() {
        return cities;
    }

    public void setCities(List<Cities> cities) {
        this.cities = cities;
    }
}
