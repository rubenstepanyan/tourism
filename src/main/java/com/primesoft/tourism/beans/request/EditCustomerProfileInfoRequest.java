package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

public class EditCustomerProfileInfoRequest {

    private String firstName;
    private String lastName;
    private String birthDay;
    private String photoUrl;


    public Customer updateCustomer(Customer customer){

        if (this.photoUrl != null && !this.photoUrl.startsWith("https://")) {
            ImageManager imageManager = new ImageManager();
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            customer.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.CUSTOMER_IMAGE_PREFIX, Constants.CUSTOMER_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        customer.setFirstName(this.firstName);
        customer.setLastName(this.lastName);
        customer.setBirthDay(this.birthDay);


        return customer;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
