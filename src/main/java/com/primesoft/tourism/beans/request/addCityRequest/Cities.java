package com.primesoft.tourism.beans.request.addCityRequest;

import com.primesoft.tourism.beans.tableBeans.City;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class Cities {

    @Size(min = 3)
    private String name;

    @Max(3)
    @Min(1)
    private Integer language;

    private Integer priority;

    private Boolean notShowInTours;

    public City getCity(Integer parentId){
        City city = new City();
        city.setParentId(parentId);
        city.setName(this.name);
        city.setLanguage(language);
        city.setPriority(priority);
        city.setNotShowInTours(notShowInTours);
        return city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Boolean getNotShowInTours() {
        return notShowInTours;
    }

    public void setNotShowInTours(Boolean notShowInTours) {
        this.notShowInTours = notShowInTours;
    }
}
