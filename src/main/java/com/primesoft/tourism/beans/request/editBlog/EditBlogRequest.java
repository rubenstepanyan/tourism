package com.primesoft.tourism.beans.request.editBlog;


import com.primesoft.tourism.beans.tableBeans.Blog;
import com.primesoft.tourism.beans.tableBeans.BlogContent;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

import java.util.List;

public class EditBlogRequest {

    private Integer id;

    private String iconUrl;

    private List<EditBlogContent> contents;


    public Blog updateBlog(Blog blog){

        if (this.iconUrl != null && !this.iconUrl.startsWith("https://")) {
            ImageManager imageManager = new ImageManager();
            if (this.iconUrl.startsWith("data:")) {
                this.iconUrl = this.iconUrl.split(",")[1];
            }
            blog.setIconUrl(imageManager.decodeAndSaveBase64(this.iconUrl, Constants.BLOG_IMAGE_PREFIX ,Constants.BLOG_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        return blog;
    }

    public List<BlogContent> updateBlogContent(List<BlogContent> blogContents){


        for (int i = 0; i < blogContents.size(); i++) {

            for (int j = 0; j < contents.size(); j++) {
                if(blogContents.get(i).getId().equals(contents.get(j).getId())){

                    blogContents.get(i).setTitle(contents.get(j).getTitle());
                }
            }

        }

        return blogContents;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public List<EditBlogContent> getContents() {
        return contents;
    }

    public void setContents(List<EditBlogContent> contents) {
        this.contents = contents;
    }
}
