package com.primesoft.tourism.beans.request;

import javax.validation.constraints.Size;

public class GuideLoginRequest {

    @Size(min = 4)
    private String phone;

    @Size(min = 4)
    private String password;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
