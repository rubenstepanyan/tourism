package com.primesoft.tourism.beans.request.OrderRequest;

public class OrderRequest {

    private  Integer id;

    private String date;

    private Integer quantity;

    private String touristAddress;

    private Double lat;

    private Double lng;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getTouristAddress() {
        return touristAddress;
    }

    public void setTouristAddress(String touristAddress) {
        this.touristAddress = touristAddress;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
