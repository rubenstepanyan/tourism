package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.beans.tableBeans.GuideReservation;

public class AddReservationRequest {

    private String reservationStart;
    private String reservationEnd;

    public GuideReservation getGuideReservation(Guide guide){

        GuideReservation guideReservation= new GuideReservation();

        guideReservation.setGuideId(guide.getId());
        guideReservation.setReservationStart(this.reservationStart);
        guideReservation.setReservationEnd(this.reservationEnd);

        return guideReservation;
    }

    public String getReservationStart() {
        return reservationStart;
    }

    public void setReservationStart(String reservationStart) {
        this.reservationStart = reservationStart;
    }

    public String getReservationEnd() {
        return reservationEnd;
    }

    public void setReservationEnd(String reservationEnd) {
        this.reservationEnd = reservationEnd;
    }
}
