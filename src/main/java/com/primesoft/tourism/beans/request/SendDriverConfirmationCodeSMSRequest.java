package com.primesoft.tourism.beans.request;


import javax.validation.constraints.NotNull;

public class SendDriverConfirmationCodeSMSRequest {

    @NotNull
    private String phone;

    private Integer type;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
