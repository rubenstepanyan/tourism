package com.primesoft.tourism.beans.request;

import com.google.gson.Gson;
import com.primesoft.tourism.beans.tableBeans.Car;
import com.primesoft.tourism.beans.tableBeans.Tour;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

public class EditTourRequest {

    private Integer id;

    private Integer price;

    private String startTime;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date startDate;

    private List<Integer> daysOfWeek;

    private Integer hour;

    private Integer minute;

    private String startAddress;

    private Double lat;

    private Double lng;

    private Integer startCity;

    private  Integer carId;

    private Boolean guided;

    private Integer tourType;

    private String languageSkills;

    @Size(min = 1,max = 1000)
    private  String description;

    @Size(min = 3,max = 40)
    private String localName;

    private  Boolean pickMeUp;

    public Tour updateTourObject(Tour tour, Car car){

        if(car!=null) {
            tour.setCarId(car.getId());
            tour.setCarInfo(car.getCarMake()+" "+car.getCarModel()+" "+car.getBodyType()+" "+car.getCarYear());
            tour.setFreeSeats(car.getQuantityPassenger());
        }

        tour.setPrice(this.price);
        tour.setStartTime("2000-01-01 "+this.startTime+":00");
        tour.setStartDate(this.startDate);
        if(daysOfWeek != null){
            tour.setDaysOfWeek(new Gson().toJson(this.daysOfWeek));
            tour.setStartDate(null);
        }else if(startDate != null){
            tour.setDaysOfWeek(null);
            tour.setStartDate(this.startDate);
        }
        tour.setHour(this.hour);
        tour.setMinute(this.minute);
        tour.setStartAddress(this.startAddress);
        tour.setLat(this.lat);
        tour.setLng(this.lng);
        tour.setStartCity(this.startCity);
        tour.setLanguageSkills(languageSkills);
        tour.setGuided(this.guided);
        tour.setTourType(this.tourType);
        tour.setDescription(this.description);
        tour.setLocalName(this.localName);
        tour.setPickMeUp(this.pickMeUp);

        return tour;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public List<Integer> getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(List<Integer> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Integer getStartCity() {
        return startCity;
    }

    public void setStartCity(Integer startCity) {
        this.startCity = startCity;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Boolean getGuided() {
        return guided;
    }

    public void setGuided(Boolean guided) {
        this.guided = guided;
    }

    public Integer getTourType() {
        return tourType;
    }

    public void setTourType(Integer tourType) {
        this.tourType = tourType;
    }

    public String getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(String languageSkills) {
        this.languageSkills = languageSkills;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Boolean getPickMeUp() {
        return pickMeUp;
    }

    public void setPickMeUp(Boolean pickMeUp) {
        this.pickMeUp = pickMeUp;
    }
}
