package com.primesoft.tourism.beans.request;

public class RegisterDeviceRequest {

    private Integer language;


    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }
}
