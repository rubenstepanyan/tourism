package com.primesoft.tourism.beans.request.editEntertainmentPlace;

import com.primesoft.tourism.beans.tableBeans.EntertainmentPlace;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceInfo;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceWorkingDays;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.List;

public class EditEntertainmentPlaceRequest {


    private Integer id;

    @Max(10)
    @Min(1)
    private Integer type;

    @Max(86)
    @Min(-86)
    private Double lat;

    @Max(181)
    @Min(-181)
    private Double lng;

    @Size(min = 4)
    private String iconUrl;

    @Min(1)
    private Integer city;

    private String phone;

    private String website;

    private String mail;

    private Double discount;

    private Double price;

    private Integer maxPersons;

    private String entPrice;

    private List<Content> contents;

    private List<WorkingDays> workingDays;

    private List<Integer> deletedPhotos;


    public void updateEntertainmentPlace(EntertainmentPlace entertainmentPlace){

        entertainmentPlace.setType(this.type);
        entertainmentPlace.setLat(this.lat);
        entertainmentPlace.setLng(this.lng);


        ImageManager imageManager = new ImageManager();
        if (this.iconUrl != null && !this.iconUrl.startsWith("https://")) {
            if (this.iconUrl.startsWith("data:")) {
                this.iconUrl = this.iconUrl.split(",")[1];
            }
            entertainmentPlace.setIconUrl(imageManager.decodeAndSaveBase64(this.iconUrl, Constants.ENTERTAINMENT_PLACE_IMAGE_PREFIX, Constants.ENTERTAINMENT_PLACE_IMAGE_PATH, Util.getRandomString((short) 20)));
        }
        entertainmentPlace.setCity(this.city);
        entertainmentPlace.setPhone(this.phone);
        entertainmentPlace.setWebsite(this.website);
        entertainmentPlace.setMail(this.mail);
        entertainmentPlace.setDiscount(this.discount);
        entertainmentPlace.setPrice(this.price);
        entertainmentPlace.setMaxPersons(this.maxPersons);
        entertainmentPlace.setEntPrice(this.entPrice);
    }


    public void updateEntertainmentPlaceInfo(List<EntertainmentPlaceInfo> ifs){

        for (Content content : contents) {
            for (EntertainmentPlaceInfo info: ifs) {
                if(content.getId().equals(info.getId())) {
                    info.setName(content.getName());
                    info.setAddress(content.getAddress());
                    info.setDescription(content.getDescription());
                }
            }
        }
    }


    public void updateWorkingDays(List<EntertainmentPlaceWorkingDays> workingDaysInfo){

        for (WorkingDays workingDay : workingDays) {
            for (EntertainmentPlaceWorkingDays info: workingDaysInfo) {
                if(workingDay.getId().equals(info.getId())) {
                    info.setName(workingDay.getName());
                    info.setValue(workingDay.getValue());
                }
            }
        }
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }

    public List<WorkingDays> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(List<WorkingDays> workingDays) {
        this.workingDays = workingDays;
    }

    public List<Integer> getDeletedPhotos() {
        return deletedPhotos;
    }

    public void setDeletedPhotos(List<Integer> deletedPhotos) {
        this.deletedPhotos = deletedPhotos;
    }

    public Integer getMaxPersons() {
        return maxPersons;
    }

    public void setMaxPersons(Integer maxPersons) {
        this.maxPersons = maxPersons;
    }

    public String getEntPrice() {
        return entPrice;
    }

    public void setEntPrice(String entPrice) {
        this.entPrice = entPrice;
    }
}
