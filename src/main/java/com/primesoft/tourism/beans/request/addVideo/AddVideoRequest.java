package com.primesoft.tourism.beans.request.addVideo;

import com.primesoft.tourism.beans.tableBeans.Video;
import com.primesoft.tourism.beans.tableBeans.VideoContent;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class AddVideoRequest {

    private String url;

    private String iconUrl;

    private List<AddVideoContent> contents;

    public Video getVideo(){

        ImageManager imageManager = new ImageManager();
        Video video = new Video();
        if (this.iconUrl != null) {
            if (this.iconUrl.startsWith("data:")) {
                this.iconUrl = this.iconUrl.split(",")[1];
            }
            video.setIconUrl(imageManager.decodeAndSaveBase64(this.iconUrl, Constants.VIDEO_IMAGE_PREFIX, Constants.VIDEO_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        video.setUrl(this.url);

        return video;
    }

    public List<VideoContent> getVideoContent(Integer videoId){

        List<VideoContent> videoContents = new ArrayList<>();

        for (int i = 0; i <contents.size() ; i++) {

            VideoContent videoContent = new VideoContent();

            videoContent.setVideoId(videoId);
            videoContent.setLanguage(contents.get(i).getLanguage());
            videoContent.setTitle(contents.get(i).getTitle());

            videoContents.add(videoContent);
        }

        return videoContents;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public List<AddVideoContent> getContents() {
        return contents;
    }

    public void setContents(List<AddVideoContent> contents) {
        this.contents = contents;
    }
}
