package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Customer;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EditCustomerRequest {

    @Min(1)
    private Integer id;

    @Size(min = 2)
    private String firstName;

    @Size(min = 2)
    private String lastName;

    @Size(min = 1)
    private String birthDay;

    @Size(min = 2)
    private String phone;

    @Email
    private String mail;

    @NotNull
    private Boolean confirmed;


    public Customer updateCustomer(Customer customer){

        customer.setFirstName(this.firstName);
        customer.setLastName(this.lastName);
        customer.setBirthDay(this.birthDay);
        customer.setMail(this.mail);
        customer.setPhone(this.phone);
        customer.setConfirmed(this.confirmed);

        return customer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }
}
