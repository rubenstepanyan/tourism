package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Driver;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class EditDriverRequest {

    @Min(1)
    private Integer id;

    private Boolean confirmed;

    @Size(min = 1)
    private String firstName;

    @Size(min = 1)
    private String lastName;

    @Size(min = 1)
    private String languageSkills;

    @Min(0)
    @Max(5)
    private Double currentRating;

    @Email
    private String mail;

    @Size(min = 1)
    private String phone;

    private Integer balance;

    @Min(0)
    @Max(100)
    private Integer incomingPercent;

    private Boolean suspended;

    private Boolean approved;

    private String accountNumber;

    public Driver updateDriver(Driver driver){

        driver.setConfirmed(this.confirmed);
        driver.setFirstName(this.firstName);
        driver.setLastName(this.lastName);
        driver.setLanguageSkills(this.languageSkills);
        driver.setMail(this.mail);
        driver.setPhone(this.phone);
        driver.setBalance(this.balance);
        driver.setIncomingPercent(this.incomingPercent);
        driver.setSuspended(this.suspended);
        driver.setApproved(this.approved);
        driver.setAccountNumber(this.accountNumber);

        return driver;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(String languageSkills) {
        this.languageSkills = languageSkills;
    }

    public Double getCurrentRating() {
        return currentRating;
    }

    public void setCurrentRating(Double currentRating) {
        this.currentRating = currentRating;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getIncomingPercent() {
        return incomingPercent;
    }

    public void setIncomingPercent(Integer incomingPercent) {
        this.incomingPercent = incomingPercent;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
