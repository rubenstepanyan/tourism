package com.primesoft.tourism.beans.request.editGuidePriceList;

import java.util.List;

public class EditGuidePriceListRequest {

    private List<GuidePriceLists> priceLists;

    public List<GuidePriceLists> getPriceLists() {
        return priceLists;
    }

    public void setPriceLists(List<GuidePriceLists> priceLists) {
        this.priceLists = priceLists;
    }
}
