package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Car;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

public class UpdateCarPhotoRequest {
    private  Integer id;
    private  String photoUrl;

    public Car updateCarPhoto(Car car){
        if (this.photoUrl != null && !this.photoUrl.startsWith("https://")) {
            ImageManager imageManager = new ImageManager();
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            car.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.CAR_IMAGE_PREFIX, Constants.CAR_IMAGE_PATH, Util.getRandomString((short) 20)));
        }
        return car;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
