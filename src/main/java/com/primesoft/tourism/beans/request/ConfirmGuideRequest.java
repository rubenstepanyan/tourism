package com.primesoft.tourism.beans.request;

import javax.validation.constraints.Size;

public class ConfirmGuideRequest {

    @Size(min = 4)
    private  String phone;

    private String confirmationCode;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }
}
