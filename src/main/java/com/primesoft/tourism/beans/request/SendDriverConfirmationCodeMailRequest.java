package com.primesoft.tourism.beans.request;

import org.hibernate.validator.constraints.Email;

public class SendDriverConfirmationCodeMailRequest {

    @Email
    private String mail;

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
