package com.primesoft.tourism.beans.request.guidePriseLIstRequest;

import java.util.List;

public class AddGuidePriceListRequest {


    private List<GuidePriceLists> guidePriceLists;


    public List<GuidePriceLists> getGuidePriceLists() {
        return guidePriceLists;
    }

    public void setGuidePriceLists(List<GuidePriceLists> guidePriceLists) {
        this.guidePriceLists = guidePriceLists;
    }
}
