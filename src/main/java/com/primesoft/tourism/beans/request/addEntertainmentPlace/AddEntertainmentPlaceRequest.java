package com.primesoft.tourism.beans.request.addEntertainmentPlace;


import com.primesoft.tourism.beans.tableBeans.EntertainmentPlace;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceInfo;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceWorkingDays;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

public class AddEntertainmentPlaceRequest {

    @Max(10)
    @Min(1)
    private Integer type;

    @Max(86)
    @Min(-86)
    private Double lat;

    @Max(181)
    @Min(-181)
    private Double lng;

    @Size(min = 4)
    private String iconUrl;

    @Min(1)
    private Integer city;

    private String phone;

    private String website;

    private String mail;

    private Double discount;

    private Double price;

    private Integer maxPersons;

    private String entPrice;

    private List<Content> contents;

    private List<WorkingDays> workingDays;

    public EntertainmentPlace getEntertainmentPlace(){

        EntertainmentPlace entertainmentPlace = new EntertainmentPlace();
        entertainmentPlace.setType(this.type);
        entertainmentPlace.setLat(this.lat);
        entertainmentPlace.setLng(this.lng);
        if (this.iconUrl != null && !this.iconUrl.startsWith("https://")) {
            ImageManager imageManager = new ImageManager();
            if (this.iconUrl.startsWith("data:")) {
                this.iconUrl = this.iconUrl.split(",")[1];
            }
            entertainmentPlace.setIconUrl(imageManager.decodeAndSaveBase64(this.iconUrl, Constants.ENTERTAINMENT_PLACE_IMAGE_PREFIX, Constants.ENTERTAINMENT_PLACE_IMAGE_PATH, Util.getRandomString((short) 20)));
        }
        entertainmentPlace.setCity(this.city);
        entertainmentPlace.setPhone(this.phone);
        entertainmentPlace.setWebsite(this.website);
        entertainmentPlace.setMail(this.mail);
        entertainmentPlace.setDiscount(this.discount);
        entertainmentPlace.setPrice(this.price);
        entertainmentPlace.setMaxPersons(this.maxPersons);
        entertainmentPlace.setEntPrice(this.entPrice);

        return entertainmentPlace;
    }

    public List<EntertainmentPlaceInfo> getEntertainmentPlaceInfo(Integer entertainmentPlaceId){

        List<EntertainmentPlaceInfo> ifs = new ArrayList<>();

        for (Content content : contents) {
            EntertainmentPlaceInfo info = new EntertainmentPlaceInfo();
            info.setEntertainmentPlaceId(entertainmentPlaceId);
            info.setLanguage(content.getLanguage());
            info.setName(content.getName());
            info.setAddress(content.getAddress());
            info.setDescription(content.getDescription());
            info.setType(content.getType());
            ifs.add(info);
        }

        return ifs;
    }


    public List<EntertainmentPlaceWorkingDays> getWorkingDays(Integer entertainmentPlaceId){

        List<EntertainmentPlaceWorkingDays> workingDaysInfo = new ArrayList<>();

        for (WorkingDays day : workingDays) {
            EntertainmentPlaceWorkingDays workingDay = new EntertainmentPlaceWorkingDays();
            workingDay.setEntertainmentPlaceId(entertainmentPlaceId);
            workingDay.setName(day.getName());
            workingDay.setValue(day.getValue());
            workingDaysInfo.add(workingDay);
        }

        return workingDaysInfo;
    }


    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }

    public List<WorkingDays> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(List<WorkingDays> workingDays) {
        this.workingDays = workingDays;
    }

    public Integer getMaxPersons() {
        return maxPersons;
    }

    public void setMaxPersons(Integer maxPersons) {
        this.maxPersons = maxPersons;
    }

    public String getEntPrice() {
        return entPrice;
    }

    public void setEntPrice(String entPrice) {
        this.entPrice = entPrice;
    }
}
