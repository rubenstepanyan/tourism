package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Generator;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Size;
import java.util.UUID;

public class DriverRegistrationRequest {

    @Size(min = 3,max = 100)
    private String firstName;

    @Size(min = 4,max = 100)
    private String lastName;

    @Size(min = 6,max = 100)
    private String password;

    private String birthDay;

    @Email
    private String mail;

    @Size(min = 8,max = 20)
    private String phone;

    private String languageSkills;

    private String photoUrl;

    private Integer agencyCode;

    private String city;



    public Driver getDriverObject(){

        Driver driver = new Driver();

        ImageManager imageManager = new ImageManager();
        if (this.photoUrl != null) {
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            driver.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.DRIVER_IMAGE_PREFIX, Constants.DRIVER_IMAGE_PATH, Util.getRandomString((short) 20)));
        }


        driver.setConfirmed(false);
        driver.setFirstName(this.firstName);
        driver.setLastName(this.lastName);
        driver.setPassword(Generator.stringToSha256(this.password));
        driver.setAuthorization(UUID.randomUUID().toString());
        driver.setBirthDay(this.birthDay);
        driver.setMail(this.mail);
        if(!this.phone.startsWith("+")){
            this.phone = "+"+this.phone;
        }
        driver.setPhone(this.phone);
        driver.setRegistrationDate(Util.getCurrentDateAddDays(0));
        driver.setBalance(0);
        driver.setIncomingPercent(10);
        driver.setCurrentRating(5.00);
        driver.setTotalRating(0);
        driver.setLanguageSkills(this.languageSkills);
        driver.setNotificationToken(firstName.substring(0,2)+Util.getRandomNumber());
        driver.setDashboardLanguage(Constants.EN_LANGUAGE);
        driver.setSuspended(false);
        driver.setApproved(false);
        driver.setCity(this.city);

        return driver;
    }


    public Driver updateDriverObject(Driver existingDriver){

        ImageManager imageManager = new ImageManager();
        if (this.photoUrl != null) {
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            existingDriver.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.DRIVER_IMAGE_PREFIX, Constants.DRIVER_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        existingDriver.setConfirmed(false);
        existingDriver.setFirstName(this.firstName);
        existingDriver.setLastName(this.lastName);
        existingDriver.setPassword(Generator.stringToSha256(this.password));
        existingDriver.setAuthorization(UUID.randomUUID().toString());
        existingDriver.setBirthDay(this.birthDay);
        existingDriver.setMail(this.mail);
        existingDriver.setPhone(this.phone);
        existingDriver.setRegistrationDate(Util.getCurrentDateAddDays(0));
        existingDriver.setBalance(0);
        existingDriver.setIncomingPercent(10);
        existingDriver.setCurrentRating(0.00);
        existingDriver.setTotalRating(0);
        existingDriver.setLanguageSkills(this.languageSkills);
        existingDriver.setSuspended(false);
        existingDriver.setCity(this.city);

        return existingDriver;

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(String languageSkills) {
        this.languageSkills = languageSkills;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Integer getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(Integer agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
