package com.primesoft.tourism.beans.request.addOfferedTour;

import com.primesoft.tourism.beans.tableBeans.OfferedTour;
import com.primesoft.tourism.beans.tableBeans.OfferedTourInfo;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

public class AddOfferedTourRequest {

    @Size(min = 5)
    private  String photoUrl;

    private List<Content> contents;

    private Boolean transfer;

    public OfferedTour getOfferedTour(){

        ImageManager imageManager = new ImageManager();
        OfferedTour offeredTour = new OfferedTour();
        if (this.photoUrl != null) {
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            offeredTour.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.TOUR_IMAGE_PREFIX, Constants.TOUR_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        offeredTour.setTransfer(this.transfer);



        return offeredTour;
    }

    public List<OfferedTourInfo> getOfferedTourInfo(Integer offeredTourId){

        List<OfferedTourInfo> offeredToursInfo = new ArrayList<>();

        for (int i = 0; i <contents.size() ; i++) {

            OfferedTourInfo offeredTourInfo = new OfferedTourInfo();
            offeredTourInfo.setOfferedTourId(offeredTourId);
            offeredTourInfo.setDescription(contents.get(i).getDescription());
            offeredTourInfo.setName(contents.get(i).getName());
            offeredTourInfo.setLanguage(contents.get(i).getLanguage());
            offeredTourInfo.setType(contents.get(i).getType());
            offeredToursInfo.add(offeredTourInfo);
        }

        return offeredToursInfo;
    }


    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }

    public Boolean getTransfer() {
        return transfer;
    }

    public void setTransfer(Boolean transfer) {
        this.transfer = transfer;
    }
}
