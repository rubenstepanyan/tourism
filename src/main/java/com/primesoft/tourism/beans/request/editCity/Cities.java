package com.primesoft.tourism.beans.request.editCity;

public class Cities {

    private Integer id;
    private String name;
    private Integer priority;
    private Boolean notShowInTours;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Boolean getNotShowInTours() {
        return notShowInTours;
    }

    public void setNotShowInTours(Boolean notShowInTours) {
        this.notShowInTours = notShowInTours;
    }
}
