package com.primesoft.tourism.beans.request.addDynamicPageRequest;

import com.primesoft.tourism.beans.tableBeans.DynamicPage;
import com.primesoft.tourism.beans.tableBeans.DynamicPageContent;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class AddDynamicPageRequest {

    private String photoUrl;

    private  Integer type;

    private List<Content> contents;

    public DynamicPage getDynamicPage(){
        ImageManager imageManager = new ImageManager();
        DynamicPage dynamicPage  = new DynamicPage();

        if (this.photoUrl != null) {
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            dynamicPage.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.DYNAMIC_PAGE_IMAGE_PREFIX ,Constants.DYNAMIC_PAGE_IMAGE_PATH, Util.getRandomString((short) 20)));
        }
        dynamicPage.setType(this.type);

        return dynamicPage;
    }

    public List<DynamicPageContent> getDynamicPageContent(DynamicPage dynamicPage){


        List<DynamicPageContent> dynamicPageContents = new ArrayList<>();

        for (int i = 0; i < contents.size(); i++) {
            DynamicPageContent dynamicPageContent =  new DynamicPageContent();
            dynamicPageContent.setDynamicPageId(dynamicPage.getId());
            dynamicPageContent.setLanguage(contents.get(i).getLanguage());
            dynamicPageContent.setTitle(contents.get(i).getTitle());
            dynamicPageContent.setContent(contents.get(i).getContent());
            dynamicPageContent.setType(contents.get(i).getType());
            dynamicPageContent.setShortContent(contents.get(i).getShortContent());
            dynamicPageContents.add(dynamicPageContent);
        }

        return dynamicPageContents;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }
}
