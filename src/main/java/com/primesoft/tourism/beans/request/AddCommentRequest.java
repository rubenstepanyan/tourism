package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Comment;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.utils.Util;

import javax.validation.constraints.Size;

public class AddCommentRequest {

    @Size(min = 1,max=1000)
    private String comment;

    private Integer blogId;

    public Comment getComment(Customer customer){

        Comment thisComment = new Comment();

        thisComment.setBlogId(this.blogId);
        thisComment.setComment(this.comment);
        thisComment.setCustomerId(customer.getId());
        thisComment.setCommentDate(Util.getCurrentDateAndTime());

        return thisComment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getBlogId() {
        return blogId;
    }

    public void setBlogId(Integer blogId) {
        this.blogId = blogId;
    }
}
