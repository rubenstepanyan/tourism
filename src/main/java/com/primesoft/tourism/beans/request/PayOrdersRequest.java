package com.primesoft.tourism.beans.request;

import java.util.List;

public class PayOrdersRequest {

    private Integer type;

    private List<Integer> ids;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }
}
