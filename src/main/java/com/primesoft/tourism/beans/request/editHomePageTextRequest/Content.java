package com.primesoft.tourism.beans.request.editHomePageTextRequest;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Content {

    private  Integer id;

    @NotNull
    private Integer language;

    @Size(min = 5)
    private String upTitle;

    @Size(min = 5)
    private String upContent;

    @Size(min = 5)
    private String downTitle;

    @Size(min = 5)
    private String downContent;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public String getUpTitle() {
        return upTitle;
    }

    public void setUpTitle(String upTitle) {
        this.upTitle = upTitle;
    }

    public String getUpContent() {
        return upContent;
    }

    public void setUpContent(String upContent) {
        this.upContent = upContent;
    }

    public String getDownTitle() {
        return downTitle;
    }

    public void setDownTitle(String downTitle) {
        this.downTitle = downTitle;
    }

    public String getDownContent() {
        return downContent;
    }

    public void setDownContent(String downContent) {
        this.downContent = downContent;
    }
}
