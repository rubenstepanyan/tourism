package com.primesoft.tourism.beans.request;


import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

public class UpdateGuidePhotoRequest {

    private  Integer id;
    private  String photoUrl;

    public Guide updateGuidPhoto(Guide guide){
        if (this.photoUrl != null && !this.photoUrl.startsWith("https://")) {
            ImageManager imageManager = new ImageManager();
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            guide.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.GUIDE_IMAGE_PREFIX, Constants.GUIDE_IMAGE_PATH, Util.getRandomString((short) 20)));
        }
        return guide;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
