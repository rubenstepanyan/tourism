package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Offers;

public class AddOffersRequest {

    private String fullName;
    private String contact;
    private String city;
    private String category;
    private String description;

    public Offers getOffersObject(){
        Offers offers = new Offers();

        offers.setFullName(this.fullName);
        offers.setContact(this.contact);
        offers.setCity(this.city);
        offers.setCategory(this.category);
        offers.setDescription(this.description);

        return  offers;
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
