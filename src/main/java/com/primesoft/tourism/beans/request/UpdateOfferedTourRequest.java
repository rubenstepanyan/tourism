package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.request.addOfferedTour.Content;
import com.primesoft.tourism.beans.tableBeans.OfferedTour;
import com.primesoft.tourism.beans.tableBeans.OfferedTourInfo;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.List;

public class UpdateOfferedTourRequest {

    @Min(1)
    private Integer id;

    @Size(min = 5)
    private  String photoUrl;

    private List<Content> contents;

    private Boolean transfer;


    public void updateOfferedTour(OfferedTour offeredTour, List<OfferedTourInfo> infoList){

        if (this.photoUrl != null && !this.photoUrl.startsWith("http")) {
            ImageManager imageManager = new ImageManager();
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            offeredTour.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.TOUR_IMAGE_PREFIX, Constants.TOUR_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        offeredTour.setTransfer(this.transfer);

        for (Content content : this.contents){
            for (OfferedTourInfo info : infoList) {
                if(info.getLanguage().equals(content.getLanguage())){
                    info.setName(content.getName());
                    info.setDescription(content.getDescription());
                    break;
                }
            }
        }
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }

    public Boolean getTransfer() {
        return transfer;
    }

    public void setTransfer(Boolean transfer) {
        this.transfer = transfer;
    }
}
