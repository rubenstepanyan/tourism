package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.beans.tableBeans.TourReservation;
import com.primesoft.tourism.utils.Constants;

public class AddTourReservationRequest {

    private Integer tourId;
    private String reservationDate;
    private Integer freeSeats;

    public TourReservation getTourReservation(Driver driver, Agency agency){

        TourReservation tourReservation = new TourReservation();

        if(driver != null){
            tourReservation.setReferenceId(driver.getId());
            tourReservation.setReferenceType(Constants.DRIVER_TYPE);
        }else if(agency !=null){
            tourReservation.setReferenceId(agency.getId());
            tourReservation.setReferenceType(Constants.AGENCY_TYPE);
        }
        tourReservation.setFreeSeats(this.freeSeats);
        tourReservation.setReservationDate(this.reservationDate);
        tourReservation.setTourId(this.tourId);

        return tourReservation;
    }

    public Integer getTourId() {
        return tourId;
    }

    public void setTourId(Integer tourId) {
        this.tourId = tourId;
    }

    public String getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(String reservationDate) {
        this.reservationDate = reservationDate;
    }

    public Integer getFreeSeats() {
        return freeSeats;
    }

    public void setFreeSeats(Integer freeSeats) {
        this.freeSeats = freeSeats;
    }
}
