package com.primesoft.tourism.beans.request;

public class CustomerRejectOrderRequest {

    private  Integer orderId;

    private Integer type;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
