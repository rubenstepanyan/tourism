package com.primesoft.tourism.beans.request;

import javax.validation.constraints.Size;

public class AdministratorLoginRequest {

    @Size(min =4,max = 100)
    private String userName;

    @Size(min =6,max = 100)
    private String password;


    public String getUserName() {
        return userName;
    }

    public void setUsername(String username) {
        this.userName = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
