package com.primesoft.tourism.beans.request.editCity;

import java.util.List;

public class EditCityRequest {

    private List<Cities> cities;


    public List<Cities> getCities() {
        return cities;
    }

    public void setCities(List<Cities> cities) {
        this.cities = cities;
    }
}
