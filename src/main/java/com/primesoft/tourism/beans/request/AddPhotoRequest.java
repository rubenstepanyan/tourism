package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.SlideShow;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

import javax.validation.constraints.Size;

public class AddPhotoRequest {

    @Size(min = 1, max = 100)
    private String description;

    private String photoUrl;

    public SlideShow getSlideShowObject(){
        SlideShow slideShow = new SlideShow();
        ImageManager imageManager = new ImageManager();
        if (this.photoUrl != null) {
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            slideShow.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.SLIDE_SHOW_IMAGE_PREFIX, Constants.SLIDE_SHOW_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        slideShow.setDescription(this.description);

        return slideShow;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
