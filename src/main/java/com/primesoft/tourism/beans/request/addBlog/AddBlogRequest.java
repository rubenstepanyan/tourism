package com.primesoft.tourism.beans.request.addBlog;

import com.primesoft.tourism.beans.tableBeans.Blog;
import com.primesoft.tourism.beans.tableBeans.BlogContent;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class AddBlogRequest {

    private String iconUrl;
    private List<AddBlogContent> contents;

    public Blog getBlog(){

        ImageManager imageManager = new ImageManager();
        Blog blog = new Blog();
        if (this.iconUrl != null) {
            if (this.iconUrl.startsWith("data:")) {
                this.iconUrl = this.iconUrl.split(",")[1];
            }
            blog.setIconUrl(imageManager.decodeAndSaveBase64(this.iconUrl, Constants.BLOG_IMAGE_PREFIX, Constants.BLOG_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        blog.setCommentCount(0);

        return blog;
    }

    public List<BlogContent> getBlogContent(Integer blogId){

        List<BlogContent> blogContents = new ArrayList<>();

        for (int i = 0; i <contents.size() ; i++) {

            BlogContent blogContent = new BlogContent();

            blogContent.setBlogId(blogId);
            blogContent.setLanguage(contents.get(i).getLanguage());
            blogContent.setTitle(contents.get(i).getTitle());

            blogContents.add(blogContent);
        }

        return blogContents;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public List<AddBlogContent> getContents() {
        return contents;
    }

    public void setContents(List<AddBlogContent> contents) {
        this.contents = contents;
    }
}
