package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.utils.Generator;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Size;
import java.util.UUID;

public class AddAdministratorRequest {


    @Size(min = 1,max = 40)
    private String firstName;

    @Size(min = 1,max = 40)
    private String lastName;

    @Size(min = 4,max = 40)
    private String userName;


    @Size(min = 6,max = 40)
    private String password;

    @Email
    private String mail;

    @Size(min = 4, max = 100)
    private String address;

    @Size(min = 4, max = 100)
    private String passport;

    @Size(min = 4, max = 40)
    private String phone;



    public Administrator getAdministratorObject(){

        Administrator administrator = new Administrator();

        administrator.setFirstName(this.firstName);
        administrator.setLastName(this.lastName);
        administrator.setUserName(this.userName);
        administrator.setMail(this.mail);
        administrator.setAddress(this.address);
        administrator.setPassport(this.passport);
        administrator.setPhone(this.phone);
        administrator.setPassword(Generator.stringToSha256(this.password));
        administrator.setAuthorization(UUID.randomUUID().toString());

        return  administrator;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
