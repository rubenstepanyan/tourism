package com.primesoft.tourism.beans.request.guidePriseLIstRequest;

import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.beans.tableBeans.GuidePriceList;

public class GuidePriceLists {

    private String language;

    private Integer price;

    private Integer type;

    private Integer city;

    public GuidePriceList getGuidPriceListObject(Guide guide, Agency agency,Long groupId){

        GuidePriceList guidePriceList = new GuidePriceList();

        guidePriceList.setGuideId(guide.getId());
        guidePriceList.setLanguage(this.language);
        guidePriceList.setPrice(this.price);
        guidePriceList.setType(this.type);
        guidePriceList.setCity(this.city);
        guidePriceList.setGroupId(groupId);
        if(agency==null){
            guidePriceList.setRating(guide.getCurrentRating());
        }else {
            guidePriceList.setRating(agency.getCurrentRating());
        }

        return guidePriceList;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }
}
