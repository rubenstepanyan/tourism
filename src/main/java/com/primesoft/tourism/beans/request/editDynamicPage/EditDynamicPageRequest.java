package com.primesoft.tourism.beans.request.editDynamicPage;

import com.primesoft.tourism.beans.tableBeans.DynamicPage;
import com.primesoft.tourism.beans.tableBeans.DynamicPageContent;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

import java.util.List;

public class EditDynamicPageRequest {

    private Integer id;
    private String photoUrl;
    private List<Content> contents;
    private List<Integer> deletedPhotos;


    public DynamicPage updatePhoto(DynamicPage dynamicPage){

        if (this.photoUrl != null && !this.photoUrl.startsWith("https://")) {
            ImageManager imageManager = new ImageManager();
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            dynamicPage.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.DYNAMIC_PAGE_IMAGE_PREFIX ,Constants.DYNAMIC_PAGE_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        return dynamicPage;
    }

    public List<DynamicPageContent> updateDynamicPageContents(List<DynamicPageContent> dynamicPageContents){


        for (int i = 0; i < dynamicPageContents.size(); i++) {

            for (int j = 0; j < contents.size(); j++) {
                if(dynamicPageContents.get(i).getId().equals(contents.get(j).getId())){

                    dynamicPageContents.get(i).setTitle(contents.get(j).getTitle());
                    dynamicPageContents.get(i).setContent(contents.get(j).getContent());
                    dynamicPageContents.get(i).setShortContent(contents.get(i).getShortContent());
                }
            }

        }

        return dynamicPageContents;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }

    public List<Integer> getDeletedPhotos() {
        return deletedPhotos;
    }

    public void setDeletedPhotos(List<Integer> deletedPhotos) {
        this.deletedPhotos = deletedPhotos;
    }
}
