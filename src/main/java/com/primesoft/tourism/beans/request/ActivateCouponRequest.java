package com.primesoft.tourism.beans.request;

public class ActivateCouponRequest {

    private String couponNumber;

    public String getCouponNumber() {
        return couponNumber;
    }

    public void setCouponNumber(String couponNumber) {
        this.couponNumber = couponNumber;
    }
}
