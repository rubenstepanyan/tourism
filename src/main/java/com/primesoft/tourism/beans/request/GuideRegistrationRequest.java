package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Generator;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.UUID;


public class GuideRegistrationRequest {

    @Size(min = 3)
    private String firstName;

    @Size(min = 4)
    private String lastName;

    private String languageSkills;

    @Size(min = 4)
    private String password;

    @Size(min = 4)
    private String birthDay;

    @Email
    private String mail;

    @Size(min = 4)
    private String phone;

    @Size(min = 1)
    private String city;

    @Min(1)
    @Max(2)
    private Integer sex;

    private String photoUrl;

    private Integer agencyCode;

    public Guide getGuidObject() {

        Guide guide = new Guide();

        ImageManager imageManager = new ImageManager();
        if (this.photoUrl != null) {
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            guide.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.GUIDE_IMAGE_PREFIX, Constants.GUIDE_IMAGE_PATH, Util.getRandomString((short) 20)));
        } else {
            guide.setPhotoUrl("https://api.visitmyarmenia.com/guide/guide.png");
        }

        guide.setConfirmed(false);
        guide.setFirstName(this.firstName);
        guide.setLastName(this.lastName);
        guide.setLanguageSkills(this.languageSkills);
        guide.setPassword(Generator.stringToSha256(this.password));
        guide.setAuthorization(UUID.randomUUID().toString());
        guide.setBirthDay(this.birthDay);
        guide.setMail(this.mail);
        if (!this.phone.startsWith("+")) {
            this.phone = "+" + this.phone;
        }
        guide.setPhone(this.phone);
        guide.setRegistrationDate(Util.getCurrentDateAddDays(0));
        guide.setCity(this.city);
        guide.setSex(this.sex);
        guide.setSuspended(false);
        guide.setCurrentRating(5.00);
        guide.setTotalRating(0);
        guide.setNotificationToken(firstName.substring(0, 2) + Util.getRandomNumber());
        guide.setDashboardLanguage(Constants.EN_LANGUAGE);
        guide.setApproved(false);

        return guide;
    }


    public Guide updateGuidObject(Guide existingGuide) {

        ImageManager imageManager = new ImageManager();
        if (this.photoUrl != null) {
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            existingGuide.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.GUIDE_IMAGE_PREFIX, Constants.GUIDE_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        existingGuide.setConfirmed(false);
        existingGuide.setFirstName(this.firstName);
        existingGuide.setLastName(this.lastName);
        existingGuide.setLanguageSkills(this.languageSkills);
        existingGuide.setPassword(Generator.stringToSha256(this.password));
        existingGuide.setAuthorization(UUID.randomUUID().toString());
        existingGuide.setBirthDay(this.birthDay);
        existingGuide.setMail(this.mail);
        existingGuide.setPhone(this.phone);
        existingGuide.setRegistrationDate(Util.getCurrentDateAddDays(0));
        existingGuide.setCity(this.city);
        existingGuide.setSex(this.sex);
        existingGuide.setSuspended(false);

        return existingGuide;

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(String languageSkills) {
        this.languageSkills = languageSkills;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Integer getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(Integer agencyCode) {
        this.agencyCode = agencyCode;
    }
}
