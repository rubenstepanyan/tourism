package com.primesoft.tourism.beans.request.editHomePageTextRequest;

import com.primesoft.tourism.beans.tableBeans.HomePageText;

import java.util.ArrayList;
import java.util.List;

public class EditHomePageRequest {

    List<Content> contents;

    public List<HomePageText> updateHomePageText(){

        List<HomePageText> homePageTexts = new ArrayList<>();

        for (int i = 0; i < contents.size(); i++) {

            HomePageText homePageText = new HomePageText();

            homePageText.setId(contents.get(i).getId());
            homePageText.setLanguage(contents.get(i).getLanguage());
            homePageText.setUpTitle(contents.get(i).getUpTitle());
            homePageText.setUpContent(contents.get(i).getUpContent());
            homePageText.setDownTitle(contents.get(i).getDownTitle());
            homePageText.setDownContent(contents.get(i).getDownContent());

            homePageTexts.add(homePageText);
        }
        return homePageTexts;
    }

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }
}
