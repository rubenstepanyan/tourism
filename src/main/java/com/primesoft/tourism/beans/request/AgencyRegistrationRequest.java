package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Generator;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Size;
import java.util.UUID;

public class AgencyRegistrationRequest {

    @Size(min = 4, max = 100)
    private String name;

    @Size(min = 4, max = 100)
    private String city;

    @Size(min = 4, max = 100)
    private String address;

    @Size(min = 6, max = 100)
    private String password;

    @Email
    private String mail;

    @Size(min = 4, max = 40)
    private String phone;

    private String logoUrl;

    public Agency getAgencyObject(){
        Agency agency = new Agency();
        ImageManager imageManager = new ImageManager();
        if (this.logoUrl != null) {
            if (this.logoUrl.startsWith("data:")) {
                this.logoUrl = this.logoUrl.split(",")[1];
            }
            agency.setLogoUrl(imageManager.decodeAndSaveBase64(this.logoUrl, Constants.AGENCY_IMAGE_PREFIX, Constants.AGENCY_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        agency.setConfirmed(false);
        agency.setName(this.name);
        agency.setPassword(Generator.stringToSha256(this.password));
        agency.setAuthorization(UUID.randomUUID().toString());
        agency.setMail(this.mail);
        if(!this.phone.startsWith("+")){
            this.phone = "+"+this.phone;
        }
        agency.setPhone(this.phone);
        agency.setRegistrationDate(Util.getCurrentDateAddDays(0));
        agency.setBalance(0);
        agency.setIncomingPercent(10);
        agency.setTotalRating(0);
        agency.setCurrentRating(5.0);
        agency.setCity(this.city);
        agency.setAddress(this.address);
        agency.setNotificationToken(name.substring(0,3)+Util.getRandomNumber());
        agency.setDashboardLanguage(Constants.EN_LANGUAGE);
        agency.setAgencyCode(Util.getRandomNumber());
        agency.setSuspended(false);
        agency.setApproved(false);

        return agency;

    }


    public Agency updateAgencyObject(Agency existingAgency){
        ImageManager imageManager = new ImageManager();
        if (this.logoUrl != null) {
            if (this.logoUrl.startsWith("data:")) {
                this.logoUrl = this.logoUrl.split(",")[1];
            }
            existingAgency.setLogoUrl(imageManager.decodeAndSaveBase64(this.logoUrl, Constants.AGENCY_IMAGE_PREFIX, Constants.AGENCY_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        existingAgency.setConfirmed(false);
        existingAgency.setName(this.name);
        existingAgency.setPassword(Generator.stringToSha256(this.password));
        existingAgency.setAuthorization(UUID.randomUUID().toString());
        existingAgency.setMail(this.mail);
        existingAgency.setPhone(this.phone);
        existingAgency.setRegistrationDate(Util.getCurrentDateAddDays(0));
        existingAgency.setBalance(0);
        existingAgency.setIncomingPercent(10);
        existingAgency.setCurrentRating(0.0);
        existingAgency.setCity(this.city);
        existingAgency.setAddress(this.address);
        existingAgency.setTotalRating(0);

        return existingAgency;

    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }
}
