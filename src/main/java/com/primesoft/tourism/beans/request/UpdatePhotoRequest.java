package com.primesoft.tourism.beans.request;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public class UpdatePhotoRequest {


    private Integer Id;

    private List<MultipartFile> images;

    private String mode;


    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public List<MultipartFile> getImages() {
        return images;
    }

    public void setImages(List<MultipartFile> images) {
        this.images = images;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
