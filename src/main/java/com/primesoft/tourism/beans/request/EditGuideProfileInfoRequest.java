package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

public class EditGuideProfileInfoRequest {

    private String firstName;
    private String lastName;
    private String mail;
    private String city;
    private String photoUrl;
    private String accountNumber;
    private String birthDay;
    private String languageSkills;


    public Guide updateGuide(Guide guide){

        if (this.photoUrl != null && !this.photoUrl.startsWith("https://")) {
            ImageManager imageManager = new ImageManager();
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            guide.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.GUIDE_IMAGE_PREFIX, Constants.GUIDE_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        guide.setFirstName(this.firstName);
        guide.setLastName(this.lastName);
        guide.setMail(this.mail);
        guide.setCity(this.city);
        guide.setAccountNumber(this.accountNumber);
        guide.setBirthDay(this.birthDay);
        guide.setLanguageSkills(this.languageSkills);

        return guide;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(String languageSkills) {
        this.languageSkills = languageSkills;
    }
}
