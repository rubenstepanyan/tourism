package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Car;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class EditCarRequest {


    @Min(0)
    private  Integer id;

    @Size(min = 1, max = 100)
    private String carModel;

    @Size(min = 1, max = 100)
    private String carMake;

    @Min(3)
    @Max(100)
    private Integer quantityPassenger;

    @Min(1990)
    private Integer carYear;

    @Size(min = 1, max = 100)
    private String bodyType;

    private Integer type;

    private  String photoUrl;

    public Car updateCarObject(Car car){

        if (this.photoUrl != null && !this.photoUrl.startsWith("https://")) {
            ImageManager imageManager = new ImageManager();
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            car.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.CAR_IMAGE_PREFIX, Constants.CAR_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        car.setCarModel(this.carModel);
        car.setCarMake(this.carMake);
        car.setQuantityPassenger(this.quantityPassenger);
        car.setCarYear(this.carYear);
        car.setBodyType(this.bodyType);
        car.setType(type);

        return  car;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarMake() {
        return carMake;
    }

    public void setCarMake(String carMake) {
        this.carMake = carMake;
    }

    public Integer getQuantityPassenger() {
        return quantityPassenger;
    }

    public void setQuantityPassenger(Integer quantityPassenger) {
        this.quantityPassenger = quantityPassenger;
    }

    public Integer getCarYear() {
        return carYear;
    }

    public void setCarYear(Integer carYear) {
        this.carYear = carYear;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
