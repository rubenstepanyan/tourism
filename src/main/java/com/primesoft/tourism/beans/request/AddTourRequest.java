package com.primesoft.tourism.beans.request;

import com.google.gson.Gson;
import com.primesoft.tourism.beans.tableBeans.*;
import com.primesoft.tourism.utils.Constants;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

public class AddTourRequest {


    private Integer offeredTourId;

    @Min(0)
    private Integer price;

    private String startTime;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date startDate;

    private List<Integer> daysOfWeek;

    private Integer hour;

    private Integer minute;

    @Size(min = 1,max = 250)
    private String startAddress;

    private Double lat;

    private Double lng;

    private String languageSkills;

    private Integer startCity;

    private  Integer carId;

    private  Integer seats;

    private Boolean guided;

    private Integer tourType;

    @Size(min = 1,max = 1000)
    private String description;

    @Size(min = 3,max = 40)
    private  String localName;

    private Boolean pickMeUp;




    public Tour getTourObject(Driver driver, Agency agency, Car car, OfferedTour offeredTour){

        Tour tour = new Tour();
        tour.setOfferedTourId(this.offeredTourId);

        if(driver!=null) {
            tour.setReference(driver.getId());
            tour.setType(Constants.DRIVER_TYPE);
            tour.setDriverId(driver.getId());
            tour.setName(driver.getFirstName()+" "+driver.getLastName());
            tour.setFreeSeats(this.seats);
            tour.setApproved(driver.getApproved());
            if(driver.getPhotoUrl()!=null) {
                tour.setIconUrl(driver.getPhotoUrl());
            }else {
                tour.setIconUrl("https://api.visitmyarmenia.com/driver/driver.png");
            }
            if(driver.getCurrentRating()>0.1){
                tour.setRating(driver.getCurrentRating());
            }else {
                tour.setRating(0.00);
            }

            if(!driver.getSuspended()){
                tour.setBlocked(false);
            }else{
                tour.setBlocked(true);
            }

        }else if(agency!=null){
            tour.setReference(agency.getId());
            tour.setType(Constants.AGENCY_TYPE);
            tour.setName(agency.getName());
            tour.setFreeSeats(car.getQuantityPassenger());
            tour.setApproved(agency.getApproved());
            if(agency.getLogoUrl()!=null) {
                tour.setIconUrl(agency.getLogoUrl());
            }else {
                tour.setIconUrl("https://api.visitmyarmenia.com/agency/agency.jpg");
            }

            if(agency.getCurrentRating()>0.1){
                tour.setRating(agency.getCurrentRating());
            }else {
                tour.setRating(0.00);
            }

            if(!agency.getSuspended()){
                tour.setBlocked(false);
            }else{
                tour.setBlocked(true);
            }
        }


        tour.setCarId(car.getId());
        tour.setLanguageSkills(this.languageSkills);
        tour.setCarInfo(car.getCarMake()+" "+car.getCarModel()+" "+car.getBodyType()+" "+car.getCarYear());
        tour.setPrice(this.price);
        tour.setStartTime("2000-01-01 "+this.startTime+":00");
        if(daysOfWeek != null){
            tour.setDaysOfWeek(new Gson().toJson(this.daysOfWeek));
            tour.setStartDate(null);
        }else if(startDate != null){
            tour.setStartDate(this.startDate);
            tour.setDaysOfWeek(null);
        }
        tour.setHour(this.hour);
        if(minute == null){
            tour.setMinute(0);
        }
        tour.setMinute(this.minute);
        tour.setStartAddress(this.startAddress);
        tour.setLat(this.lat);
        tour.setLng(this.lng);
        tour.setStartCity(this.startCity);
        tour.setGuided(this.guided);
        tour.setTourType(this.tourType);
        tour.setDescription(this.description);
        tour.setLocalName(this.localName);
        tour.setPickMeUp(this.pickMeUp);
        tour.setTransfer(offeredTour.getTransfer());
        tour.setDeleted(false);

        return tour;
    }


    public Integer getOfferedTourId() {
        return offeredTourId;
    }

    public void setOfferedTourId(Integer offeredTourId) {
        this.offeredTourId = offeredTourId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public List<Integer> getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(List<Integer> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(String languageSkills) {
        this.languageSkills = languageSkills;
    }

    public Integer getStartCity() {
        return startCity;
    }

    public void setStartCity(Integer startCity) {
        this.startCity = startCity;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Boolean getGuided() {
        return guided;
    }

    public void setGuided(Boolean guided) {
        this.guided = guided;
    }

    public Integer getTourType() {
        return tourType;
    }

    public void setTourType(Integer tourType) {
        this.tourType = tourType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public Boolean getPickMeUp() {
        return pickMeUp;
    }

    public void setPickMeUp(Boolean pickMeUp) {
        this.pickMeUp = pickMeUp;
    }
}
