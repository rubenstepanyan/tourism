package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Partners;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

public class AddPartnerRequest {

    private String logoUrl;

    private String url;

    public Partners getPartnersObject(){
        Partners partners = new Partners();

        ImageManager imageManager = new ImageManager();
        if (this.logoUrl != null) {
            if (this.logoUrl.startsWith("data:")) {
                this.logoUrl = this.logoUrl.split(",")[1];
            }
            partners.setLogoUrl(imageManager.decodeAndSaveBase64(this.logoUrl, Constants.PARTNER_IMAGE_PREFIX, Constants.PARTNER_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

       partners.setUrl(this.url);

        return  partners;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
