package com.primesoft.tourism.beans.request;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Size;

public class CustomerLoginRequest {


    @Email
    private String mail;

    @Size(min =6,max = 100)
    private String password;

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
