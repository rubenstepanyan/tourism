package com.primesoft.tourism.beans.request;

public class AddLanguagesResponse {

    private String language;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
