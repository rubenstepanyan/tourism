package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

public class EditAgencyProfileInfoRequest {

    private String name;
    private String city;
    private String address;
    private String logoUrl;
    private String mail;
    private String accountNumber;


    public Agency updateAgency(Agency agency){

        if (this.logoUrl != null && !this.logoUrl.startsWith("https://")) {
            ImageManager imageManager = new ImageManager();
            if (this.logoUrl.startsWith("data:")) {
                this.logoUrl = this.logoUrl.split(",")[1];
            }
            agency.setLogoUrl(imageManager.decodeAndSaveBase64(this.logoUrl, Constants.AGENCY_IMAGE_PREFIX, Constants.AGENCY_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        agency.setName(this.name);
        agency.setCity(this.city);
        agency.setAddress(this.address);
        agency.setMail(this.mail);
        agency.setAccountNumber(this.accountNumber);

        return agency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
