package com.primesoft.tourism.beans.request;


import com.primesoft.tourism.beans.tableBeans.Guide;

public class EditGuideRequest {

    private Integer id;

    private Boolean confirmed;

    private String firstName;

    private String lastName;

    private String birthDay;

    private String mail;

    private String city;

    private Integer sex;

    private Boolean suspended;

    private Boolean approved;

    private String accountNumber;

    public Guide updateGuide(Guide guide){

        guide.setConfirmed(this.confirmed);
        guide.setFirstName(this.firstName);
        guide.setLastName(this.lastName);
        guide.setBirthDay(this.birthDay);
        guide.setMail(this.mail);
        guide.setCity(this.city);
        guide.setSex(this.sex);
        guide.setSuspended(this.suspended);
        guide.setAccountNumber(this.accountNumber);

        return guide;
    }


    public Guide editGuide(Guide guide){

        guide.setFirstName(this.firstName);
        guide.setLastName(this.lastName);
        guide.setBirthDay(this.birthDay);
        guide.setMail(this.mail);
        guide.setCity(this.city);
        guide.setSex(this.sex);

        return guide;
    }


    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
