package com.primesoft.tourism.beans.request;

public class EditTourReservationRequest {

    private  Integer tourId;
    private Integer freeSeats;

    public Integer getFreeSeats() {
        return freeSeats;
    }

    public void setFreeSeats(Integer freeSeats) {
        this.freeSeats = freeSeats;
    }

    public Integer getTourId() {
        return tourId;
    }

    public void setTourId(Integer tourId) {
        this.tourId = tourId;
    }
}
