package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Agency;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.UUID;

public class EditAgencyRequest {

    @Min(1)
    private Integer id;

    @Size(min = 1)
    private String name;

    @Size(min = 1)
    private String city;

    @Size(min = 1)
    private String address;

    @Email
    private String mail;

    @Size(min = 6)
    private String phone;

    private Integer balance;

    @Min(0)
    @Max(100)
    private Integer incomingPercent;

    private Boolean confirmed;

    @Min(0)
    @Max(5)
    private Double currentRating;

    private Boolean suspended;

    private Boolean approved;

    private String accountNumber;

    public Agency updateAgency(Agency agency){

        agency.setName(this.name);
        agency.setCity(this.city);
        agency.setAddress(this.address);
        agency.setConfirmed(this.confirmed);
        agency.setCurrentRating(this.currentRating);
        agency.setMail(this.mail);
        agency.setPhone(this.phone);
        agency.setBalance(this.balance);
        agency.setIncomingPercent(this.incomingPercent);
        agency.setApproved(this.approved);

        if(!agency.getSuspended().equals(this.suspended)) {
            agency.setSuspended(this.suspended);
            agency.setAuthorization(UUID.randomUUID().toString());
        }
        agency.setAccountNumber(this.accountNumber);

        return agency;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getIncomingPercent() {
        return incomingPercent;
    }

    public void setIncomingPercent(Integer incomingPercent) {
        this.incomingPercent = incomingPercent;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Double getCurrentRating() {
        return currentRating;
    }

    public void setCurrentRating(Double currentRating) {
        this.currentRating = currentRating;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
