package com.primesoft.tourism.beans.request.addEntertainmentPlace;

public class WorkingDays {

    private Integer name;

    private String value;


    public Integer getName() {
        return name;
    }

    public void setName(Integer name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
