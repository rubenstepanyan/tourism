package com.primesoft.tourism.beans.request.addBlog;

import javax.validation.constraints.Size;

public class AddBlogContent {

    @Size(min = 1,max = 1000)
    private String title;

    private Integer language;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }
}
