package com.primesoft.tourism.beans.request;

public class ChangePasswordRequest {

    private String mailOrPhone;
    private String confirmationCode;
    private String password;
    private Integer type;

    public String getMailOrPhone() {
        return mailOrPhone;
    }

    public void setMailOrPhone(String mailOrPhone) {
        this.mailOrPhone = mailOrPhone;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
