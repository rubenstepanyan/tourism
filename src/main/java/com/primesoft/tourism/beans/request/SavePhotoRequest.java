package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

public class SavePhotoRequest {

    private String photoUrl;

    public SavePhotoRequest savePhoto() {
        ImageManager imageManager = new ImageManager();
        if (this.photoUrl != null) {
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            photoUrl = imageManager.decodeAndSaveBase64(this.photoUrl, Constants.DYNAMIC_PAGE_IMAGE_PREFIX, Constants.DYNAMIC_PAGE_IMAGE_PATH, Util.getRandomString((short) 20));
            return this;
        }
        photoUrl = "Չհաջողվեց պահպանել նկարը";
        return this;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
