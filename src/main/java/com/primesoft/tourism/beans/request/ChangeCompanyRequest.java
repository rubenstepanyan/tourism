package com.primesoft.tourism.beans.request;

public class ChangeCompanyRequest {

    private Integer agencyCode;

    public Integer getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(Integer agencyCode) {
        this.agencyCode = agencyCode;
    }
}
