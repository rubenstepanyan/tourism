package com.primesoft.tourism.beans.request.editVideo;

import com.primesoft.tourism.beans.tableBeans.Video;
import com.primesoft.tourism.beans.tableBeans.VideoContent;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

import java.util.List;

public class EditVideoRequest {

    private Integer id;

    private String url;

    private String iconUrl;

    private List<EditVideoContent> contents;


    public Video updateVideo(Video video){

        if (this.iconUrl != null && !this.iconUrl.startsWith("https://")) {
            ImageManager imageManager = new ImageManager();
            if (this.iconUrl.startsWith("data:")) {
                this.iconUrl = this.iconUrl.split(",")[1];
            }
            video.setIconUrl(imageManager.decodeAndSaveBase64(this.iconUrl, Constants.VIDEO_IMAGE_PREFIX ,Constants.VIDEO_IMAGE_PATH, Util.getRandomString((short) 20)));
        }
        video.setUrl(this.url);

        return video;
    }

    public List<VideoContent> updateVideoContent(List<VideoContent> videoContents){


        for (int i = 0; i < videoContents.size(); i++) {

            for (int j = 0; j < contents.size(); j++) {
                if(videoContents.get(i).getId().equals(contents.get(j).getId())){

                    videoContents.get(i).setTitle(contents.get(j).getTitle());
                }
            }

        }

        return videoContents;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public List<EditVideoContent> getContents() {
        return contents;
    }

    public void setContents(List<EditVideoContent> contents) {
        this.contents = contents;
    }
}
