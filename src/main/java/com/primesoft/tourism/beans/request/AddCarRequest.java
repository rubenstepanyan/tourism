package com.primesoft.tourism.beans.request;

import com.primesoft.tourism.beans.tableBeans.Car;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class AddCarRequest {

    @Size(min = 1, max = 100)
    private String carModel;

    @Size(min = 1, max = 100)
    private String carMake;

    @Min(3)
    @Max(100)
    private Integer quantityPassenger;

    @Min(1990)
    private Integer carYear;

    @Size(min = 1, max = 100)
    private String bodyType;

    private String photoUrl;


    public Car getCarObject(Integer reference, Integer type){
        Car car = new Car();
        ImageManager imageManager = new ImageManager();
        if (this.photoUrl != null) {
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            car.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.CAR_IMAGE_PREFIX, Constants.CAR_IMAGE_PATH, Util.getRandomString((short) 20)));
        }

        car.setCarModel(this.carModel);
        car.setCarMake(this.carMake);
        car.setQuantityPassenger(this.quantityPassenger);
        car.setCarYear(this.carYear);
        car.setBodyType(this.bodyType);
        car.setType(type);
        car.setReference(reference);

        return  car;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public Integer getQuantityPassenger() {
        return quantityPassenger;
    }

    public void setQuantityPassenger(Integer quantityPassenger) {
        this.quantityPassenger = quantityPassenger;
    }

    public Integer getCarYear() {
        return carYear;
    }

    public void setCarYear(Integer carYear) {
        this.carYear = carYear;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getCarMake() {
        return carMake;
    }

    public void setCarMake(String carMake) {
        this.carMake = carMake;
    }
}
