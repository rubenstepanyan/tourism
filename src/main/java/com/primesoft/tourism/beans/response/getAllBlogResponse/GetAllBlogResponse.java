package com.primesoft.tourism.beans.response.getAllBlogResponse;

import java.util.List;

public class GetAllBlogResponse {

    private List<BlogContentResponse> contents;

    private Long pageCount;

    public GetAllBlogResponse(List<BlogContentResponse> contents, Long pageCount) {
        this.contents = contents;
        this.pageCount = pageCount;
    }

    public List<BlogContentResponse> getContents() {
        return contents;
    }

    public void setContents(List<BlogContentResponse> contents) {
        this.contents = contents;
    }

    public Long getPageCount() {
        return pageCount;
    }

    public void setPageCount(Long pageCount) {
        this.pageCount = pageCount;
    }
}
