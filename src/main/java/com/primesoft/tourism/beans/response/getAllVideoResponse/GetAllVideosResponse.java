package com.primesoft.tourism.beans.response.getAllVideoResponse;

import java.util.List;

public class GetAllVideosResponse {

   private List<VideoContentResponse> contents;
    private Long pageCount;

    public GetAllVideosResponse(List<VideoContentResponse> contents,Long pageCount) {
        this.contents = contents;
        this.pageCount = pageCount;
    }

    public List<VideoContentResponse> getContents() {
        return contents;
    }

    public void setContents(List<VideoContentResponse> contents) {
        this.contents = contents;
    }

    public Long getPageCount() {
        return pageCount;
    }

    public void setPageCount(Long pageCount) {
        this.pageCount = pageCount;
    }
}
