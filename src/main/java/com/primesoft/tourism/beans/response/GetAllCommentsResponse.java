package com.primesoft.tourism.beans.response;

public class GetAllCommentsResponse {


    private Integer id;
    private Integer customerId;
    private Integer blogId;
    private String comment;
    private String commentDate;

    public GetAllCommentsResponse(Integer id, Integer customerId, Integer blogId, String comment, String commentDate) {
        this.id = id;
        this.customerId = customerId;
        this.blogId = blogId;
        this.comment = comment;
        this.commentDate = commentDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getBlogId() {
        return blogId;
    }

    public void setBlogId(Integer blogId) {
        this.blogId = blogId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }
}
