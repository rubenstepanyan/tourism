package com.primesoft.tourism.beans.response;

public class DriverLoginResponse {
    private String authorization;

    private String notificationToken;

    private Boolean personalDriver;

    public DriverLoginResponse(String authorization, String notificationToken, Boolean personalDriver) {
        this.authorization = authorization;
        this.notificationToken = notificationToken;
        this.personalDriver = personalDriver;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getNotificationToken() {
        return notificationToken;
    }

    public void setNotificationToken(String notificationToken) {
        this.notificationToken = notificationToken;
    }

    public Boolean getPersonalDriver() {
        return personalDriver;
    }

    public void setPersonalDriver(Boolean personalDriver) {
        this.personalDriver = personalDriver;
    }
}
