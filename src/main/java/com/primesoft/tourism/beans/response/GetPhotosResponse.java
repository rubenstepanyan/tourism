package com.primesoft.tourism.beans.response;

import com.primesoft.tourism.beans.tableBeans.SlideShow;

import java.util.List;

public class GetPhotosResponse {

    List<SlideShow> slideShows;

    public GetPhotosResponse(List<SlideShow> slideShows) {
        this.slideShows = slideShows;
    }

    public List<SlideShow> getSlideShows() {
        return slideShows;
    }

    public void setSlideShows(List<SlideShow> slideShows) {
        this.slideShows = slideShows;
    }
}
