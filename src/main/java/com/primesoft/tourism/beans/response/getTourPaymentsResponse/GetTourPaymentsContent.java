package com.primesoft.tourism.beans.response.getTourPaymentsResponse;

public class GetTourPaymentsContent {

    private Object id;
    private Object quantity;
    private Object totalPrice;
    private Object toPay;
    private Object tourDate;
    private Object status;
    private Object localName;
    private Object isTransfer;
    private Object price;
    private Object tourName;
    private Object customerName;
    private Object customerMail;
    private Object customerPhone;
    private Object reasonComment;
    private Object myTripsPaid;
    private Object partnerName;
    private Object refund;

    public GetTourPaymentsContent(Object[] objects) {
        this.id = objects[0];
        this.quantity = objects[1];
        this.totalPrice = objects[2];
        this.toPay = objects[3];
        this.tourDate = objects[4];
        this.status = objects[5];
        this.localName = objects[6];
        this.isTransfer = objects[7];
        this.price = objects[8];
        this.tourName = objects[9];
        this.customerName = objects[10];
        this.customerMail = objects[11];
        this.customerPhone = objects[12];
        this.reasonComment = objects[13];
        this.myTripsPaid = objects[14];
        this.partnerName = objects[15];
        this.refund = objects[16];
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getQuantity() {
        return quantity;
    }

    public void setQuantity(Object quantity) {
        this.quantity = quantity;
    }

    public Object getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Object totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Object getToPay() {
        return toPay;
    }

    public void setToPay(Object toPay) {
        this.toPay = toPay;
    }

    public Object getTourDate() {
        return tourDate;
    }

    public void setTourDate(Object tourDate) {
        this.tourDate = tourDate;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getLocalName() {
        return localName;
    }

    public void setLocalName(Object localName) {
        this.localName = localName;
    }

    public Object getIsTransfer() {
        return isTransfer;
    }

    public void setIsTransfer(Object isTransfer) {
        this.isTransfer = isTransfer;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public Object getTourName() {
        return tourName;
    }

    public void setTourName(Object tourName) {
        this.tourName = tourName;
    }

    public Object getCustomerName() {
        return customerName;
    }

    public void setCustomerName(Object customerName) {
        this.customerName = customerName;
    }

    public Object getCustomerMail() {
        return customerMail;
    }

    public void setCustomerMail(Object customerMail) {
        this.customerMail = customerMail;
    }

    public Object getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(Object customerPhone) {
        this.customerPhone = customerPhone;
    }

    public Object getReasonComment() {
        return reasonComment;
    }

    public void setReasonComment(Object reasonComment) {
        this.reasonComment = reasonComment;
    }

    public Object getMyTripsPaid() {
        return myTripsPaid;
    }

    public void setMyTripsPaid(Object myTripsPaid) {
        this.myTripsPaid = myTripsPaid;
    }

    public Object getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(Object partnerName) {
        this.partnerName = partnerName;
    }

    public Object getRefund() {
        return refund;
    }

    public void setRefund(Object refund) {
        this.refund = refund;
    }
}
