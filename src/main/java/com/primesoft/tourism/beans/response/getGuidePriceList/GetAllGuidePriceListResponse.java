package com.primesoft.tourism.beans.response.getGuidePriceList;

import java.util.List;

public class GetAllGuidePriceListResponse {

    private List<PriceListGroup> priceListGroups;

    public List<PriceListGroup> getPriceListGroups() {
        return priceListGroups;
    }

    public void setPriceListGroups(List<PriceListGroup> priceListGroups) {
        this.priceListGroups = priceListGroups;
    }
}
