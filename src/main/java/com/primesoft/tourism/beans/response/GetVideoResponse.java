package com.primesoft.tourism.beans.response;

import com.primesoft.tourism.beans.tableBeans.Video;
import com.primesoft.tourism.beans.tableBeans.VideoContent;

import java.util.List;

public class GetVideoResponse {

    private Integer id;

    private String url;

    private String iconUrl;

    private List<VideoContent>  videoContents;

    public GetVideoResponse(Video video, List<VideoContent> videoContents) {
        this.id = video.getId();
        this.url = video.getUrl();
        this.iconUrl = video.getIconUrl();
        this.videoContents = videoContents;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public List<VideoContent> getVideoContents() {
        return videoContents;
    }

    public void setVideoContents(List<VideoContent> videoContents) {
        this.videoContents = videoContents;
    }
}
