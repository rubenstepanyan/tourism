package com.primesoft.tourism.beans.response.getAllGuideResponse;


public class GetAllGuideContentResponse {

    private Object id;
    private Object photoUrl;
    private Object fullName;
    private Object age;
    private Object rating;
    private Object language;
    private Integer price;
    private Object type;
    private Object city;
    private Object approved;
    private Object priceListId;

    public GetAllGuideContentResponse(Object[] objects) {
        this.id = objects[0];
        this.photoUrl = objects[1];
        this.fullName = objects[2];
        this.age = objects[3];
        this.rating = objects[4];
        this.language = objects[5];
        this.price =(Integer) objects[6];
        this.type = objects[7];
        this.city = objects[8];
        this.approved = objects[9];
        this.priceListId = objects[10];
    }


    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(Object photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Object getFullName() {
        return fullName;
    }

    public void setFullName(Object fullName) {
        this.fullName = fullName;
    }

    public Object getAge() {
        return age;
    }

    public void setAge(Object age) {
        this.age = age;
    }

    public Object getRating() {
        return rating;
    }

    public void setRating(Object rating) {
        this.rating = rating;
    }

    public Object getLanguage() {
        return language;
    }

    public void setLanguage(Object language) {
        this.language = language;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getApproved() {
        return approved;
    }

    public void setApproved(Object approved) {
        this.approved = approved;
    }

    public Object getPriceListId() {
        return priceListId;
    }

    public void setPriceListId(Object priceListId) {
        this.priceListId = priceListId;
    }
}
