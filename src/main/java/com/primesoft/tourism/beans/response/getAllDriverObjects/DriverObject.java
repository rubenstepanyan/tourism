package com.primesoft.tourism.beans.response.getAllDriverObjects;

import java.util.Date;

public class DriverObject {

    private Integer id;
    private Boolean confirmed;
    private String firstName;
    private String lastName;
    private String languageSkills;
    private Double currentRating;
    private String birthDay;
    private String mail;
    private String phone;
    private Date registrationDate;
    private Integer osType;
    private Integer balance;
    private String photoUrl;
    private Integer incomingPercent;
    private Boolean suspended;
    private String accountNumber;
    private Boolean approved;

    public DriverObject(Integer id, Boolean confirmed, String firstName, String lastName, String languageSkills, Double currentRating, String birthDay, String mail, String phone, Date registrationDate, Integer osType, Integer balance, String photoUrl, Integer incomingPercent, Boolean suspended, String accountNumber, Boolean approved) {
        this.id = id;
        this.confirmed = confirmed;
        this.firstName = firstName;
        this.lastName = lastName;
        this.languageSkills = languageSkills;
        this.currentRating = currentRating;
        this.birthDay = birthDay;
        this.mail = mail;
        this.phone = phone;
        this.registrationDate = registrationDate;
        this.osType = osType;
        this.balance = balance;
        this.photoUrl = photoUrl;
        this.incomingPercent = incomingPercent;
        this.suspended = suspended;
        this.accountNumber = accountNumber;
        this.approved = approved;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(String languageSkills) {
        this.languageSkills = languageSkills;
    }

    public Double getCurrentRating() {
        return currentRating;
    }

    public void setCurrentRating(Double currentRating) {
        this.currentRating = currentRating;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Integer getOsType() {
        return osType;
    }

    public void setOsType(Integer osType) {
        this.osType = osType;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Integer getIncomingPercent() {
        return incomingPercent;
    }

    public void setIncomingPercent(Integer incomingPercent) {
        this.incomingPercent = incomingPercent;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }
}
