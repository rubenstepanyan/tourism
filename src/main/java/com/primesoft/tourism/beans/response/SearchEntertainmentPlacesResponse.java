package com.primesoft.tourism.beans.response;

import com.primesoft.tourism.beans.response.getEntertainmentPlaces.Content;

import java.util.List;

public class SearchEntertainmentPlacesResponse {
    private List<Content> content;


    public SearchEntertainmentPlacesResponse(List<Content> content) {
        this.content = content;
    }

    public List<Content> getContent() {
        return content;
    }

    public void setContent(List<Content> content) {
        this.content = content;
    }
}
