package com.primesoft.tourism.beans.response.getAllDriverObjects;

import java.util.List;

public class GetAllDriverObjectResponse {

    List<DriverObject> driver;

    public GetAllDriverObjectResponse(List<DriverObject> driver) {
        this.driver = driver;
    }

    public List<DriverObject> getDriver() {
        return driver;
    }

    public void setDriver(List<DriverObject> driver) {
        this.driver = driver;
    }
}
