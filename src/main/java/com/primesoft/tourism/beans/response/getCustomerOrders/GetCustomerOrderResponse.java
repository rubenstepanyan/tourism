package com.primesoft.tourism.beans.response.getCustomerOrders;

import java.util.List;

public class GetCustomerOrderResponse {

    private List<TourOrderContent> tourOrderContent;

    private List<GuideOrderContent> guideOrderContent;

    public GetCustomerOrderResponse(List<TourOrderContent> tourOrderContent, List<GuideOrderContent> guideOrderContent) {
        this.tourOrderContent = tourOrderContent;
        this.guideOrderContent = guideOrderContent;
    }

    public List<TourOrderContent> getTourOrderContent() {
        return tourOrderContent;
    }

    public void setTourOrderContent(List<TourOrderContent> tourOrderContent) {
        this.tourOrderContent = tourOrderContent;
    }

    public List<GuideOrderContent> getGuideOrderContent() {
        return guideOrderContent;
    }

    public void setGuideOrderContent(List<GuideOrderContent> guideOrderContent) {
        this.guideOrderContent = guideOrderContent;
    }
}
