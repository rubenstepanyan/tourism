package com.primesoft.tourism.beans.response.getOfferedTourCustomerResponse;

public class Content {

    private  Object id;
    private  Object photoUrl;
    private  Object price;
    private  Object name;

    public Content(Object[] objects) {

        this.id = objects[0];
        this.photoUrl = objects[1];
        this.price = objects[2];
        this.name = objects[3];
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(Object photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }
}