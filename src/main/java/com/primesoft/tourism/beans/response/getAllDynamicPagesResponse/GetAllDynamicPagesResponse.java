package com.primesoft.tourism.beans.response.getAllDynamicPagesResponse;



import java.util.List;

public class GetAllDynamicPagesResponse {

    List<Content> dynamicPages;

    public GetAllDynamicPagesResponse(List<Content> dynamicPages) {
        this.dynamicPages = dynamicPages;
    }

    public List<Content> getDynamicPages() {
        return dynamicPages;
    }

    public void setDynamicPages(List<Content> dynamicPages) {
        this.dynamicPages = dynamicPages;
    }
}
