package com.primesoft.tourism.beans.response;

public class PurchasedCoupons {

    private Object iconUrl;
    private Object name;
    private Object persons;
    private Object discount;
    private Object address;
    private Object phone;
    private Object couponCode;
    private Object payed;
    private Object errorMessage;
    private Object id;
    private Object activated;
    private Object couponId;

    public PurchasedCoupons(Object[] objects) {
        this.iconUrl = objects[0];
        this.name = objects[1];
        this.persons = objects[2];
        this.discount = objects[3];
        this.address = objects[4];
        this.phone = objects[5];
        this.couponCode = objects[6];
        this.payed = objects[7];
        this.errorMessage = objects[8];
        this.id = objects[9];
        this.activated = objects[10];
        this.couponId = objects[11];
    }

    public Object getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(Object iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public Object getPersons() {
        return persons;
    }

    public void setPersons(Object persons) {
        this.persons = persons;
    }

    public Object getDiscount() {
        return discount;
    }

    public void setDiscount(Object discount) {
        this.discount = discount;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public Object getPhone() {
        return phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }

    public Object getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(Object couponCode) {
        this.couponCode = couponCode;
    }

    public Object getPayed() {
        return payed;
    }

    public void setPayed(Object payed) {
        this.payed = payed;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getActivated() {
        return activated;
    }

    public void setActivated(Object activated) {
        this.activated = activated;
    }

    public Object getCouponId() {
        return couponId;
    }

    public void setCouponId(Object couponId) {
        this.couponId = couponId;
    }
}
