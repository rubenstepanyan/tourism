package com.primesoft.tourism.beans.response.getAllGuideResponse;

import java.util.ArrayList;
import java.util.List;

public class GetAllGuideResponse {

    private  List<GetAllGuideContentResponse> contents;
    private Integer pageCount = 1;
    private Integer itemsCount = 0;
    private Integer minPrice = Integer.MAX_VALUE;
    private Integer maxPrice = 0;

    public GetAllGuideResponse(List<GetAllGuideContentResponse> contents, Integer page) {

        itemsCount = contents.size();

        for (GetAllGuideContentResponse content: contents) {

            if(content.getPrice()<minPrice){
                minPrice = content.getPrice();
            }

            if(content.getPrice()>maxPrice){
                maxPrice = content.getPrice();
            }

        }

        if(page!=null && contents.size()>12){
            pageCount = (contents.size()/12);

            if(contents.size()%12>0){
                pageCount+=1;
            }

            int startPosition = (page-1)*12;
            int endPosition = page*12;

            if(startPosition > contents.size()){
                this.contents = new ArrayList<>();
                return;
            }

            if(endPosition > contents.size()){
                endPosition = contents.size();
            }

            this.contents = contents.subList(startPosition, endPosition);

        }else {
            this.contents = contents;
        }
    }

    public List<GetAllGuideContentResponse> getContents() {
        return contents;
    }

    public void setContents(List<GetAllGuideContentResponse> contents) {
        this.contents = contents;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public Integer getItemsCount() {
        return itemsCount;
    }

    public void setItemsCount(Integer itemsCount) {
        this.itemsCount = itemsCount;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Integer maxPrice) {
        this.maxPrice = maxPrice;
    }
}
