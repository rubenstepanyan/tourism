package com.primesoft.tourism.beans.response.getGuidePaymentsResponse;

public class GuidePaymentContent {

    private Object id;
    private Object quantityType;
    private Object totalPrice;
    private Object toPay;
    private Object dateFrom;
    private Object dateTo;
    private Object status;
    private Object price;
    private Object guideName;
    private Object customerName;
    private Object customerMail;
    private Object customerPhone;
    private Object reasonComment;
    private Object myTripsPaid;
    private Object direction;
    private Object startCity;
    private Object customerPhotoUrl;
    private Object language;
    private Object refund;

    public GuidePaymentContent(Object[] objects) {
        this.id = objects[0];
        this.quantityType = objects[1];
        this.totalPrice = objects[2];
        this.toPay = objects[3];
        this.dateFrom = objects[4];
        this.dateTo = objects[5];
        this.status = objects[6];
        this.price = objects[7];
        this.guideName = objects[8];
        this.customerName = objects[9];
        this.customerMail = objects[10];
        this.customerPhone = objects[11];
        this.reasonComment = objects[12];
        this.myTripsPaid = objects[13];
        this.direction = objects[14];
        this.startCity = objects[15];
        this.customerPhotoUrl = objects[16];
        this.language = objects[17];
        this.refund = objects[18];
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getQuantityType() {
        return quantityType;
    }

    public void setQuantityType(Object quantityType) {
        this.quantityType = quantityType;
    }

    public Object getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Object totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Object getToPay() {
        return toPay;
    }

    public void setToPay(Object toPay) {
        this.toPay = toPay;
    }

    public Object getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Object dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Object getDateTo() {
        return dateTo;
    }

    public void setDateTo(Object dateTo) {
        this.dateTo = dateTo;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public Object getGuideName() {
        return guideName;
    }

    public void setGuideName(Object guideName) {
        this.guideName = guideName;
    }

    public Object getCustomerName() {
        return customerName;
    }

    public void setCustomerName(Object customerName) {
        this.customerName = customerName;
    }

    public Object getCustomerMail() {
        return customerMail;
    }

    public void setCustomerMail(Object customerMail) {
        this.customerMail = customerMail;
    }

    public Object getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(Object customerPhone) {
        this.customerPhone = customerPhone;
    }

    public Object getReasonComment() {
        return reasonComment;
    }

    public void setReasonComment(Object reasonComment) {
        this.reasonComment = reasonComment;
    }

    public Object getMyTripsPaid() {
        return myTripsPaid;
    }

    public void setMyTripsPaid(Object myTripsPaid) {
        this.myTripsPaid = myTripsPaid;
    }

    public Object getDirection() {
        return direction;
    }

    public void setDirection(Object direction) {
        this.direction = direction;
    }


    public Object getStartCity() {
        return startCity;
    }

    public void setStartCity(Object startCity) {
        this.startCity = startCity;
    }

    public Object getCustomerPhotoUrl() {
        return customerPhotoUrl;
    }

    public void setCustomerPhotoUrl(Object customerPhotoUrl) {
        this.customerPhotoUrl = customerPhotoUrl;
    }

    public Object getLanguage() {
        return language;
    }

    public void setLanguage(Object language) {
        this.language = language;
    }

    public Object getRefund() {
        return refund;
    }

    public void setRefund(Object refund) {
        this.refund = refund;
    }
}
