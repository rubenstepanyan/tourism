package com.primesoft.tourism.beans.response;

public class AgencyLoginResponse {

    private String authorization;

    private String topic;

    private Integer id;

    public AgencyLoginResponse(String authorization, String topic, Integer id) {
        this.authorization = authorization;
        this.topic = topic;
        this.id = id;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
