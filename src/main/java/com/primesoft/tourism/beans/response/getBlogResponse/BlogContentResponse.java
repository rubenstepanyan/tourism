package com.primesoft.tourism.beans.response.getBlogResponse;


public class BlogContentResponse {

    private Integer id;
    private Integer BlogId;
    private String title;
    private Integer language;

    public BlogContentResponse(Integer id, Integer blogId, String title, Integer language) {
        this.id = id;
        BlogId = blogId;
        this.title = title;
        this.language = language;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBlogId() {
        return BlogId;
    }

    public void setBlogId(Integer blogId) {
        BlogId = blogId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }
}
