package com.primesoft.tourism.beans.response.getOurPartnersResponse;


import java.util.List;

public class GetOurPartnersResponse {

    private List<GetOurPartnersContent> agencyList;
    private List<GetOurPartnersContent> driverList;
    private List<GetOurPartnersContent> guideList;
    private List<GetOurPartnersContentEntertainmentPlace> entertainmentPlaceList;


    public GetOurPartnersResponse(List<GetOurPartnersContent> agencyList, List<GetOurPartnersContent> driverList, List<GetOurPartnersContent> guideList, List<GetOurPartnersContentEntertainmentPlace> entertainmentPlaceList) {
        this.agencyList = agencyList;
        this.driverList = driverList;
        this.guideList = guideList;
        this.entertainmentPlaceList = entertainmentPlaceList;
    }

    public List<GetOurPartnersContent> getAgencyList() {
        return agencyList;
    }

    public void setAgenciyList(List<GetOurPartnersContent> agencyList) {
        this.agencyList = agencyList;
    }

    public List<GetOurPartnersContent> getDriverList() {
        return driverList;
    }

    public void setDriverList(List<GetOurPartnersContent> driverList) {
        this.driverList = driverList;
    }

    public List<GetOurPartnersContent> getGuideList() {
        return guideList;
    }

    public void setGuideList(List<GetOurPartnersContent> guideList) {
        this.guideList = guideList;
    }

    public List<GetOurPartnersContentEntertainmentPlace> getEntertainmentPlaceList() {
        return entertainmentPlaceList;
    }

    public void setEntertainmentPlaceList(List<GetOurPartnersContentEntertainmentPlace> entertainmentPlaceList) {
        this.entertainmentPlaceList = entertainmentPlaceList;
    }
}
