package com.primesoft.tourism.beans.response.getOrdersResponse;

import com.primesoft.tourism.utils.Util;

public class GetGuideOrderContentsResponse {

    private Object id;
    private Object language;
    private Object quantityType;
    private Object price;
    private Object dateFrom;
    private Object dateTo;
    private Object fullName;
    private Object customerName;
    private Object status;
    private Object direction;
    private Object customerPhotoUrl;
    private Object city;
    private Object duration;
    private Object customerNationality;
    private Object phone;
    private Object age;

    public GetGuideOrderContentsResponse(Object objects[]) {
        this.id = objects[0];
        this.language = objects[1];
        this.quantityType = objects[2];
        this.price = objects[3];
        this.dateFrom = objects[4];
        this.dateTo = objects[5];
        this.fullName = objects[6];
        this.customerName = objects[7];
        this.status = objects[8];
        this.direction = objects[9];
        this.customerPhotoUrl = objects[10];
        this.city = objects[11];
        this.customerNationality = objects[12];
        this.phone = objects[13];
        this.age = objects[14];
        this.duration = (Util.getDateDiffInDay(dateFrom.toString(), dateTo.toString()) + 1);
    }


    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getLanguage() {
        return language;
    }

    public void setLanguage(Object language) {
        this.language = language;
    }

    public Object getQuantityType() {
        return quantityType;
    }

    public void setQuantityType(Object quantityType) {
        this.quantityType = quantityType;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public Object getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Object dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Object getDateTo() {
        return dateTo;
    }

    public void setDateTo(Object dateTo) {
        this.dateTo = dateTo;
    }

    public Object getFullName() {
        return fullName;
    }

    public void setFullName(Object fullName) {
        this.fullName = fullName;
    }

    public Object getCustomerName() {
        return customerName;
    }

    public void setCustomerName(Object customerName) {
        this.customerName = customerName;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getDirection() {
        return direction;
    }

    public void setDirection(Object direction) {
        this.direction = direction;
    }

    public Object getCustomerPhotoUrl() {
        return customerPhotoUrl;
    }

    public void setCustomerPhotoUrl(Object customerPhotoUrl) {
        this.customerPhotoUrl = customerPhotoUrl;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getDuration() {
        return duration;
    }

    public void setDuration(Object duration) {
        this.duration = duration;
    }

    public Object getCustomerNationality() {
        return customerNationality;
    }

    public void setCustomerNationality(Object customerNationality) {
        this.customerNationality = customerNationality;
    }

    public Object getPhone() {
        return phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }

    public Object getAge() {
        return age;
    }

    public void setAge(Object age) {
        this.age = age;
    }
}
