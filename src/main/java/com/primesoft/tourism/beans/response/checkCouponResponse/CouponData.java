package com.primesoft.tourism.beans.response.checkCouponResponse;

public class CouponData {

    private Object id;
    private Object price;
    private Object discount;
    private Object number;
    private Object activated;
    private Object payed;
    private Object iconUrl;
    private Object city;
    private Object phone;
    private Object maxPersons;
    private Object address;
    private Object name;


    public CouponData(Object objects[]) {
        this.id = objects[0];
        this.price = objects[1];
        this.discount = objects[2];
        this.number = objects[3];
        this.activated = objects[4];
        this.payed = objects[5];
        this.iconUrl = objects[6];
        this.city = objects[7];
        this.phone = objects[8];
        this.maxPersons = objects[9];
        this.address = objects[10];
        this.name = objects[11];
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public Object getDiscount() {
        return discount;
    }

    public void setDiscount(Object discount) {
        this.discount = discount;
    }

    public Object getNumber() {
        return number;
    }

    public void setNumber(Object number) {
        this.number = number;
    }

    public Object getActivated() {
        return activated;
    }

    public void setActivated(Object activated) {
        this.activated = activated;
    }

    public Object getPayed() {
        return payed;
    }

    public void setPayed(Object payed) {
        this.payed = payed;
    }

    public Object getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(Object iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getPhone() {
        return phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }

    public Object getMaxPersons() {
        return maxPersons;
    }

    public void setMaxPersons(Object maxPersons) {
        this.maxPersons = maxPersons;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }
}
