package com.primesoft.tourism.beans.response;

import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.ImageManager;
import com.primesoft.tourism.utils.Util;

public class UpdateDriverPhotoRequest {
    private  Integer id;
    private  String photoUrl;

    public Driver updateDriverPhoto(Driver driver){

        if (this.photoUrl != null && !this.photoUrl.startsWith("https://")) {
            ImageManager imageManager = new ImageManager();
            if (this.photoUrl.startsWith("data:")) {
                this.photoUrl = this.photoUrl.split(",")[1];
            }
            driver.setPhotoUrl(imageManager.decodeAndSaveBase64(this.photoUrl, Constants.DRIVER_IMAGE_PREFIX, Constants.DRIVER_IMAGE_PATH, Util.getRandomString((short) 20)));
        }
        return driver;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
