
package com.primesoft.tourism.beans.response.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "USDAMD",
    "USDEUR",
    "USDRUB"
})
public class Quotes {

    @JsonProperty("USDAMD")
    private Double uSDAMD;
    @JsonProperty("USDEUR")
    private Double uSDEUR;
    @JsonProperty("USDRUB")
    private Double uSDRUB;

    @JsonProperty("USDAMD")
    public Double getUSDAMD() {
        return uSDAMD;
    }

    @JsonProperty("USDAMD")
    public void setUSDAMD(Double uSDAMD) {
        this.uSDAMD = uSDAMD;
    }

    @JsonProperty("USDEUR")
    public Double getUSDEUR() {
        return uSDEUR;
    }

    @JsonProperty("USDEUR")
    public void setUSDEUR(Double uSDEUR) {
        this.uSDEUR = uSDEUR;
    }

    @JsonProperty("USDRUB")
    public Double getUSDRUB() {
        return uSDRUB;
    }

    @JsonProperty("USDRUB")
    public void setUSDRUB(Double uSDRUB) {
        this.uSDRUB = uSDRUB;
    }
}
