package com.primesoft.tourism.beans.response;

public class AdministratorLoginResponse {

    private String authorization;


    public AdministratorLoginResponse(String authorization) {
        this.authorization = authorization;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }
}
