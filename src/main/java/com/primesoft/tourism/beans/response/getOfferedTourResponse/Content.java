package com.primesoft.tourism.beans.response.getOfferedTourResponse;

public class Content {

    private Object id;

    private Object photoUrl;

    private Object name;

    private Object description;

    private Object transfer;


    public Content(Object[] objects) {

        this.id = objects[0];
        this.photoUrl = objects[1];
        this.name = objects[2];
        this.description = objects[3];
        this.transfer = objects[4];

    }


    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(Object photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Object getTransfer() {
        return transfer;
    }

    public void setTransfer(Object transfer) {
        this.transfer = transfer;
    }
}
