package com.primesoft.tourism.beans.response.getAllTouesResponse;

public class GetAllTourContentResponse {

    private Object id;
    private Object tourType;
    private Object carInfo;
    private Object localName;
    private Object price;
    private Object startCity;
    private Object description;
    private Object startDate;
    private Object daysOfWeek;
    private Object hour;
    private Object startAddress;
    private Object lat;
    private Object lng;
    private Object startTime;
    private Object languageSkills;
    private Object guided;
    private Object name;
    private Object offeredTourId;
    private Object carId;
    private Object minute;
    private Object seats;
    private Object pickMeUp;
    private Object transfer;
    private Object carImage;

    public GetAllTourContentResponse(Object objects[]) {
        this.id = objects[0];
        this.tourType = objects[1];
        this.carInfo = objects[2];
        this.localName = objects[3];
        this.price = objects[4];
        this.startCity = objects[5];
        this.description = objects[6];
        this.startDate = objects[7];
        this.daysOfWeek = objects[8];
        this.hour = objects[9];
        this.startAddress = objects[10];
        this.lat = objects[11];
        this.lng = objects[12];
        this.startTime = objects[13];
        this.languageSkills = objects[14];
        this.guided = objects[15];
        this.name = objects[16];
        this.offeredTourId = objects[17];
        this.carId = objects[18];
        this.minute = objects[19];
        this.seats = objects[20];
        this.pickMeUp = objects[21];
        this.transfer = objects[22];
        this.carImage = objects[23];
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getTourType() {
        return tourType;
    }

    public void setTourType(Object tourType) {
        this.tourType = tourType;
    }

    public Object getCarInfo() {
        return carInfo;
    }

    public void setCarInfo(Object carInfo) {
        this.carInfo = carInfo;
    }

    public Object getLocalName() {
        return localName;
    }

    public void setLocalName(Object localName) {
        this.localName = localName;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public Object getStartCity() {
        return startCity;
    }

    public void setStartCity(Object startCity) {
        this.startCity = startCity;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Object getStartDate() {
        return startDate;
    }

    public void setStartDate(Object startDate) {
        this.startDate = startDate;
    }

    public Object getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(Object daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    public Object getHour() {
        return hour;
    }

    public void setHour(Object hour) {
        this.hour = hour;
    }

    public Object getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(Object startAddress) {
        this.startAddress = startAddress;
    }

    public Object getLat() {
        return lat;
    }

    public void setLat(Object lat) {
        this.lat = lat;
    }

    public Object getLng() {
        return lng;
    }

    public void setLng(Object lng) {
        this.lng = lng;
    }

    public Object getStartTime() {
        return startTime;
    }

    public void setStartTime(Object startTime) {
        this.startTime = startTime;
    }

    public Object getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(Object languageSkills) {
        this.languageSkills = languageSkills;
    }

    public Object getGuided() {
        return guided;
    }

    public void setGuided(Object guided) {
        this.guided = guided;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public Object getOfferedTourId() {
        return offeredTourId;
    }

    public void setOfferedTourId(Object offeredTourId) {
        this.offeredTourId = offeredTourId;
    }

    public Object getCarId() {
        return carId;
    }

    public void setCarId(Object carId) {
        this.carId = carId;
    }

    public Object getMinute() {
        return minute;
    }

    public void setMinute(Object minute) {
        this.minute = minute;
    }

    public Object getSeats() {
        return seats;
    }

    public void setSeats(Object seats) {
        this.seats = seats;
    }

    public Object getPickMeUp() {
        return pickMeUp;
    }

    public void setPickMeUp(Object pickMeUp) {
        this.pickMeUp = pickMeUp;
    }

    public Object getTransfer() {
        return transfer;
    }

    public void setTransfer(Object transfer) {
        this.transfer = transfer;
    }

    public Object getCarImage() {
        return carImage;
    }

    public void setCarImage(Object carImage) {
        this.carImage = carImage;
    }
}
