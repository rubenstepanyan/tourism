package com.primesoft.tourism.beans.response.getOfferedTourResponse;

import com.primesoft.tourism.beans.tableBeans.OfferedTour;
import com.primesoft.tourism.beans.tableBeans.OfferedTourInfo;

import java.util.List;

public class GetOfferedTourResponse {

    private OfferedTour offeredTour;
    private List<OfferedTourInfo> contents;

    public GetOfferedTourResponse(OfferedTour offeredTour, List<OfferedTourInfo> content) {
        this.offeredTour = offeredTour;
        this.contents = content;
    }

    public OfferedTour getOfferedTour() {
        return offeredTour;
    }

    public void setOfferedTour(OfferedTour offeredTour) {
        this.offeredTour = offeredTour;
    }

    public List<OfferedTourInfo> getContents() {
        return contents;
    }

    public void setContents(List<OfferedTourInfo> contents) {
        this.contents = contents;
    }
}
