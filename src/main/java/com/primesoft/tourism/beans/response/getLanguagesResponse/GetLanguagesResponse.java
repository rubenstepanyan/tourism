package com.primesoft.tourism.beans.response.getLanguagesResponse;

import com.primesoft.tourism.beans.tableBeans.Languages;

import java.util.List;

public class GetLanguagesResponse {

    List<Languages> contentLanguages;

    public GetLanguagesResponse(List<Languages> contentLanguages) {
        this.contentLanguages = contentLanguages;
    }

    public List<Languages> getContentLanguages() {
        return contentLanguages;
    }

    public void setContentLanguages(List<Languages> contentLanguages) {
        this.contentLanguages = contentLanguages;
    }
}
