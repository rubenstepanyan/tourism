package com.primesoft.tourism.beans.response.getAllCustomerObjects;

import java.util.List;

public class GetAllCustomerObjectsResponse {

    private List<CustomerObject> customer;

    public GetAllCustomerObjectsResponse(List<CustomerObject> customer) {
        this.customer = customer;
    }

    public List<CustomerObject> getCustomer() {
        return customer;
    }

    public void setCustomer(List<CustomerObject> customer) {
        this.customer = customer;
    }
}
