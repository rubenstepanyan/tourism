package com.primesoft.tourism.beans.response.getBlogCommentResponse;

public class BlogCommentContent {

    private Object id;
    private Object customerId;
    private Object blogId;
    private Object comment;
    private Object commentDate;
    private Object firstName;
    private Object lastName;
    private Object photoUrl;

    public BlogCommentContent(Object[] objects){

        this.id = objects[0];
        this.customerId = objects[1];
        this.blogId = objects[2];
        this.comment = objects[3];
        this.commentDate = objects[4];
        this.firstName = objects[5];
        this.lastName = objects[6];
        this.photoUrl = objects[7];
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Object customerId) {
        this.customerId = customerId;
    }

    public Object getBlogId() {
        return blogId;
    }

    public void setBlogId(Object blogId) {
        this.blogId = blogId;
    }

    public Object getComment() {
        return comment;
    }

    public void setComment(Object comment) {
        this.comment = comment;
    }

    public Object getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Object commentDate) {
        this.commentDate = commentDate;
    }

    public Object getFirstName() {
        return firstName;
    }

    public void setFirstName(Object firstName) {
        this.firstName = firstName;
    }

    public Object getLastName() {
        return lastName;
    }

    public void setLastName(Object lastName) {
        this.lastName = lastName;
    }

    public Object getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(Object photoUrl) {
        this.photoUrl = photoUrl;
    }
}
