package com.primesoft.tourism.beans.response;

import com.primesoft.tourism.beans.tableBeans.TourReservation;

import java.util.List;

public class GetTourReservationResponse {

    private  List<TourReservation> tourReservations;

    public GetTourReservationResponse(List<TourReservation> tourReservations) {
        this.tourReservations = tourReservations;
    }

    public List<TourReservation> getTourReservations() {
        return tourReservations;
    }

    public void setTourReservations(List<TourReservation> tourReservations) {
        this.tourReservations = tourReservations;
    }
}
