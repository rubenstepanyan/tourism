package com.primesoft.tourism.beans.response;

public class ConfirmCustomerResponse {

    private String authorization;

    public ConfirmCustomerResponse(String authorization) {
        this.authorization = authorization;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }
}
