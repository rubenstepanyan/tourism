package com.primesoft.tourism.beans.response.getAllDynamicPagesResponse;

public class Content {

    private Object id;

    private Object photoUrl;

    private Object type;

    private Object title;

    private Object content;

    private Object shortContent;

    public Content(Object[] row) {
        this.id = row[0];
        this.photoUrl = row[1];
        this.type = row[2];
        this.title = row[3];
        this.content = row[3];
        this.shortContent = row[4];
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(Object photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public Object getTitle() {
        return title;
    }

    public void setTitle(Object title) {
        this.title = title;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public Object getShortContent() {
        return shortContent;
    }

    public void setShortContent(Object shortContent) {
        this.shortContent = shortContent;
    }
}
