package com.primesoft.tourism.beans.response.getAllTourismObjects;

import java.util.List;

public class GetAllTourismObjectsResponse {


    private List<TourismObject> drivers;
    private List<TourismObject> agencies;

    public GetAllTourismObjectsResponse(List<TourismObject> drivers, List<TourismObject> agencies) {
        this.drivers = drivers;
        this.agencies = agencies;
    }

    public List<TourismObject> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<TourismObject> drivers) {
        this.drivers = drivers;
    }

    public List<TourismObject> getAgencies() {
        return agencies;
    }

    public void setAgencies(List<TourismObject> agencies) {
        this.agencies = agencies;
    }
}
