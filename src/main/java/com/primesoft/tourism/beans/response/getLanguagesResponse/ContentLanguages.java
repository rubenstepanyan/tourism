package com.primesoft.tourism.beans.response.getLanguagesResponse;

public class ContentLanguages {

    private  Integer id;
    private  String language;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
