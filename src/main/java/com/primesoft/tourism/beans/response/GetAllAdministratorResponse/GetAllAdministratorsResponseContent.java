package com.primesoft.tourism.beans.response.GetAllAdministratorResponse;

public class GetAllAdministratorsResponseContent {

    private Object id;

    private Object firstName;

    private Object lastName;

    private Object userName;

    private Object mail;

    private Object address;

    private Object passport;

    private Object phone;

    public GetAllAdministratorsResponseContent(Object[] objects) {
        this.id = objects[0];
        this.firstName = objects[1];
        this.lastName = objects[2];
        this.userName = objects[3];
        this.mail = objects[4];
        this.address = objects[5];
        this.passport = objects[6];
        this.phone = objects[7];
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getFirstName() {
        return firstName;
    }

    public void setFirstName(Object firstName) {
        this.firstName = firstName;
    }

    public Object getLastName() {
        return lastName;
    }

    public void setLastName(Object lastName) {
        this.lastName = lastName;
    }

    public Object getUserName() {
        return userName;
    }

    public void setUserName(Object userName) {
        this.userName = userName;
    }

    public Object getMail() {
        return mail;
    }

    public void setMail(Object mail) {
        this.mail = mail;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public Object getPassport() {
        return passport;
    }

    public void setPassport(Object passport) {
        this.passport = passport;
    }

    public Object getPhone() {
        return phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }
}
