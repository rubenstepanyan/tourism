package com.primesoft.tourism.beans.response;

public class GetCustomerOptionsResponse {

    private String topicAndroid;
    private String topicIos;
    private Boolean isNew;
    private String tutorialUrl;

    public String getTopicAndroid() {
        return topicAndroid;
    }

    public void setTopicAndroid(String topicAndroid) {
        this.topicAndroid = topicAndroid;
    }

    public String getTopicIos() {
        return topicIos;
    }

    public void setTopicIos(String topicIos) {
        this.topicIos = topicIos;
    }

    public Boolean getNew() {
        return isNew;
    }

    public void setNew(Boolean aNew) {
        isNew = aNew;
    }

    public String getTutorialUrl() {
        return tutorialUrl;
    }

    public void setTutorialUrl(String tutorialUrl) {
        this.tutorialUrl = tutorialUrl;
    }
}
