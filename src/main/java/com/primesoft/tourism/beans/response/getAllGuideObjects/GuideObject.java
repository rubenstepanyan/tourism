package com.primesoft.tourism.beans.response.getAllGuideObjects;

import java.util.Date;

public class GuideObject {

    private Integer id;
    private Boolean confirmed;
    private String firstName;
    private String lastName;
    private String birthDay;
    private String mail;
    private String phone;
    private Date registrationDate;
    private Integer osType;
    private String photoUrl;
    private Boolean suspended;
    private String city;
    private Integer sex;
    private String accountNumber;
    private Boolean approved;
    private String languageSkills;

    public GuideObject(Integer id, Boolean confirmed, String firstName, String lastName, String birthDay, String mail, String phone, Date registrationDate, Integer osType, String photoUrl, Boolean suspended, String city, Integer sex, String accountNumber, Boolean approved, String languageSkills) {
        this.id = id;
        this.confirmed = confirmed;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDay = birthDay;
        this.mail = mail;
        this.phone = phone;
        this.registrationDate = registrationDate;
        this.osType = osType;
        this.photoUrl = photoUrl;
        this.suspended = suspended;
        this.city = city;
        this.sex = sex;
        this.accountNumber = accountNumber;
        this.approved = approved;
        this.languageSkills = languageSkills;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Integer getOsType() {
        return osType;
    }

    public void setOsType(Integer osType) {
        this.osType = osType;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public String getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(String languageSkills) {
        this.languageSkills = languageSkills;
    }
}
