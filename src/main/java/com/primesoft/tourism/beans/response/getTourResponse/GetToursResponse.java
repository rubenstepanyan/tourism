package com.primesoft.tourism.beans.response.getTourResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GetToursResponse {

    private List<TourContent> contents;
    private Integer pageCount = 1;
    private Integer itemsCount;
    private Integer minPrice = Integer.MAX_VALUE;
    private Integer maxPrice = 0;


    public GetToursResponse(List<TourContent> contents, Integer page) {

        itemsCount = contents.size();

        for (TourContent tour: contents) {

            if(tour.getPrice()<minPrice){
                minPrice = tour.getPrice();
            }

            if(tour.getPrice()>maxPrice){
                maxPrice = tour.getPrice();
            }

        }

        if(page!=null && contents.size()>12){
            pageCount = (contents.size()/12);

            if(contents.size()%12>0){
                pageCount+=1;
            }

            int startPosition = (page-1)*12;
            int endPosition = page*12;

            if(startPosition > contents.size()){
                this.contents = new ArrayList<>();
                return;
            }

            if(endPosition > contents.size()){
                endPosition = contents.size();
            }

            this.contents = contents.subList(startPosition, endPosition);

        }else {
            Collections.shuffle(contents);
            this.contents = contents;
        }
    }


    public List<TourContent> getContents() {
        return contents;
    }

    public void setContents(List<TourContent> contents) {
        this.contents = contents;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public Integer getItemsCount() {
        return itemsCount;
    }

    public void setItemsCount(Integer itemsCount) {
        this.itemsCount = itemsCount;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Integer maxPrice) {
        this.maxPrice = maxPrice;
    }
}
