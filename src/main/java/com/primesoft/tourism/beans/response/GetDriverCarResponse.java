package com.primesoft.tourism.beans.response;

import com.primesoft.tourism.beans.tableBeans.Car;

import java.util.List;

public class GetDriverCarResponse {

    private List<Car> car;

    public GetDriverCarResponse(List<Car> car) {
        this.car = car;
    }

    public List<Car> getCar() {
        return car;
    }

    public void setCar(List<Car> car) {
        this.car = car;
    }
}
