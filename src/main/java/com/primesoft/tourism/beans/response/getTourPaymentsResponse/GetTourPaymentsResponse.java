package com.primesoft.tourism.beans.response.getTourPaymentsResponse;

import java.util.List;

public class GetTourPaymentsResponse {

    private List<GetTourPaymentsContent> contents;

    public GetTourPaymentsResponse(List<GetTourPaymentsContent> contents) {
        this.contents = contents;
    }

    public List<GetTourPaymentsContent> getContents() {
        return contents;
    }

    public void setContents(List<GetTourPaymentsContent> contents) {
        this.contents = contents;
    }
}
