package com.primesoft.tourism.beans.response.getAllBlogResponse;

public class BlogContentResponse {

    private Object id;
    private Object iconUrl;
    private Object commentCount;
    private Object title;

    public BlogContentResponse(Object[] objects) {
        this.id = objects[0];
        this.iconUrl = objects[1];
        this.commentCount = objects[2];
        this.title = objects[3];
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(Object iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Object getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Object commentCount) {
        this.commentCount = commentCount;
    }

    public Object getTitle() {
        return title;
    }

    public void setTitle(Object title) {
        this.title = title;
    }
}
