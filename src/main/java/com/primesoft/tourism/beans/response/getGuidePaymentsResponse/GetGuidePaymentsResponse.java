package com.primesoft.tourism.beans.response.getGuidePaymentsResponse;

import java.util.List;

public class GetGuidePaymentsResponse {

   private List<GuidePaymentContent> contents;

    public GetGuidePaymentsResponse(List<GuidePaymentContent> contents) {
        this.contents = contents;
    }

    public List<GuidePaymentContent> getContents() {
        return contents;
    }

    public void setContents(List<GuidePaymentContent> contents) {
        this.contents = contents;
    }
}
