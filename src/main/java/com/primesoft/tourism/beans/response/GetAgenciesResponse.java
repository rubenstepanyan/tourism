package com.primesoft.tourism.beans.response;

import java.util.Date;

public class GetAgenciesResponse {

    private Integer id;
    private String name;
    private String city;
    private String address;
    private String mail;
    private String phone;
    private String logoUrl;
    private Date registrationDate;
    private Integer balance;
    private Integer incomingPercent;
    private Boolean confirmed;
    private Double currentRating;
    private String accountNumber;
    private Boolean suspended;
    private Boolean approved;

    public GetAgenciesResponse(Integer id, String name, String city, String address, String mail, String phone, String logoUrl, Date registrationDate, Integer balance, Integer incomingPercent, Boolean confirmed, Double currentRating,String accountNumber, Boolean suspended, Boolean approved) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.address = address;
        this.mail = mail;
        this.phone = phone;
        this.logoUrl = logoUrl;
        this.registrationDate = registrationDate;
        this.balance = balance;
        this.incomingPercent = incomingPercent;
        this.confirmed = confirmed;
        this.currentRating = currentRating;
        this.accountNumber = accountNumber;
        this.suspended = suspended;
        this.approved = approved;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getIncomingPercent() {
        return incomingPercent;
    }

    public void setIncomingPercent(Integer incomingPercent) {
        this.incomingPercent = incomingPercent;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Double getCurrentRating() {
        return currentRating;
    }

    public void setCurrentRating(Double currentRating) {
        this.currentRating = currentRating;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }
}
