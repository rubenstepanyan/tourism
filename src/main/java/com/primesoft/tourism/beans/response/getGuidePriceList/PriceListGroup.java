package com.primesoft.tourism.beans.response.getGuidePriceList;

import com.primesoft.tourism.beans.tableBeans.GuidePriceList;

import java.util.ArrayList;
import java.util.List;

public class PriceListGroup {

    private List<GuidePriceList> guidePriceLists = new ArrayList<>();

    public List<GuidePriceList> getGuidePriceLists() {
        return guidePriceLists;
    }

    public void setGuidePriceLists(List<GuidePriceList> guidePriceLists) {
        this.guidePriceLists = guidePriceLists;
    }
}
