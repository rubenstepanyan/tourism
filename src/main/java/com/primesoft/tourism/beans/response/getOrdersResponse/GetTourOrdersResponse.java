package com.primesoft.tourism.beans.response.getOrdersResponse;

import java.util.List;

public class GetTourOrdersResponse {

    private List<GetTourOrderContentResponse> tourContents;

    private List<GetGuideOrderContentsResponse> guideContents;

    public GetTourOrdersResponse(List<GetTourOrderContentResponse> tourContents, List<GetGuideOrderContentsResponse> guideContents) {

        if(tourContents!=null){
            this.tourContents = tourContents;
        }

        if(guideContents!=null) {
            this.guideContents = guideContents;
        }

    }



    public List<GetTourOrderContentResponse> getTourContents() {
        return tourContents;
    }

    public void setTourContents(List<GetTourOrderContentResponse> tourContents) {
        this.tourContents = tourContents;
    }

    public List<GetGuideOrderContentsResponse> getGuideContents() {
        return guideContents;
    }

    public void setGuideContents(List<GetGuideOrderContentsResponse> guideContents) {
        this.guideContents = guideContents;
    }

}
