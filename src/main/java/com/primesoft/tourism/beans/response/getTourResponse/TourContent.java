package com.primesoft.tourism.beans.response.getTourResponse;

public class TourContent {

    private Object id;
    private Object freeSeats;
    private Object iconUrl;
    private Integer price;
    private Object hour;
    private Object name;
    private Object daysOfWeek;
    private Object startDate;
    private Object startTime;
    private Object startAddress;
    private Object languageSkills;
    private Object rating;
    private Object photoUrl;
    private Object tourName;
    private Object carInfo;
    private Object carPhotoUrl;
    private Object description;
    private Object lat;
    private Object lng;
    private Object guided;
    private Object otDescription;
    private Object minute;
    private Object pickMeUp;
    private Object tourType;
    private Object startCity;
    private Object transfer;
    private Object approved;

    public TourContent(Object[] objects) {
        this.id = objects[0];
        this.freeSeats = objects[1];
        this.iconUrl = objects[2];
        this.price = (Integer) objects[3];
        this.hour = objects[4];
        this.name = objects[5];
        this.daysOfWeek = objects[6];
        this.startDate = objects[7];
        this.startTime = objects[8];
        this.startAddress = objects[9];
        this.languageSkills = objects[10];
        this.rating = objects[11];
        this.photoUrl = objects[12];
        this.tourName = objects[13];
        this.carInfo = objects[14];
        this.carPhotoUrl = objects[15];
        this.description = objects[16];
        this.lat = objects[17];
        this.lng = objects[18];
        this.guided = objects[19];
        this.otDescription = objects[20];
        this.minute = objects[21];
        this.pickMeUp = objects[22];
        this.tourType = objects[23];
        this.startCity = objects[24];
        this.transfer = objects[25];
        this.approved = objects[26];
    }


    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getFreeSeats() {
        return freeSeats;
    }

    public void setFreeSeats(Object freeSeats) {
        this.freeSeats = freeSeats;
    }

    public Object getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(Object iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Object getHour() {
        return hour;
    }

    public void setHour(Object hour) {
        this.hour = hour;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public Object getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(Object daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    public Object getStartDate() {
        return startDate;
    }

    public void setStartDate(Object startDate) {
        this.startDate = startDate;
    }

    public Object getStartTime() {
        return startTime;
    }

    public void setStartTime(Object startTime) {
        this.startTime = startTime;
    }

    public Object getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(Object startAddress) {
        this.startAddress = startAddress;
    }

    public Object getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(Object languageSkills) {
        this.languageSkills = languageSkills;
    }

    public Object getRating() {
        return rating;
    }

    public void setRating(Object rating) {
        this.rating = rating;
    }

    public Object getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(Object photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Object getTourName() {
        return tourName;
    }

    public void setTourName(Object tourName) {
        this.tourName = tourName;
    }

    public Object getCarInfo() {
        return carInfo;
    }

    public void setCarInfo(Object carInfo) {
        this.carInfo = carInfo;
    }

    public Object getCarPhotoUrl() {
        return carPhotoUrl;
    }

    public void setCarPhotoUrl(Object carPhotoUrl) {
        this.carPhotoUrl = carPhotoUrl;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Object getLat() {
        return lat;
    }

    public void setLat(Object lat) {
        this.lat = lat;
    }

    public Object getLng() {
        return lng;
    }

    public void setLng(Object lng) {
        this.lng = lng;
    }

    public Object getGuided() {
        return guided;
    }

    public void setGuided(Object guided) {
        this.guided = guided;
    }

    public Object getOtDescription() {
        return otDescription;
    }

    public void setOtDescription(Object otDescription) {
        this.otDescription = otDescription;
    }

    public Object getMinute() {
        return minute;
    }

    public void setMinute(Object minute) {
        this.minute = minute;
    }

    public Object getPickMeUp() {
        return pickMeUp;
    }

    public void setPickMeUp(Object pickMeUp) {
        this.pickMeUp = pickMeUp;
    }

    public Object getTourType() {
        return tourType;
    }

    public void setTourType(Object tourType) {
        this.tourType = tourType;
    }

    public Object getStartCity() {
        return startCity;
    }

    public void setStartCity(Object startCity) {
        this.startCity = startCity;
    }

    public Object getTransfer() {
        return transfer;
    }

    public void setTransfer(Object transfer) {
        this.transfer = transfer;
    }

    public Object getApproved() {
        return approved;
    }

    public void setApproved(Object approved) {
        this.approved = approved;
    }
}
