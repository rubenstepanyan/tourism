package com.primesoft.tourism.beans.response.getAllTouesResponse;

import java.util.List;

public class GetAllTourResponse {

    private List<GetAllTourContentResponse> contents;

    public GetAllTourResponse(List<GetAllTourContentResponse> contents) {
        this.contents = contents;
    }

    public List<GetAllTourContentResponse> getContents() {
        return contents;
    }

    public void setContents(List<GetAllTourContentResponse> contents) {
        this.contents = contents;
    }
}
