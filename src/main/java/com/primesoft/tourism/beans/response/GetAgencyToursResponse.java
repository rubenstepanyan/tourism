package com.primesoft.tourism.beans.response;

import com.primesoft.tourism.beans.response.getAllTouesResponse.GetAllTourContentResponse;

import java.util.List;

public class GetAgencyToursResponse {

    private List<GetAllTourContentResponse> tours;

    public GetAgencyToursResponse(List<GetAllTourContentResponse> tours) {
        this.tours = tours;
    }

    public List<GetAllTourContentResponse> getTours() {
        return tours;
    }

    public void setTours(List<GetAllTourContentResponse> tours) {
        this.tours = tours;
    }
}
