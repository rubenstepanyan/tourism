package com.primesoft.tourism.beans.response.getCustomerHistory;

import com.primesoft.tourism.utils.Util;

public class GetCustomerGuideOrderHistoryContent {

    private Object id;
    private Object language;
    private Object quantityType;
    private Object totalPrice;
    private Object status;
    private Object dateFrom;
    private Object dateTo;
    private Object guideFullName;
    private Object age;
    private Object customerName;
    private Object customerId;
    private Object pricePerDay;
    private Integer workDays;
    private Object photoUrl;
    private Object rating;
    private Object direction;
    private Object customerRating;

    public GetCustomerGuideOrderHistoryContent(Object objects[]) {
        this.id = objects[0];
        this.language = objects[1];
        this.quantityType = objects[2];
        this.totalPrice = objects[3];
        this.status = objects[4];
        this.dateFrom = objects[5];
        this.dateTo = objects[6];
        this.guideFullName = objects[7];
        this.age = objects[8];
        this.customerName = objects[9];
        this.customerId = objects[10];
        this.pricePerDay = objects[11];
        this.photoUrl = objects[12];
        this.rating = objects[13];
        this.direction = objects[14];
        this.customerRating = objects[15];
        this.workDays = Util.getDateDiffInDay(dateFrom.toString(),dateTo.toString())+1;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getLanguage() {
        return language;
    }

    public void setLanguage(Object language) {
        this.language = language;
    }

    public Object getQuantityType() {
        return quantityType;
    }

    public void setQuantityType(Object quantityType) {
        this.quantityType = quantityType;
    }

    public Object getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Object totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Object dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Object getDateTo() {
        return dateTo;
    }

    public void setDateTo(Object dateTo) {
        this.dateTo = dateTo;
    }

    public Object getGuideFullName() {
        return guideFullName;
    }

    public void setGuideFullName(Object guideFullName) {
        this.guideFullName = guideFullName;
    }

    public Object getAge() {
        return age;
    }

    public void setAge(Object age) {
        this.age = age;
    }

    public Object getCustomerName() {
        return customerName;
    }

    public void setCustomerName(Object customerName) {
        this.customerName = customerName;
    }

    public Object getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Object customerId) {
        this.customerId = customerId;
    }

    public Object getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(Object pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public Integer getWorkDays() {
        return workDays;
    }

    public void setWorkDays(Integer workDays) {
        this.workDays = workDays;
    }

    public Object getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(Object photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Object getRating() {
        return rating;
    }

    public void setRating(Object rating) {
        this.rating = rating;
    }

    public Object getDirection() {
        return direction;
    }

    public void setDirection(Object direction) {
        this.direction = direction;
    }

    public Object getCustomerRating() {
        return customerRating;
    }

    public void setCustomerRating(Object customerRating) {
        this.customerRating = customerRating;
    }
}
