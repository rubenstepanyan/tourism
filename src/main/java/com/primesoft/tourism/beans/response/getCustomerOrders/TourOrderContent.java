package com.primesoft.tourism.beans.response.getCustomerOrders;

import com.primesoft.tourism.utils.Constants;

public class TourOrderContent {

    private Object id;
    private Object quantity;
    private Object price;
    private Object status;
    private Object tourDate;
    private Object carInfo;
    private Object name;
    private Object iconUrl;
    private Object startAddress;
    private Object tourName;
    private Object type;
    private Object languageSkills;
    private Object guided;
    private Object city;
    private Object hour;
    private Object minute;
    private Object qr;
    private Object payed;
    private Object errorMessage;
    private Object touristAddress;
    private Object lat;
    private Object lng;
    private Object phoneNumber;
    private Object offeredTourPhoto;
    private Object carPhoto;
    private Object isTransfer;
    private Object rating;
    private Object tourType;

    public TourOrderContent(Object[] objects){

        this.id = objects[0];
        this.quantity = objects[1];
        this.price = objects[2];
        this.status = objects[3];
        this.tourDate = objects[4];
        this.carInfo = objects[5];
        this.name = objects[6];
        this.iconUrl = objects[7];
        this.startAddress = objects[8];
        this.tourName = objects[9];
        this.type = objects[10];
        this.languageSkills = objects[11];
        this.guided = objects[12];
        this.city = objects[13];
        this.hour = objects[14];
        this.minute = objects[15];
        if(this.status.equals(Constants.CUSTOMER_PAYED)){
            this.qr = objects[16];
            this.phoneNumber = objects[17];
        }
        this.payed = objects[18];
        this.errorMessage = objects[19];
        this.touristAddress = objects[20];
        this.lat = objects[21];
        this.lng = objects[22];
        this.offeredTourPhoto = objects[23];
        this.carPhoto = objects[24];
        this.isTransfer = objects[25];
        this.rating = objects[26];
        this.tourType = objects[27];
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getQuantity() {
        return quantity;
    }

    public void setQuantity(Object quantity) {
        this.quantity = quantity;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getTourDate() {
        return tourDate;
    }

    public void setTourDate(Object tourDate) {
        this.tourDate = tourDate;
    }

    public Object getCarInfo() {
        return carInfo;
    }

    public void setCarInfo(Object carInfo) {
        this.carInfo = carInfo;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public Object getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(Object iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Object getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(Object startAddress) {
        this.startAddress = startAddress;
    }

    public Object getTourName() {
        return tourName;
    }

    public void setTourName(Object tourName) {
        this.tourName = tourName;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public Object getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(Object languageSkills) {
        this.languageSkills = languageSkills;
    }

    public Object getGuided() {
        return guided;
    }

    public void setGuided(Object guided) {
        this.guided = guided;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getHour() {
        return hour;
    }

    public void setHour(Object hour) {
        this.hour = hour;
    }

    public Object getMinute() {
        return minute;
    }

    public void setMinute(Object minute) {
        this.minute = minute;
    }

    public Object getQr() {
        return qr;
    }

    public void setQr(Object qr) {
        this.qr = qr;
    }

    public Object getPayed() {
        return payed;
    }

    public void setPayed(Object payed) {
        this.payed = payed;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Object getTouristAddress() {
        return touristAddress;
    }

    public void setTouristAddress(Object touristAddress) {
        this.touristAddress = touristAddress;
    }

    public Object getLat() {
        return lat;
    }

    public void setLat(Object lat) {
        this.lat = lat;
    }

    public Object getLng() {
        return lng;
    }

    public void setLng(Object lng) {
        this.lng = lng;
    }

    public Object getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Object phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    public Object getOfferedTourPhoto() {
        return offeredTourPhoto;
    }

    public void setOfferedTourPhoto(Object offeredTourPhoto) {
        this.offeredTourPhoto = offeredTourPhoto;
    }

    public Object getCarPhoto() {
        return carPhoto;
    }

    public void setCarPhoto(Object carPhoto) {
        this.carPhoto = carPhoto;
    }

    public Object getIsTransfer() {
        return isTransfer;
    }

    public void setIsTransfer(Object isTransfer) {
        this.isTransfer = isTransfer;
    }

    public Object getRating() {
        return rating;
    }

    public void setRating(Object rating) {
        this.rating = rating;
    }

    public Object getTourType() {
        return tourType;
    }

    public void setTourType(Object tourType) {
        this.tourType = tourType;
    }
}
