package com.primesoft.tourism.beans.response.getOurPartnersResponse;

public class GetOurPartnersContent {

    private Integer id;
    private String name;
    private String accountNumber;

    public GetOurPartnersContent(Integer id, String name, String accountNumber) {
        this.id = id;
        this.name = name;
        this.accountNumber = accountNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
