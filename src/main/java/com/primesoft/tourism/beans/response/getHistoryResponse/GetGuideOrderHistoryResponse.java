package com.primesoft.tourism.beans.response.getHistoryResponse;



import java.util.ArrayList;
import java.util.List;

public class GetGuideOrderHistoryResponse {

    private List<GetGuideOrderHistoryContentResponse> guideContents;
    private Integer pageCount = 1;

    public GetGuideOrderHistoryResponse(List<GetGuideOrderHistoryContentResponse> guideContents,Integer page) {

        if(page!=null && guideContents.size()>20){
            pageCount = (guideContents.size()/20);

            if(guideContents.size()%20>0){
                pageCount+=1;
            }

            int startPosition = (page-1)*20;
            int endPosition = page*20;

            if(startPosition > guideContents.size()){
                this.guideContents = new ArrayList<>();
                return;
            }

            if(endPosition > guideContents.size()){
                endPosition = guideContents.size();
            }

            this.guideContents = guideContents.subList(startPosition, endPosition);

        }else {
            this.guideContents = guideContents;
        }
    }

    public List<GetGuideOrderHistoryContentResponse> getGuideContents() {
        return guideContents;
    }

    public void setGuideContents(List<GetGuideOrderHistoryContentResponse> guideContents) {
        this.guideContents = guideContents;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }
}
