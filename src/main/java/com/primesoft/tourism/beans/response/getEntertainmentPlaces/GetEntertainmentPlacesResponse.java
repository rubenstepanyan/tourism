package com.primesoft.tourism.beans.response.getEntertainmentPlaces;

import java.util.List;

public class GetEntertainmentPlacesResponse {

    private Long itemCount;
    private Long pageCount;
    private List<Content> content;

    public GetEntertainmentPlacesResponse(Long itemCount, Long pageCount, List<Content> content) {
        this.itemCount = itemCount;
        this.pageCount = pageCount;
        this.content = content;
    }


    public GetEntertainmentPlacesResponse(Long pageCount, List<Content> content) {
        this.pageCount = pageCount;
        this.content = content;
    }


    public Long getItemCount() {
        return itemCount;
    }

    public void setItemCount(Long itemCount) {
        this.itemCount = itemCount;
    }

    public Long getPageCount() {
        return pageCount;
    }

    public void setPageCount(Long pageCount) {
        this.pageCount = pageCount;
    }

    public List<Content> getContent() {
        return content;
    }

    public void setContent(List<Content> content) {
        this.content = content;
    }
}
