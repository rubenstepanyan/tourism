package com.primesoft.tourism.beans.response.getCouponPeynetsResponse;

public class GetCouponPaymentContent {

    private Object id;
    private Object price;
    private Object payed;
    private Object EPName;
    private Object customerName;
    private Object customerMail;
    private Object customerPhone;

    public GetCouponPaymentContent(Object[] objects) {
        this.id = objects[0];
        this.price = objects[1];
        this.payed = objects[2];
        this.EPName = objects[3];
        this.customerName = objects[4];
        this.customerMail = objects[5];
        this.customerPhone = objects[6];
    }


    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public Object getPayed() {
        return payed;
    }

    public void setPayed(Object payed) {
        this.payed = payed;
    }

    public Object getEPName() {
        return EPName;
    }

    public void setEPName(Object EPName) {
        this.EPName = EPName;
    }

    public Object getCustomerName() {
        return customerName;
    }

    public void setCustomerName(Object customerName) {
        this.customerName = customerName;
    }

    public Object getCustomerMail() {
        return customerMail;
    }

    public void setCustomerMail(Object customerMail) {
        this.customerMail = customerMail;
    }

    public Object getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(Object customerPhone) {
        this.customerPhone = customerPhone;
    }
}
