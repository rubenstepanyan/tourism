package com.primesoft.tourism.beans.response;


public class GetCustomNotificationResponse {

    private Integer action;

    private String message;


    public GetCustomNotificationResponse(Integer action, String message) {
        this.action = action;
        this.message = message;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
