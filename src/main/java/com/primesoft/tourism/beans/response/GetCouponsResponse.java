package com.primesoft.tourism.beans.response;

import java.util.ArrayList;

public class GetCouponsResponse {

    private ArrayList<PurchasedCoupons> coupons;


    public GetCouponsResponse(ArrayList<PurchasedCoupons> coupons) {
        for (PurchasedCoupons purchasedCoupon:coupons) {
            if(purchasedCoupon.getPayed().equals(0)){
                purchasedCoupon.setCouponCode(null);
            }
        }
        this.coupons = coupons;
    }

    public ArrayList<PurchasedCoupons> getCoupons() {
        return coupons;
    }

    public void setCoupons(ArrayList<PurchasedCoupons> coupons) {
        this.coupons = coupons;
    }
}
