package com.primesoft.tourism.beans.response.getEntertainmentPlaces;

public class Content {

    private Object id;
    private Object name;
    private Object iconUrl;
    private Object discount;
    private Object price;
    private Object maxPersons;
    private Object entPrice;
    private Object lat;
    private Object lng;
    private Object address;


    public Content(Object[] result) {
        this.id = result[0];
        this.name = result[1];
        this.iconUrl = result[2];
        this.discount = result[3];
        this.price = result[4];
        this.maxPersons = result[5];
        this.entPrice = result[6];
        this.lat = result[7];
        this.lng = result[8];
        this.address = result[9];
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public Object getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(Object iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Object getDiscount() {
        return discount;
    }

    public void setDiscount(Object discount) {
        this.discount = discount;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public Object getMaxPersons() {
        return maxPersons;
    }

    public void setMaxPersons(Object maxPersons) {
        this.maxPersons = maxPersons;
    }

    public Object getEntPrice() {
        return entPrice;
    }

    public void setEntPrice(Object entPrice) {
        this.entPrice = entPrice;
    }

    public Object getLat() {
        return lat;
    }

    public void setLat(Object lat) {
        this.lat = lat;
    }

    public Object getLng() {
        return lng;
    }

    public void setLng(Object lng) {
        this.lng = lng;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }
}
