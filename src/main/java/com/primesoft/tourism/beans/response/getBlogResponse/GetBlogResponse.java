package com.primesoft.tourism.beans.response.getBlogResponse;

import com.primesoft.tourism.beans.tableBeans.Blog;
import com.primesoft.tourism.beans.tableBeans.BlogContent;

import java.util.List;

public class GetBlogResponse {

    private Blog blog;

    private List<BlogContent> contents;

    public GetBlogResponse(Blog blog, List<BlogContent> contents) {
        this.blog = blog;
        this.contents = contents;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public List<BlogContent> getContents() {
        return contents;
    }

    public void setContents(List<BlogContent> contents) {
        this.contents = contents;
    }
}
