package com.primesoft.tourism.beans.response.getOfferedTourCustomerResponse;

import java.util.List;

public class GetOfferedToursCustomerRequest {

    List<Content> contents;

    public GetOfferedToursCustomerRequest(List<Content> contents) {
        this.contents = contents;
    }

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }
}
