package com.primesoft.tourism.beans.response.getBlogCommentResponse;

import java.util.List;

public class GetBlogCommentsResponse {

    private List<BlogCommentContent> contents;

    public GetBlogCommentsResponse(List<BlogCommentContent> contents) {
        this.contents = contents;
    }

    public List<BlogCommentContent> getContents() {
        return contents;
    }

    public void setContents(List<BlogCommentContent> contents) {
        this.contents = contents;
    }
}
