package com.primesoft.tourism.beans.response.getCustomerHistory;

public class GetCustomerOrderHistoryContent {

    private Object id;
    private Object quantity;
    private Object totalPrice;
    private Object status;
    private Object tourDate;
    private Object carInfo;
    private Object name;
    private Object startAddress;
    private Object tourName;
    private Object tourType;
    private Object guided;
    private Object languageSkills;
    private Object startCity;
    private Object hour;
    private Object localName;
    private Object startTime;
    private Object isAgency;
    private Object rating;
    private Object price;
    private Object customerId;
    private Object customerName;
    private Object photoUrl;
    private Object minute;
    private Object transfer;
    private Object customerRating;

    public GetCustomerOrderHistoryContent(Object objects[]) {
        this.id = objects[0];
        this.quantity = objects[1];
        this.totalPrice = objects[2];
        this.status = objects[3];
        this.tourDate = objects[4];
        this.carInfo = objects[5];
        this.name = objects[6];
        this.startAddress = objects[7];
        this.tourName = objects[8];
        this.tourType = objects[9];
        this.guided = objects[10];
        this.languageSkills = objects[11];
        this.startCity = objects[12];
        this.hour = objects[13];
        this.localName = objects[14];
        this.startTime = objects[15];
        this.isAgency = objects[16];
        this.rating = objects[17];
        this.price = objects[18];
        this.customerId = objects[19];
        this.customerName = objects[20];
        this.photoUrl = objects[21];
        this.minute = objects[22];
        this.transfer = objects[23];
        this.customerRating = objects[24];
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getQuantity() {
        return quantity;
    }

    public void setQuantity(Object quantity) {
        this.quantity = quantity;
    }

    public Object getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Object totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getTourDate() {
        return tourDate;
    }

    public void setTourDate(Object tourDate) {
        this.tourDate = tourDate;
    }

    public Object getCarInfo() {
        return carInfo;
    }

    public void setCarInfo(Object carInfo) {
        this.carInfo = carInfo;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public Object getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(Object startAddress) {
        this.startAddress = startAddress;
    }

    public Object getTourName() {
        return tourName;
    }

    public void setTourName(Object tourName) {
        this.tourName = tourName;
    }

    public Object getTourType() {
        return tourType;
    }

    public void setTourType(Object tourType) {
        this.tourType = tourType;
    }

    public Object getGuided() {
        return guided;
    }

    public void setGuided(Object guided) {
        this.guided = guided;
    }

    public Object getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(Object languageSkills) {
        this.languageSkills = languageSkills;
    }

    public Object getStartCity() {
        return startCity;
    }

    public void setStartCity(Object startCity) {
        this.startCity = startCity;
    }

    public Object getHour() {
        return hour;
    }

    public void setHour(Object hour) {
        this.hour = hour;
    }

    public Object getLocalName() {
        return localName;
    }

    public void setLocalName(Object localName) {
        this.localName = localName;
    }

    public Object getStartTime() {
        return startTime;
    }

    public void setStartTime(Object startTime) {
        this.startTime = startTime;
    }

    public Object getIsAgency() {
        return isAgency;
    }

    public void setIsAgency(Object isAgency) {
        this.isAgency = isAgency;
    }

    public Object getRating() {
        return rating;
    }

    public void setRating(Object rating) {
        this.rating = rating;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public Object getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Object customerId) {
        this.customerId = customerId;
    }

    public Object getCustomerName() {
        return customerName;
    }

    public void setCustomerName(Object customerName) {
        this.customerName = customerName;
    }

    public Object getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(Object photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Object getMinute() {
        return minute;
    }

    public void setMinute(Object minute) {
        this.minute = minute;
    }

    public Object getTransfer() {
        return transfer;
    }

    public void setTransfer(Object transfer) {
        this.transfer = transfer;
    }

    public Object getCustomerRating() {
        return customerRating;
    }

    public void setCustomerRating(Object customerRating) {
        this.customerRating = customerRating;
    }
}
