package com.primesoft.tourism.beans.response.getCustomerOrders;


public class GuideOrderContent {

    private Object id;
    private Object language;
    private Object quantityType;
    private Object price;
    private Object status;
    private Object dateFrom;
    private Object dateTo;
    private Object fullName;
    private Object age;
    private Object photoUrl;
    private Object qr;
    private Object phone;
    private Object payed;
    private Object errorMessage;
    private Object direction;
    private Object rating;

    public GuideOrderContent(Object[] objects){

        this.id = objects[0];
        this.language = objects[1];
        this.quantityType = objects[2];
        this.price = objects[3];
        this.status = objects[4];
        this.dateFrom = objects[5];
        this.dateTo = objects[6];
        this.fullName = objects[7];
        this.age = objects[8];
        this.photoUrl = objects[9];
        this.payed = objects[12];
        if(payed.equals(1)){
            this.qr = objects[10];
        }
        this.phone = objects[11];
        this.errorMessage = objects[13];
        this.direction = objects[14];
        this.rating = objects[15];
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getLanguage() {
        return language;
    }

    public void setLanguage(Object language) {
        this.language = language;
    }

    public Object getQuantityType() {
        return quantityType;
    }

    public void setQuantityType(Object quantityType) {
        this.quantityType = quantityType;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getFullName() {
        return fullName;
    }

    public void setFullName(Object fullName) {
        this.fullName = fullName;
    }

    public Object getAge() {
        return age;
    }

    public void setAge(Object age) {
        this.age = age;
    }

    public Object getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(Object photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Object getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Object dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Object getDateTo() {
        return dateTo;
    }

    public void setDateTo(Object dateTo) {
        this.dateTo = dateTo;
    }

    public Object getQr() {
        return qr;
    }

    public void setQr(Object qr) {
        this.qr = qr;
    }


    public Object getPhone() {
        return phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }

    public Object getPayed() {
        return payed;
    }

    public void setPayed(Object payed) {
        this.payed = payed;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Object getDirection() {
        return direction;
    }

    public void setDirection(Object direction) {
        this.direction = direction;
    }

    public Object getRating() {
        return rating;
    }

    public void setRating(Object rating) {
        this.rating = rating;
    }
}
