package com.primesoft.tourism.beans.response;

public class FinishOrderResponse {

    private Object id;

    private Object quantity;

    private Object totalPrice;

    private Object tourDate;

    private Object direction;

    private Object photoUrl;

    private Object fullName;

    private Object startAddress;

    private Object touristAddress;


    public FinishOrderResponse(Object objects[]) {
        this.id = objects[0];
        this.quantity = objects[1];
        this.totalPrice = objects[2];
        this.tourDate = objects[3];
        this.direction = objects[4];
        this.photoUrl = objects[5];
        this.fullName = objects[6];
        if(touristAddress != null){
            this.startAddress = objects[8];
        }else{
            this.startAddress = objects[7];
        }
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getQuantity() {
        return quantity;
    }

    public void setQuantity(Object quantity) {
        this.quantity = quantity;
    }

    public Object getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Object totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Object getTourDate() {
        return tourDate;
    }

    public void setTourDate(Object tourDate) {
        this.tourDate = tourDate;
    }

    public Object getDirection() {
        return direction;
    }

    public void setDirection(Object direction) {
        this.direction = direction;
    }

    public Object getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(Object photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Object getFullName() {
        return fullName;
    }

    public void setFullName(Object fullName) {
        this.fullName = fullName;
    }

    public Object getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(Object startAddress) {
        this.startAddress = startAddress;
    }

    public Object getTouristAddress() {
        return touristAddress;
    }

    public void setTouristAddress(Object touristAddress) {
        this.touristAddress = touristAddress;
    }
}
