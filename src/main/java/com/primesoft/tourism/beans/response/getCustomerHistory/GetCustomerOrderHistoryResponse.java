package com.primesoft.tourism.beans.response.getCustomerHistory;

import java.util.ArrayList;
import java.util.List;

public class GetCustomerOrderHistoryResponse {

    private List<GetCustomerOrderHistoryContent> orderContents;
    private Integer pageCount = 1;


    public GetCustomerOrderHistoryResponse(List<GetCustomerOrderHistoryContent> orderContents,Integer page) {
        if(page!=null && orderContents.size()>20){
            pageCount = (orderContents.size()/20);

            if(orderContents.size()%20>0){
                pageCount+=1;
            }

            int startPosition = (page-1)*20;
            int endPosition = page*20;

            if(startPosition > orderContents.size()){
                this.orderContents = new ArrayList<>();
                return;
            }

            if(endPosition > orderContents.size()){
                endPosition = orderContents.size();
            }

            this.orderContents = orderContents.subList(startPosition, endPosition);

        }else {
            this.orderContents = orderContents;
        }
    }

    public List<GetCustomerOrderHistoryContent> getOrderContents() {
        return orderContents;
    }

    public void setOrderContents(List<GetCustomerOrderHistoryContent> orderContents) {
        this.orderContents = orderContents;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }
}
