package com.primesoft.tourism.beans.response.getCustomerHistory;

import java.util.ArrayList;
import java.util.List;

public class GetCustomerGuideOrderHistoryResponse {

    private List<GetCustomerGuideOrderHistoryContent> guideOrderContents;

    private Integer pageCount=1;

    public GetCustomerGuideOrderHistoryResponse(List<GetCustomerGuideOrderHistoryContent> guideOrderContents,Integer page) {
        if(page!=null && guideOrderContents.size()>20){
            pageCount = (guideOrderContents.size()/20);

            if(guideOrderContents.size()%20>0){
                pageCount+=1;
            }

            int startPosition = (page-1)*20;
            int endPosition = page*20;

            if(startPosition > guideOrderContents.size()){
                this.guideOrderContents = new ArrayList<>();
                return;
            }

            if(endPosition > guideOrderContents.size()){
                endPosition = guideOrderContents.size();
            }

            this.guideOrderContents = guideOrderContents.subList(startPosition, endPosition);

        }else {
            this.guideOrderContents = guideOrderContents;
        }
    }

    public List<GetCustomerGuideOrderHistoryContent> getGuideOrderContents() {
        return guideOrderContents;
    }

    public void setGuideOrderContents(List<GetCustomerGuideOrderHistoryContent> guideOrderContents) {
        this.guideOrderContents = guideOrderContents;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }
}
