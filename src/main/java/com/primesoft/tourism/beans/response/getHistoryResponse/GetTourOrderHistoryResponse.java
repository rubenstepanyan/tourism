package com.primesoft.tourism.beans.response.getHistoryResponse;



import java.util.ArrayList;
import java.util.List;

public class GetTourOrderHistoryResponse {

    private List<GetTourOrderHistoryContentResponse> tourContents;
    private Integer pageCount = 1;

    public GetTourOrderHistoryResponse(List<GetTourOrderHistoryContentResponse> tourContents,Integer page) {

        if(page!=null && tourContents.size()>20){
            pageCount = (tourContents.size()/20);

            if(tourContents.size()%20>0){
                pageCount+=1;
            }

            int startPosition = (page-1)*20;
            int endPosition = page*20;

            if(startPosition > tourContents.size()){
                this.tourContents = new ArrayList<>();
                return;
            }

            if(endPosition > tourContents.size()){
                endPosition = tourContents.size();
            }

            this.tourContents = tourContents.subList(startPosition, endPosition);

        }else {
            this.tourContents = tourContents;
        }    }


    public List<GetTourOrderHistoryContentResponse> getTourContents() {
        return tourContents;
    }

    public void setTourContents(List<GetTourOrderHistoryContentResponse> tourContents) {
        this.tourContents = tourContents;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }
}
