package com.primesoft.tourism.beans.response.getAllGuideObjects;

import java.util.List;

public class GetAllGuideObjectResponse {

    List<GuideObject> guideObjects;

    public GetAllGuideObjectResponse(List<GuideObject> guideObjects) {
        this.guideObjects = guideObjects;
    }

    public List<GuideObject> getGuideObjects() {
        return guideObjects;
    }

    public void setGuideObjects(List<GuideObject> guideObjects) {
        this.guideObjects = guideObjects;
    }
}
