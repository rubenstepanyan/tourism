package com.primesoft.tourism.beans.response.getAllAgencyNamesObjects;


import java.util.List;

public class GetAgencyNamesResponse {

    private List<GetAllAgencyNamesObject> agencies;


    public GetAgencyNamesResponse(List<GetAllAgencyNamesObject> agencies) {
        this.agencies = agencies;
    }

    public List<GetAllAgencyNamesObject> getAgencies() {
        return agencies;
    }

    public void setAgencies(List<GetAllAgencyNamesObject> agencies) {
        this.agencies = agencies;
    }
}
