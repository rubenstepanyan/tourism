package com.primesoft.tourism.beans.response;

public class GuideLoginResponse {

    private  String authorization;

    private String notificationToken;

    private Boolean personalGuide;

    public GuideLoginResponse(String authorization,String notificationToken, Boolean personalGuide) {
        this.authorization = authorization;
        this.notificationToken = notificationToken;
        this.personalGuide = personalGuide;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getNotificationToken() {
        return notificationToken;
    }

    public void setNotificationToken(String notificationToken) {
        this.notificationToken = notificationToken;
    }

    public Boolean getPersonalGuide() {
        return personalGuide;
    }

    public void setPersonalGuide(Boolean personalGuide) {
        this.personalGuide = personalGuide;
    }
}
