package com.primesoft.tourism.beans.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ArcaStatusResponse {


    @SerializedName("expiration")
    @Expose
    private String expiration;

    @SerializedName("cardholderName")
    @Expose
    private String cardholderName;

    @SerializedName("depositAmount")
    @Expose
    private Integer depositAmount;

    @SerializedName("currency")
    @Expose
    private String currency;

    @SerializedName("approvalCode")
    @Expose
    private String approvalCode;

    @SerializedName("authCode")
    @Expose
    private Integer authCode;

    @SerializedName("ErrorCode")
    @Expose
    private String errorCode;

    @SerializedName("ErrorMessage")
    @Expose
    private String errorMessage;

    @SerializedName("OrderStatus")
    @Expose
    private Integer orderStatus;

    @SerializedName("OrderNumber")
    @Expose
    private String orderNumber;

    @SerializedName("actionCodeDescription")
    @Expose
    private String actionCodeDescription;

    @SerializedName("Pan")
    @Expose
    private String pan;
    @SerializedName("Amount")
    @Expose
    private Integer amount;
    @SerializedName("Ip")
    @Expose
    private String ip;
    @SerializedName("SvfeResponse")
    @Expose
    private String svfeResponse;


    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public String getCardholderName() {
        return cardholderName;
    }

    public void setCardholderName(String cardholderName) {
        this.cardholderName = cardholderName;
    }

    public Integer getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(Integer depositAmount) {
        this.depositAmount = depositAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public Integer getAuthCode() {
        return authCode;
    }

    public void setAuthCode(Integer authCode) {
        this.authCode = authCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getActionCodeDescription() {
        return actionCodeDescription;
    }

    public void setActionCodeDescription(String actionCodeDescription) {
        this.actionCodeDescription = actionCodeDescription;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getSvfeResponse() {
        return svfeResponse;
    }

    public void setSvfeResponse(String svfeResponse) {
        this.svfeResponse = svfeResponse;
    }
}
