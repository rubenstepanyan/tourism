package com.primesoft.tourism.beans.response;

import com.primesoft.tourism.beans.tableBeans.GuideReservation;

import java.util.List;

public class GetAllReservationResponse {

    private List<GuideReservation> reservations;

    public GetAllReservationResponse(List<GuideReservation> reservations) {
        this.reservations = reservations;
    }

    public List<GuideReservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<GuideReservation> reservations) {
        this.reservations = reservations;
    }
}
