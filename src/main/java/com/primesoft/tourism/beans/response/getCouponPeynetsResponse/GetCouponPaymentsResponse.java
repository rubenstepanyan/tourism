package com.primesoft.tourism.beans.response.getCouponPeynetsResponse;

import java.util.List;

public class GetCouponPaymentsResponse {

   private List<GetCouponPaymentContent> contents;

    public GetCouponPaymentsResponse(List<GetCouponPaymentContent> contents) {
        this.contents = contents;
    }

    public List<GetCouponPaymentContent> getContents() {
        return contents;
    }

    public void setContents(List<GetCouponPaymentContent> contents) {
        this.contents = contents;
    }
}
