package com.primesoft.tourism.beans.response;

import com.primesoft.tourism.beans.tableBeans.Comment;
import com.primesoft.tourism.beans.tableBeans.Customer;

public class AddCommentResponse {
    private Comment comment;
    private Customer customer;

    public AddCommentResponse(Comment comment, Customer customer) {
        this.comment = comment;
        this.customer = customer;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
