package com.primesoft.tourism.beans.response.getAllVideoResponse;

public class VideoContentResponse {

    private  Object id;
    private  Object url;
    private  Object iconUrl;
    private  Object title;

    public VideoContentResponse(Object[] objects) {
       this.id = objects[0];
       this.url = objects[1];
       this.iconUrl = objects[2];
       this.title = objects[3];
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getUrl() {
        return url;
    }

    public void setUrl(Object url) {
        this.url = url;
    }

    public Object getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(Object iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Object getTitle() {
        return title;
    }

    public void setTitle(Object title) {
        this.title = title;
    }
}
