package com.primesoft.tourism.beans.response.GetAllAdministratorResponse;

import java.util.List;

public class GetAllAdministratorResponse {

    private List<GetAllAdministratorsResponseContent> administrators;

    public GetAllAdministratorResponse(List<GetAllAdministratorsResponseContent> administrators) {
        this.administrators = administrators;
    }

    public List<GetAllAdministratorsResponseContent> getAdministrators() {
        return administrators;
    }

    public void setAdministrators(List<GetAllAdministratorsResponseContent> administrators) {
        this.administrators = administrators;
    }
}
