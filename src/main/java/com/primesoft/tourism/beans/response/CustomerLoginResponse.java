package com.primesoft.tourism.beans.response;

public class CustomerLoginResponse {

    private String authorization;

    public CustomerLoginResponse(String authorization) {
        this.authorization = authorization;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }
}
