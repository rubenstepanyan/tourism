package com.primesoft.tourism.beans.response;

import com.primesoft.tourism.beans.tableBeans.Follower;

import java.util.List;

public class GetFollowersResponse {

    private List<Follower> followers;

    public GetFollowersResponse(List<Follower> followers) {
        this.followers = followers;
    }

    public List<Follower> getFollowers() {
        return followers;
    }

    public void setFollowers(List<Follower> followers) {
        this.followers = followers;
    }
}
