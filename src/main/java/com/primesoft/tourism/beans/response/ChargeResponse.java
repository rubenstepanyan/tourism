package com.primesoft.tourism.beans.response;

public class ChargeResponse {


    private String formUrl;
    private String orderId;

    public ChargeResponse(String formUrl, String orderId) {
        this.formUrl=formUrl;
        this.orderId=orderId;
    }


    public String getFormUrl() {
        return formUrl;
    }

    public void setFormUrl(String formUrl) {
        this.formUrl = formUrl;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
