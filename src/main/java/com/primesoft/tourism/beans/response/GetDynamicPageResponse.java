package com.primesoft.tourism.beans.response;

import com.primesoft.tourism.beans.tableBeans.DynamicPage;
import com.primesoft.tourism.beans.tableBeans.DynamicPagePhoto;

import java.util.List;

public class GetDynamicPageResponse {

    private DynamicPage dynamicPage;

    private Object content;

    private List<DynamicPagePhoto> photos;

    public GetDynamicPageResponse(DynamicPage dynamicPage, Object content, List<DynamicPagePhoto> photos) {
        this.dynamicPage = dynamicPage;
        this.content = content;
        this.photos = photos;
    }

    public DynamicPage getDynamicPage() {
        return dynamicPage;
    }

    public void setDynamicPage(DynamicPage dynamicPage) {
        this.dynamicPage = dynamicPage;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public List<DynamicPagePhoto> getPhotos() {
        return photos;
    }

    public void setPhotos(List<DynamicPagePhoto> photos) {
        this.photos = photos;
    }
}
