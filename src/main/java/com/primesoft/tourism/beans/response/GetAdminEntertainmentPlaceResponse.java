package com.primesoft.tourism.beans.response;

import com.primesoft.tourism.beans.tableBeans.EntertainmentPlace;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceInfo;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlacePhoto;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceWorkingDays;

import java.util.List;

public class GetAdminEntertainmentPlaceResponse {


    private EntertainmentPlace place;
    private List<EntertainmentPlaceInfo> info;
    private List<EntertainmentPlaceWorkingDays> workingDays;
    private List<EntertainmentPlacePhoto> urls;

    public GetAdminEntertainmentPlaceResponse(EntertainmentPlace place, List<EntertainmentPlaceInfo> info, List<EntertainmentPlaceWorkingDays> workingDays, List<EntertainmentPlacePhoto> urls) {
        this.place = place;
        this.info = info;
        this.workingDays = workingDays;
        this.urls = urls;
    }

    public EntertainmentPlace getPlace() {
        return place;
    }

    public void setPlace(EntertainmentPlace place) {
        this.place = place;
    }

    public List<EntertainmentPlaceInfo> getInfo() {
        return info;
    }

    public void setInfo(List<EntertainmentPlaceInfo> info) {
        this.info = info;
    }

    public List<EntertainmentPlaceWorkingDays> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(List<EntertainmentPlaceWorkingDays> workingDays) {
        this.workingDays = workingDays;
    }

    public List<EntertainmentPlacePhoto> getUrls() {
        return urls;
    }

    public void setUrls(List<EntertainmentPlacePhoto> urls) {
        this.urls = urls;
    }
}
