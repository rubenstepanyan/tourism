package com.primesoft.tourism.beans.response;

public class ConfirmAgencyResponse {


    private String authorization;

    public ConfirmAgencyResponse(String authorization) {
        this.authorization = authorization;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }
}
