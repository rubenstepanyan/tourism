package com.primesoft.tourism.beans.response.getOrdersResponse;

public class GetTourOrderContentResponse {

    private Object id;
    private Object quantity;
    private Object price;
    private Object name;
    private Object tourName;
    private Object fullName;
    private Object tourType;
    private Object tourDate;
    private Object status;
    private Object touristAddress;
    private Object lat;
    private Object lng;
    private Object startAddress;
    private Object transfer;
    private Object startCity;
    private Object photoUrl;
    private Object nationality;
    private Object phone;

    public GetTourOrderContentResponse(Object objects[]) {
        this.id = objects[0];
        this.quantity = objects[1];
        this.price = objects[2];
        this.name = objects[3];
        this.tourName = objects[4];
        this.fullName = objects[5];
        this.tourType = objects[6];
        this.tourDate = objects[7];
        this.status = objects[8];
        this.touristAddress = objects[9];
        this.lat = objects[10];
        this.lng= objects[11];
        this.startAddress= objects[12];
        this.transfer= objects[13];
        this.startCity= objects[14];
        this.photoUrl= objects[15];
        this.nationality= objects[16];
        this.phone= objects[17];
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getQuantity() {
        return quantity;
    }

    public void setQuantity(Object quantity) {
        this.quantity = quantity;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public Object getTourName() {
        return tourName;
    }

    public void setTourName(Object tourName) {
        this.tourName = tourName;
    }

    public Object getFullName() {
        return fullName;
    }

    public void setFullName(Object fullName) {
        this.fullName = fullName;
    }

    public Object getTourType() {
        return tourType;
    }

    public void setTourType(Object tourType) {
        this.tourType = tourType;
    }

    public Object getTourDate() {
        return tourDate;
    }

    public void setTourDate(Object tourDate) {
        this.tourDate = tourDate;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getTouristAddress() {
        return touristAddress;
    }

    public void setTouristAddress(Object touristAddress) {
        this.touristAddress = touristAddress;
    }

    public Object getLat() {
        return lat;
    }

    public void setLat(Object lat) {
        this.lat = lat;
    }

    public Object getLng() {
        return lng;
    }

    public void setLng(Object lng) {
        this.lng = lng;
    }

    public Object getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(Object startAddress) {
        this.startAddress = startAddress;
    }

    public Object getTransfer() {
        return transfer;
    }

    public void setTransfer(Object transfer) {
        this.transfer = transfer;
    }

    public Object getStartCity() {
        return startCity;
    }

    public void setStartCity(Object startCity) {
        this.startCity = startCity;
    }

    public Object getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(Object photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Object getNationality() {
        return nationality;
    }

    public void setNationality(Object nationality) {
        this.nationality = nationality;
    }

    public Object getPhone() {
        return phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }
}
