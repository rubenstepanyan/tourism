package com.primesoft.tourism.beans.response;

import com.primesoft.tourism.beans.tableBeans.Car;

import java.util.List;

public class GetAgencyCarsResponse {

    private List<Car> cars;

    public GetAgencyCarsResponse(List<Car> cars) {
        this.cars = cars;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }
}
