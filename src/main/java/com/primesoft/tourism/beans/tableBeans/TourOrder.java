package com.primesoft.tourism.beans.tableBeans;

import org.hibernate.annotations.Type;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Indexed
@Entity
@Table(name = "tour_order")
public class TourOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Field
    @NotNull
    private Integer quantity;

    @Field
    @NotNull
    private Integer price;

    @Field
    @NotNull
    private Integer status;

    @Field
    @NotNull
    private Integer customerId;

    @Field
    @NotNull
    private Integer tourId;

    @Field
    @NotNull
    private Integer offeredTourId;

    @Field
    @NotNull
    private String lastUpdateDate;

    @Field
    @NotNull
    private Integer reference;

    @Field
    @NotNull
    private Integer referenceType;

    @Field
    @NotNull
    private String tourDate;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean isTransfer;

    @Field
    @NotNull
    private String qr;

    @Field
    private Integer rating;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean payed;

    @Field
    private String errorMessage;

    @Field
    private String arcaOrderId;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean myTripsPaid;

    @Field
    private String reasonComment;

    @Field
    private String touristAddress;

    @Field
    private Double lat;

    @Field
    private Double lng;

    @Field
    @NotNull
    private String phoneNumber;

    @Field
    @NotNull
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean refund;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getTourId() {
        return tourId;
    }

    public void setTourId(Integer tourId) {
        this.tourId = tourId;
    }

    public Integer getOfferedTourId() {
        return offeredTourId;
    }

    public void setOfferedTourId(Integer offeredTourId) {
        this.offeredTourId = offeredTourId;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Integer getReference() {
        return reference;
    }

    public void setReference(Integer reference) {
        this.reference = reference;
    }

    public Integer getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(Integer referenceType) {
        this.referenceType = referenceType;
    }

    public String getTourDate() {
        return tourDate;
    }

    public void setTourDate(String tourDate) {
        this.tourDate = tourDate;
    }

    public Boolean getTransfer() {
        return isTransfer;
    }

    public void setTransfer(Boolean transfer) {
        isTransfer = transfer;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Boolean getPayed() {
        return payed;
    }

    public void setPayed(Boolean payed) {
        this.payed = payed;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getArcaOrderId() {
        return arcaOrderId;
    }

    public void setArcaOrderId(String arcaOrderId) {
        this.arcaOrderId = arcaOrderId;
    }

    public Boolean getMyTripsPaid() {
        return myTripsPaid;
    }

    public void setMyTripsPaid(Boolean myTripsPaid) {
        this.myTripsPaid = myTripsPaid;
    }

    public String getReasonComment() {
        return reasonComment;
    }

    public void setReasonComment(String reasonComment) {
        this.reasonComment = reasonComment;
    }

    public String getTouristAddress() {
        return touristAddress;
    }

    public void setTouristAddress(String touristAddress) {
        this.touristAddress = touristAddress;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getRefund() {
        return refund;
    }

    public void setRefund(Boolean refund) {
        this.refund = refund;
    }
}

