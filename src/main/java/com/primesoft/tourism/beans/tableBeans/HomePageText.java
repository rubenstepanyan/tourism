package com.primesoft.tourism.beans.tableBeans;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Indexed
@Entity
@Table(name = "home_page_text")
public class HomePageText {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Field
    @NotNull
    private Integer language;

    @Field
    @NotNull
    @Column(length = 500)
    private String upTitle;

    @Field
    @NotNull
    @Column(length = 5000)
    private String upContent;

    @Field
    @NotNull
    @Column(length = 500)
    private String downTitle;

    @Field
    @NotNull
    @Column(length = 10000)
    private String downContent;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUpTitle() {
        return upTitle;
    }

    public void setUpTitle(String upTitle) {
        this.upTitle = upTitle;
    }

    public String getUpContent() {
        return upContent;
    }

    public void setUpContent(String upContent) {
        this.upContent = upContent;
    }

    public String getDownTitle() {
        return downTitle;
    }

    public void setDownTitle(String downTitle) {
        this.downTitle = downTitle;
    }

    public String getDownContent() {
        return downContent;
    }

    public void setDownContent(String downContent) {
        this.downContent = downContent;
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }
}
