package com.primesoft.tourism.beans.tableBeans;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Indexed
@Entity
@Table(name = "offered_tour_info")
public class OfferedTourInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Field
    @NotNull
    private Integer offeredTourId;

    @Field
    @NotNull
    private Integer language;

    @Field
    @NotNull
    @Column(length = 1000)
    private String name;

    @Field
    @NotNull
    @Column(length = 10000)
    private String description;

    @Field
    @NotNull
    @Column(length = 15)
    private String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOfferedTourId() {
        return offeredTourId;
    }

    public void setOfferedTourId(Integer offeredTourId) {
        this.offeredTourId = offeredTourId;
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
