package com.primesoft.tourism.beans.tableBeans;


import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Indexed
@Entity
@Table(name = "entertainment_place_photo")
public class EntertainmentPlacePhoto {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;


    @Field
    @NotNull
    private Integer entertainmentPlaceId;


    @Field
    @NotNull
    private String url;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEntertainmentPlaceId() {
        return entertainmentPlaceId;
    }

    public void setEntertainmentPlaceId(Integer entertainmentPlaceId) {
        this.entertainmentPlaceId = entertainmentPlaceId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
