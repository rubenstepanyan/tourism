package com.primesoft.tourism.beans.tableBeans;

import org.hibernate.annotations.Type;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Indexed
@Entity
@Table(name = "tour")
public class Tour {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Field
    @NotNull
    private Integer offeredTourId;


    @Field
    @NotNull
    private Integer reference;


    @Field
    @NotNull
    private Integer type;


    @Field
    private Integer driverId;


    @Field
    private Integer carId;


    @Field
    private Integer guideId;


    @Field
    private String guideLanguage;


    @Field
    private String guideName;


    @Field
    @NotNull
    private Integer price;


    @Field
    private Integer freeSeats;

    @NotNull
    @Field
    private String startTime;

    @Field
    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Field
    private String daysOfWeek;

    @Field
    @NotNull
    private Integer hour;

    @Field
    @NotNull
    private Integer minute;

    @Field
    @NotNull
    private String startAddress;

    @Field
    @NotNull
    private Double lat;

    @Field
    @NotNull
    private Double lng;

    @Field
    @NotNull
    private Integer startCity;

    @Field
    private String name;

    @Field
    private String iconUrl;

    @Field
    private String carInfo;

    @Field
    @Column(length = 1000)
    private String languageSkills;

    @Field
    @NotNull
    private Double rating;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean guided;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean transfer;

    @Field
    @NotNull
    private  Integer tourType;


    @Field
    @NotNull
    @Column(length = 1000)
    private String description;

    @Field
    @NotNull
    private String localName;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean pickMeUp;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean approved;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean blocked;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean deleted;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOfferedTourId() {
        return offeredTourId;
    }

    public void setOfferedTourId(Integer offeredTourId) {
        this.offeredTourId = offeredTourId;
    }

    public Integer getReference() {
        return reference;
    }

    public void setReference(Integer reference) {
        this.reference = reference;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getGuideId() {
        return guideId;
    }

    public void setGuideId(Integer guideId) {
        this.guideId = guideId;
    }

    public String getGuideLanguage() {
        return guideLanguage;
    }

    public void setGuideLanguage(String guideLanguage) {
        this.guideLanguage = guideLanguage;
    }

    public String getGuideName() {
        return guideName;
    }

    public void setGuideName(String guideName) {
        this.guideName = guideName;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getFreeSeats() {
        return freeSeats;
    }

    public void setFreeSeats(Integer freeSeats) {
        this.freeSeats = freeSeats;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(String daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Integer getStartCity() {
        return startCity;
    }

    public void setStartCity(Integer startCity) {
        this.startCity = startCity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getCarInfo() {
        return carInfo;
    }

    public void setCarInfo(String carInfo) {
        this.carInfo = carInfo;
    }

    public String getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(String languageSkills) {
        this.languageSkills = languageSkills;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Boolean getGuided() {
        return guided;
    }

    public void setGuided(Boolean guided) {
        this.guided = guided;
    }

    public Integer getTourType() {
        return tourType;
    }

    public void setTourType(Integer tourType) {
        this.tourType = tourType;
    }

    public Boolean getTransfer() {
        return transfer;
    }

    public void setTransfer(Boolean transfer) {
        this.transfer = transfer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Boolean getPickMeUp() {
        return pickMeUp;
    }

    public void setPickMeUp(Boolean pickMeUp) {
        this.pickMeUp = pickMeUp;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
