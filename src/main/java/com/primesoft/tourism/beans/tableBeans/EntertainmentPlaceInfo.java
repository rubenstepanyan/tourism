package com.primesoft.tourism.beans.tableBeans;


import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Indexed
@Entity
@Table(name = "entertainment_place_info")
public class EntertainmentPlaceInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Field
    @NotNull
    private Integer entertainmentPlaceId;

    @Field
    @NotNull
    private Integer language;

    @Field
    @NotNull
    private String name;

    @Field
    @NotNull
    @Column(length = 500)
    private String address;


    @Field
    @NotNull
    @Column(length = 10000)
    private String description;

    @Field
    private String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEntertainmentPlaceId() {
        return entertainmentPlaceId;
    }

    public void setEntertainmentPlaceId(Integer entertainmentPlaceId) {
        this.entertainmentPlaceId = entertainmentPlaceId;
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
