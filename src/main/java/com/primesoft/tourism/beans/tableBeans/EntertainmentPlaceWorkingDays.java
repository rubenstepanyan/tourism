package com.primesoft.tourism.beans.tableBeans;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Indexed
@Entity
@Table(name = "entertainment_place_working_days")
public class EntertainmentPlaceWorkingDays {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Field
    @NotNull
    private Integer entertainmentPlaceId;

    @Field
    @NotNull
    private Integer name;

    @Field
    @NotNull
    private String value;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEntertainmentPlaceId() {
        return entertainmentPlaceId;
    }

    public void setEntertainmentPlaceId(Integer entertainmentPlaceId) {
        this.entertainmentPlaceId = entertainmentPlaceId;
    }

    public Integer getName() {
        return name;
    }

    public void setName(Integer name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
