package com.primesoft.tourism.beans.tableBeans;

import org.hibernate.annotations.Type;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Indexed
@Entity
@Table(name = "agency")
public class Agency {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Field
    @NotNull
    private String name;


    @Field
    @NotNull
    private String city;


    @Field
    @NotNull
    private String address;


    @Field
    @NotNull
    private String password;


    @Field
    @NotNull
    private String mail;


    @Field
    @NotNull
    private String phone;


    @Field
    @NotNull
    private String logoUrl;

    @NotNull
    @Field
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;


    @Field
    @NotNull
    private Integer balance;


    @Field
    @NotNull
    private Integer incomingPercent;


    @Field
    @NotNull
    private String authorization;

    @Field
    private String confirmationCode;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean isConfirmed;

    @Field
    @NotNull
    private Integer totalRating;

    @Field
    @NotNull
    private Double currentRating;

    @Field
    @NotNull
    private Integer agencyCode;

    @Field
    private String notificationToken;

    @Field
    private String appVersion;

    @Field
    private Integer osType;

    @Field
    private String accountNumber;


    @Field
    private Integer dashboardLanguage;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean suspended;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean approved;

    @Field
    private String TopicAndroidAndWeb;

    @Field
    private String topicIos;

    public void cleanNonShowFields(){
        this.confirmationCode = null;
        this.password = null;
        this.authorization = null;
        this.totalRating = null;
        this.isConfirmed = null;
        this.incomingPercent = null;
        this.TopicAndroidAndWeb = null;
        this.topicIos = null;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getIncomingPercent() {
        return incomingPercent;
    }

    public void setIncomingPercent(Integer incomingPercent) {
        this.incomingPercent = incomingPercent;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public Boolean getConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        isConfirmed = confirmed;
    }

    public Integer getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(Integer totalRating) {
        this.totalRating = totalRating;
    }

    public Double getCurrentRating() {
        return currentRating;
    }

    public void setCurrentRating(Double currentRating) {
        this.currentRating = currentRating;
    }

    public Integer getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(Integer agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getNotificationToken() {
        return notificationToken;
    }

    public void setNotificationToken(String notificationToken) {
        this.notificationToken = notificationToken;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public Integer getOsType() {
        return osType;
    }

    public void setOsType(Integer osType) {
        this.osType = osType;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Integer getDashboardLanguage() {
        return dashboardLanguage;
    }

    public void setDashboardLanguage(Integer dashboardLanguage) {
        this.dashboardLanguage = dashboardLanguage;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public String getTopicAndroidAndWeb() {
        return TopicAndroidAndWeb;
    }

    public void setTopicAndroidAndWeb(String topicAndroidAndWeb) {
        TopicAndroidAndWeb = topicAndroidAndWeb;
    }

    public String getTopicIos() {
        return topicIos;
    }

    public void setTopicIos(String topicIos) {
        this.topicIos = topicIos;
    }
}
