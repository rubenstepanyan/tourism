package com.primesoft.tourism.beans.tableBeans;

import org.hibernate.annotations.Type;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Indexed
@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean isConfirmed;

    @Field
    private String confirmationCode;

    @Field
    @NotNull
    private String firstName;

    @Field
    @NotNull
    private String lastName;

    @Field
    @NotNull
    private String password;

    @Field
    @NotNull
    private String authorization;

    @Field
    @NotNull
    private String birthDay;

    @Field
    @NotNull
    @Email
    private String mail;

    @Field
    @NotNull
    private String phone;


    @NotNull
    @Field
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;

    @Field
    private Integer osType;

    @Field
    private String photoUrl;

    @Field
    @NotNull
    private String nationality;

    @Field
    private String topicIos;

    @Field
    private String topicAndroidAndWeb;

    @Field
    private Integer dashboardLanguage;


    public void cleanNonShowFields(){
        this.confirmationCode = null;
        this.password = null;
        this.authorization = null;
        this.osType = null;
        this.topicIos = null;
        this.topicAndroidAndWeb = null;

    }

    public void cleanShowFields(){
        this.confirmationCode = null;
        this.password = null;
        this.authorization = null;
        this.osType = null;
        this.registrationDate = null;
        this.phone = null;
        this.mail = null;
        this.birthDay = null;
        this.isConfirmed = null;
        this.topicIos = null;
        this.topicAndroidAndWeb = null;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        isConfirmed = confirmed;
    }


    public String getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Integer getOsType() {
        return osType;
    }

    public void setOsType(Integer osType) {
        this.osType = osType;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getTopicIos() {
        return topicIos;
    }

    public void setTopicIos(String topicIos) {
        this.topicIos = topicIos;
    }

    public String getTopicAndroidAndWeb() {
        return topicAndroidAndWeb;
    }

    public void setTopicAndroidAndWeb(String topicAndroidAndWeb) {
        this.topicAndroidAndWeb = topicAndroidAndWeb;
    }

    public Integer getDashboardLanguage() {
        return dashboardLanguage;
    }

    public void setDashboardLanguage(Integer dashboardLanguage) {
        this.dashboardLanguage = dashboardLanguage;
    }
}
