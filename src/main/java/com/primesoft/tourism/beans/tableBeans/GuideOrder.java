package com.primesoft.tourism.beans.tableBeans;

import org.hibernate.annotations.Type;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Indexed
@Entity
@Table(name = "guide_order")
public class GuideOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Field
    @NotNull
    private Integer guideId;

    @Field
    @NotNull
    private Integer guidePriceListId;

    @Field
    @NotNull
    private String language;

    @Field
    @NotNull
    private Integer quantityType;

    @Field
    @NotNull
    private Integer price;

    @Field
    @NotNull
    private Integer status;

    @Field
    @NotNull
    private Integer customerId;

    @Field
    @NotNull
    private String lastUpdateDate;

    @Field
    private Integer reference;

    @Field
    @NotNull
    private String fromDate;


    @Field
    @NotNull
    private String toDate;

    @Field
    @NotNull
    private String qr;

    @Field
    private Integer rating;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean payed;

    @Field
    private String errorMessage;

    @Field
    private String arcaOrderId;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean myTripsPaid;

    @Field
    private String reasonComment;

    @Field
    @Column(length = 1000)
    private String direction;

    @Field
    @NotNull
    private Double priceRating;

    @Field
    @NotNull
    private Integer pricePerDay;

    @Field
    @NotNull
    private Integer city;

    @Field
    @NotNull
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean refund;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGuideId() {
        return guideId;
    }

    public void setGuideId(Integer guideId) {
        this.guideId = guideId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getQuantityType() {
        return quantityType;
    }

    public void setQuantityType(Integer quantityType) {
        this.quantityType = quantityType;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Integer getReference() {
        return reference;
    }

    public void setReference(Integer reference) {
        this.reference = reference;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public Integer getGuidePriceListId() {
        return guidePriceListId;
    }

    public void setGuidePriceListId(Integer guidePriceListId) {
        this.guidePriceListId = guidePriceListId;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Boolean getPayed() {
        return payed;
    }

    public void setPayed(Boolean payed) {
        this.payed = payed;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getArcaOrderId() {
        return arcaOrderId;
    }

    public void setArcaOrderId(String arcaOrderId) {
        this.arcaOrderId = arcaOrderId;
    }

    public Boolean getMyTripsPaid() {
        return myTripsPaid;
    }

    public void setMyTripsPaid(Boolean myTripsPaid) {
        this.myTripsPaid = myTripsPaid;
    }

    public String getReasonComment() {
        return reasonComment;
    }

    public void setReasonComment(String reasonComment) {
        this.reasonComment = reasonComment;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public Double getPriceRating() {
        return priceRating;
    }

    public void setPriceRating(Double priceRating) {
        this.priceRating = priceRating;
    }

    public Integer getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(Integer pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public Boolean getRefund() {
        return refund;
    }

    public void setRefund(Boolean refund) {
        this.refund = refund;
    }
}
