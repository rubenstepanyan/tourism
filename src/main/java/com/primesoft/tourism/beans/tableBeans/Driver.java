package com.primesoft.tourism.beans.tableBeans;


import org.hibernate.annotations.Type;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Indexed
@Entity
@Table(name = "driver")
public class Driver {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Field
    private Integer tourismAgencyId;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private Boolean isConfirmed;

    @Field
    private String confirmationCode;

    @Field
    @NotNull
    private String firstName;

    @Field
    @NotNull
    private String lastName;

    @Field
    @NotNull
    private String languageSkills;


    @Field
    @NotNull
    private Integer totalRating;

    @Field
    @NotNull
    private Double currentRating;


    @Field
    @NotNull
    private String password;


    @Field
    @NotNull
    private String authorization;


    @Field
    @NotNull
    private String birthDay;

    @Field
    @NotNull
    @Email
    private String mail;


    @Field
    @NotNull
    private String phone;


    @NotNull
    @Field
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;


    @Field
    private Integer osType;

    @Field
    @NotNull
    private Integer balance;

    @Field
    private String photoUrl;

    @Field
    @NotNull
    private Integer incomingPercent;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private  Boolean suspended;

    @Field
    private String notificationToken;

    @Field
    private String appVersion;

    @Field
    private String accountNumber;


    @Field
    private  Integer dashboardLanguage;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Field
    @NotNull
    private  Boolean approved;


    @Field
    private String city;

    @Field
    private String TopicAndroidAndWeb;

    @Field
    private String topicIos;


    public void cleanNonShowFields(){
        this.isConfirmed = null;
        this.confirmationCode = null;
        this.password = null;
        this.authorization = null;
        this.currentRating = null;
        this.registrationDate = null;
        this.osType = null;
        this.incomingPercent = null;
        this.suspended = null;
        this.notificationToken = null;
        this.appVersion = null;
        this.dashboardLanguage = null;
        this.approved = null;
        this.TopicAndroidAndWeb = null;
        this.topicIos = null;

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTourismAgencyId() {
        return tourismAgencyId;
    }

    public void setTourismAgencyId(Integer tourismAgencyId) {
        this.tourismAgencyId = tourismAgencyId;
    }

    public Boolean getConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        isConfirmed = confirmed;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(String languageSkills) {
        this.languageSkills = languageSkills;
    }

    public Integer getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(Integer totalRating) {
        this.totalRating = totalRating;
    }

    public Double getCurrentRating() {
        return currentRating;
    }

    public void setCurrentRating(Double currentRating) {
        this.currentRating = currentRating;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Integer getOsType() {
        return osType;
    }

    public void setOsType(Integer osType) {
        this.osType = osType;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Integer getIncomingPercent() {
        return incomingPercent;
    }

    public void setIncomingPercent(Integer incomingPercent) {
        this.incomingPercent = incomingPercent;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public String getNotificationToken() {
        return notificationToken;
    }

    public void setNotificationToken(String notificationToken) {
        this.notificationToken = notificationToken;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Integer getDashboardLanguage() {
        return dashboardLanguage;
    }

    public void setDashboardLanguage(Integer dashboardLanguage) {
        this.dashboardLanguage = dashboardLanguage;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTopicAndroidAndWeb() {
        return TopicAndroidAndWeb;
    }

    public void setTopicAndroidAndWeb(String topicAndroidAndWeb) {
        TopicAndroidAndWeb = topicAndroidAndWeb;
    }

    public String getTopicIos() {
        return topicIos;
    }

    public void setTopicIos(String topicIos) {
        this.topicIos = topicIos;
    }
}
