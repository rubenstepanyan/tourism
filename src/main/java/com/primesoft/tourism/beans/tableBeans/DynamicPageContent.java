package com.primesoft.tourism.beans.tableBeans;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Indexed
@Entity
@Table(name = "dynamic_page_content")
public class DynamicPageContent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Field
    @NotNull
    private  Integer dynamicPageId;

    @Field
    @NotNull
    private Integer language;

    @Field
    @NotNull
    @Column(length = 500)
    private String title;

    @Field
    @NotNull
    @Column(length = 20000)
    private String content;

    @Field
    @NotNull
    private String type;

    @Field
    @NotNull
    @Column(length = 300)
    private String shortContent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDynamicPageId() {
        return dynamicPageId;
    }

    public void setDynamicPageId(Integer dynamicPageId) {
        this.dynamicPageId = dynamicPageId;
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getShortContent() {
        return shortContent;
    }

    public void setShortContent(String shortContent) {
        this.shortContent = shortContent;
    }
}
