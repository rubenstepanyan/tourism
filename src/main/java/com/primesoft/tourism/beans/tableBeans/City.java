package com.primesoft.tourism.beans.tableBeans;


import org.hibernate.annotations.Type;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Indexed
@Entity
@Table(name = "city")
public class City {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Field
    @NotNull
    private Integer parentId;

    @Field
    @NotNull
    private String name;


    @Field
    @NotNull
    private Integer language;

    @Field
    @NotNull
    private Integer priority;

    @Field
    @NotNull
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean notShowInTours;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Boolean getNotShowInTours() {
        return notShowInTours;
    }

    public void setNotShowInTours(Boolean notShowInTours) {
        this.notShowInTours = notShowInTours;
    }
}
