package com.primesoft.tourism.beans.tableBeans;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Indexed
@Entity
@Table(name = "custom_notification")
public class CustomNotification {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Field
    @NotNull
    private Integer customerId;


    @Field
    @NotNull
    private Integer action;

    @Field
    @NotNull
    private String messageAM;

    @Field
    @NotNull
    private String messageRU;

    @Field
    @NotNull
    private String messageEN;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public String getMessageAM() {
        return messageAM;
    }

    public void setMessageAM(String messageAM) {
        this.messageAM = messageAM;
    }

    public String getMessageRU() {
        return messageRU;
    }

    public void setMessageRU(String messageRU) {
        this.messageRU = messageRU;
    }

    public String getMessageEN() {
        return messageEN;
    }

    public void setMessageEN(String messageEN) {
        this.messageEN = messageEN;
    }
}
