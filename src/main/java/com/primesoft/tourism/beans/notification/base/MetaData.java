package com.primesoft.tourism.beans.notification.base;

public class MetaData {

    private String action;
    private Object data;
    private Integer status;
    private Boolean isTransfer;
    private Integer orderId;

    public MetaData(String action, Object message, Integer status, Boolean isTransfer,Integer orderId) {
        this.action = action;
        this.data = message;
        this.status = status;
        this.isTransfer = isTransfer;
        this.orderId = orderId;
    }

    public MetaData() {

    }


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getTransfer() {
        return isTransfer;
    }

    public void setTransfer(Boolean transfer) {
        isTransfer = transfer;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }
}