package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.TourReservation;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface TourReservationRepository extends CrudRepository<TourReservation, Integer> {

    List<TourReservation> findAllByReferenceIdAndReferenceTypeAndTourId(Integer referenceId, Integer referenceType,Integer tourId);
    List<TourReservation> findAllByReferenceIdAndReferenceTypeAndTourIdAndReservationDate(Integer referenceId, Integer referenceType,Integer tourId, String reservationDate);

    TourReservation findOneById(Integer id);

    TourReservation findFirstByTourIdAndReservationDate(Integer tourId, String reservationDate);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "DELETE FROM tour_reservation WHERE reservation_date<= NOW()- INTERVAL 10 DAY ", nativeQuery = true)
    void deleteReservation();

}
