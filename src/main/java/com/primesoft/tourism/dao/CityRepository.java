package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.City;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CityRepository extends CrudRepository<City, Integer> {



    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE city SET name= :name, priority= :priority, not_show_in_tours= :notShowInTours WHERE city.id= :id ;", nativeQuery = true)
    void updateCity(@Param("id") Integer id,
                    @Param("name") String name,
                    @Param("priority") Integer priority,
                    @Param("notShowInTours") Boolean notShowInTours);

    List<City> findAll();
    List<City> findAllByLanguageAndNotShowInToursOrderByPriorityDesc(Integer language, Boolean showInEntPlace);
    List<City> findAllByLanguageOrderByPriorityDesc(Integer language);
    List<City> findAllByParentIdOrderByPriorityDesc(Integer parentId);

    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteAllByParentId(Integer parentId);


}
