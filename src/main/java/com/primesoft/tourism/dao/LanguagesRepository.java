package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Languages;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LanguagesRepository extends CrudRepository<Languages, Integer> {

    List<Languages> findAll();
}
