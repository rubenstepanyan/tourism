package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Blog;
import org.springframework.data.repository.CrudRepository;

public interface BlogRepository extends CrudRepository<Blog, Integer> {

    Blog findOneById(Integer id);
}
