package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.SlideShow;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SlideShowRepository extends CrudRepository<SlideShow, Integer> {

    List<SlideShow> findAll();
}
