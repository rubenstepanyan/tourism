package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Agency;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AgencyRepository extends CrudRepository<Agency, Integer> {

    List<Agency> findAll();
    Agency findOneByPhoneAndPassword(String phone, String password);
    Agency findFirstByPhone(String phone);
    Agency findOneByAuthorization(String authorization);
    Agency findOneByPhoneAndConfirmationCode(String phone, String confirmationCode);
    Agency findOneById(Integer id);
    Agency findOneByAgencyCode(Integer agencyCode);
}
