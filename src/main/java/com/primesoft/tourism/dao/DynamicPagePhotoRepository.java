package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.DynamicPagePhoto;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface DynamicPagePhotoRepository extends CrudRepository<DynamicPagePhoto,Integer> {

    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteAllByDynamicPageId(Integer dynamicPageId);


    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteAllByIdIn(List<Integer> ids);

    List<DynamicPagePhoto> findAllByDynamicPageId(Integer dynamicPageId);
}
