package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.EntertainmentPlace;
import org.springframework.data.repository.CrudRepository;

public interface EntertainmentPlaceRepository extends CrudRepository<EntertainmentPlace, Integer> {

    Long countAllByTypeAndCity(Integer type, Integer city);
    Long countAllByType(Integer type);
    Long countAllByCity(Integer city);

}
