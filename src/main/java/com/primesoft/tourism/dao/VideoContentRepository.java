package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.VideoContent;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface VideoContentRepository extends CrudRepository<VideoContent, Integer> {

    List<VideoContent> findAllByVideoId(Integer videoId);

    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteAllByVideoId(Integer videoId);
}
