package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Guide;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface GuideRepository extends CrudRepository<Guide, Integer> {

    Guide findOneByPhoneAndPassword(String phone, String password);

    Guide findOneByPhone(String phone);

    Guide findOneByPhoneAndConfirmationCode(String phone, String confirmationCode);

    Guide findOneByAuthorization(String authorization);

    Guide findOneById(Integer id);

    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteById(Integer id);
}
