package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Partners;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PartnersRepository extends CrudRepository<Partners,Integer> {


    List<Partners> findAll();
}
