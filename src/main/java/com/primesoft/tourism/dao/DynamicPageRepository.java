package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.DynamicPage;
import org.springframework.data.repository.CrudRepository;

public interface DynamicPageRepository extends CrudRepository<DynamicPage, Integer> {

    DynamicPage findOneById(Integer id);

}
