package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.GuidePriceList;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface GuidePriceListRepository extends CrudRepository<GuidePriceList,Integer> {

    GuidePriceList findFirstById(Integer id);

    GuidePriceList findFirstByGuideIdAndLanguageAndType(Integer guideId,String language,Integer type);

    List<GuidePriceList> findAllByGuideIdOrderByGroupId(Integer guideId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE guide_price_list SET rating= :rating WHERE guide_price_list.guide_id= :guideId ;", nativeQuery = true)
    void updateGuideRating(@Param("guideId") Integer guideId,
                           @Param("rating") Double rating);

    void save(List<GuidePriceList> guidePriceLists);


    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE guide_price_list SET language = :language , price= :price, type= :type WHERE guide_price_list.id= :id ;", nativeQuery = true)
    void updatePriceList(@Param("id") Integer id,
                         @Param("language") String language,
                         @Param("price") Integer price,
                         @Param("type") Integer type);

    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteAllByIdIn(List<Integer> ids);


    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteAllByGroupId(Long groupId);
}
