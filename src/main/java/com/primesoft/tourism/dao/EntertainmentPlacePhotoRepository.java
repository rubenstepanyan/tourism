package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.EntertainmentPlacePhoto;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface EntertainmentPlacePhotoRepository extends CrudRepository<EntertainmentPlacePhoto, Integer> {

    List<EntertainmentPlacePhoto> findAllByEntertainmentPlaceId(Integer entertainmentPlaceId);

    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteAllByIdIn(List<Integer> ids);

}
