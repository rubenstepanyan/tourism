package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Coupon;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CouponRepository extends CrudRepository<Coupon, Integer> {

    List<Coupon> findAllByCustomerId(Integer customerId);

    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteByCustomerIdAndIdAndActivated(Integer customerId, Integer id, Boolean activated);

    Coupon findOneByNumberAndActivatedAndPayed(String number, Boolean activated, Boolean payed);

    Coupon findOneByCustomerId(Integer customerId);

    Coupon findFirstByEntertainmentPlaceIdAndCustomerIdAndPayed(Integer entertainmentPlaceId,Integer customerId,Boolean payed);

}
