package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.CustomNotification;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CustomNotificationRepository extends CrudRepository<CustomNotification,Integer> {

    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteByCustomerId(Integer customerId);

    List<CustomNotification> findAllByCustomerId(Integer customerId);
}
