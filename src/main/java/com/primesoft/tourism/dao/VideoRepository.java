package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Video;
import org.springframework.data.repository.CrudRepository;

public interface VideoRepository extends CrudRepository<Video, Integer> {

    Video findOneById(Integer id);

}
