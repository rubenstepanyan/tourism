package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.HomePageText;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface HomePageTextRepository extends CrudRepository<HomePageText, Integer> {

//    @Transactional
//    @Modifying(clearAutomatically = true)
//    @Query(value = "UPDATE home_page_text SET up_title= :upTitle, up_content= :upContent,down_title= :downTitle, down_content= :downContent WHERE home_page_text.id= :Id;", nativeQuery = true)
//    void updateHomePageText(@Param("id") Integer id,
//                            @Param("upTitle") String upTitle,
//                            @Param("upContent") String upContent,
//                            @Param("downTitle") String downTitle,
//                            @Param("downContent") String downContent);


    List<HomePageText> findAll();

    List<HomePageText> findAllByLanguage(Integer language);

    HomePageText findOneById(Integer id);
}
