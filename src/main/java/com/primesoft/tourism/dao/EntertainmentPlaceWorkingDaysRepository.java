package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceWorkingDays;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface EntertainmentPlaceWorkingDaysRepository extends CrudRepository<EntertainmentPlaceWorkingDays, Integer> {

    List<EntertainmentPlaceWorkingDays> findAllByEntertainmentPlaceId(Integer placeId);


    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteAllByEntertainmentPlaceId(Integer placeId);

}
