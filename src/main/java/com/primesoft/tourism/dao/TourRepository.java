package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Tour;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TourRepository extends CrudRepository<Tour, Integer> {


    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteAllByOfferedTourId(Integer offeredTourId);

    List<Tour> findAll();

    Tour findOneById( Integer id);

    @Query(value = "SELECT * FROM  tour WHERE tour.id= :id AND tour.deleted= false AND (tour.start_date= :tourDate or tour.days_of_week LIKE :dayOfWeek) ;",nativeQuery = true)
    Tour getTour(@Param("id") Integer id,
                 @Param("tourDate") String date,
                 @Param("dayOfWeek") String dayOfWeek);


    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE tour SET rating= :rating WHERE tour.reference= :reference and tour.type= :referenceType ;", nativeQuery = true)
    void updateTourRating(@Param("reference") Integer reference,
                          @Param("rating") Double rating,
                          @Param("referenceType") Integer referenceType);


    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE tour SET name= :agencyName WHERE tour.type= :referenceType and tour.reference= :reference ;", nativeQuery = true)
    void updateTours(@Param("referenceType") Integer referenceType,
                     @Param("reference") Integer reference,
                     @Param("agencyName") String agencyName);


    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE tour SET icon_url= :logoUrl WHERE tour.type= :referenceType and tour.reference= :reference ;", nativeQuery = true)
    void updateToursLogoUrl(@Param("referenceType") Integer referenceType,
                            @Param("reference") Integer reference,
                            @Param("logoUrl") String logoUrl);


    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE tour SET approved= :approved WHERE tour.type= :referenceType and tour.reference= :reference ;", nativeQuery = true)
    void updateApproved(@Param("reference") Integer reference,
                        @Param("referenceType") Integer referenceType,
                        @Param("approved") Boolean approved);


    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE tour SET blocked= :blocked WHERE tour.type= :referenceType and tour.reference= :reference ;", nativeQuery = true)
    void updateBlocked(@Param("reference") Integer reference,
                        @Param("referenceType") Integer referenceType,
                        @Param("blocked") Boolean blocked);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE tour SET deleted= true WHERE tour.offered_tour_id= :offeredTourId ;", nativeQuery = true)
    void deleteAllOfferedTourId(@Param("offeredTourId") Integer reference);

}
