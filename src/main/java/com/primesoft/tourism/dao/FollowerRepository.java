package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Follower;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FollowerRepository extends CrudRepository<Follower, Integer> {

    Follower findOneByMail(String mail);

    List<Follower> findAll();
}
