package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceInfo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface EntertainmentPlaceInfoRepository extends CrudRepository<EntertainmentPlaceInfo, Integer>{

    EntertainmentPlaceInfo findFirstByEntertainmentPlaceIdAndLanguage(Integer placeId, Integer languageId);

    List<EntertainmentPlaceInfo> findAllByEntertainmentPlaceId(Integer id);

    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteAllByEntertainmentPlaceId(Integer placeId);

}
