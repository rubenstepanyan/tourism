package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Arca;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ArcaRepository extends CrudRepository<Arca, Integer> {
    List<Arca> findAllByCustomerIdAndCharged(Integer customerId,Boolean charged);
    List<Arca> findAllByCharged(Boolean charged);
    Arca findFirstByCustomerIdAndTypeAndReferenceAndCharged(Integer customerId, Integer type , Integer reference, Boolean charged);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "DELETE FROM arca WHERE charged = 0 AND charge_date<=  :currentDate - INTERVAL 30 MINUTE ", nativeQuery = true)
    void deleteArca(@Param("currentDate") String currentDate);

    Arca findOneByOrderId(String orderId);
}
