package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.TourOrder;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface OrderRepository extends CrudRepository<TourOrder, Integer> {

    TourOrder findOneById(Integer id);

    TourOrder findOneByIdAndCustomerId(Integer id,Integer customerId);

    @Query(value = "SELECT * FROM tour_order WHERE status= 1 AND last_update_date<= :currentDate- INTERVAL 10 MINUTE ", nativeQuery = true)
    List<TourOrder> getAutoPartnerCancel(@Param("currentDate") String currentDate);

    @Query(value = "SELECT * FROM tour_order WHERE status= 3 AND last_update_date<=  :currentDate- INTERVAL 10 MINUTE ", nativeQuery = true)
    List<TourOrder> getAutoCustomerCancel(@Param("currentDate") String currentDate);


    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE tour_order SET my_trips_paid= 1 WHERE tour_order.id IN (:ids) ;", nativeQuery = true)
    void updateTourOrders(@Param("ids") List<Integer> ids);

    TourOrder findOneByQrAndPayedAndStatusAndReferenceAndReferenceType(String qr, Boolean payed, Integer status, Integer reference, Integer referenceType);

    @Query(value = "SELECT * FROM tour_order WHERE status= 4 AND tour_date<=  :currentDate- INTERVAL 1 DAY ", nativeQuery = true)
    List<TourOrder> getAutoFinish(@Param("currentDate") String currentDate);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE tour_order SET phone_number= :phone WHERE reference= :reference AND reference_type= :referenceType", nativeQuery = true)
    void updateTourOrders(@Param("phone") String phone,
                          @Param("reference") Integer reference,
                          @Param("referenceType") Integer referenceType);

    List<TourOrder> findAllByReferenceAndReferenceTypeAndStatus(Integer driverId,Integer type, Integer status);

    List<TourOrder> findAllByCustomerIdAndStatus(Integer customerId, Integer status);
}
