package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Offers;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OfferRepository extends CrudRepository<Offers, Integer> {

    List<Offers> findAll();
}
