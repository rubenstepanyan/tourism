package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Car;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CarRepository extends CrudRepository<Car, Integer> {

     Car findOneById(Integer id);
     Car findFirstByReferenceAndType(Integer reference, Integer type);
     List<Car> findAllByReferenceAndType(Integer reference, Integer type);
     Car findOneByReferenceAndType(Integer reference, Integer type);

}
