package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.GuideOrder;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface GuideOrderRepository extends CrudRepository<GuideOrder,Integer> {

    GuideOrder findOneById(Integer id);

    GuideOrder findOneByIdAndQuantityTypeAndLanguage(Integer id,Integer quantityType,String language);

    GuideOrder findOneByIdAndCustomerId(Integer id,Integer customerId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "SELECT * FROM guide_order WHERE status= 1 AND last_update_date<= :currentDate- INTERVAL 10 MINUTE ", nativeQuery = true)
    List<GuideOrder> getAutoPartnerCancel(@Param("currentDate") String currentDate);


    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "SELECT  * FROM guide_order WHERE status= 3 AND last_update_date<= :currentDate- INTERVAL 10 MINUTE ", nativeQuery = true)
    List<GuideOrder> getAutoCustomerCancel(@Param("currentDate") String currentDate);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE guide_order SET my_trips_paid= 1 WHERE guide_order.id IN (:ids) ;", nativeQuery = true)
    void updateGuideOrders(@Param("ids") List<Integer> ids);

    GuideOrder findOneByQrAndPayedAndStatusAndReference(String qr, Boolean payed, Integer status, Integer id);

    GuideOrder findOneByQrAndPayedAndStatusAndGuideId(String qr, Boolean payed, Integer status, Integer id);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "SELECT * FROM guide_order WHERE status= 4 AND from_date<=  :currentDate- INTERVAL 1 DAY ", nativeQuery = true)
    List<GuideOrder> getAutoFinish(@Param("currentDate") String currentDate);

    List<GuideOrder> findAllByGuideIdAndStatus(Integer guideId,Integer status);

    List<GuideOrder> findAllByCustomerIdAndStatus(Integer customerId,Integer status);


}
