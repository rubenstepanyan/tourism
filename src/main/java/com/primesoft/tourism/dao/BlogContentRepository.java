package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.BlogContent;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BlogContentRepository extends CrudRepository<BlogContent, Integer> {

    List<BlogContent> findAllByBlogId(Integer blogId);

    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteByBlogId(Integer blogId);
}
