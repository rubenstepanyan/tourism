package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.DynamicPageContent;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface DynamicPageContentRepository extends CrudRepository<DynamicPageContent, Integer> {

    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteAllByDynamicPageId(Integer dynamicPageId);

    List<DynamicPageContent> findOneByDynamicPageId(Integer DynamicPageId);

    DynamicPageContent findOneByDynamicPageIdAndLanguage(Integer dynamicPageId, Integer language);

}
