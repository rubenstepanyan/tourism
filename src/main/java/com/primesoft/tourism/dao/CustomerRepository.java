package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository  extends CrudRepository<Customer, Integer> {


    Customer findOneByMailAndPassword(String mail, String password);

    Customer findFirstByMail(String mail);

    Customer findOneByMailAndConfirmationCode(String mail, String confirmationCode);

    Customer findOneByAuthorization(String authorization);

    Customer findOneById(Integer id);

    Customer findFirstByPhone(String phone);
}
