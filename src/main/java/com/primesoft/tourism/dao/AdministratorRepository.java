package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Administrator;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface AdministratorRepository extends CrudRepository<Administrator, Integer> {

    Administrator findOneByAuthorization(String authorization);
    Administrator findOneByUserName(String userName);
    Administrator findOneByUserNameAndPassword(String userName, String password);

    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteById(Integer id);

    //List<Administrator> findAll();
}
