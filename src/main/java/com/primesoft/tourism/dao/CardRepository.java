package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Card;
import org.springframework.data.repository.CrudRepository;

public interface CardRepository  extends CrudRepository<Card, Integer> {

}
