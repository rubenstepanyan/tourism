package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.OfferedTourInfo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface OfferedTourInfoRepository extends CrudRepository<OfferedTourInfo, Integer> {

    List<OfferedTourInfo> findAllByOfferedTourId(Integer offeredTourId);

    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteAllByOfferedTourId(Integer offeredTourId);

    OfferedTourInfo findOneByOfferedTourIdAndLanguage(Integer offeredTourId,Integer language);


    @Query(value = "SELECT * FROM offered_tour_info WHERE offered_tour_info.name REGEXP :tourNamesRegexp group by offered_tour_id;", nativeQuery = true)
    List<OfferedTourInfo> getOfferedTourIds(@Param("tourNamesRegexp") String tourNamesRegexp);


}
