package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.OfferedTour;
import org.springframework.data.repository.CrudRepository;

public interface OfferedTourRepository extends CrudRepository<OfferedTour, Integer> {

}
