package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.GuideReservation;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GuideReservationRepository extends CrudRepository<GuideReservation, Integer> {

    List<GuideReservation> findAllByGuideId(Integer guideId);
}
