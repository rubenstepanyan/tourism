package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Driver;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface DriverRepository extends CrudRepository<Driver,Integer> {

    List<Driver> findAll();
    Driver findOneByAuthorization(String authorization);
    Driver findFirstByPhone(String phone);
    Driver findOneByPhoneAndConfirmationCode(String phone, String confirmationCode);
    Driver findOneByPhoneAndPassword(String mail, String password);
    Driver findOneByIdAndAuthorization(Integer id, String authorization);
    Driver findOneById(Integer id);

    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteById(Integer id);
}
