package com.primesoft.tourism.dao;

import com.primesoft.tourism.beans.tableBeans.Comment;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;


public interface CommentRepository extends CrudRepository<Comment, Integer> {

    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteByBlogId(Integer blogId);


    @Transactional
    @Modifying(clearAutomatically = true)
    void deleteByIdAndCustomerId(Integer id,Integer customerId);

    Comment findFirstByIdAndCustomerId(Integer id,Integer customerId);

    Comment findOneById(Integer id);



}
