package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.response.PurchasedCoupons;
import com.primesoft.tourism.beans.response.checkCouponResponse.CouponData;
import com.primesoft.tourism.beans.response.getCouponPeynetsResponse.GetCouponPaymentContent;
import com.primesoft.tourism.beans.tableBeans.Coupon;

import java.util.ArrayList;
import java.util.List;

public interface CouponService {

    Coupon getCoupon(Integer id);

    void save(Coupon coupon);

    void delete(Integer customerId, Integer couponId);

    ArrayList<PurchasedCoupons> getCouponsByCustomerId(Integer customerId, Integer language);

    CouponData getCouponByNumber(String number, Boolean activated, Boolean payed, Integer language);

    Coupon getCouponByNumber(String number, Boolean activated, Boolean payed);

    Coupon getCoupon(Integer entertainmentPlaceId,Integer customerId,Boolean payed);

    List<GetCouponPaymentContent> getAllPayments(Integer entertainmentPlaceId, Integer language);
}
