package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.response.GetAgenciesResponse;
import com.primesoft.tourism.beans.response.getAllAgencyNamesObjects.GetAllAgencyNamesObject;
import com.primesoft.tourism.beans.response.getAllTourismObjects.TourismObject;
import com.primesoft.tourism.beans.response.getOurPartnersResponse.GetOurPartnersContent;
import com.primesoft.tourism.beans.tableBeans.Agency;

import java.util.List;

public interface AgencyService {

    List<Agency> getAll();
    List<TourismObject> getTourismObjects();
    Agency getDriverByPhoneAndPassword(String phone, String password);
    Agency getAgencyByPhone(String phone);

    void  save(Agency agency);

    Agency getAgency(String authorization);

    Agency getAgencyByPhoneAndConfirmationCode(String phone, String confirmationCode);

    List<GetAllAgencyNamesObject> getAllNames();

    List<GetAgenciesResponse> getAgencies();

    Agency getAgency(Integer id);

    Agency getAgencyByAgencyCode(Integer agencyCode);

    List<GetOurPartnersContent> getAllAgencies();



}
