package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.City;

import java.util.List;

public interface CityService {

    void save(City city);
    void save(List<City> city);
    void update(Integer id, String name,Integer priority,Boolean notShowInTours);
    List<City> getAll(Integer language, Boolean notShowInTours);
    List<City> getAllByParentId(Integer parentId);
    List<City> getAllByLanguage(Integer language);
    List<City> getAll();

    void deleteCity(Integer parentId);

}
