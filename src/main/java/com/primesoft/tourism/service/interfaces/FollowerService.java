package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.Follower;

import java.util.List;

public interface FollowerService {

    void save(Follower follower);

    Follower getFollower(String mail);

    List<Follower> getAll();

    void delete(Integer id);
}
