package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.BlogContent;

import java.util.List;

public interface BlogContentService {

    void save(List<BlogContent> blogContents);

    List<BlogContent> getBlogContent(Integer blogId);

    void delete(Integer blogId);

}
