package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.EntertainmentPlacePhoto;

import java.util.List;

public interface EntertainmentPlacePhotoService {

    void save(EntertainmentPlacePhoto entertainmentPlacePhoto);
    List<EntertainmentPlacePhoto> getAll(Integer entertainmentPlaceId);
    void deleteById(List<Integer> ids);

}
