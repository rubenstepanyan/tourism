package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.Offers;

import java.util.List;

public interface OfferService {

    void save(Offers offers);

    List<Offers> getAll();

    void  delete(Integer id);
}
