package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.response.FinishOrderResponse;
import com.primesoft.tourism.beans.response.getCustomerHistory.GetCustomerOrderHistoryContent;
import com.primesoft.tourism.beans.response.getCustomerOrders.TourOrderContent;
import com.primesoft.tourism.beans.response.getHistoryResponse.GetTourOrderHistoryContentResponse;
import com.primesoft.tourism.beans.response.getOrdersResponse.GetTourOrderContentResponse;
import com.primesoft.tourism.beans.response.getTourPaymentsResponse.GetTourPaymentsContent;
import com.primesoft.tourism.beans.tableBeans.TourOrder;

import java.util.List;

public interface OrderService {

    void save(TourOrder tourOrder);

    void save(List<TourOrder> tourOrders);

    TourOrder getTourOrder(Integer id);

    List<TourOrderContent> getTourOrderByCustomerId(Integer customerId,Integer language);

    List<GetTourOrderContentResponse> getOrder(Integer agencyId, Integer driverId, Integer language);

    List<GetCustomerOrderHistoryContent> getCustomerOrderHistory(Integer customerId, Integer language);

    List<GetTourOrderHistoryContentResponse> getOrderHistory(Integer agencyId, Integer driverId, Integer language,Integer tourId);

    TourOrder getTourOrder(Integer id,Integer customerId);

    List<TourOrder> getAutoPartnerCancel();

    List<TourOrder> getAutoCustomerCancel();

    List<GetTourPaymentsContent> getAllPayments(Integer referenceType, Integer reference, Integer language, String from, String to);

    List<GetTourPaymentsContent> getAllDriverPayments(Integer referenceType, Integer reference, Integer language, String from, String to);

    void updateTourOrders(List<Integer> ids);

    TourOrder getTourOrderByQr(String qr, Boolean payed, Integer status, Integer reference, Integer referenceType);

    List<TourOrder> getAutoFinish();

    FinishOrderResponse getTourOrderInfoByQr(String qr, Integer languageId);

    void updateTourOrders(Integer id, String phone,Integer type);

    List<TourOrder> getTourOrderByDriverIdAndStatus(Integer driverId,Integer type ,Integer status);

    List<TourOrder> getTourOrderByCustomerIdAndStatus(Integer customerId,Integer status);
}
