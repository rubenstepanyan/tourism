package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.response.getAllVideoResponse.VideoContentResponse;
import com.primesoft.tourism.beans.tableBeans.Video;

import java.util.List;

public interface VideoService {

    void save(Video video);

    Video getVideo(Integer id);

    List<VideoContentResponse> getAllVideos(Integer language,Integer startPosition, Integer itemCount);

    Long getCount();

    void delete(Integer id);

}
