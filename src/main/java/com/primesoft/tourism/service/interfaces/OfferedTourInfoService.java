package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.OfferedTourInfo;

import java.util.List;

public interface OfferedTourInfoService {

    void save(List<OfferedTourInfo> offeredToursInfo);
    List<OfferedTourInfo> getInfo(Integer offeredTourId);
    void deleteByOfferedTourId(Integer offeredTourId);

    OfferedTourInfo getOfferedTourInfo(Integer offeredTourId, Integer language);

    List<OfferedTourInfo> getOfferedTourIds(String tourNamesRegexp);

}
