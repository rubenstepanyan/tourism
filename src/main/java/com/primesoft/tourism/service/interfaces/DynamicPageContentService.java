package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.DynamicPageContent;

import java.util.List;

public interface DynamicPageContentService {

    void save(List<DynamicPageContent> dynamicPageContent);

    void delete(Integer dynamicPageId);

    List<DynamicPageContent> getDynamicPageContents(Integer dynamicPageId);

    DynamicPageContent getDynamicPageContent(Integer dynamicPageId,Integer language);
}
