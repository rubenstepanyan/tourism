package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.TourReservation;

import java.util.List;

public interface TourReservationService {

    void save(TourReservation tourReservation);

    List<TourReservation> getAllTourReservation(Integer referenceId, Integer referenceType,Integer tourId);
    List<TourReservation> getTourReservation(Integer referenceId, Integer referenceType,Integer tourId, String reservationDate);

    void delete(Integer id);

    TourReservation getTourReservation(Integer id);

    TourReservation getTourReservation(Integer tourId,String date);

    void deleteReservation();


}
