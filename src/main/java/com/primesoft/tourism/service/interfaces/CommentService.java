package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.response.GetAllCommentsResponse;
import com.primesoft.tourism.beans.response.getBlogCommentResponse.BlogCommentContent;
import com.primesoft.tourism.beans.tableBeans.Comment;

import java.util.List;

public interface CommentService {

    void delete(Integer blogId);

    void  save(Comment comment);

    void deleteComment(Integer id);
    void deleteComment(Integer id, Integer customerId);
    List<BlogCommentContent> getAllBlogComment(Integer blogId, Integer lastId);
    Comment getComment(Integer id, Integer customerId);

   List<GetAllCommentsResponse> getAllComments(String from, String to);
   Comment getComment(Integer id);

   Comment getOneComment(Integer Id);

}
