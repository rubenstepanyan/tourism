package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.Car;

import java.util.List;

public interface CarService {

    void saveCar(Car car);
    Car getCar(Integer id);
    Car getDriverCar(Integer driverId);
    List<Car> getCars(Integer reference, Integer type);
    Car getCar(Integer reference, Integer type);
    void delete(Integer id);
}
