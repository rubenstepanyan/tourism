package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.Languages;

import java.util.List;

public interface LanguagesService {

    void  save(Languages language);

    List<Languages> getAll();

    void delete(Integer id);
}
