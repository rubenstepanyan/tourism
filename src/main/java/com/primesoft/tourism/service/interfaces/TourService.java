package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.response.getAllTouesResponse.GetAllTourContentResponse;
import com.primesoft.tourism.beans.response.getTourResponse.TourContent;
import com.primesoft.tourism.beans.tableBeans.Tour;

import java.util.List;

public interface TourService {

    void save(Tour tour);
    void deleteByOfferedTourId(Integer offeredTourId);
    List<GetAllTourContentResponse> getAll(Integer language);
    List<GetAllTourContentResponse> getAll(Integer type,Integer language);
    List<GetAllTourContentResponse> getAll(Integer reference, Integer type,Integer language);
    Tour getTour(Integer id);
    void  delete(Integer id);
    List<TourContent> searchTour(Integer offeredTourId,String offeredTourIds, Integer startCity, Integer language, Double budget, Integer quantity, String from, String to, String days, String thisDay,Integer guided,String tourLanguage, Integer tourType,Boolean transfer,Boolean pickMeUp);

    Tour getTour(Integer id,String date, String dayOfWeek);

    void  updateTourRating(Integer reference,Double rating,Integer referenceType);

    void  updateTours(Integer type, Integer agencyId, String name);

    void  updateToursLogoUrl(Integer type, Integer agencyId, String logoUrl);

    void updateApproved(Integer reference,Integer referenceType, Boolean approved);

    void updateBlocked(Integer reference,Integer referenceType, Boolean blocked);

    void deleteAllByOfferedTorId(Integer offeredTourId);

}