package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.response.getOurPartnersResponse.GetOurPartnersContentEntertainmentPlace;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceInfo;

import java.util.List;

public interface EntertainmentPlaceInfoService {

    void save(List<EntertainmentPlaceInfo> placeInfoList);

    EntertainmentPlaceInfo getInfo(Integer placeId, Integer languageId);

    List<EntertainmentPlaceInfo> getAllInfo(Integer id);
    void delete(Integer placeId);

    List<GetOurPartnersContentEntertainmentPlace> getAllEntertainmentPlace(Integer language);

}
