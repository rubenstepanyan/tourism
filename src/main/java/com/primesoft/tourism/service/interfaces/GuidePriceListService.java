package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.GuidePriceList;

import java.util.List;

public interface GuidePriceListService {

    void save(GuidePriceList guidePriceList);


    GuidePriceList getGuidePriceList(Integer id);

    GuidePriceList getPriceList(Integer guideId,String language,Integer type);

    List<GuidePriceList> getAll(Integer guideId);

    void delete(List<Integer> ids);

    void deleteByGroupId(Long groupId);


    void updateGuideRating(Integer guideId, Double rating);

    List<Long> getGroupIds(Integer guideId);

    void update(Integer id, String language, Integer price, Integer type);
}
