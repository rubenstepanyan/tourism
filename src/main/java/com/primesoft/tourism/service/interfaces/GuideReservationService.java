package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.GuideReservation;

import java.util.List;

public interface GuideReservationService {

    void save(GuideReservation guideReservation);

    List<GuideReservation> getAll(Integer guideId);

    void delete(Integer id);
}
