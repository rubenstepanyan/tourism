package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.response.getAllDriverObjects.DriverObject;
import com.primesoft.tourism.beans.response.getAllTourismObjects.TourismObject;
import com.primesoft.tourism.beans.response.getOurPartnersResponse.GetOurPartnersContent;
import com.primesoft.tourism.beans.tableBeans.Driver;

import java.util.List;

public interface DriverService {

    List<Driver> getAll();
    List<TourismObject> getTourismObjects();
    Driver getDriver(String authorization);
    Driver getDriverByPhone(String phone);
    Driver getDriverByPhoneAndConfirmationCode(String phone, String confirmationCode);
    void  save(Driver driver);
    Driver getDriverByIdAndAuthorization(Integer id, String authorization);
    Driver getDriverByPhoneAndPassword(String phone, String password);

    List<DriverObject> getDrivers(Integer type, String key);

    Driver getDriver(Integer id);

    List<GetOurPartnersContent> getAllDrivers();

    void deleteDriver(Integer id);


}
