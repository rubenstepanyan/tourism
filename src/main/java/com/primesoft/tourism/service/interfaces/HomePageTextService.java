package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.HomePageText;

import java.util.List;

public interface HomePageTextService {

//   void update(Integer id, String upTitle,String upContent,String downTitle,String downContent);

   List<HomePageText> getAll();

   List<HomePageText> getAll(Integer language);

   void  save(List<HomePageText> homePageTexts);

   HomePageText getHomePageText(Integer id);
}
