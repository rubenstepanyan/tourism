package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.Arca;

import java.util.List;

public interface ArcaService {

    void save(Arca arca);

    void delete(Integer id);

    List<Arca> getArcaList(Integer customerId, Boolean charged);
    List<Arca> getArcaList(Boolean charged);
    Arca getArca(Integer customerId, Integer type, Integer reference, Boolean charged);

    void deleteArca();

    Arca getArca(String arcaOrderId);
}
