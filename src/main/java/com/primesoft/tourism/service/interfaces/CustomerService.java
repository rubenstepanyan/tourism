package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.response.getAllCustomerObjects.CustomerObject;
import com.primesoft.tourism.beans.tableBeans.Customer;

import java.util.List;

public interface CustomerService {

    Customer getCustomerByMailAndPassword(String mail, String password);
    Customer getCustomer(Integer id);

    Customer getCustomerByMail(String mail);

    void  save(Customer customer);

    Customer getCustomerByMailAndConfirmationCode(String mail,String confirmationCode);

    Customer getCustomerByAuthorization(String authorization);
    Customer getCustomerByPhone(String phone);

    List<CustomerObject> getCustomerObjects();

}
