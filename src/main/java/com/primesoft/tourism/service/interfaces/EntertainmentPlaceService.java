package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.response.getEntertainmentPlaces.Content;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlace;

import java.util.List;

public interface EntertainmentPlaceService {

    void save(EntertainmentPlace entertainmentPlace);

    Long getCount(Integer type, Integer city);
    List<Content> getAll(Integer language, Integer type, Integer city, Integer startPosition,Integer itemCount);
    EntertainmentPlace getById(Integer id);
    void delete(Integer id);

    List<Content> search(Integer language, String name);
}
