package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.VideoContent;

import java.util.List;

public interface VideoContentService {

    void  save(List<VideoContent> videoContent);

    List<VideoContent> getVideoContent(Integer videoId);

    void delete(Integer videoId);
}
