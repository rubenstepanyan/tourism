package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.response.getAllGuideObjects.GuideObject;
import com.primesoft.tourism.beans.response.getAllGuideResponse.GetAllGuideContentResponse;
import com.primesoft.tourism.beans.response.getOurPartnersResponse.GetOurPartnersContent;
import com.primesoft.tourism.beans.tableBeans.Guide;

import java.util.List;

public interface GuideService {

    Guide getGuid(String phone, String password);

    void save(Guide guide);

    Guide getGuidByPhone(String phone);

    Guide getGuidByPhoneAndConfirmationCode(String phone, String confirmationCode);

    Guide getGuidByAuthorization(String authorization);

    Guide getGuidById(Integer id);

    List<GuideObject> getGuide(Integer type,String key);

    List<GetAllGuideContentResponse> getAll(String from,String to,String guideLanguage,Integer type,Integer city);

    List<GetOurPartnersContent> getAllGuides();

    void deleteGuide(Integer id);

}
