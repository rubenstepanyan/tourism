package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.response.getCustomerHistory.GetCustomerGuideOrderHistoryContent;
import com.primesoft.tourism.beans.response.getCustomerOrders.GuideOrderContent;
import com.primesoft.tourism.beans.response.getGuidePaymentsResponse.GuidePaymentContent;
import com.primesoft.tourism.beans.response.getHistoryResponse.GetGuideOrderHistoryContentResponse;
import com.primesoft.tourism.beans.response.getOrdersResponse.GetGuideOrderContentsResponse;
import com.primesoft.tourism.beans.tableBeans.GuideOrder;

import java.util.List;

public interface GuideOrderService {

    void save(GuideOrder guideOrder);

    void save(List<GuideOrder> guideOrders);

    GuideOrder getGuideOrder(Integer id);
    GuideOrder getGuideOrder(Integer id,Integer quantityType,String language);

    List<GuideOrderContent> getGuideOrderByCustomerId(Integer customerId);

    List<GetGuideOrderContentsResponse> getGuideOrder(Integer agencyId, Integer guideId);

    List<GetCustomerGuideOrderHistoryContent> getCustomerGuideOrderHistory(Integer customerId);

    List<GetGuideOrderHistoryContentResponse> getGuideOrderHistory(Integer agencyId, Integer guideId);

    GuideOrder getGuideOrderByIdAndCustomerId(Integer id, Integer customerId);

    List<GuideOrder> getAutoPartnerCancel();
    List<GuideOrder> getAutoCustomerCancel();

    List<GuidePaymentContent> getAllPayments(Integer reference, Integer type, String from, String to);

    void updateGuideOrders(List<Integer> ids);

    GuideOrder getGuideOrderByQr(String qr, Boolean payed, Integer status, Integer id);

    GuideOrder getGuideOrderByQrAndGuideId(String qr, Boolean payed, Integer status, Integer id);

    List<GuideOrder> getAutoFinish();

    GuidePaymentContent getGuideOrderByQrInfo(String qr);

    List<GuideOrder> getGuideOrderByGuideIdAndStatus(Integer guideId,Integer status);

    List<GuideOrder> getGuideOrderByCustomerIdAndStatus(Integer customerId, Integer status);
}
