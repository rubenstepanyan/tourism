package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.Partners;

import java.util.List;

public interface PartnerService {

    void save(Partners partners);

    List<Partners> getAll();

    void delete(Integer id);

}
