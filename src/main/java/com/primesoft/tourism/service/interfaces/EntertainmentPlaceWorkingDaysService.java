package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceWorkingDays;

import java.util.List;

public interface EntertainmentPlaceWorkingDaysService {

    void save(List<EntertainmentPlaceWorkingDays> workingDays);
    List<EntertainmentPlaceWorkingDays> findAll(Integer placeId);
    void delete(Integer placeId);
}
