package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.response.getAllBlogResponse.BlogContentResponse;
import com.primesoft.tourism.beans.tableBeans.Blog;

import java.util.List;

public interface BlogService {

    void save(Blog blog);

    Blog getBlog(Integer id);

    void delete(Integer id);

    Long getCount();

    List<BlogContentResponse> getAllBlog(Integer language,Integer startPosition, Integer itemCount);
}
