package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.response.GetAllAdministratorResponse.GetAllAdministratorsResponseContent;
import com.primesoft.tourism.beans.tableBeans.Administrator;

import java.util.List;

public interface AdministratorService {

    void saveAdministrator(Administrator administrator);

    Administrator getAdministratorByAuthorization(String authorization);

    Administrator getAdministratorByUserName(String userName);

    Administrator getAdministratorByUserNameAndPassword(String userName, String password);

    void deleteById(Integer id);

    List<GetAllAdministratorsResponseContent> getAdministrator();
}
