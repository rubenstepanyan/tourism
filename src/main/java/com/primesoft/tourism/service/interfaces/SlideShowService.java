package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.SlideShow;

import java.util.List;

public interface SlideShowService {

    void  save(SlideShow slideShow);

    void delete(Integer id);

    List<SlideShow> getPhotos();
}
