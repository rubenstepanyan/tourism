package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.response.getOfferedTourResponse.Content;
import com.primesoft.tourism.beans.tableBeans.OfferedTour;

import java.util.ArrayList;

public interface OfferedTourService {

    void save(OfferedTour offeredTour);
    ArrayList<Content> getAll(Integer language);
    ArrayList<Content> getAll(Boolean transfer, Integer language);
    OfferedTour getOfferedTour(Integer id);
    void delete(Integer id);

    ArrayList<com.primesoft.tourism.beans.response.getOfferedTourCustomerResponse.Content> getAllOfferedTour(Integer language, Boolean transfer);



}
