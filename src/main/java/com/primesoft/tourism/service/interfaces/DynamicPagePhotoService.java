package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.DynamicPagePhoto;

import java.util.List;

public interface DynamicPagePhotoService {

    void save(DynamicPagePhoto dynamicPagePhoto);

    void delete(Integer dynamicPageId);

    void deleteById(List<Integer> ids);

    List<DynamicPagePhoto> getDynamicPagePhoto(Integer dynamicPageId);
}
