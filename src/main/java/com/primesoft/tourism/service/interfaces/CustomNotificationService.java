package com.primesoft.tourism.service.interfaces;

import com.primesoft.tourism.beans.tableBeans.CustomNotification;

import java.util.List;


public interface CustomNotificationService {

    void save(CustomNotification customNotification);

    void  delete(Integer customerId);

    List<CustomNotification> getAll(Integer customerId);
}
