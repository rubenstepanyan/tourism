package com.primesoft.tourism.service.interfaces;


import com.primesoft.tourism.beans.response.getAllDynamicPagesResponse.Content;
import com.primesoft.tourism.beans.tableBeans.DynamicPage;

import java.util.List;

public interface DynamicPageService {

    void save(DynamicPage dynamicPage);

    void delete(Integer id);

    DynamicPage getDynamicPage(Integer id);

    List<Content> getAll(Integer type, Integer language);
}
