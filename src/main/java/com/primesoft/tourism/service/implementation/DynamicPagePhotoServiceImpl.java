package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.DynamicPagePhoto;
import com.primesoft.tourism.dao.DynamicPagePhotoRepository;
import com.primesoft.tourism.service.interfaces.DynamicPagePhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DynamicPagePhotoServiceImpl implements DynamicPagePhotoService {

    @Autowired
    private DynamicPagePhotoRepository dynamicPagePhotoRepository;

    @Override
    public void save(DynamicPagePhoto dynamicPagePhoto) {
        dynamicPagePhotoRepository.save(dynamicPagePhoto);
    }

    @Override
    public void delete(Integer dynamicPageId) {
        dynamicPagePhotoRepository.deleteAllByDynamicPageId(dynamicPageId);
    }

    @Override
    public void deleteById(List<Integer> ids) {
        dynamicPagePhotoRepository.deleteAllByIdIn(ids);
    }

    @Override
    public List<DynamicPagePhoto> getDynamicPagePhoto(Integer dynamicPageId) {
        return dynamicPagePhotoRepository.findAllByDynamicPageId(dynamicPageId);
    }
}
