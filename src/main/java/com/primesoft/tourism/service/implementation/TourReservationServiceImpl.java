package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.TourReservation;
import com.primesoft.tourism.dao.TourReservationRepository;
import com.primesoft.tourism.service.interfaces.TourReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TourReservationServiceImpl implements TourReservationService {

    @Autowired
    private TourReservationRepository tourReservationRepository;

    @Override
    public void save(TourReservation tourReservation) {
        tourReservationRepository.save(tourReservation);
    }

    @Override
    public List<TourReservation> getAllTourReservation(Integer referenceId, Integer referenceType, Integer tourId) {
        return tourReservationRepository.findAllByReferenceIdAndReferenceTypeAndTourId(referenceId, referenceType, tourId);
    }

    @Override
    public List<TourReservation> getTourReservation(Integer referenceId, Integer referenceType, Integer tourId, String reservationDate) {
        return tourReservationRepository.findAllByReferenceIdAndReferenceTypeAndTourIdAndReservationDate(referenceId, referenceType, tourId, reservationDate);
    }

    @Override
    public void delete(Integer id) {
        tourReservationRepository.delete(id);
    }

    @Override
    public TourReservation getTourReservation(Integer id) {
        return tourReservationRepository.findOneById(id);
    }

    @Override
    public TourReservation getTourReservation(Integer tourId, String date) {
        return tourReservationRepository.findFirstByTourIdAndReservationDate(tourId, date);
    }

    @Override
    public void deleteReservation() {
        tourReservationRepository.deleteReservation();
    }
}
