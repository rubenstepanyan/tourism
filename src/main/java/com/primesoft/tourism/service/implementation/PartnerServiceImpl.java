package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.Partners;
import com.primesoft.tourism.dao.PartnersRepository;
import com.primesoft.tourism.service.interfaces.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PartnerServiceImpl implements PartnerService {

    @Autowired
    private PartnersRepository partnersRepository;

    @Override
    public void save(Partners partners) {
        partnersRepository.save(partners);
    }

    @Override
    public List<Partners> getAll() {
        return partnersRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        partnersRepository.delete(id);
    }
}
