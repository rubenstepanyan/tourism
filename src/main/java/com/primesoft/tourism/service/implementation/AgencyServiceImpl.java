package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.response.GetAgenciesResponse;
import com.primesoft.tourism.beans.response.getAllAgencyNamesObjects.GetAllAgencyNamesObject;
import com.primesoft.tourism.beans.response.getAllTourismObjects.TourismObject;
import com.primesoft.tourism.beans.response.getOurPartnersResponse.GetOurPartnersContent;
import com.primesoft.tourism.beans.tableBeans.Agency;
import com.primesoft.tourism.dao.AgencyRepository;
import com.primesoft.tourism.service.interfaces.AgencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Service
public class AgencyServiceImpl implements AgencyService {

    @Autowired
    private AgencyRepository agencyRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Agency> getAll() {
        return agencyRepository.findAll();
    }

    @Override
    public List<TourismObject> getTourismObjects() {

        String query = "SELECT new com.primesoft.tourism.beans.response.getAllTourismObjects.TourismObject(a.id,a.name) FROM Agency a";

        TypedQuery<TourismObject> typedQuery = entityManager.createQuery(query , TourismObject.class);
        List<TourismObject> results = typedQuery.getResultList();

        return results;
    }

    @Override
    public Agency getDriverByPhoneAndPassword(String phone, String password) {
        return agencyRepository.findOneByPhoneAndPassword(phone,password);
    }

    @Override
    public Agency getAgencyByPhone(String phone) {
        return agencyRepository.findFirstByPhone(phone);
    }

    @Override
    public void save(Agency agency) {
        agencyRepository.save(agency);
    }

    @Override
    public Agency getAgency(String authorization) {
        return agencyRepository.findOneByAuthorization(authorization);
    }

    @Override
    public Agency getAgencyByPhoneAndConfirmationCode(String phone, String confirmationCode) {
        return agencyRepository.findOneByPhoneAndConfirmationCode(phone, confirmationCode);
    }

    @Override
    public List<GetAllAgencyNamesObject> getAllNames() {
        String query = "SELECT new com.primesoft.tourism.beans.response.getAllAgencyNamesObjects.GetAllAgencyNamesObject(a.id,a.name) FROM Agency a";

        TypedQuery<GetAllAgencyNamesObject> typedQuery = entityManager.createQuery(query , GetAllAgencyNamesObject.class);
        List<GetAllAgencyNamesObject> results = typedQuery.getResultList();

        return results;
    }

    @Override
    public List<GetAgenciesResponse> getAgencies() {
        String query = "SELECT new com.primesoft.tourism.beans.response.GetAgenciesResponse(a.id,a.name,a.city,a.address,a.mail,a.phone,a.logoUrl,a.registrationDate,a.balance,a.incomingPercent,a.isConfirmed,a.currentRating,a.accountNumber,a.suspended,a.approved) FROM Agency a";

        TypedQuery<GetAgenciesResponse> typedQuery = entityManager.createQuery(query , GetAgenciesResponse.class);
        List<GetAgenciesResponse> results = typedQuery.getResultList();

        return results;
    }

    @Override
    public Agency getAgency(Integer id) {
        return agencyRepository.findOneById(id);
    }

    @Override
    public Agency getAgencyByAgencyCode(Integer agencyCode) {
        return agencyRepository.findOneByAgencyCode(agencyCode);
    }

    @Override
    public List<GetOurPartnersContent> getAllAgencies() {
        String query = "SELECT new com.primesoft.tourism.beans.response.getOurPartnersResponse.GetOurPartnersContent(a.id,a.name,a.accountNumber) FROM Agency a";

        TypedQuery<GetOurPartnersContent> typedQuery = entityManager.createQuery(query , GetOurPartnersContent.class);
        List<GetOurPartnersContent> results = typedQuery.getResultList();

        return results;
    }
}
