package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.GuideReservation;
import com.primesoft.tourism.dao.GuideReservationRepository;
import com.primesoft.tourism.service.interfaces.GuideReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuideReservationServiceImpl implements GuideReservationService {

    @Autowired
    private GuideReservationRepository guideReservationRepository;

    @Override
    public void save(GuideReservation guideReservation) {
        guideReservationRepository.save(guideReservation);
    }

    @Override
    public List<GuideReservation> getAll(Integer guideId) {
        return guideReservationRepository.findAllByGuideId(guideId);
    }

    @Override
    public void delete(Integer id) {
        guideReservationRepository.delete(id);
    }
}
