package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.EntertainmentPlacePhoto;
import com.primesoft.tourism.dao.EntertainmentPlacePhotoRepository;
import com.primesoft.tourism.service.interfaces.EntertainmentPlacePhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntertainmentPlacePhotoServiceImpl implements EntertainmentPlacePhotoService {


    @Autowired
    private EntertainmentPlacePhotoRepository entertainmentPlacePhotoRepository;


    @Override
    public void save(EntertainmentPlacePhoto entertainmentPlacePhoto) {
        entertainmentPlacePhotoRepository.save(entertainmentPlacePhoto);
    }

    @Override
    public List<EntertainmentPlacePhoto> getAll(Integer entertainmentPlaceId) {
        return entertainmentPlacePhotoRepository.findAllByEntertainmentPlaceId(entertainmentPlaceId);
    }

    @Override
    public void deleteById(List<Integer> ids) {
        entertainmentPlacePhotoRepository.deleteAllByIdIn(ids);
    }


}
