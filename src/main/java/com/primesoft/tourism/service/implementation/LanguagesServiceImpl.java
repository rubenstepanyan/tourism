package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.Languages;
import com.primesoft.tourism.dao.LanguagesRepository;
import com.primesoft.tourism.service.interfaces.LanguagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LanguagesServiceImpl implements LanguagesService {

    @Autowired
    private LanguagesRepository languagesRepository;

    @Override
    public void save(Languages language) {
        languagesRepository.save(language);
    }

    @Override
    public List<Languages> getAll() {
        return languagesRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        languagesRepository.delete(id);
    }
}
