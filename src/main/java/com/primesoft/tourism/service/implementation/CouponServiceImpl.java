package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.response.PurchasedCoupons;
import com.primesoft.tourism.beans.response.checkCouponResponse.CouponData;
import com.primesoft.tourism.beans.response.getCouponPeynetsResponse.GetCouponPaymentContent;
import com.primesoft.tourism.beans.tableBeans.Coupon;
import com.primesoft.tourism.dao.CouponRepository;
import com.primesoft.tourism.service.interfaces.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

@Service
public class CouponServiceImpl implements CouponService {

    @Autowired
    private CouponRepository couponRepository;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Override
    public Coupon getCoupon(Integer id) {
        return couponRepository.findOne(id);
    }

    @Override
    public void save(Coupon coupon) {
        couponRepository.save(coupon);
    }

    @Override
    public CouponData getCouponByNumber(String number, Boolean activated, Boolean payed, Integer language) {

        EntityManager session = entityManagerFactory.createEntityManager();

        try {
            String query = "SELECT c.id,c.price,c.discount,c.number,c.activated,c.payed,ep.icon_url,ep.city,ep.phone,ep.max_persons,epi.address,epi.name " +
           "FROM coupon as c INNER JOIN entertainment_place as ep on c.entertainment_place_id=ep.id INNER JOIN entertainment_place_info as epi ON epi.entertainment_place_id=c.entertainment_place_id where c.number='"+number+"' AND c.activated=0 AND c.payed=1 and epi.language="+language+";";

            System.out.println(query);

            List<Object[]> result = session.createNativeQuery(query).getResultList();

            if(result!=null && result.size()>0) {
                return new CouponData(result.get(0));
            }else {
                return null;
            }
        } finally {
            if (session.isOpen()) session.close();
        }

    }

    @Override
    public Coupon getCouponByNumber(String number, Boolean activated, Boolean payed) {
        return couponRepository.findOneByNumberAndActivatedAndPayed(number, activated, payed);
    }

    @Override
    public void delete(Integer customerId, Integer couponId) {
        couponRepository.deleteByCustomerIdAndIdAndActivated(customerId, couponId, true);
    }

    @Override
    public ArrayList<PurchasedCoupons> getCouponsByCustomerId(Integer customerId, Integer language) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {
            String query = "select ep.icon_url, epi.name, ep.max_persons, c.discount, epi.address, ep.phone, c.number,c.payed, c.error_message, ep.id, c.activated, c.id as couponId FROM coupon as c " +
                    " inner join entertainment_place as ep on c.entertainment_place_id=ep.id inner join entertainment_place_info as epi on c.entertainment_place_id=epi.entertainment_place_id where c.customer_id="+customerId+" AND epi.language="+language+" ORDER BY couponId DESC;";

            System.out.println("coupon query: "+ query);

            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<PurchasedCoupons> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    PurchasedCoupons content = new PurchasedCoupons(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public Coupon getCoupon(Integer entertainmentPlaceId,Integer customerId,Boolean payed) {
        return couponRepository.findFirstByEntertainmentPlaceIdAndCustomerIdAndPayed(entertainmentPlaceId, customerId, payed);
    }


    @Override
    public List<GetCouponPaymentContent> getAllPayments(Integer entertainmentPlaceId, Integer language) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {
            String query = "select co.id, co.price, co.payed, epi.name, concat(c.first_name, ' ',c.last_name), c.mail, c.phone from coupon as co " +
                    " inner join entertainment_place_info as epi on co.entertainment_place_id=epi.entertainment_place_id inner join customer as c on co.customer_id=c.id where epi.language="+language;

                    if(entertainmentPlaceId != 0){
                        query += " AND co.entertainment_place_id=" + entertainmentPlaceId;
                    }
            System.out.println(query);

            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<GetCouponPaymentContent> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GetCouponPaymentContent content = new GetCouponPaymentContent(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }
}
