package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.DynamicPageContent;
import com.primesoft.tourism.dao.DynamicPageContentRepository;
import com.primesoft.tourism.service.interfaces.DynamicPageContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DynamicPageContentServiceImpl implements DynamicPageContentService {

    @Autowired
    private DynamicPageContentRepository dynamicPageContentRepository;

    @Override
    public void save(List<DynamicPageContent> dynamicPageContent) {
        dynamicPageContentRepository.save(dynamicPageContent);
    }

    @Override
    public void delete(Integer dynamicPageId) {
        dynamicPageContentRepository.deleteAllByDynamicPageId(dynamicPageId);
    }

    @Override
    public List<DynamicPageContent> getDynamicPageContents(Integer dynamicPageId) {
        return dynamicPageContentRepository.findOneByDynamicPageId(dynamicPageId);
    }

    @Override
    public DynamicPageContent getDynamicPageContent(Integer dynamicPageId, Integer language) {
        return dynamicPageContentRepository.findOneByDynamicPageIdAndLanguage(dynamicPageId, language);
    }
}
