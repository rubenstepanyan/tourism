package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.OfferedTourInfo;
import com.primesoft.tourism.dao.OfferedTourInfoRepository;
import com.primesoft.tourism.service.interfaces.OfferedTourInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfferedTourInfoServiceImpl implements OfferedTourInfoService {

    @Autowired
    private OfferedTourInfoRepository offeredTourInfoRepository;


    @Override
    public void save(List<OfferedTourInfo> offeredToursInfo) {
        offeredTourInfoRepository.save(offeredToursInfo);
    }

    @Override
    public List<OfferedTourInfo> getInfo(Integer offeredTourId) {
        return offeredTourInfoRepository.findAllByOfferedTourId(offeredTourId);
    }

    @Override
    public void deleteByOfferedTourId(Integer offeredTourId) {
        offeredTourInfoRepository.deleteAllByOfferedTourId(offeredTourId);
    }

    @Override
    public OfferedTourInfo getOfferedTourInfo(Integer offeredTourId, Integer language) {
        return offeredTourInfoRepository.findOneByOfferedTourIdAndLanguage(offeredTourId, language);
    }

    @Override
    public List<OfferedTourInfo> getOfferedTourIds(String tourNamesRegexp) {
        return offeredTourInfoRepository.getOfferedTourIds(tourNamesRegexp);

    }
}
