package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.HomePageText;
import com.primesoft.tourism.dao.HomePageTextRepository;
import com.primesoft.tourism.service.interfaces.HomePageTextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class HomePageTextServiceImpl implements HomePageTextService {

    @Autowired
    private HomePageTextRepository homePageTextRepository;

    @Override
    public List<HomePageText> getAll() {
        return homePageTextRepository.findAll();
    }

    @Override
    public List<HomePageText> getAll(Integer language) {
        return homePageTextRepository.findAllByLanguage(language);
    }

    @Override
    public void save(List<HomePageText> homePageText) {
        homePageTextRepository.save(homePageText);
    }

    @Override
    public HomePageText getHomePageText(Integer id) {
        return homePageTextRepository.findOneById(id);
    }
}
