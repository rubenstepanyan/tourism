package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.response.getAllDriverObjects.DriverObject;
import com.primesoft.tourism.beans.response.getAllTourismObjects.TourismObject;
import com.primesoft.tourism.beans.response.getOurPartnersResponse.GetOurPartnersContent;
import com.primesoft.tourism.beans.tableBeans.Driver;
import com.primesoft.tourism.dao.DriverRepository;
import com.primesoft.tourism.service.interfaces.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Service
public class DriverServiceImpl implements DriverService {

    @Autowired
    private DriverRepository driverRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Driver> getAll() {
        return driverRepository.findAll();
    }

    @Override
    public List<TourismObject> getTourismObjects() {

        String query = "SELECT new com.primesoft.tourism.beans.response.getAllTourismObjects.TourismObject(d.id,CONCAT(d.firstName, ' ', d.lastName)) FROM Driver d";

        TypedQuery<TourismObject> typedQuery = entityManager.createQuery(query , TourismObject.class);
        List<TourismObject> results = typedQuery.getResultList();

        return results;
    }

    @Override
    public Driver getDriver(String authorization) {
        return driverRepository.findOneByAuthorization(authorization);
    }

    @Override
    public Driver getDriverByPhone(String phone) {
        return driverRepository.findFirstByPhone(phone);
    }

    @Override
    public void save(Driver driver) {
        driverRepository.save(driver);
    }

    @Override
    public Driver getDriverByPhoneAndPassword(String phone, String password) {
        return driverRepository.findOneByPhoneAndPassword(phone, password);
    }

    @Override
    public Driver getDriverByPhoneAndConfirmationCode(String phone, String confirmationCode) {
        return driverRepository.findOneByPhoneAndConfirmationCode(phone,confirmationCode);
    }

    @Override
    public Driver getDriverByIdAndAuthorization(Integer id, String authorization) {
        return driverRepository.findOneByIdAndAuthorization(id,authorization);
    }

    @Override
    public List<DriverObject> getDrivers(Integer type, String key) {

        String query = null;


        String[] keys;
        String key1 = null;
        String key2 = null;

        if(type == 4) {
            if (key != null) {
                keys = key.split(" ");
                key1 = keys[0];

                if (keys.length > 1) {
                    key2 = keys[1];
                } else {
                    key2 = keys[0];
                }
            }else {
                type = 1;
            }
        }

        if(type == 1){
            query = "SELECT new com.primesoft.tourism.beans.response.getAllDriverObjects.DriverObject(d.id,d.isConfirmed,d.firstName,d.lastName,d.languageSkills,d.currentRating,d.birthDay,d.mail,d.phone,d.registrationDate,d.osType,d.balance,d.photoUrl,d.incomingPercent,d.suspended,d.accountNumber,d.approved) FROM Driver d ";
        }else if(type ==2){
            query = "SELECT new com.primesoft.tourism.beans.response.getAllDriverObjects.DriverObject(d.id,d.isConfirmed,d.firstName,d.lastName,d.languageSkills,d.currentRating,d.birthDay,d.mail,d.phone,d.registrationDate,d.osType,d.balance,d.photoUrl,d.incomingPercent,d.suspended,d.accountNumber,d.approved) FROM Driver d WHERE tourismAgencyId IS NULL ";
        }else if(type == 3 && key != null){
            query = "SELECT new com.primesoft.tourism.beans.response.getAllDriverObjects.DriverObject(d.id,d.isConfirmed,d.firstName,d.lastName,d.languageSkills,d.currentRating,d.birthDay,d.mail,d.phone,d.registrationDate,d.osType,d.balance,d.photoUrl,d.incomingPercent,d.suspended,d.accountNumber,d.approved) FROM Driver d WHERE d.tourismAgencyId = '"+key+"'";
        }else if(type == 4 && key != null){
            query = "SELECT new com.primesoft.tourism.beans.response.getAllDriverObjects.DriverObject(d.id,d.isConfirmed,d.firstName,d.lastName,d.languageSkills,d.currentRating,d.birthDay,d.mail,d.phone,d.registrationDate,d.osType,d.balance,d.photoUrl,d.incomingPercent,d.suspended,d.accountNumber,d.approved) FROM Driver d " +
                    " WHERE d.firstName Like '"+key1+"' OR d.firstName Like '"+key2+"' OR  d.lastName Like '"+key1+"' OR d.lastName Like '"+key2+"'";
        }


        TypedQuery<DriverObject> typedQuery = entityManager.createQuery(query , DriverObject.class);
        List<DriverObject> results = typedQuery.getResultList();

        return results;
    }


    @Override
    public Driver getDriver(Integer id) {
        return driverRepository.findOneById(id);
    }

    @Override
    public List<GetOurPartnersContent> getAllDrivers() {
        String query = "SELECT new com.primesoft.tourism.beans.response.getOurPartnersResponse.GetOurPartnersContent(d.id,concat(d.firstName, ' ',d.lastName),d.accountNumber) FROM Driver d where d.isConfirmed=1 and d.tourismAgencyId is null";

        TypedQuery<GetOurPartnersContent> typedQuery = entityManager.createQuery(query , GetOurPartnersContent.class);
        List<GetOurPartnersContent> results = typedQuery.getResultList();

        return results;
    }

    @Override
    public void deleteDriver(Integer id) {
        driverRepository.deleteById(id);
    }
}
