package com.primesoft.tourism.service.implementation;


import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceWorkingDays;
import com.primesoft.tourism.dao.EntertainmentPlaceWorkingDaysRepository;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceWorkingDaysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntertainmentPlaceWorkingDaysServiceImpl implements EntertainmentPlaceWorkingDaysService {

    @Autowired
    private EntertainmentPlaceWorkingDaysRepository repository;

    @Override
    public void save(List<EntertainmentPlaceWorkingDays> workingDays) {
        repository.save(workingDays);
    }


    @Override
    public List<EntertainmentPlaceWorkingDays> findAll(Integer placeId) {
        return repository.findAllByEntertainmentPlaceId(placeId);
    }

    @Override
    public void delete(Integer placeId) {
        repository.deleteAllByEntertainmentPlaceId(placeId);
    }
}
