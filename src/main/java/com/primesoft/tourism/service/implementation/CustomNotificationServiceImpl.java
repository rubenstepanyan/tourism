package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.CustomNotification;
import com.primesoft.tourism.dao.CustomNotificationRepository;
import com.primesoft.tourism.service.interfaces.CustomNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomNotificationServiceImpl implements CustomNotificationService {

    @Autowired
    private CustomNotificationRepository customNotificationRepository;

    @Override
    public void save(CustomNotification customNotification) {
        customNotificationRepository.save(customNotification);
    }

    @Override
    public void delete(Integer customerId) {
        customNotificationRepository.deleteByCustomerId(customerId);
    }

    @Override
    public List<CustomNotification> getAll(Integer customerId) {
        return customNotificationRepository.findAllByCustomerId(customerId);
    }
}
