package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.Follower;
import com.primesoft.tourism.dao.FollowerRepository;
import com.primesoft.tourism.service.interfaces.FollowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FollowerServiceImpl implements FollowerService {
    @Autowired
    private FollowerRepository followerRepository;

    @Override
    public void save(Follower follower) {
        followerRepository.save(follower);
    }

    @Override
    public Follower getFollower(String mail) {
        return followerRepository.findOneByMail(mail);
    }

    @Override
    public List<Follower> getAll() {
        return followerRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        followerRepository.delete(id);
    }
}
