package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.SlideShow;
import com.primesoft.tourism.dao.SlideShowRepository;
import com.primesoft.tourism.service.interfaces.SlideShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SlideShowServiceImpl implements SlideShowService {

    @Autowired
    private SlideShowRepository slideShowRepository;

    @Override
    public void save(SlideShow slideShow) {
        slideShowRepository.save(slideShow);
    }

    @Override
    public void delete(Integer id) {
        slideShowRepository.delete(id);
    }

    @Override
    public List<SlideShow> getPhotos() {
        return slideShowRepository.findAll();
    }
}
