package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.response.getOfferedTourResponse.Content;
import com.primesoft.tourism.beans.tableBeans.OfferedTour;
import com.primesoft.tourism.dao.OfferedTourRepository;
import com.primesoft.tourism.service.interfaces.OfferedTourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

@Service
public class OfferedTourServiceImpl implements OfferedTourService {

    @Autowired
    private OfferedTourRepository offeredTourRepository;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Override
    public void save(OfferedTour offeredTour) {
        offeredTourRepository.save(offeredTour);
    }

    @Override
    public ArrayList<Content> getAll(Integer language) {

        EntityManager session = entityManagerFactory.createEntityManager();


        try {
            String query = "select ot.id,ot.photo_url,oti.name,oti.description,ot.transfer from offered_tour as ot " +
                    " inner join offered_tour_info as oti on ot.id=oti.offered_tour_id where oti.language="+language;



            List<Object[]> result = session.createNativeQuery(query+";")
                    .getResultList();

            ArrayList<Content> offeredTours = new ArrayList<>();
            for (int i=0; i<result.size();i++){
                Content tour = new Content(result.get(i));
                offeredTours.add(tour);
            }
            return offeredTours;
        } finally {
            if (session.isOpen()) session.close();
        }
    }


    @Override
    public ArrayList<Content> getAll(Boolean transfer, Integer language) {

        EntityManager session = entityManagerFactory.createEntityManager();


        try {
            String query = "select ot.id,ot.photo_url,oti.name,oti.description,ot.transfer from offered_tour as ot " +
                    " inner join offered_tour_info as oti on ot.id=oti.offered_tour_id where ot.transfer=0 and oti.language="+language;



            List<Object[]> result = session.createNativeQuery(query+";")
                    .getResultList();

            ArrayList<Content> offeredTours = new ArrayList<>();
            for (int i=0; i<result.size();i++){
                Content tour = new Content(result.get(i));
                offeredTours.add(tour);
            }
            return offeredTours;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public ArrayList<com.primesoft.tourism.beans.response.getOfferedTourCustomerResponse.Content> getAllOfferedTour(Integer language, Boolean transfer) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select ot.id, ot.photo_url,round(avg(t.price),0),oti.name " +
                    " from offered_tour ot inner join tour as t on ot.id=t.offered_tour_id inner join offered_tour_info as oti on ot.id=oti.offered_tour_id " +
                    " where oti.language="+language;

            if(transfer != null){
                query += " and ot.transfer="+transfer;
            }

            query += " group by oti.id;";

            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<com.primesoft.tourism.beans.response.getOfferedTourCustomerResponse.Content> offeredTours = new ArrayList<>();
            for (int i=0; i<result.size();i++){
                com.primesoft.tourism.beans.response.getOfferedTourCustomerResponse.Content tour = new com.primesoft.tourism.beans.response.getOfferedTourCustomerResponse.Content(result.get(i));
                offeredTours.add(tour);
            }
            return offeredTours;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public OfferedTour getOfferedTour(Integer id) {
        return offeredTourRepository.findOne(id);
    }


    @Override
    public void delete(Integer id) {
        offeredTourRepository.delete(id);
    }
}
