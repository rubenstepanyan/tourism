package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.response.GetAllCommentsResponse;
import com.primesoft.tourism.beans.response.getBlogCommentResponse.BlogCommentContent;
import com.primesoft.tourism.beans.tableBeans.Comment;
import com.primesoft.tourism.dao.CommentRepository;
import com.primesoft.tourism.service.interfaces.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;


    @Autowired
    private EntityManagerFactory entityManagerFactory;


    @Autowired
    private EntityManager entityManager;



    @Override
    public void delete(Integer blogId) {
        commentRepository.deleteByBlogId(blogId);
    }

    @Override
    public void save(Comment comment) {
        commentRepository.save(comment);
    }

    @Override
    public void deleteComment(Integer id) {
        commentRepository.delete(id);
    }

    @Override
    public void deleteComment(Integer id, Integer customerId) {
        commentRepository.deleteByIdAndCustomerId(id, customerId);
    }

    @Override
    public ArrayList<BlogCommentContent> getAllBlogComment(Integer blogId, Integer lastId) {

        EntityManager session = entityManagerFactory.createEntityManager();


        try {
            List<Object[]> result = session.createNativeQuery("select com.id,com.customer_id,com.blog_id,com.comment,com.comment_date,cus.first_name,cus.last_name,cus.photo_url from comment as com " +
                    " inner join customer as cus on com.customer_id=cus.id where com.blog_id="+blogId+" and com.id<"+lastId+" ORDER BY id DESC LIMIT 20;")
                    .getResultList();

            ArrayList<BlogCommentContent> comments = new ArrayList<>();
            for (int i=0; i<result.size();i++){
                BlogCommentContent comment = new BlogCommentContent(result.get(i));
                comments.add(comment);
            }
            return comments;
        } finally {
            if (session.isOpen()) session.close();
        }
    }


    @Override
    public Comment getComment(Integer id, Integer customerId) {
        return commentRepository.findFirstByIdAndCustomerId(id, customerId);
    }

    @Override
    public List<GetAllCommentsResponse> getAllComments(String from, String to) {

        String query = "SELECT new com.primesoft.tourism.beans.response.GetAllCommentsResponse(c.id,c.customerId,c.blogId,c.comment,c.commentDate)" +
                " FROM Comment c WHERE c.commentDate>="+from+" and c.commentDate<="+to+"";

        TypedQuery<GetAllCommentsResponse> typedQuery = entityManager.createQuery(query , GetAllCommentsResponse.class);
        List<GetAllCommentsResponse> results = typedQuery.getResultList();

        return results;
    }

    @Override
    public Comment getComment(Integer id) {
        return commentRepository.findOneById(id);
    }


    @Override
    public Comment getOneComment(Integer id) {
        return commentRepository.findOneById(id);
    }
}
