package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.response.getAllGuideObjects.GuideObject;
import com.primesoft.tourism.beans.response.getAllGuideResponse.GetAllGuideContentResponse;
import com.primesoft.tourism.beans.response.getOurPartnersResponse.GetOurPartnersContent;
import com.primesoft.tourism.beans.tableBeans.Guide;
import com.primesoft.tourism.dao.GuideRepository;
import com.primesoft.tourism.service.interfaces.GuideService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Service
public class GuideServiceImpl implements GuideService {

    @Autowired
    private GuideRepository guideRepository;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Override
    public Guide getGuid(String phone, String password) {
        return guideRepository.findOneByPhoneAndPassword(phone, password);
    }

    @Override
    public void save(Guide guide) {
        guideRepository.save(guide);
    }

    @Override
    public Guide getGuidByPhone(String phone) {
        return guideRepository.findOneByPhone(phone);
    }

    @Override
    public Guide getGuidByAuthorization(String authorization) {
        return guideRepository.findOneByAuthorization(authorization);
    }

    @Override
    public Guide getGuidById(Integer id) {
        return guideRepository.findOneById(id);
    }

    @Override
    public Guide getGuidByPhoneAndConfirmationCode(String phone, String confirmationCode) {
        return guideRepository.findOneByPhoneAndConfirmationCode(phone, confirmationCode);
    }

    @Override
    public List<GuideObject> getGuide(Integer type, String key) {
        String query = null;


        String[] keys;
        String key1 = null;
        String key2 = null;

        if(type == 4) {
            if (key != null) {
                keys = key.split(" ");
                key1 = keys[0];

                if (keys.length > 1) {
                    key2 = keys[1];
                } else {
                    key2 = keys[0];
                }
            }else {
                type = 1;
            }
        }

        if(type == 1){
            query = "SELECT new com.primesoft.tourism.beans.response.getAllGuideObjects.GuideObject(g.id,g.isConfirmed,g.firstName,g.lastName,g.birthDay,g.mail,g.phone,g.registrationDate,g.osType,g.photoUrl,g.suspended,g.city,g.sex,g.accountNumber,g.approved,g.languageSkills) FROM Guide g ";
        }else if(type ==2){
            query = "SELECT new com.primesoft.tourism.beans.response.getAllGuideObjects.GuideObject(g.id,g.isConfirmed,g.firstName,g.lastName,g.birthDay,g.mail,g.phone,g.registrationDate,g.osType,g.photoUrl,g.suspended,g.city,g.sex,g.accountNumber,g.approved,g.languageSkills) FROM Guide g WHERE tourismAgencyId IS NULL ";
        }else if(type == 3 && key != null){
            query = "SELECT new com.primesoft.tourism.beans.response.getAllGuideObjects.GuideObject(g.id,g.isConfirmed,g.firstName,g.lastName,g.birthDay,g.mail,g.phone,g.registrationDate,g.osType,g.photoUrl,g.suspended,g.city,g.sex,g.accountNumber,g.approved,g.languageSkills) FROM Guide g WHERE g.tourismAgencyId = '"+key+"'";
        }else if(type == 4 && key != null){
            query = "SELECT new com.primesoft.tourism.beans.response.getAllGuideObjects.GuideObject(g.id,g.isConfirmed,g.firstName,g.lastName,g.birthDay,g.mail,g.phone,g.registrationDate,g.osType,g.photoUrl,g.suspended,g.city,g.sex,g.accountNumber,g.approved,g.languageSkills) FROM Guide g " +
                    " WHERE g.firstName Like '"+key1+"' OR g.firstName Like '"+key2+"' OR  g.lastName Like '"+key1+"' OR g.lastName Like '"+key2+"'";
        }


        TypedQuery<GuideObject> typedQuery = entityManager.createQuery(query , GuideObject.class);
        List<GuideObject> results = typedQuery.getResultList();

        return results;
    }


    @Override
    public List<GetAllGuideContentResponse> getAll(String from, String to, String guideLanguage, Integer type,Integer city) {

        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select g.id,g.photo_url,concat(g.first_name, ' ',g.last_name) as fullName,TIMESTAMPDIFF(YEAR, g.birth_day, CURDATE()) as age, gpl.rating, gpl.language,gpl.price,gpl.type,gpl.city, g.approved, gpl.id as priceListId " +
                    " from guide g inner join guide_price_list as gpl on g.id=gpl.guide_id " +
                    " left join guide_reservation as gr on g.id=gr.guide_id where g.suspended=false  and gpl.type="+type+" and gpl.city="+city;

            if(guideLanguage != null){
                query += " and gpl.language='"+guideLanguage+"' ";
            }

            if(from != null && to != null){
                query += " and (gr.reservation_start is null or ((gr.reservation_start>'"+from+"' or gr.reservation_end<'"+from+"') ";
                query += " and (gr.reservation_start>'"+to+"' or gr.reservation_end<'"+to+"'))) ";
            }

            System.out.println(query+";");

            List<Object[]> result = session.createNativeQuery(query+";")
                    .getResultList();

            ArrayList<GetAllGuideContentResponse> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GetAllGuideContentResponse content = new GetAllGuideContentResponse(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public List<GetOurPartnersContent> getAllGuides() {
        String query = "SELECT new com.primesoft.tourism.beans.response.getOurPartnersResponse.GetOurPartnersContent(g.id,concat(g.firstName, ' ',g.lastName),g.accountNumber) FROM Guide g where g.isConfirmed=1 and g.tourismAgencyId is null";

        TypedQuery<GetOurPartnersContent> typedQuery = entityManager.createQuery(query , GetOurPartnersContent.class);
        List<GetOurPartnersContent> results = typedQuery.getResultList();

        return results;
    }

    @Override
    public void deleteGuide(Integer id) {
        guideRepository.deleteById(id);
    }
}
