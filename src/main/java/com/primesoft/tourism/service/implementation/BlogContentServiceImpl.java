package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.BlogContent;
import com.primesoft.tourism.dao.BlogContentRepository;
import com.primesoft.tourism.service.interfaces.BlogContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogContentServiceImpl implements BlogContentService {

    @Autowired
    private BlogContentRepository blogContentRepository;

    @Override
    public void save(List<BlogContent> blogContents) {
        blogContentRepository.save(blogContents);
    }

    @Override
    public List<BlogContent> getBlogContent(Integer blogId) {
        return blogContentRepository.findAllByBlogId(blogId);
    }

    @Override
    public void delete(Integer blogId) {
        blogContentRepository.deleteByBlogId(blogId);
    }
}
