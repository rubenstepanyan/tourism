package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.City;
import com.primesoft.tourism.dao.CityRepository;
import com.primesoft.tourism.service.interfaces.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityRepository cityRepository;

    @Override
    public void save(City city) {
        cityRepository.save(city);
    }

    @Override
    public void save(List<City> city) {
        cityRepository.save(city);
    }

    @Override
    public void update(Integer id, String name,Integer priority,Boolean notShowInTours) {
        cityRepository.updateCity(id, name,priority, notShowInTours);
    }

    @Override
    public List<City> getAll(Integer language, Boolean notShowInTours) {
        return cityRepository.findAllByLanguageAndNotShowInToursOrderByPriorityDesc(language, notShowInTours);
    }

    @Override
    public List<City> getAllByParentId(Integer parentId) {
        return cityRepository.findAllByParentIdOrderByPriorityDesc(parentId);
    }

    @Override
    public List<City> getAll() {
        return cityRepository.findAll();
    }

    @Override
    public void deleteCity(Integer parentId) {
        cityRepository.deleteAllByParentId(parentId);
    }

    @Override
    public List<City> getAllByLanguage(Integer language) {
        return cityRepository.findAllByLanguageOrderByPriorityDesc(language);
    }
}
