package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.response.GetAllAdministratorResponse.GetAllAdministratorsResponseContent;
import com.primesoft.tourism.beans.tableBeans.Administrator;
import com.primesoft.tourism.dao.AdministratorRepository;
import com.primesoft.tourism.service.interfaces.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

@Service
public class AdministratorServiceImpl implements AdministratorService {

    @Autowired
    private AdministratorRepository administratorRepository;



    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Override
    public void saveAdministrator(Administrator administrator) {
        administratorRepository.save(administrator);
    }

    @Override
    public Administrator getAdministratorByAuthorization(String authorization) {
        return administratorRepository.findOneByAuthorization(authorization);
    }

    @Override
    public Administrator getAdministratorByUserName(String userName) {
        return administratorRepository.findOneByUserName(userName);
    }

    @Override
    public Administrator getAdministratorByUserNameAndPassword(String userName, String password) {
        return administratorRepository.findOneByUserNameAndPassword(userName,password);
    }

    @Override
    public void deleteById(Integer id) {
        administratorRepository.deleteById(id);
    }

    @Override
    public List<GetAllAdministratorsResponseContent> getAdministrator() {

        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select a.id, a.first_name, a.last_name, a.user_name, a.mail, a.address, a.passport, a.phone from  administrator a;" ;


            List<Object[]> result = session.createNativeQuery(query+";")
                    .getResultList();

            ArrayList<GetAllAdministratorsResponseContent> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GetAllAdministratorsResponseContent content = new GetAllAdministratorsResponseContent(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }
}
