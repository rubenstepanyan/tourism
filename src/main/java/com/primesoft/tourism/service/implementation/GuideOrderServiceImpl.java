package com.primesoft.tourism.service.implementation;


import com.primesoft.tourism.beans.response.getCustomerHistory.GetCustomerGuideOrderHistoryContent;
import com.primesoft.tourism.beans.response.getCustomerOrders.GuideOrderContent;
import com.primesoft.tourism.beans.response.getGuidePaymentsResponse.GuidePaymentContent;
import com.primesoft.tourism.beans.response.getHistoryResponse.GetGuideOrderHistoryContentResponse;
import com.primesoft.tourism.beans.response.getOrdersResponse.GetGuideOrderContentsResponse;
import com.primesoft.tourism.beans.tableBeans.GuideOrder;
import com.primesoft.tourism.dao.GuideOrderRepository;
import com.primesoft.tourism.service.interfaces.GuideOrderService;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

@Service
public class GuideOrderServiceImpl implements GuideOrderService {

    @Autowired
    private GuideOrderRepository guideOrderRepository;


    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Override
    public void save(GuideOrder guideOrder) {
        guideOrderRepository.save(guideOrder);
    }

    @Override
    public List<GuideOrderContent> getGuideOrderByCustomerId(Integer customerId) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select gr.id,gr.language,gr.quantity_type as quantity,gr.price,gr.status,gr.from_date,gr.to_date,concat(g.first_name, ' ',g.last_name) as fullName,TIMESTAMPDIFF(YEAR, g.birth_day, CURDATE()) as age,g.photo_url, gr.qr, g.phone, gr.payed, gr.error_message, gr.direction, gpl.rating " +
                    " from guide_order gr inner join guide as g on gr.guide_id=g.id inner join guide_price_list as gpl on gr.guide_id=gpl.guide_id where gr.status IN (1,3,4) and gr.customer_id="+customerId+"   GROUP BY gr.id ORDER BY gr.id DESC ;";


            System.out.println("guide  :"+ query);



            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<GuideOrderContent> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GuideOrderContent content = new GuideOrderContent(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public List<GetGuideOrderContentsResponse> getGuideOrder(Integer agencyId, Integer guideId) {


        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select gr.id,gr.language,gr.quantity_type,gr.price,gr.from_date,gr.to_date,concat(g.first_name, ' ',g.last_name) as fullName,concat(c.first_name, ' ',c.last_name) as customerName,gr.status, gr.direction, c.photo_url as customerPhotoUrl, gpl.city, c.nationality, c.phone, TIMESTAMPDIFF(YEAR, g.birth_day, CURDATE()) as age " +
                    " from guide_order gr inner join guide as g on gr.guide_id=g.id inner join customer as c on gr.customer_id=c.id inner join guide_price_list as gpl on gr.guide_price_list_id=gpl.id where gr.status IN (1,3,4) and  (g.tourism_agency_id="+agencyId+" or g.id="+guideId+") ORDER BY gr.id desc ;";


            System.out.println("\n guide query: "+query+"\n");

            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<GetGuideOrderContentsResponse> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GetGuideOrderContentsResponse content = new GetGuideOrderContentsResponse(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public List<GetCustomerGuideOrderHistoryContent> getCustomerGuideOrderHistory(Integer customerId) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select gr.id,gr.language,gr.quantity_type,gr.price as totalPrice,gr.status,gr.from_date,gr.to_date,concat(g.first_name, ' ',g.last_name) as guideFullName,TIMESTAMPDIFF(YEAR, g.birth_day, CURDATE()) as age," +
                    " concat(cu.first_name, ' ',cu.last_name) as customerName,cu.id as customerId,gr.price_per_day as pricePerDay,g.photo_url, gr.price_rating, gr.direction, gr.rating as customerRating " +
                    " from guide_order gr inner join guide as g on gr.guide_id=g.id inner join customer as cu on gr.customer_id=cu.id " +
                    " where gr.status IN (2,5,6) and gr.customer_id="+customerId+";";

            System.out.println(query);

            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<GetCustomerGuideOrderHistoryContent> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GetCustomerGuideOrderHistoryContent content = new GetCustomerGuideOrderHistoryContent(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public List<GetGuideOrderHistoryContentResponse> getGuideOrderHistory(Integer agencyId, Integer guideId) {

        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select gr.id,gr.language,gr.quantity_type,ROUND(gr.price*0.90, 0) as totalPrice,gr.status,gr.from_date,gr.to_date,concat(g.first_name, ' ',g.last_name) as guideFullName,TIMESTAMPDIFF(YEAR, g.birth_day, CURDATE()) as age," +
                    " concat(cu.first_name, ' ',cu.last_name) as customerName,cu.id as customerId,gr.price_per_day as pricePerDay,gr.direction,  cu.photo_url as customerPhotoUrl, gr.city, cu.nationality, gr.my_trips_paid, cu.phone " +
                    " from guide_order gr inner join guide as g on gr.guide_id=g.id inner join customer as cu on gr.customer_id=cu.id " +
                    " where gr.status IN (2,5,6) and (g.tourism_agency_id="+agencyId+" or g.id="+guideId+") order by gr.id desc ;";


            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<GetGuideOrderHistoryContentResponse> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GetGuideOrderHistoryContentResponse content = new GetGuideOrderHistoryContentResponse(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public GuideOrder getGuideOrder(Integer id, Integer quantityType, String language) {
        return guideOrderRepository.findOneByIdAndQuantityTypeAndLanguage(id, quantityType, language);
    }

    @Override
    public GuideOrder getGuideOrderByIdAndCustomerId(Integer id, Integer customerId) {
        return guideOrderRepository.findOneByIdAndCustomerId(id, customerId);
    }

    @Override
    public List<GuideOrder> getAutoPartnerCancel() {
        String currentDate = Util.getCurrentDateAndTimeSeconds();
        return guideOrderRepository.getAutoPartnerCancel(currentDate);
    }

    @Override
    public List<GuideOrder> getAutoCustomerCancel() {
        String currentDate = Util.getCurrentDateAndTimeSeconds();
        return guideOrderRepository.getAutoCustomerCancel(currentDate);
    }

    @Override
    public void save(List<GuideOrder> guideOrders) {
        guideOrderRepository.save(guideOrders);
    }

    @Override
    public GuideOrder getGuideOrder(Integer id) {
        return guideOrderRepository.findOneById(id);
    }

    @Override
    public List<GuidePaymentContent> getAllPayments(Integer reference, Integer type, String from, String to) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select g_o.id,g_o.quantity_type,g_o.price as totalPrice, ROUND(g_o.price*0.90, 0) as toPay, g_o.from_date,g_o.to_date,g_o.status,gpl.price as price," +
                    " concat(g.first_name, ' ',g.last_name) as guideName,concat(c.first_name, ' ',c.last_name) as customerName,c.mail as customerMail,c.phone as customerPhone, g_o.reason_comment, g_o.my_trips_paid , g_o.direction, ci.name, c.photo_url,g_o.language, g_o.refund " +
                    " from guide_order AS g_o inner join guide as g on g_o.guide_id=g.id LEFT OUTER JOIN guide_price_list as gpl on gpl.id=g_o.guide_price_list_id inner join customer as c on g_o.customer_id=c.id LEFT OUTER  JOIN city as ci on ci.id=gpl.city " +
                    " WHERE STR_TO_DATE(g_o.from_date, '%Y-%m-%d')>='"+from+"' AND STR_TO_DATE(g_o.to_date, '%Y-%m-%d')<='"+to+"'";


            if(reference!=0) {
                if(type == 1){
                    query += " AND g_o.reference=" + reference;
                }else if(type == 0){
                    query += " AND g_o.guide_id=" + reference;
                }
            }else{
                query+= ";";
            }

            System.out.println(query);
            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<GuidePaymentContent> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GuidePaymentContent content = new GuidePaymentContent(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public void updateGuideOrders(List<Integer> ids) {
        guideOrderRepository.updateGuideOrders(ids);
    }


    @Override
    public GuideOrder getGuideOrderByQr(String qr, Boolean payed, Integer status, Integer id) {
        return guideOrderRepository.findOneByQrAndPayedAndStatusAndReference(qr, payed, status, id );
    }

    @Override
    public GuideOrder getGuideOrderByQrAndGuideId(String qr, Boolean payed, Integer status, Integer id) {
        return guideOrderRepository.findOneByQrAndPayedAndStatusAndGuideId(qr, payed, status, id);
    }

    @Override
    public List<GuideOrder> getAutoFinish() {
        String currentDate = Util.getCurrentDateAndTimeSeconds();
        return guideOrderRepository.getAutoFinish(currentDate);
    }


    @Override
    public GuidePaymentContent getGuideOrderByQrInfo(String qr) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {


            String query = "select g_o.id,g_o.quantity_type,g_o.price as totalPrice, ROUND(g_o.price*0.90, 0) as toPay, g_o.from_date,g_o.to_date,g_o.status,gpl.price as price," +
                    " concat(g.first_name, ' ',g.last_name) as guideName,concat(c.first_name, ' ',c.last_name) as customerName,c.mail as customerMail,c.phone as customerPhone, g_o.reason_comment, g_o.my_trips_paid , g_o.direction, ci.name, c.photo_url,g_o.language " +
                    " from guide_order AS g_o inner join guide as g on g_o.guide_id=g.id inner JOIN guide_price_list as gpl on gpl.id=g_o.guide_price_list_id inner join customer as c on g_o.customer_id=c.id  inner join city as ci on ci.id=gpl.city " +
                    " where  g_o.qr= '"+qr+"';";



            System.out.println(query);

            List<Object[]> result = session.createNativeQuery(query).getResultList();

            if(result!=null && result.size()>0) {
                return new GuidePaymentContent(result.get(0));


            }else {
                return null;
            }
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public List<GuideOrder> getGuideOrderByGuideIdAndStatus(Integer guideId, Integer status) {
        return guideOrderRepository.findAllByGuideIdAndStatus(guideId,status);
    }

    @Override
    public List<GuideOrder> getGuideOrderByCustomerIdAndStatus(Integer customerId, Integer status) {
        return guideOrderRepository.findAllByCustomerIdAndStatus(customerId, status);
    }
}