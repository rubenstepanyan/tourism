package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.response.getAllCustomerObjects.CustomerObject;
import com.primesoft.tourism.beans.tableBeans.Customer;
import com.primesoft.tourism.dao.CustomerRepository;
import com.primesoft.tourism.service.interfaces.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public Customer getCustomerByMailAndPassword(String mail, String password) {
        return customerRepository.findOneByMailAndPassword(mail,password);
    }

    @Override
    public Customer getCustomerByMail(String mail) {
        return customerRepository.findFirstByMail(mail);
    }

    @Override
    public void save(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public Customer getCustomerByMailAndConfirmationCode(String mail, String confirmationCode) {
        return customerRepository.findOneByMailAndConfirmationCode(mail, confirmationCode);
    }

    @Override
    public Customer getCustomerByAuthorization(String authorization) {
        return customerRepository.findOneByAuthorization(authorization);
    }

    @Override
    public List<CustomerObject> getCustomerObjects() {

        String query = "SELECT new com.primesoft.tourism.beans.response.getAllCustomerObjects.CustomerObject(c.id, c.isConfirmed, c.firstName, c.lastName, c.birthDay, c.mail, c.phone, c.registrationDate, c.osType, c.photoUrl) FROM Customer c";

        TypedQuery<CustomerObject> typedQuery = entityManager.createQuery(query , CustomerObject.class);
        List<CustomerObject> results = typedQuery.getResultList();

        return results;
    }

    @Override
    public Customer getCustomer(Integer id) {
        return customerRepository.findOneById(id);
    }

    @Override
    public Customer getCustomerByPhone(String phone) {
        return customerRepository.findFirstByPhone(phone);
    }
}
