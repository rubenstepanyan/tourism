package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.response.getAllBlogResponse.BlogContentResponse;
import com.primesoft.tourism.beans.tableBeans.Blog;
import com.primesoft.tourism.dao.BlogRepository;
import com.primesoft.tourism.service.interfaces.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    private BlogRepository blogRepository;


    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Override
    public void save(Blog blog) {
        blogRepository.save(blog);
    }

    @Override
    public Blog getBlog(Integer id) {
        return blogRepository.findOneById(id);
    }

    @Override
    public void delete(Integer id) {
        blogRepository.delete(id);
    }

    @Override
    public Long getCount() {
        return blogRepository.count();
    }

    @Override
    public ArrayList<BlogContentResponse> getAllBlog(Integer language, Integer startPosition, Integer itemCount) {

        EntityManager session = entityManagerFactory.createEntityManager();

        try {
            List<Object[]> result = session.createNativeQuery("select b.id,b.icon_url,b.comment_count,bc.title from blog as b " +
                    " inner join blog_content as bc on b.id=bc.blog_id where bc.language="+language+" ORDER BY id DESC LIMIT "+startPosition+","+ itemCount +";")
                    .getResultList();

            ArrayList<BlogContentResponse> blogItems = new ArrayList<>();
            for (int i=0; i<result.size();i++){
                BlogContentResponse blog = new BlogContentResponse(result.get(i));
                blogItems.add(blog);
            }
            return blogItems;
        } finally {
            if (session.isOpen()) session.close();
        }
    }
}
