package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.Arca;
import com.primesoft.tourism.dao.ArcaRepository;
import com.primesoft.tourism.service.interfaces.ArcaService;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArcaServiceImpl implements ArcaService {

    @Autowired
    private ArcaRepository arcaRepository;

    @Override
    public void save(Arca arca) {
        arcaRepository.save(arca);
    }

    @Override
    public void delete(Integer id) {
        arcaRepository.delete(id);
    }

    @Override
    public List<Arca> getArcaList(Integer customerId, Boolean charged) {
        return arcaRepository.findAllByCustomerIdAndCharged(customerId, charged);
    }

    @Override
    public List<Arca> getArcaList(Boolean charged) {
        return arcaRepository.findAllByCharged(charged);
    }

    @Override
    public Arca getArca(Integer customerId, Integer type, Integer reference, Boolean charged) {
        return arcaRepository.findFirstByCustomerIdAndTypeAndReferenceAndCharged(customerId, type, reference, charged);
    }

    @Override
    public void deleteArca() {
        String currentDate = Util.getCurrentDateAndTimeSeconds();
        arcaRepository.deleteArca(currentDate);
    }

    @Override
    public Arca getArca(String arcaOrderId) {
        return arcaRepository.findOneByOrderId(arcaOrderId);
    }
}
