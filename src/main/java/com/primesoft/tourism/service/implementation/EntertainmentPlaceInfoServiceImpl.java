package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.response.getOurPartnersResponse.GetOurPartnersContentEntertainmentPlace;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlaceInfo;
import com.primesoft.tourism.dao.EntertainmentPlaceInfoRepository;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Service
public class EntertainmentPlaceInfoServiceImpl implements EntertainmentPlaceInfoService {

    @Autowired
    private EntertainmentPlaceInfoRepository entertainmentPlaceInfoRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public void save(List<EntertainmentPlaceInfo> placeInfoList) {
        entertainmentPlaceInfoRepository.save(placeInfoList);
    }

    @Override
    public EntertainmentPlaceInfo getInfo(Integer placeId, Integer languageId) {
        return entertainmentPlaceInfoRepository.findFirstByEntertainmentPlaceIdAndLanguage(placeId,languageId);
    }


    @Override
    public void delete(Integer placeId) {
        entertainmentPlaceInfoRepository.deleteAllByEntertainmentPlaceId(placeId);
    }

    @Override
    public List<EntertainmentPlaceInfo> getAllInfo(Integer id) {
        return entertainmentPlaceInfoRepository.findAllByEntertainmentPlaceId(id);
    }

    @Override
    public List<GetOurPartnersContentEntertainmentPlace> getAllEntertainmentPlace(Integer language) {
        String query = "SELECT new com.primesoft.tourism.beans.response.getOurPartnersResponse.GetOurPartnersContentEntertainmentPlace(epi.entertainmentPlaceId,epi.name) FROM EntertainmentPlaceInfo epi WHERE language="+language;

        TypedQuery<GetOurPartnersContentEntertainmentPlace> typedQuery = entityManager.createQuery(query , GetOurPartnersContentEntertainmentPlace.class);
        List<GetOurPartnersContentEntertainmentPlace> results = typedQuery.getResultList();

        return results;
    }
}
