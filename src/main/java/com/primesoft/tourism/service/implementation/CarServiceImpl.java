package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.Car;
import com.primesoft.tourism.dao.CarRepository;
import com.primesoft.tourism.service.interfaces.CarService;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepository;

    @Override
    public void saveCar(Car car) {
        carRepository.save(car);
    }

    @Override
    public Car getCar(Integer id) {
        return carRepository.findOneById(id);
    }

    @Override
    public Car getDriverCar(Integer driverId) {
        return carRepository.findFirstByReferenceAndType(driverId, Constants.DRIVER_TYPE);
    }

    @Override
    public List<Car> getCars(Integer reference, Integer type) {
        return carRepository.findAllByReferenceAndType(reference,type);
    }

    @Override
    public Car getCar(Integer reference, Integer type) {
        return carRepository.findOneByReferenceAndType(reference, type);
    }

    @Override
    public void delete(Integer id) {
        carRepository.delete(id);
    }
}
