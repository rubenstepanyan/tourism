package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.VideoContent;
import com.primesoft.tourism.dao.VideoContentRepository;
import com.primesoft.tourism.service.interfaces.VideoContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VideoContentServiceImpl implements VideoContentService {

    @Autowired
    private  VideoContentRepository videoContentRepository;

    @Override
    public void save(List<VideoContent> videoContent) {
        videoContentRepository.save(videoContent);
    }

    @Override
    public List<VideoContent> getVideoContent(Integer videoId) {
        return videoContentRepository.findAllByVideoId(videoId);
    }

    @Override
    public void delete(Integer videoId) {
        videoContentRepository.deleteAllByVideoId(videoId);
    }
}
