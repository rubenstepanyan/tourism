package com.primesoft.tourism.service.implementation;


import com.primesoft.tourism.beans.response.getEntertainmentPlaces.Content;
import com.primesoft.tourism.beans.tableBeans.EntertainmentPlace;
import com.primesoft.tourism.dao.EntertainmentPlaceRepository;
import com.primesoft.tourism.service.interfaces.EntertainmentPlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

@Service
public class EntertainmentPlaceServiceImpl implements EntertainmentPlaceService {

    @Autowired
    private EntertainmentPlaceRepository entertainmentPlaceRepository;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Override
    public void save(EntertainmentPlace entertainmentPlace) {
        entertainmentPlaceRepository.save(entertainmentPlace);
    }

    @Override
    public Long getCount(Integer type, Integer city) {
        if(type.equals(0) && city.equals(0)){
            return entertainmentPlaceRepository.count();
        }else if(type.equals(0)){
            return entertainmentPlaceRepository.countAllByCity(city);
        }else if(city.equals(0)){
            return entertainmentPlaceRepository.countAllByType(type);
        }else {
            return entertainmentPlaceRepository.countAllByTypeAndCity(type, city);
        }
    }

    @Override
    public List<Content> getAll(Integer language, Integer type, Integer city, Integer startPosition, Integer itemCount) {

        EntityManager session = entityManagerFactory.createEntityManager();

        try {
            String query ="select ep.id,epi.name,ep.icon_url,ep.discount,ep.price,ep.max_persons,ep.ent_price, ep.lat, ep.lng, epi.address from entertainment_place as ep " +
                    " inner join entertainment_place_info as epi on ep.id=epi.entertainment_place_id where ";

            if(!type.equals(0)) {
                query += " ep.type=" + type + " AND ";
            }
            if(!city.equals(0)) {
                query += " ep.city=" + city + " AND ";
            }

            query += " epi.language="+language+" ORDER BY id DESC LIMIT "+startPosition+","+ itemCount +";";


            List<Object[]> result = session.createNativeQuery(query).getResultList();

            ArrayList<Content> places = new ArrayList<>();
            for (int i=0; i<result.size();i++){
                Content place = new Content(result.get(i));
                places.add(place);
            }

            return places;
        } finally {
            if (session.isOpen()) session.close();
        }

    }

    @Override
    public EntertainmentPlace getById(Integer id) {
        return entertainmentPlaceRepository.findOne(id);
    }

    @Override
    public void delete(Integer id) {
        entertainmentPlaceRepository.delete(id);
    }

    @Override
    public List<Content> search(Integer language, String name) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {
            String query ="select ep.id,epi.name,ep.icon_url,ep.discount,ep.price,ep.max_persons,ep.ent_price, ep.lat, ep.lng,epi.address  from entertainment_place as ep " +
                    " inner join entertainment_place_info as epi on ep.id=epi.entertainment_place_id where epi.name LIKE '%"+name+"%' GROUP BY epi.entertainment_place_id ";


            List<Object[]> result = session.createNativeQuery(query).getResultList();

            ArrayList<Content> places = new ArrayList<>();
            for (int i=0; i<result.size();i++){
                Content place = new Content(result.get(i));
                places.add(place);
            }

            return places;
        } finally {
            if (session.isOpen()) session.close();
        }
    }
}
