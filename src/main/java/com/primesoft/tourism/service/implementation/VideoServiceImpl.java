package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.response.getAllVideoResponse.VideoContentResponse;
import com.primesoft.tourism.beans.tableBeans.Video;
import com.primesoft.tourism.dao.VideoRepository;
import com.primesoft.tourism.service.interfaces.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

@Service
public class VideoServiceImpl implements VideoService {

    @Autowired
    private VideoRepository videoRepository;


    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Override
    public void save(Video video) {
        videoRepository.save(video);
    }

    @Override
    public Video getVideo(Integer id) {
        return videoRepository.findOneById(id);
    }

    @Override
    public ArrayList<VideoContentResponse> getAllVideos(Integer language,Integer startPosition,Integer itemCount) {
        EntityManager session = entityManagerFactory.createEntityManager();


        try {
            List<Object[]> result = session.createNativeQuery("select v.id,v.url,v.icon_url,vc.title from video as v " +
                    " inner join video_content as vc on v.id=vc.video_id where vc.language="+language+" ORDER BY id DESC LIMIT "+startPosition+","+ itemCount +";")
                    .getResultList();

            ArrayList<VideoContentResponse> videos = new ArrayList<>();
            for (int i=0; i<result.size();i++){
                VideoContentResponse video = new VideoContentResponse(result.get(i));
                videos.add(video);
            }
            return videos;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public Long getCount() {
        return videoRepository.count();
    }

    @Override
    public void delete(Integer id) {
        videoRepository.delete(id);
    }
}
