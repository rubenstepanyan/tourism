package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.response.getAllTouesResponse.GetAllTourContentResponse;
import com.primesoft.tourism.beans.response.getTourResponse.TourContent;
import com.primesoft.tourism.beans.tableBeans.Tour;
import com.primesoft.tourism.dao.TourRepository;
import com.primesoft.tourism.service.interfaces.TourService;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

@Service
public class TourServiceImpl implements TourService {

    @Autowired
    private TourRepository tourRepository;


    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Override
    public void save(Tour tour) {
        tourRepository.save(tour);
    }

    @Override
    public void deleteByOfferedTourId(Integer offeredTourId) {
        tourRepository.deleteAllByOfferedTourId(offeredTourId);
    }

    @Override
    public List<GetAllTourContentResponse> getAll(Integer language) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {
            String query = "select t.id,t.tour_type,t.car_info,t.local_name,t.price,t.start_city,t.description,t.start_date," +
                    " t.days_of_week,t.hour,t.start_address,t.lat,t.lng,t.start_time,t.language_skills,t.guided,oti.name,t.offered_tour_id,t.car_id, t.minute, t.free_seats, t.pick_me_up, t.transfer,c.photo_url as carImage "+
                    " from tour t inner join offered_tour_info as oti on t.offered_tour_id=oti.offered_tour_id inner join car as c on t.car_id=c.id" +
                    " where oti.language="+language+" AND t.deleted=false ;";


            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<GetAllTourContentResponse> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GetAllTourContentResponse content = new GetAllTourContentResponse(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public List<GetAllTourContentResponse> getAll(Integer type, Integer language) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {
            String query = "select t.id,t.tour_type,t.car_info,t.local_name,t.price,t.start_city,t.description,t.start_date," +
                    " t.days_of_week,t.hour,t.start_address,t.lat,t.lng,t.start_time,t.language_skills,t.guided,oti.name,t.offered_tour_id,t.car_id, t.minute, t.free_seats, t.pick_me_up, t.transfer,c.photo_url as carImage "+
                    " from tour t inner join offered_tour_info as oti on t.offered_tour_id=oti.offered_tour_id inner join car as c on t.car_id=c.id" +
                    " where oti.language="+language+" and t.type="+type+" AND t.deleted=false ;";


            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<GetAllTourContentResponse> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GetAllTourContentResponse content = new GetAllTourContentResponse(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public List<GetAllTourContentResponse> getAll(Integer reference, Integer type, Integer language) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {
            String query = "select t.id,t.tour_type,t.car_info,t.local_name,t.price,t.start_city,t.description,t.start_date," +
                    " t.days_of_week,t.hour,t.start_address,t.lat,t.lng,t.start_time,t.language_skills,t.guided,oti.name,t.offered_tour_id,t.car_id, t.minute, t.free_seats, t.pick_me_up, t.transfer, c.photo_url as carImage "+
                    " from tour t inner join offered_tour_info as oti on t.offered_tour_id=oti.offered_tour_id inner join car as c on t.car_id=c.id " +
                    " where oti.language="+language+" and t.reference="+reference+" and t.type="+type+" AND t.deleted=false ;";


            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<GetAllTourContentResponse> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GetAllTourContentResponse content = new GetAllTourContentResponse(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public Tour getTour(Integer id) {
        return tourRepository.findOneById(id);
    }

    @Override
    public void delete(Integer id) {
        tourRepository.delete(id);
    }

    @Override
    public List<TourContent> searchTour(Integer offeredTourId, String offeredTourIds, Integer startCity, Integer language, Double budget, Integer quantity, String from, String to,
                                        String days, String thisDay, Integer guided,String tourLanguage, Integer tourType,Boolean transfer,Boolean pickMeUp) {

        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            if(quantity==null){
                quantity=1;
            }

            String query = "select t.id,t.free_seats, t.icon_url,t.price,t.hour,t.name as name,t.days_of_week,t.start_date,t.start_time,t.start_address,t.language_skills, t.rating, ot.photo_url as photoUrl,oti.name " +
                    "as tourName ,t.car_info,c.photo_url as carPhotoUrl,t.description as description,t.lat,t.lng,t.guided,oti.description as otDescription, t.minute, t.pick_me_up, t.tour_type , city.name as cityName, t.transfer, t.approved" +
                    " from tour t inner join city on t.start_city=city.parent_id inner join offered_tour as ot on t.offered_tour_id=ot.id inner join offered_tour_info as oti on oti.offered_tour_id=t.offered_tour_id inner join car as c on c.id=t.car_id" +
                    " left join tour_reservation as tr on tr.tour_id=t.id where t.blocked=false AND t.deleted=false and city.language="+language+" AND oti.language="+language+" and t.free_seats>="+quantity+" and (tr.reservation_date is null or tr.free_seats>="+quantity+")";

            if(from== null && to== null){
                query+= " and (t.start_date is null or t.start_date>='"+ Util.getCurrentDateAddDays(1, "Asia/Yerevan") +"' or (t.start_date>='"+ Util.getCurrentDateAddDays(0, "Asia/Yerevan") +"' and t.start_time>'2000-01-01 "+Util.getTime()+"') )";
            }
            if(transfer){
                query += " and ot.transfer=1 ";
            }else {
                query += " and ot.transfer=0 ";
            }


            if(tourType!=null && !tourType.equals(0)){
                query += " and t.tour_type="+tourType;
            }

            if(guided!=null && !guided.equals(0)){

                if(guided.equals(1)) {
                    query += " and t.guided=1";
                }else {
                    query += " and t.guided=0";
                }
            }

            if(tourLanguage!=null){
                query += " and t.language_skills LIKE '%"+tourLanguage+"%' ";
            }

            if(startCity!=null){
                query += " and t.start_city="+startCity;
            }

            if(offeredTourId!=null && !offeredTourId.equals(0)){
                if(offeredTourIds!=null) {
                    query += " and (t.offered_tour_id=" + offeredTourId + " or t.offered_tour_id in " + offeredTourIds + ") ";
                }else {
                    query += " and t.offered_tour_id=" + offeredTourId;

                }

            }

            if(budget!=null){
                query += " and t.price<="+budget;
            }

            if(thisDay!=null){
                query += " and ((t.days_of_week LIKE '%"+thisDay+"%' and t.start_time>'2000-01-01 "+Util.getTime()+"') ";
                query += " or (t.start_date='"+ Util.getCurrentDateAddDays(0, "Asia/Yerevan") +"' AND t.start_time>'2000-01-01 "+Util.getTime()+"')";
            }


            if(days!=null){

                if(thisDay!=null){
                    query += " OR ";
                }else {
                    query += " AND ";
                }

                query += " (t.days_of_week REGEXP '"+days+"' OR (t.start_date>='"+from+"' AND t.start_date<='"+to+"'))";
            }

            if(thisDay!=null) {
                query += ")";
            }

            if(pickMeUp != null && pickMeUp){
                query+=" AND t.pick_me_up="+pickMeUp;
            }

            query += " GROUP BY t.id ORDER BY lat ;";
            System.out.println(query);

            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<TourContent> tours = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    TourContent tour = new TourContent(result.get(i));
                    tours.add(tour);
                }
            }
            return tours;
        } finally {
            if (session.isOpen()) session.close();
        }
    }


    @Override
    public Tour getTour(Integer id, String date,String dayOfWeek) {
        return tourRepository.getTour(id, date, dayOfWeek);
    }

    @Override
    public void updateTourRating(Integer reference, Double rating, Integer referenceType) {
        tourRepository.updateTourRating(reference, rating, referenceType);
    }

    @Override
    public void updateTours(Integer type, Integer agencyId, String name) {
        tourRepository.updateTours(type, agencyId, name);
    }

    @Override
    public void updateToursLogoUrl(Integer type, Integer agencyId, String logoUrl) {
        tourRepository.updateToursLogoUrl(type, agencyId, logoUrl);
    }

    @Override
    public void updateApproved(Integer reference, Integer referenceType, Boolean approved) {
        tourRepository.updateApproved(reference, referenceType, approved);
    }

    @Override
    public void updateBlocked(Integer reference, Integer referenceType, Boolean blocked) {
        tourRepository.updateBlocked(reference, referenceType, blocked);
    }

    @Override
    public void deleteAllByOfferedTorId(Integer offeredTourId) {
       tourRepository.deleteAllOfferedTourId(offeredTourId);
    }


}
