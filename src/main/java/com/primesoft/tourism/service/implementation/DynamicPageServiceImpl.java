package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.response.getAllDynamicPagesResponse.Content;
import com.primesoft.tourism.beans.tableBeans.DynamicPage;
import com.primesoft.tourism.dao.DynamicPageRepository;
import com.primesoft.tourism.service.interfaces.DynamicPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

@Service
public class DynamicPageServiceImpl implements DynamicPageService {

    @Autowired
    private DynamicPageRepository dynamicPageRepository;

    @Autowired
    private EntityManagerFactory entityManagerFactory;


    @Override
    public void save(DynamicPage dynamicPage) {
        dynamicPageRepository.save(dynamicPage);
    }

    @Override
    public void delete(Integer id) {
        dynamicPageRepository.delete(id);
    }

    @Override
    public DynamicPage getDynamicPage(Integer id) {
        return dynamicPageRepository.findOneById(id);
    }

    @Override
    public List<Content> getAll(Integer type, Integer language) {

        EntityManager session = entityManagerFactory.createEntityManager();

        String query;

        if(type == 0){
            query = "select dp.id,dp.photo_url,dp.type,dpc.title,dpc.content,dpc.short_content from dynamic_page as dp " +
                    " INNER JOIN dynamic_page_content as dpc on dp.id=dpc.dynamic_page_id where dpc.language="+language+";";
        }else{
           query =  "select dp.id,dp.photo_url,dp.type,dpc.title,dpc.content,dpc.short_content from dynamic_page as dp " +
                    " INNER JOIN dynamic_page_content as dpc on dp.id=dpc.dynamic_page_id where dpc.language="+language+" AND dp.type="+type+";";
        }

        try {
            List<Object[]> result = session.createNativeQuery(query).getResultList();

            ArrayList<Content> contents = new ArrayList<>();
            for (int i=0; i<result.size();i++){
                Content content = new Content(result.get(i));
                contents.add(content);
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }
}