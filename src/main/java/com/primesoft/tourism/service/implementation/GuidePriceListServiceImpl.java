package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.GuidePriceList;
import com.primesoft.tourism.dao.GuidePriceListRepository;
import com.primesoft.tourism.service.interfaces.GuidePriceListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

@Service
public class GuidePriceListServiceImpl implements GuidePriceListService {

    @Autowired
    private GuidePriceListRepository guidePriceListRepository;

    @Autowired
    private EntityManagerFactory entityManagerFactory;



    @Override
    public void save(GuidePriceList guidePriceList) {
        guidePriceListRepository.save(guidePriceList);
    }


    @Override
    public GuidePriceList getGuidePriceList(Integer id) {
        return guidePriceListRepository.findFirstById(id);
    }

    @Override
    public List<GuidePriceList> getAll(Integer guideId) {


        return guidePriceListRepository.findAllByGuideIdOrderByGroupId(guideId);
    }

    @Override
    public void delete(List<Integer> ids) {
        guidePriceListRepository.deleteAllByIdIn(ids);
    }

    @Override
    public void deleteByGroupId(Long groupId) {
        guidePriceListRepository.deleteAllByGroupId(groupId);
    }

    @Override
    public GuidePriceList getPriceList(Integer guideId, String language, Integer type) {
        return guidePriceListRepository.findFirstByGuideIdAndLanguageAndType(guideId, language, type);
    }

    @Override
    public void updateGuideRating(Integer guideId, Double rating) {
        guidePriceListRepository.updateGuideRating(guideId, rating);
    }

    @Override
    public List<Long> getGroupIds(Integer guideId) {
                EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select gpl.group_id from  guide_price_list gpl where guide_id = "+guideId+" group by gpl.group_id;" ;


            List<Object[]> result = session.createNativeQuery(query+";")
                    .getResultList();

            ArrayList<Long> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    Long content = new Long(String.valueOf(result.get(i)));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }

    }

    @Override
    public void update(Integer id, String language, Integer price, Integer type) {
        guidePriceListRepository.updatePriceList(id, language, price, type);
    }
}
