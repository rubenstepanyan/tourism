package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.response.FinishOrderResponse;
import com.primesoft.tourism.beans.response.getCustomerHistory.GetCustomerOrderHistoryContent;
import com.primesoft.tourism.beans.response.getCustomerOrders.TourOrderContent;
import com.primesoft.tourism.beans.response.getHistoryResponse.GetTourOrderHistoryContentResponse;
import com.primesoft.tourism.beans.response.getOrdersResponse.GetTourOrderContentResponse;
import com.primesoft.tourism.beans.response.getTourPaymentsResponse.GetTourPaymentsContent;
import com.primesoft.tourism.beans.tableBeans.TourOrder;
import com.primesoft.tourism.dao.OrderRepository;
import com.primesoft.tourism.service.interfaces.OrderService;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private EntityManagerFactory entityManagerFactory;


    @Override
    public void save(TourOrder tourOrder) {
        orderRepository.save(tourOrder);
    }

    @Override
    public TourOrder getTourOrder(Integer id) {
        return orderRepository.findOneById(id);
    }

    @Override
    public List<TourOrderContent> getTourOrderByCustomerId(Integer customerId, Integer language) {

        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select t_o.id,t_o.quantity,t_o.price,t_o.status,t_o.tour_date,t.car_info,t.name as name,t.icon_url,t.start_address,oti.name as tourName,t.type,t.language_skills," +
                    "t.guided,c.name as city,t.hour,t.minute, t_o.qr, t_o.phone_number, t_o.payed, t_o.error_message, t_o.tourist_address, t_o.lat, t_o.lng, ot.photo_url as offeredTourPhoto, cr.photo_url as carPhoto, t_o.is_transfer, t.rating, t.tour_type" +
                    " from tour_order AS t_o inner join tour as t on t_o.tour_id=t.id inner JOIN offered_tour_info as oti on oti.offered_tour_id=t.offered_tour_id" +
                    " inner join city as c on c.parent_id=t.start_city inner join offered_tour as ot on ot.id=t.offered_tour_id inner join car as cr on cr.id=t.car_id " +
                    " WHERE t_o.status IN (1,3,4) AND oti.language="+language+" and t_o.customer_id="+customerId+" and c.language="+language+" AND t.deleted=false ORDER BY t_o.id DESC;";

            System.out.println(query);

            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<TourOrderContent> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    TourOrderContent content = new TourOrderContent(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public List<GetTourOrderContentResponse> getOrder(Integer agencyId, Integer driverId, Integer language) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select t_o.id,t_o.quantity,t_o.price,t.name as name,oti.name as tourName,concat(c.first_name, ' ',c.last_name) as fullName,t.tour_type,t_o.tour_date,t_o.status, t_o.tourist_address, t_o.lat, t_o.lng, t.start_address, t_o.is_transfer as transfer, t.start_city, c.photo_url, c.nationality, c.phone " +
                    " from tour_order AS t_o inner join tour as t on t_o.tour_id=t.id inner JOIN offered_tour_info as oti on oti.offered_tour_id=t.offered_tour_id inner join customer as c on t_o.customer_id=c.id" +
                    " where (t_o.status IN (1,3,4) AND oti.language="+language+") and ((t.reference="+agencyId+" and t.type="+ Constants.AGENCY_TYPE +") or (t.reference="+driverId+" and t.type="+ Constants.DRIVER_TYPE +")) AND t.deleted=false ORDER BY t_o.id DESC;";

            System.out.println("get order query: "+query);

            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<GetTourOrderContentResponse> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GetTourOrderContentResponse content = new GetTourOrderContentResponse(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public List<GetCustomerOrderHistoryContent> getCustomerOrderHistory(Integer customerId, Integer language) {

        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select t_o.id,t_o.quantity,t_o.price as totalPrice,t_o.status,t_o.tour_date,t.car_info,t.name as name,t.start_address,oti.name as tourName,t.tour_type,t.guided,t.language_skills," +
                    " cy.name as startCity,t.hour,t.local_name,t.start_time,t.type as isAgency,t.rating,t.price,cu.id as customerId,concat(cu.first_name, ' ',cu.last_name) as customerName,ot.photo_url, t.minute, t.transfer, t_o.rating as customerRating " +
                    " from tour_order t_o inner join tour as t on t_o.tour_id=t.id inner JOIN offered_tour_info as oti on oti.offered_tour_id=t.offered_tour_id inner join customer as cu on t_o.customer_id=cu.id " +
                    " inner join city as cy on cy.parent_id=t.start_city inner join offered_tour as ot on ot.id=t.offered_tour_id where t_o.status IN (2,5,6) and oti.language="+language+" and t_o.customer_id="+customerId+" and cy.language="+language +" ORDER BY id DESC ;";

            System.out.println(query);


            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<GetCustomerOrderHistoryContent> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GetCustomerOrderHistoryContent content = new GetCustomerOrderHistoryContent(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public List<GetTourOrderHistoryContentResponse> getOrderHistory(Integer agencyId, Integer driverId, Integer language,Integer tourId) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select t_o.id,t_o.quantity,ROUND(t_o.price*0.90, 0) as price,t_o.status,t_o.tour_date,t.car_info,t.name as name,t.start_address,oti.name as tourName,t.tour_type,t.guided,t.language_skills," +
                    " t.start_city as startCity,t.hour,t.local_name,t.start_time,t.type as isAgency,t.rating,cu.id as customerId,concat(cu.first_name, ' ',cu.last_name) as customerName, t.minute, t.transfer, cu.photo_url, cu.nationality, t_o.my_trips_paid, cu.phone " +
                    " from tour_order t_o inner join tour as t on t_o.tour_id=t.id inner JOIN offered_tour_info as oti on oti.offered_tour_id=t.offered_tour_id inner join customer as cu on t_o.customer_id=cu.id " +
                    " inner join city as cy on cy.parent_id=t.start_city where ";

            if(tourId != 0){
                query+= " t.id="+tourId+" and ";
            }

            query += " (cy.language="+language +" and t_o.status IN (2,5,6) and oti.language="+language+") and ((t.reference="+agencyId+" and t.type="+ Constants.AGENCY_TYPE +") or (t.reference="+driverId+" and t.type="+ Constants.DRIVER_TYPE +")) ORDER BY t_o.id DESC ;";

            System.out.println(query);

            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<GetTourOrderHistoryContentResponse> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GetTourOrderHistoryContentResponse content = new GetTourOrderHistoryContentResponse(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public TourOrder getTourOrder(Integer id, Integer customerId) {
        return orderRepository.findOneByIdAndCustomerId(id, customerId);
    }


    @Override
    public List<TourOrder> getAutoPartnerCancel() {
        String currentDate = Util.getCurrentDateAndTimeSeconds();
        return orderRepository.getAutoPartnerCancel(currentDate);
    }


    @Override
    public List<TourOrder> getAutoCustomerCancel() {
        String currentDate = Util.getCurrentDateAndTimeSeconds();
        return orderRepository.getAutoCustomerCancel(currentDate);
    }

    @Override
    public void save(List<TourOrder> tourOrders) {
        orderRepository.save(tourOrders);
    }


    @Override
    public List<GetTourPaymentsContent> getAllPayments(Integer referenceType, Integer reference, Integer language, String from, String to) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select t_o.id,t_o.quantity,t_o.price as totalPrice, ROUND(t_o.price*0.90, 0) as toPay, t_o.tour_date, t_o.status, oti.name as localName,t_o.is_transfer, ROUND(t_o.price/t_o.quantity) as price,oti.name as tourName,concat(c.first_name, ' ',c.last_name) as customerName,c.mail as customerMail,c.phone as customerPhone, t_o.reason_comment, t_o.my_trips_paid, a.name as partnerName, t_o.refund " +
                    " from tour_order AS t_o inner JOIN offered_tour_info as oti on oti.offered_tour_id=t_o.offered_tour_id inner join customer as c on t_o.customer_id=c.id inner join agency as a on a.id=t_o.reference" +
                    " where oti.language="+language+" AND STR_TO_DATE(t_o.tour_date, '%Y-%m-%d')>='"+from+"' AND STR_TO_DATE(t_o.tour_date, '%Y-%m-%d')<='"+to+"' AND t_o.reference_type=" + referenceType + " ";



            if(reference!=0) {
                query += "  AND t_o.reference=" + reference;
            }

            query+= ";";

            System.out.println("query: "+query);
            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<GetTourPaymentsContent> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GetTourPaymentsContent content = new GetTourPaymentsContent(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public List<GetTourPaymentsContent> getAllDriverPayments(Integer referenceType, Integer reference, Integer language, String from, String to) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select t_o.id,t_o.quantity,t_o.price as totalPrice, ROUND(t_o.price*0.90, 0) as toPay, t_o.tour_date, t_o.status, oti.name as localName,t_o.is_transfer, ROUND(t_o.price/t_o.quantity) as price,oti.name as tourName,concat(c.first_name, ' ',c.last_name) as customerName,c.mail as customerMail,c.phone as customerPhone, t_o.reason_comment, t_o.my_trips_paid, concat(d.first_name, ' ',d.last_name) as partnerName, t_o.refund " +
                    " from tour_order AS t_o inner JOIN offered_tour_info as oti on oti.offered_tour_id=t_o.offered_tour_id inner join customer as c on t_o.customer_id=c.id inner join driver as d on d.id=t_o.reference " +
                    " where oti.language="+language+" AND STR_TO_DATE(t_o.tour_date, '%Y-%m-%d')>='"+from+"' AND STR_TO_DATE(t_o.tour_date, '%Y-%m-%d')<='"+to+"' AND t_o.reference_type=" + referenceType + " ";



            if(reference!=0) {
                query += "  AND t_o.reference=" + reference;
            }

            query+= ";";

            System.out.println("query: "+query);
            List<Object[]> result = session.createNativeQuery(query)
                    .getResultList();

            ArrayList<GetTourPaymentsContent> contents = new ArrayList<>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    GetTourPaymentsContent content = new GetTourPaymentsContent(result.get(i));
                    contents.add(content);
                }
            }
            return contents;
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public void updateTourOrders(List<Integer> ids) {
        orderRepository.updateTourOrders(ids);
    }

    @Override
    public TourOrder getTourOrderByQr(String qr, Boolean payed, Integer status, Integer reference, Integer referenceType) {
        return orderRepository.findOneByQrAndPayedAndStatusAndReferenceAndReferenceType(qr, payed, status, reference, referenceType);
    }

    @Override
    public List<TourOrder> getAutoFinish() {
        String currentDate = Util.getCurrentDateAndTimeSeconds();
        return orderRepository.getAutoFinish(currentDate);
    }

    @Override
    public FinishOrderResponse getTourOrderInfoByQr(String qr, Integer languageId) {
        EntityManager session = entityManagerFactory.createEntityManager();

        try {

            String query = "select t_o.id,t_o.quantity,t_o.price as totalPrice, t_o.tour_date, oti.name as direction, c.photo_url, concat(c.first_name, ' ',c.last_name) as fullName , t.start_address, t_o.tourist_address" +
                    " from tour_order AS t_o inner JOIN offered_tour_info as oti on oti.offered_tour_id=t_o.offered_tour_id inner join customer as c on t_o.customer_id=c.id inner join tour as t on t.id=t_o.tour_id " +
                    " where oti.language= "+languageId+" and t_o.qr= '"+qr+"';";


            System.out.println(query);

            List<Object[]> result = session.createNativeQuery(query).getResultList();

            if(result!=null && result.size()>0) {
                return new FinishOrderResponse(result.get(0));
            }else {
                return null;
            }
        } finally {
            if (session.isOpen()) session.close();
        }
    }

    @Override
    public void updateTourOrders(Integer id, String phone, Integer referenceType) {
        orderRepository.updateTourOrders(phone, id, referenceType);
    }

    @Override
    public List<TourOrder> getTourOrderByDriverIdAndStatus(Integer driverId, Integer type, Integer status) {
        return orderRepository.findAllByReferenceAndReferenceTypeAndStatus(driverId, type, status);
    }

    @Override
    public List<TourOrder> getTourOrderByCustomerIdAndStatus(Integer customerId, Integer status) {
        return orderRepository.findAllByCustomerIdAndStatus(customerId, status);
    }
}
