package com.primesoft.tourism.service.implementation;

import com.primesoft.tourism.beans.tableBeans.Offers;
import com.primesoft.tourism.dao.OfferRepository;
import com.primesoft.tourism.service.interfaces.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfferServiceImpl implements OfferService {

    @Autowired
    private OfferRepository offerRepository;


    @Override
    public void save(Offers offers) {
        offerRepository.save(offers);
    }

    @Override
    public List<Offers> getAll() {
        return offerRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        offerRepository.delete(id);
    }
}
