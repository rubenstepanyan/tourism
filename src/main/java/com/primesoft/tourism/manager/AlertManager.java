package com.primesoft.tourism.manager;

import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.FileEditManager;
import com.primesoft.tourism.utils.Util;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ThreadLocalRandom;

public class AlertManager {

    private static final String CONFIRMATION_MAIL_FILE_PATH = "/var/www/files/confirmationMail.txt";
    private static final String CANCEL_MAIL_FILE_PATH = "/var/www/files/cancelMail.txt";

    private static final String CONFIRMATION_CODE = "confirmationCode";
    private static final String USER_EMAIL_ADDRESS = "userEmailAddress";

    private static final String SENDER_MAIL = "no-replay@visitmyarmenia.com";
    private static final String SENDER_PASSWORD = "Qw1234567897";
    private static final String FROM = "My Tours";
    private static final String EMAIL_CONFIRMATION = "E-mail Confirmation";

    public boolean sendSms(String phoneNumber,String massage){

        System.out.println("phoneNumber: "+phoneNumber);

        DateFormat dateFormat = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
        Date date = new Date();
        String currentTime = dateFormat.format(date).toString();
        int messageID = ThreadLocalRandom.current().nextInt(1,2000000000+1);

        try {
            String url="http://31.47.195.66:80/broker/";
            URL object;
            object = new URL(url);
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/xml");
            con.setRequestProperty("charset", "UTF-8");
            con.setRequestMethod("POST");
            OutputStreamWriter wr;

            wr = new OutputStreamWriter(con.getOutputStream());

            String requestBody ="<?xml version=\"1.0\" encoding=\"UTF-8\"?> <bulk-request login=\"mytrips\" "+
                    " password=\"mytrips\" ref-id=\""+currentTime+"\" delivery-notification-requested=\"true\" "+
                    " version=\"1.0\"> <message id=\""+messageID+"\" msisdn=\""+phoneNumber+"\" service-number=\"MY TRIPS\" "+
                    " defer-date=\""+currentTime+"\" "+
                    " validity-period=\"3\" priority=\"1\"> <content type=\"text/plain\">Your confirmation code is: "+massage+"</content>"+
                    " </message></bulk-request>";
            System.out.println(requestBody);

            wr.write(requestBody);
            wr.flush();
            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();

            System.out.println("responseCode: "+HttpResult+" message: "+con.getResponseMessage());

            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();


                return true;
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Problem sending sms: "+e);
        }
        return false;
    }


    public void sendConfirmationMail(String mail, String text){

        String mailText = generateConfirmationMail(CONFIRMATION_MAIL_FILE_PATH, text, mail).toString();
        send(mail, EMAIL_CONFIRMATION, mailText);

    }


    private StringBuilder generateConfirmationMail(final String filePath, final String confirmationCode,
                                                            final String userEmailAddress) {

        StringBuilder mail = new StringBuilder(FileEditManager.readFile(filePath));
        Util.replaceAll(mail, CONFIRMATION_CODE, confirmationCode);
        Util.replaceAll(mail, USER_EMAIL_ADDRESS, userEmailAddress);
        return mail;
    }


    public void sendCancelMail(String recipientEmail, String title){
        StringBuilder mail = new StringBuilder(FileEditManager.readFile(CANCEL_MAIL_FILE_PATH));

        System.out.println(mail.toString());
        System.out.println("recipientEmail: "+recipientEmail);
        System.out.println("title: "+title);
        send(recipientEmail, title, mail.toString());

    }

    public void send(String recipientEmail, String title, String content){

        System.out.println("SEND MAIL Title: "+title);

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.yandex.ru");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(SENDER_MAIL,SENDER_PASSWORD);
                    }
                });

        try {

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(SENDER_MAIL, FROM));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(recipientEmail));
            message.setSubject(title);
            message.setText(content);
            message.setContent(content, "text/html; charset=utf-8");
            message.setSentDate(new Date());

            Transport.send(message);

        } catch (Exception e) {
            System.out.println(e);
        }

    }

}
