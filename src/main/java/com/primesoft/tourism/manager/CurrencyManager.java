package com.primesoft.tourism.manager;

import com.primesoft.tourism.beans.response.Currency;
import com.primesoft.tourism.beans.response.api.CurrencyResponse;
import com.primesoft.tourism.utils.Util;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

public class CurrencyManager {

    private static Long timeStamp = System.currentTimeMillis();
    private static Currency currency = new Currency();


    public static Currency getCurrency(){
        Long currentTimeStamp = System.currentTimeMillis();
        if(currency.getUsd()<5 || currentTimeStamp-timeStamp>6000000){
            timeStamp =currentTimeStamp;
            updateCurrency();
        }
        return currency;
    }


    public static void updateCurrency(){

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<CurrencyResponse> response = restTemplate.exchange(("http://apilayer.net/api/live?access_key=b03c93f0a82ebd63e58f958c30d06033"), HttpMethod.GET, entity , CurrencyResponse.class);

        if(response.getBody().getSuccess()){
            currency.setUsd(Util.getTrueDouble(response.getBody().getQuotes().getUSDAMD()));
            currency.setEur(Util.getTrueDouble(currency.getUsd()/response.getBody().getQuotes().getUSDEUR()));
            currency.setRub(Util.getTrueDouble(currency.getUsd()/response.getBody().getQuotes().getUSDRUB()));
        }
    }
}