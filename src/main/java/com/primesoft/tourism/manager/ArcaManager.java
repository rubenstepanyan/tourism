package com.primesoft.tourism.manager;


import com.google.gson.Gson;
import com.primesoft.tourism.beans.response.ArcaRefundResponse;
import com.primesoft.tourism.beans.response.ArcaResponse;
import com.primesoft.tourism.beans.response.ArcaStatusResponse;
import com.primesoft.tourism.beans.tableBeans.*;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.*;
import com.primesoft.tourism.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class ArcaManager {


    @Autowired
    private ArcaService arcaService;

    @Autowired
    private CouponService couponService;

    @Autowired
    private EntertainmentPlaceService entertainmentPlaceService;

    @Autowired
    private GuideOrderService guideOrderService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private TourReservationService tourReservationService;

    @Autowired
    private TourService  tourService;

    @Autowired
    private OfferedTourService offeredTourService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private GuideService guideService;

    public ArcaResponse charge(String language, String pageView, String description, Integer amount,  Long orderNumber, String returnUrl){

        amount = amount*100;
        String request = "https://ipay.arca.am/payment/rest/register.do?userName=34541064_api&password=Qw1234567897&currency=051&pageView="+pageView+"&description="+description+"&amount="+amount+"&language="+language+"&orderNumber="+orderNumber+"&returnUrl="+returnUrl;
        System.out.println(request);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type","text/plain;charset=UTF-8");

        HttpEntity<String> entity = new HttpEntity<>(headers);

        String response = restTemplate.exchange(request, HttpMethod.GET, entity , String.class).getBody();

        System.out.println(response);

        return new Gson().fromJson(response, ArcaResponse.class);

    }



    public ArcaStatusResponse getOrderStatus(String orderId){

        String request = "https://ipay.arca.am/payment/rest/getOrderStatus.do?userName=34541064_api&password=Qw1234567897&orderId="+orderId;
        System.out.println(request);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type","text/plain;charset=UTF-8");

        HttpEntity<String> entity = new HttpEntity<>(headers);

        String response = restTemplate.exchange(request, HttpMethod.GET, entity , String.class).getBody();

        return new Gson().fromJson(response, ArcaStatusResponse.class);
    }


    @Scheduled(fixedRate = 60000)
    public void checkOrders(){
        checkOrder(null);
        arcaService.deleteArca();
    }

    public void checkOrder(Customer customer){

        List<Arca> arcaList;

        NotificationManager notificationManager = new NotificationManager();

        if(customer!=null) {
            arcaList = arcaService.getArcaList(customer.getId(), false);

        }else {
            arcaList = arcaService.getArcaList(false);

        }

        if(arcaList!=null && arcaList.size()>0) {

            for (Arca arca : arcaList) {

                ArcaStatusResponse arcaStatusResponse = new ArcaManager().getOrderStatus(arca.getOrderId());
                System.out.println("Status: " + arcaStatusResponse.getOrderStatus());

                if (arcaStatusResponse.getOrderStatus() != null) {

                    if(arcaStatusResponse.getOrderStatus().equals(2)) {
                        if (arca.getType().equals(Constants.BUY_COUPON)) {
                            Coupon coupon = couponService.getCoupon(arca.getReference());
                            coupon.setPayed(true);
                            coupon.setErrorMessage(null);
                            couponService.save(coupon);
                        } else if (arca.getType().equals(Constants.BUY_GUIDE)) {
                            GuideOrder guideOrder = guideOrderService.getGuideOrder(arca.getReference());

                            Guide guide = guideService.getGuidById(guideOrder.getGuideId());

                            guideOrder.setStatus(Constants.CUSTOMER_PAYED);
                            guideOrder.setPayed(true);
                            guideOrder.setErrorMessage(null);
                            guideOrder.setArcaOrderId(arca.getOrderId());
                            guideOrderService.save(guideOrder);

                            if(guide.getTourismAgencyId() == null){
                                notificationManager.sendNotif(Constants.GUIDE_ORDER_STATUS_CHANGED, LanguageManager.getLanguageInstance(guide.getDashboardLanguage()).getOrderHasBeenPaid()+" "+guideOrder.getId(),guide.getTopicAndroidAndWeb(),guide.getTopicIos(),guideOrder.getStatus(),null,guideOrder.getId());
                            }else {
                                Agency agency = agencyService.getAgency(guide.getTourismAgencyId());
                                notificationManager.sendNotif(Constants.GUIDE_ORDER_STATUS_CHANGED, LanguageManager.getLanguageInstance(agency.getDashboardLanguage()).getOrderHasBeenPaid()+" "+guideOrder.getId(),agency.getTopicAndroidAndWeb(),agency.getTopicIos(), guideOrder.getStatus(),null,guideOrder.getId());
                            }

                        } else if (arca.getType().equals(Constants.BUY_TOUR) || arca.getType().equals(Constants.BUY_TRANSFER)) {
                            TourOrder tourOrder = orderService.getTourOrder(arca.getReference());


                            tourOrder.setStatus(Constants.CUSTOMER_PAYED);
                            tourOrder.setPayed(true);
                            tourOrder.setErrorMessage(null);
                            tourOrder.setArcaOrderId(arca.getOrderId());
                            orderService.save(tourOrder);

                            if(tourOrder.getReferenceType().equals(Constants.AGENCY_TYPE)){
                                Agency agency = agencyService.getAgency(tourOrder.getReference());
                                if(tourOrder.getTransfer()){
                                    notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(agency.getDashboardLanguage()).getOrderHasBeenPaid()+" "+tourOrder.getId(),agency.getTopicAndroidAndWeb(),agency.getTopicIos(),tourOrder.getStatus(),true,tourOrder.getId());
                                }else{
                                    notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(agency.getDashboardLanguage()).getOrderHasBeenPaid()+" "+tourOrder.getId(),agency.getTopicAndroidAndWeb(),agency.getTopicIos(),tourOrder.getStatus(),false,tourOrder.getId());
                                }
                            }else {
                                Driver driver  = driverService.getDriver(tourOrder.getReference());
                                if(tourOrder.getTransfer()){
                                    notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(driver.getDashboardLanguage()).getOrderHasBeenPaid()+" "+tourOrder.getId(),driver.getTopicAndroidAndWeb(),driver.getTopicIos(),tourOrder.getStatus(),true,tourOrder.getId());
                                }else{
                                    notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(driver.getDashboardLanguage()).getOrderHasBeenPaid()+" "+tourOrder.getId(),driver.getTopicAndroidAndWeb(),driver.getTopicIos(),tourOrder.getStatus(),true,tourOrder.getId());
                                }
                            }

                            TourReservation tourReservation = tourReservationService.getTourReservation(tourOrder.getTourId(),tourOrder.getTourDate().split(" ")[0]);

                            System.out.println("tourId: "+tourOrder.getTourId());
                            System.out.println("TourDate: "+tourOrder.getTourDate());

                            Tour tour = tourService.getTour(tourOrder.getTourId());

                            if (tourReservation == null) {

                                TourReservation newTourReservation = new TourReservation();

                                newTourReservation.setReferenceId(tour.getReference());
                                newTourReservation.setReferenceType(tour.getType());

                                newTourReservation.setReservationDate(tourOrder.getTourDate().split(" ")[0]);
                                newTourReservation.setTourId(tourOrder.getTourId());
                                newTourReservation.setFreeSeats(tour.getFreeSeats()-tourOrder.getQuantity());
                                tourReservationService.save(newTourReservation);


                            } else {
                                tourReservation.setFreeSeats(tourReservation.getFreeSeats() - tourOrder.getQuantity());
                                tourReservationService.save(tourReservation);
                            }
                        }

                        arca.setCharged(true);
                        arcaService.save(arca);


                    }else if(arcaStatusResponse.getErrorMessage()!=null && !arcaStatusResponse.getErrorMessage().equalsIgnoreCase("success")) {
                        System.out.println("Message: "+arcaStatusResponse.getErrorMessage());

                        if (arca.getType().equals(Constants.BUY_COUPON)) {
                            Coupon coupon = couponService.getCoupon(arca.getReference());
                            coupon.setPayed(false);
                            coupon.setErrorMessage(arcaStatusResponse.getErrorMessage());
                            couponService.save(coupon);
                        } else if (arca.getType().equals(Constants.BUY_GUIDE)) {
                            System.out.println("arca.getReference(): "+arca.getReference());
                            GuideOrder guideOrder = guideOrderService.getGuideOrder(arca.getReference());
                            guideOrder.setPayed(false);
                            guideOrder.setErrorMessage(arcaStatusResponse.getErrorMessage());
                            guideOrderService.save(guideOrder);
                        } else if (arca.getType().equals(Constants.BUY_TOUR) || arca.getType().equals(Constants.BUY_TRANSFER)) {
                            System.out.println("arca.getReference(): "+arca.getReference());
                            TourOrder tourOrder = orderService.getTourOrder(arca.getReference());
                            tourOrder.setPayed(false);
                            tourOrder.setErrorMessage(arcaStatusResponse.getErrorMessage());
                            orderService.save(tourOrder);
                        }
                    }
                    arca.setErrorCode(arcaStatusResponse.getErrorCode());
                    arca.setErrorMessage(arcaStatusResponse.getErrorMessage());
                    arcaService.save(arca);

                }


            }
        }
    }

    public ArcaRefundResponse refund (String orderId, Integer amount){

        amount = amount*100;
        String request = "https://ipay.arca.am/payment/rest/refund.do?userName=34541064_api&password=Qw1234567897&&orderId="+orderId+"&amount="+amount;
        System.out.println(request);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type","text/plain;charset=UTF-8");

        HttpEntity<String> entity = new HttpEntity<>(headers);

        String response = restTemplate.exchange(request, HttpMethod.GET, entity , String.class).getBody();

        System.out.println(response);

        return new Gson().fromJson(response, ArcaRefundResponse.class);

    }



}
