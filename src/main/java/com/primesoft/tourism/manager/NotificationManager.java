package com.primesoft.tourism.manager;

import com.google.gson.Gson;
import com.primesoft.tourism.beans.notification.base.MetaData;
import com.primesoft.tourism.beans.notification.base.Notification;
import com.primesoft.tourism.beans.notification.base.NotificationBean;
import com.primesoft.tourism.beans.notification.base.NotificationDataBean;
import com.primesoft.tourism.beans.responseBase.Response;
import com.primesoft.tourism.utils.Constants;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;

public class NotificationManager {

    private Gson gson = new Gson();

    public void sendNotif(String action, String body, String topicAndroid, String topicIos, Integer status, Boolean isTransfer, Integer orderId) {

        sendNotification(action, body, null, topicAndroid, topicIos, status, isTransfer, orderId);

    }

    public void sendNotification(String action, String body, Object data, String topicAndroid, String topicIos, Integer status, Boolean isTransfer, Integer orderId){



        sendNotification(gson.toJson(createNotifIos(action, body, data, topicIos,status, isTransfer,orderId)));
        sendNotification(gson.toJson(createNotifAndroid(action, body, data, topicAndroid, status, isTransfer, orderId)));
    }


    private NotificationBean<NotificationDataBean> createNotifIos(String action, String body, Object data,  String topicIos, Integer status, Boolean isTransfer, Integer orderId){


        NotificationBean<NotificationDataBean> notificationBean = new NotificationBean<>();
        NotificationDataBean dataBean = new NotificationDataBean();
        notificationBean.setTo("/topics/"+topicIos);
        notificationBean.setPriority("high");

        dataBean.setMetaData(new MetaData(action, data, status, isTransfer, orderId));
        notificationBean.setNotification(new Notification(Constants.NOTIFICATION_TITLE, body, "default", "1"));

        notificationBean.setData(dataBean);

        return notificationBean;
    }


    private NotificationBean<NotificationDataBean> createNotifAndroid(String action, String body, Object data, String topicAndroid, Integer status, Boolean isTransfer, Integer orderId){


        NotificationBean<NotificationDataBean> notificationBean = new NotificationBean<>();
        NotificationDataBean dataBean = new NotificationDataBean();
        notificationBean.setTo("/topics/"+topicAndroid);
        notificationBean.setPriority("high");

        dataBean.setAction(action);
        dataBean.setData(data);
        dataBean.setStatus(status);
        dataBean.setTransfer(isTransfer);
        dataBean.setOrderId(orderId);
        dataBean.setNotification(new Notification(Constants.NOTIFICATION_TITLE, body));


        notificationBean.setData(dataBean);

        return notificationBean;
    }


    private void sendNotification(String body) {

        System.out.println("body: "+body);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", Constants.FIREBASE_AUTHORIZATION);
        MediaType mediaType = new MediaType("application", "json", StandardCharsets.UTF_8);
        headers.setContentType(mediaType);


        HttpEntity<String> entity = new HttpEntity<>(body, headers);
        Response<Object> result = new RestTemplate().postForObject(Constants.FIREBASE_NOTIFICATION_API_URL, entity, Response.class);

        System.out.println("notif: "+new Gson().toJson(result));
    }


}
