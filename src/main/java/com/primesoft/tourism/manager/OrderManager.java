package com.primesoft.tourism.manager;


import com.primesoft.tourism.beans.tableBeans.*;
import com.primesoft.tourism.language.LanguageManager;
import com.primesoft.tourism.service.interfaces.*;
import com.primesoft.tourism.utils.Constants;
import com.primesoft.tourism.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderManager {

    @Autowired
    private TourReservationService tourReservationService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private GuideOrderService guideOrderService;

    @Autowired
    private CustomNotificationService customNotificationService;

    @Autowired
    private AgencyService agencyService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private GuideService guideService;

    @Autowired
    private CustomerService customerService;


    @Scheduled(fixedRate = 86400000)
    public void deleteReservation(){
        tourReservationService.deleteReservation();
    }


    @Scheduled(fixedRate = 60000)
    public void autoCancel(){
        List<TourOrder> tourOrders =  orderService.getAutoPartnerCancel();

        System.out.println("tourOrders.size(): "+tourOrders.size());

        NotificationManager notificationManager = new NotificationManager();

        CustomNotification notification = new CustomNotification();

        for (TourOrder tourOrder : tourOrders) {
            tourOrder.setStatus(Constants.CANCELED);
            tourOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());


            notification.setCustomerId(tourOrder.getCustomerId());
            notification.setAction(Constants.CANCELED);
            notification.setMessageAM(LanguageManager.getLanguageInstance(Constants.AM_LANGUAGE).getYourOrderRejected());
            notification.setMessageEN(LanguageManager.getLanguageInstance(Constants.EN_LANGUAGE).getYourOrderRejected());
            notification.setMessageRU(LanguageManager.getLanguageInstance(Constants.RU_LANGUAGE).getYourOrderRejected());

            customNotificationService.save(notification);

            Customer customer = customerService.getCustomer(tourOrder.getCustomerId());

            if(tourOrder.getTransfer()){
                notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(customer.getDashboardLanguage()).getYourOrderRejected(),customer.getTopicAndroidAndWeb(),customer.getTopicIos(),tourOrder.getStatus(),true,tourOrder.getId());
            }else{
                notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(customer.getDashboardLanguage()).getYourOrderRejected(),customer.getTopicAndroidAndWeb(),customer.getTopicIos(),tourOrder.getStatus(),false,tourOrder.getId());
            }
        }

        orderService.save(tourOrders);


        List<TourOrder> orderList = orderService.getAutoCustomerCancel();

        System.out.println("orderList.size(): "+orderList.size());

        for (TourOrder tourOrder : orderList) {
            tourOrder.setStatus(Constants.CUSTOMER_CANCELED);
            tourOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());

            if(tourOrder.getReferenceType() == Constants.AGENCY_TYPE){
                Agency agency = agencyService.getAgency(tourOrder.getReference());
                if(tourOrder.getTransfer()){
                    notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(agency.getDashboardLanguage()).getYourOrderRejected(),agency.getTopicAndroidAndWeb(),agency.getTopicIos(),tourOrder.getStatus(),true,tourOrder.getId());
                }else{
                    notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(agency.getDashboardLanguage()).getYourOrderRejected(),agency.getTopicAndroidAndWeb(),agency.getTopicIos(),tourOrder.getStatus(),false,tourOrder.getId());
                }
            }else if(tourOrder.getReferenceType() == Constants.DRIVER_TYPE){
                Driver driver = driverService.getDriver(tourOrder.getReference());
                if(tourOrder.getTransfer()){
                    notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(driver.getDashboardLanguage()).getYourOrderRejected(),driver.getTopicAndroidAndWeb(),driver.getTopicIos(),tourOrder.getStatus(),true,tourOrder.getId());
                }else{
                    notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(driver.getDashboardLanguage()).getYourOrderRejected(),driver.getTopicAndroidAndWeb(),driver.getTopicIos(),tourOrder.getStatus(),false,tourOrder.getId());
                }
            }

        }
        orderService.save(orderList);


        List<GuideOrder> guideOrders = guideOrderService.getAutoPartnerCancel();

        for(GuideOrder guideOrder : guideOrders){
            guideOrder.setStatus(Constants.CANCELED);
            guideOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());

            notification.setCustomerId(guideOrder.getCustomerId());
            notification.setAction(Constants.CANCELED);
            notification.setMessageAM(LanguageManager.getLanguageInstance(Constants.AM_LANGUAGE).getYourOrderRejected());
            notification.setMessageRU(LanguageManager.getLanguageInstance(Constants.RU_LANGUAGE).getYourOrderRejected());
            notification.setMessageEN(LanguageManager.getLanguageInstance(Constants.EN_LANGUAGE).getYourOrderRejected());

            customNotificationService.save(notification);

            Customer customer = customerService.getCustomer(guideOrder.getCustomerId());
            notificationManager.sendNotif(Constants.GUIDE_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(customer.getDashboardLanguage()).getYourOrderRejected(),customer.getTopicAndroidAndWeb(),customer.getTopicIos(),guideOrder.getStatus(),null,guideOrder.getId());

        }
        guideOrderService.save(guideOrders);


        List<GuideOrder> guideOrderList = guideOrderService.getAutoCustomerCancel();

        for(GuideOrder guideOrder : guideOrderList){

                guideOrder.setStatus(Constants.CUSTOMER_CANCELED);
                guideOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());

                if(guideOrder.getReference() != null){
                    Agency agency = agencyService.getAgency(guideOrder.getReference());
                    notificationManager.sendNotif(Constants.GUIDE_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(agency.getDashboardLanguage()).getYourOrderRejected(),agency.getTopicAndroidAndWeb(),agency.getTopicIos(),guideOrder.getStatus(),null,guideOrder.getId());
                }else{
                    Guide guide = guideService.getGuidById(guideOrder.getGuideId());
                    notificationManager.sendNotif(Constants.GUIDE_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(guide.getDashboardLanguage()).getYourOrderRejected(),guide.getTopicAndroidAndWeb(),guide.getTopicIos(),guideOrder.getStatus(),null,guideOrder.getId());
                }
            }
            guideOrderService.save(guideOrderList);
    }


    @Scheduled(fixedRate = 3600000)
    public void autoFinish(){

        List<TourOrder> tourOrders = orderService.getAutoFinish();
        CustomNotification notification = new CustomNotification();
        NotificationManager notificationManager = new NotificationManager();

        if(tourOrders != null){

            for (TourOrder tourOrder : tourOrders) {
                tourOrder.setStatus(Constants.FINISHED);
                tourOrder.setReasonComment("Պատվերը կատարվել է առանց օգտատիրոջ ներկայության");
                tourOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());


                notification.setCustomerId(tourOrder.getCustomerId());
                notification.setAction(Constants.FINISHED);
                notification.setMessageAM(LanguageManager.getLanguageInstance(Constants.AM_LANGUAGE).getTheOrderWasMadeWithoutYourPresence());
                notification.setMessageRU(LanguageManager.getLanguageInstance(Constants.RU_LANGUAGE).getTheOrderWasMadeWithoutYourPresence());
                notification.setMessageEN(LanguageManager.getLanguageInstance(Constants.EN_LANGUAGE).getTheOrderWasMadeWithoutYourPresence());

                customNotificationService.save(notification);

                Customer customer = customerService.getCustomer(tourOrder.getCustomerId());
                if(tourOrder.getTransfer()){
                    notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(customer.getDashboardLanguage()).getTheOrderWasMadeWithoutYourPresence(),customer.getTopicAndroidAndWeb(),customer.getTopicIos(),tourOrder.getStatus(),true,tourOrder.getId());
                }else{
                    notificationManager.sendNotif(Constants.TOUR_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(customer.getDashboardLanguage()).getTheOrderWasMadeWithoutYourPresence(),customer.getTopicAndroidAndWeb(),customer.getTopicIos(),tourOrder.getStatus(),false,tourOrder.getId());
                }
            }

            orderService.save(tourOrders);
        }

        List<GuideOrder> guideOrders = guideOrderService.getAutoFinish();

        if(guideOrders != null){

            for(GuideOrder guideOrder : guideOrders){
                guideOrder.setStatus(Constants.FINISHED);
                guideOrder.setReasonComment("Պատվերը կատարվել է առանց օգտատիրոջ ներկայության");
                guideOrder.setLastUpdateDate(Util.getCurrentDateAndTimeSeconds());

                notification.setCustomerId(guideOrder.getCustomerId());
                notification.setAction(Constants.FINISHED);
                notification.setMessageAM(LanguageManager.getLanguageInstance(Constants.AM_LANGUAGE).getTheOrderWasMadeWithoutYourPresence());
                notification.setMessageEN(LanguageManager.getLanguageInstance(Constants.EN_LANGUAGE).getTheOrderWasMadeWithoutYourPresence());
                notification.setMessageRU(LanguageManager.getLanguageInstance(Constants.RU_LANGUAGE).getTheOrderWasMadeWithoutYourPresence());

                customNotificationService.save(notification);

                Customer customer = customerService.getCustomer(guideOrder.getCustomerId());
                notificationManager.sendNotif(Constants.GUIDE_ORDER_STATUS_CHANGED,LanguageManager.getLanguageInstance(customer.getDashboardLanguage()).getTheOrderWasMadeWithoutYourPresence(),customer.getTopicAndroidAndWeb(),customer.getTopicIos(),guideOrder.getStatus(),null,guideOrder.getId());


            }
            guideOrderService.save(guideOrders);
        }
    }



}
