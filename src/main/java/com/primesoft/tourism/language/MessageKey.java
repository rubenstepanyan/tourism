package com.primesoft.tourism.language;

public class MessageKey {

    private String success = null;
    private String reject = null;
    private String sessionExpired = null;
    private String videoNotFound = null;
    private String photoNotFound = null;
    private String footballClubNotFound = null;
    private String playerNotFound = null;
    private String newsNotFound = null;
    private String thisItemIsMain = null;
    private String voteNotFound = null;
    private String customerNotFound = null;
    private String entertainmentPlaceNotFound = null;
    private String notConfirmed= null;
    private String tourNotFound= null;
    private String guideNotFound= null;
    private String tourIsNotPerformed= null;
    private String orderSuccessfullyAdded= null;


    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getReject() {
        return reject;
    }

    public void setReject(String reject) {
        this.reject = reject;
    }

    public String getSessionExpired() {
        return sessionExpired;
    }

    public void setSessionExpired(String sessionExpired) {
        this.sessionExpired = sessionExpired;
    }

    public String getVideoNotFound() {
        return videoNotFound;
    }

    public void setVideoNotFound(String videoNotFound) {
        this.videoNotFound = videoNotFound;
    }

    public String getPhotoNotFound() {
        return photoNotFound;
    }

    public void setPhotoNotFound(String photoNotFound) {
        this.photoNotFound = photoNotFound;
    }

    public String getFootballClubNotFound() {
        return footballClubNotFound;
    }

    public void setFootballClubNotFound(String footballClubNotFound) {
        this.footballClubNotFound = footballClubNotFound;
    }

    public String getPlayerNotFound() {
        return playerNotFound;
    }

    public void setPlayerNotFound(String playerNotFound) {
        this.playerNotFound = playerNotFound;
    }

    public String getNewsNotFound() {
        return newsNotFound;
    }

    public void setNewsNotFound(String newsNotFound) {
        this.newsNotFound = newsNotFound;
    }

    public String getThisItemIsMain() {
        return thisItemIsMain;
    }

    public void setThisItemIsMain(String thisItemIsMain) {
        this.thisItemIsMain = thisItemIsMain;
    }

    public String getVoteNotFound() {
        return voteNotFound;
    }

    public void setVoteNotFound(String voteNotFound) {
        this.voteNotFound = voteNotFound;
    }

    public String getEntertainmentPlaceNotFound() {
        return entertainmentPlaceNotFound;
    }

    public void setEntertainmentPlaceNotFound(String entertainmentPlaceNotFound) {
        this.entertainmentPlaceNotFound = entertainmentPlaceNotFound;
    }

    public String getCustomerNotFound() {
        return customerNotFound;
    }

    public void setCustomerNotFound(String customerNotFound) {
        this.customerNotFound = customerNotFound;
    }

    public String getNotConfirmed() {
        return notConfirmed;
    }

    public void setNotConfirmed(String notConfirmed) {
        this.notConfirmed = notConfirmed;
    }

    public String getTourNotFound() {
        return tourNotFound;
    }

    public void setTourNotFound(String tourNotFound) {
        this.tourNotFound = tourNotFound;
    }

    public String getGuideNotFound() {
        return guideNotFound;
    }

    public void setGuideNotFound(String guideNotFound) {
        this.guideNotFound = guideNotFound;
    }

    public String getTourIsNotPerformed() {
        return tourIsNotPerformed;
    }

    public void setTourIsNotPerformed(String tourIsNotPerformed) {
        this.tourIsNotPerformed = tourIsNotPerformed;
    }

    public String getOrderSuccessfullyAdded() {
        return orderSuccessfullyAdded;
    }

    public void setOrderSuccessfullyAdded(String orderSuccessfullyAdded) {
        this.orderSuccessfullyAdded = orderSuccessfullyAdded;
    }
}