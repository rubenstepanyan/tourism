package com.primesoft.tourism.language;

public class Language {

    private String wrongLoginOrPassword = null;
    private String sessionExpired = null;
    private String successfullyAdded = null;
    private String successfullyEdited = null;
    private String successfullyDeleted = null;
    private String alreadyRegistered = null;
    private String wrongConfirmationCode = null;
    private String existingAdministrator = null;
    private String successfullyChanged = null;
    private String customerNotFound = null;
    private String verificationCodeSentToPhone = null;
    private String yourAccountIsNotVerifiedPleaseConfirmIt = null;
    private String successfullyConfirmed = null;
    private String successfullySent= null;
    private String yourPasswordSuccessfullyChanged = null;
    private String carSuccessfullyAdded = null;
    private String photoSuccessfullyChanged = null;
    private String entertainmentPlaceNotFound = null;
    private String verificationCodeSentToMail = null;
    private String thankYouForYourPurchase = null;
    private String insufficientFunds = null;
    private String couponActivated = null;
    private String wrongCouponNumber = null;
    private String incorrectPassword = null;
    private String yourCompanyChanged = null;
    private String wrongAgencyCode = null;
    private String successfullyFollowed = null;
    private String yourOrderAccepted = null;
    private String thisTourDoesNotHaveSoManyFreeSeats = null;
    private String yourOrdersUpdated = null;
    private String couponNotFound = null;
    private String yourOrderRejected = null;
    private String agencyNotFound = null;
    private String driverNotFound = null;
    private String guideNotFound = null;
    private String notFound = null;
    private String youHaveNewOrder = null;
    private String youHaveCanceledOrder = null;
    private String inThisDayTourIsNotPerformed = null;
    private String tourNotFound = null;
    private String youDontCanceledThisOrder = null;
    private String thisTourNotAvailable = null;
    private String thankYouForYourRating = null;
    private String yourAccountIsSuspended = null;
    private String successfullyFinished = null;
    private String manyChangesCustomNotification = null;
    private String incorrectDate = null;
    private String pleaseAddAddress = null;
    private String orderOutOffTime = null;
    private String wrongCode = null;
    private String theOrderWasMadeWithoutYourPresence = null;
    private String orderHasBeenPaid = null;
    private String pleaseWaitForTheOrganizerToAcceptYourOrder = null;
    private String yourOrderHasBeenCanceled = null;
    private String pleaseWaitForTheGuideToAcceptYourOrder = null;
    private String youWillReceiveAnAdditionalNoticeAboutTheEndOfTheClientsPaymentIfClientDoesntPayWithin15MinutesThOrderWillBeCancelAutomaticallyThankYou = null;
    private String pleaseAddStartDate = null;
    private String successfullyRefunded = null;




    public String getWrongLoginOrPassword() {
        return wrongLoginOrPassword;
    }

    public void setWrongLoginOrPassword(String wrongLoginOrPassword) {
        this.wrongLoginOrPassword = wrongLoginOrPassword;
    }

    public String getSessionExpired() {
        return sessionExpired;
    }

    public void setSessionExpired(String sessionExpired) {
        this.sessionExpired = sessionExpired;
    }

    public String getSuccessfullyAdded() {
        return successfullyAdded;
    }

    public void setSuccessfullyAdded(String successfullyAdded) {
        this.successfullyAdded = successfullyAdded;
    }

    public String getSuccessfullyEdited() {
        return successfullyEdited;
    }

    public void setSuccessfullyEdited(String successfullyEdited) {
        this.successfullyEdited = successfullyEdited;
    }

    public String getSuccessfullyDeleted() {
        return successfullyDeleted;
    }

    public void setSuccessfullyDeleted(String successfullyDeleted) {
        this.successfullyDeleted = successfullyDeleted;
    }

    public String getAlreadyRegistered() {
        return alreadyRegistered;
    }

    public void setAlreadyRegistered(String alreadyRegistered) {
        this.alreadyRegistered = alreadyRegistered;
    }

    public String getWrongConfirmationCode() {
        return wrongConfirmationCode;
    }

    public void setWrongConfirmationCode(String wrongConfirmationCode) {
        this.wrongConfirmationCode = wrongConfirmationCode;
    }

    public String getExistingAdministrator() {
        return existingAdministrator;
    }

    public void setExistingAdministrator(String existingAdministrator) {
        this.existingAdministrator = existingAdministrator;
    }

    public String getSuccessfullyChanged() {
        return successfullyChanged;
    }

    public void setSuccessfullyChanged(String successfullyChanged) {
        this.successfullyChanged = successfullyChanged;
    }

    public String getCustomerNotFound() {
        return customerNotFound;
    }

    public void setCustomerNotFound(String customerNotFound) {
        this.customerNotFound = customerNotFound;
    }

    public String getVerificationCodeSentToPhone() {
        return verificationCodeSentToPhone;
    }

    public void setVerificationCodeSentToPhone(String verificationCodeSentToPhone) {
        this.verificationCodeSentToPhone = verificationCodeSentToPhone;
    }

    public String getYourAccountIsNotVerifiedPleaseConfirmIt() {
        return yourAccountIsNotVerifiedPleaseConfirmIt;
    }

    public void setYourAccountIsNotVerifiedPleaseConfirmIt(String yourAccountIsNotVerifiedPleaseConfirmIt) {
        this.yourAccountIsNotVerifiedPleaseConfirmIt = yourAccountIsNotVerifiedPleaseConfirmIt;
    }

    public String getSuccessfullyConfirmed() {
        return successfullyConfirmed;
    }

    public void setSuccessfullyConfirmed(String successfullyConfirmed) {
        this.successfullyConfirmed = successfullyConfirmed;
    }

    public String getSuccessfullySent() {
        return successfullySent;
    }

    public void setSuccessfullySent(String successfullySent) {
        this.successfullySent = successfullySent;
    }

    public String getYourPasswordSuccessfullyChanged() {
        return yourPasswordSuccessfullyChanged;
    }

    public void setYourPasswordSuccessfullyChanged(String yourPasswordSuccessfullyChanged) {
        this.yourPasswordSuccessfullyChanged = yourPasswordSuccessfullyChanged;
    }

    public String getCarSuccessfullyAdded() {
        return carSuccessfullyAdded;
    }

    public void setCarSuccessfullyAdded(String carSuccessfullyAdded) {
        this.carSuccessfullyAdded = carSuccessfullyAdded;
    }

    public String getPhotoSuccessfullyChanged() {
        return photoSuccessfullyChanged;
    }

    public void setPhotoSuccessfullyChanged(String photoSuccessfullyChanged) {
        this.photoSuccessfullyChanged = photoSuccessfullyChanged;
    }

    public String getEntertainmentPlaceNotFound() {
        return entertainmentPlaceNotFound;
    }

    public void setEntertainmentPlaceNotFound(String entertainmentPlaceNotFound) {
        this.entertainmentPlaceNotFound = entertainmentPlaceNotFound;
    }

    public String getVerificationCodeSentToMail() {
        return verificationCodeSentToMail;
    }

    public void setVerificationCodeSentToMail(String verificationCodeSentToMail) {
        this.verificationCodeSentToMail = verificationCodeSentToMail;
    }

    public String getThankYouForYourPurchase() {
        return thankYouForYourPurchase;
    }

    public void setThankYouForYourPurchase(String thankYouForYourPurchase) {
        this.thankYouForYourPurchase = thankYouForYourPurchase;
    }

    public String getInsufficientFunds() {
        return insufficientFunds;
    }

    public void setInsufficientFunds(String insufficientFunds) {
        this.insufficientFunds = insufficientFunds;
    }

    public String getCouponActivated() {
        return couponActivated;
    }

    public void setCouponActivated(String couponActivated) {
        this.couponActivated = couponActivated;
    }

    public String getWrongCouponNumber() {
        return wrongCouponNumber;
    }

    public void setWrongCouponNumber(String wrongCouponNumber) {
        this.wrongCouponNumber = wrongCouponNumber;
    }


    public String getIncorrectPassword() {
        return incorrectPassword;
    }

    public void setIncorrectPassword(String incorrectPassword) {
        this.incorrectPassword = incorrectPassword;
    }

    public String getYourCompanyChanged() {
        return yourCompanyChanged;
    }

    public void setYourCompanyChanged(String yourCompanyChanged) {
        this.yourCompanyChanged = yourCompanyChanged;
    }

    public String getWrongAgencyCode() {
        return wrongAgencyCode;
    }

    public void setWrongAgencyCode(String wrongAgencyCode) {
        this.wrongAgencyCode = wrongAgencyCode;
    }

    public String getSuccessfullyFollowed() {
        return successfullyFollowed;
    }

    public void setSuccessfullyFollowed(String successfullyFollowed) {
        this.successfullyFollowed = successfullyFollowed;
    }

    public String getYourOrderAccepted() {
        return yourOrderAccepted;
    }

    public void setYourOrderAccepted(String yourOrderAccepted) {
        this.yourOrderAccepted = yourOrderAccepted;
    }

    public String getThisTourDoesNotHaveSoManyFreeSeats() {
        return thisTourDoesNotHaveSoManyFreeSeats;
    }

    public void setThisTourDoesNotHaveSoManyFreeSeats(String thisTourDoesNotHaveSoManyFreeSeats) {
        this.thisTourDoesNotHaveSoManyFreeSeats = thisTourDoesNotHaveSoManyFreeSeats;
    }

    public String getYourOrdersUpdated() {
        return yourOrdersUpdated;
    }

    public void setYourOrdersUpdated(String yourOrdersUpdated) {
        this.yourOrdersUpdated = yourOrdersUpdated;
    }

    public String getCouponNotFound() {
        return couponNotFound;
    }

    public void setCouponNotFound(String couponNotFound) {
        this.couponNotFound = couponNotFound;
    }

    public String getYourOrderRejected() {
        return yourOrderRejected;
    }

    public void setYourOrderRejected(String yourOrderRejected) {
        this.yourOrderRejected = yourOrderRejected;
    }

    public String getAgencyNotFound() {
        return agencyNotFound;
    }

    public void setAgencyNotFound(String agencyNotFound) {
        this.agencyNotFound = agencyNotFound;
    }

    public String getDriverNotFound() {
        return driverNotFound;
    }

    public void setDriverNotFound(String driverNotFound) {
        this.driverNotFound = driverNotFound;
    }

    public String getGuideNotFound() {
        return guideNotFound;
    }

    public void setGuideNotFound(String guideNotFound) {
        this.guideNotFound = guideNotFound;
    }

    public String getNotFound() {
        return notFound;
    }

    public void setNotFound(String notFound) {
        this.notFound = notFound;
    }

    public String getYouHaveNewOrder() {
        return youHaveNewOrder;
    }

    public void setYouHaveNewOrder(String youHaveNewOrder) {
        this.youHaveNewOrder = youHaveNewOrder;
    }

    public String getYouHaveCanceledOrder() {
        return youHaveCanceledOrder;
    }

    public void setYouHaveCanceledOrder(String youHaveCanceledOrder) {
        this.youHaveCanceledOrder = youHaveCanceledOrder;
    }

    public String getInThisDayTourIsNotPerformed() {
        return inThisDayTourIsNotPerformed;
    }

    public void setInThisDayTourIsNotPerformed(String inThisDayTourIsNotPerformed) {
        this.inThisDayTourIsNotPerformed = inThisDayTourIsNotPerformed;
    }

    public String getTourNotFound() {
        return tourNotFound;
    }

    public void setTourNotFound(String tourNotFound) {
        this.tourNotFound = tourNotFound;
    }

    public String getYouDontCanceledThisOrder() {
        return youDontCanceledThisOrder;
    }

    public void setYouDontCanceledThisOrder(String youDontCanceledThisOrder) {
        this.youDontCanceledThisOrder = youDontCanceledThisOrder;
    }

    public String getThisTourNotAvailable() {
        return thisTourNotAvailable;
    }

    public void setThisTourNotAvailable(String thisTourNotAvailable) {
        this.thisTourNotAvailable = thisTourNotAvailable;
    }

    public String getThankYouForYourRating() {
        return thankYouForYourRating;
    }

    public void setThankYouForYourRating(String thankYouForYourRating) {
        this.thankYouForYourRating = thankYouForYourRating;
    }

    public String getYourAccountIsSuspended() {
        return yourAccountIsSuspended;
    }

    public void setYourAccountIsSuspended(String yourAccountIsSuspended) {
        this.yourAccountIsSuspended = yourAccountIsSuspended;
    }

    public String getSuccessfullyFinished() {
        return successfullyFinished;
    }

    public void setSuccessfullyFinished(String successfullyFinished) {
        this.successfullyFinished = successfullyFinished;
    }

    public String getManyChangesCustomNotification() {
        return manyChangesCustomNotification;
    }

    public void setManyChangesCustomNotification(String manyChangesCustomNotification) {
        this.manyChangesCustomNotification = manyChangesCustomNotification;
    }


    public String getIncorrectDate() {
        return incorrectDate;
    }

    public void setIncorrectDate(String incorrectDate) {
        this.incorrectDate = incorrectDate;
    }

    public String getPleaseAddAddress() {
        return pleaseAddAddress;
    }

    public void setPleaseAddAddress(String pleaseAddAddress) {
        this.pleaseAddAddress = pleaseAddAddress;
    }


    public String getOrderOutOffTime() {
        return orderOutOffTime;
    }

    public void setOrderOutOffTime(String orderOutOffTime) {
        this.orderOutOffTime = orderOutOffTime;
    }

    public String getWrongCode() {
        return wrongCode;
    }

    public void setWrongCode(String wrongCode) {
        this.wrongCode = wrongCode;
    }

    public String getTheOrderWasMadeWithoutYourPresence() {
        return theOrderWasMadeWithoutYourPresence;
    }

    public void setTheOrderWasMadeWithoutYourPresence(String theOrderWasMadeWithoutYourPresence) {
        this.theOrderWasMadeWithoutYourPresence = theOrderWasMadeWithoutYourPresence;
    }

    public String getOrderHasBeenPaid() {
        return orderHasBeenPaid;
    }

    public void setOrderHasBeenPaid(String orderHasBeenPaid) {
        this.orderHasBeenPaid = orderHasBeenPaid;
    }

    public String getPleaseWaitForTheOrganizerToAcceptYourOrder() {
        return pleaseWaitForTheOrganizerToAcceptYourOrder;
    }

    public void setPleaseWaitForTheOrganizerToAcceptYourOrder(String pleaseWaitForTheOrganizerToAcceptYourOrder) {
        this.pleaseWaitForTheOrganizerToAcceptYourOrder = pleaseWaitForTheOrganizerToAcceptYourOrder;
    }

    public String getYourOrderHasBeenCanceled() {
        return yourOrderHasBeenCanceled;
    }

    public void setYourOrderHasBeenCanceled(String yourOrderHasBeenCanceled) {
        this.yourOrderHasBeenCanceled = yourOrderHasBeenCanceled;
    }

    public String getPleaseWaitForTheGuideToAcceptYourOrder() {
        return pleaseWaitForTheGuideToAcceptYourOrder;
    }

    public void setPleaseWaitForTheGuideToAcceptYourOrder(String pleaseWaitForTheGuideToAcceptYourOrder) {
        this.pleaseWaitForTheGuideToAcceptYourOrder = pleaseWaitForTheGuideToAcceptYourOrder;
    }

    public String getYouWillReceiveAnAdditionalNoticeAboutTheEndOfTheClientsPaymentIfClientDoesntPayWithin15MinutesThOrderWillBeCancelAutomaticallyThankYou() {
        return youWillReceiveAnAdditionalNoticeAboutTheEndOfTheClientsPaymentIfClientDoesntPayWithin15MinutesThOrderWillBeCancelAutomaticallyThankYou;
    }

    public void setYouWillReceiveAnAdditionalNoticeAboutTheEndOfTheClientsPaymentIfClientDoesntPayWithin15MinutesThOrderWillBeCancelAutomaticallyThankYou(String youWillReceiveAnAdditionalNoticeAboutTheEndOfTheClientsPaymentIfClientDoesntPayWithin15MinutesThOrderWillBeCancelAutomaticallyThankYou) {
        this.youWillReceiveAnAdditionalNoticeAboutTheEndOfTheClientsPaymentIfClientDoesntPayWithin15MinutesThOrderWillBeCancelAutomaticallyThankYou = youWillReceiveAnAdditionalNoticeAboutTheEndOfTheClientsPaymentIfClientDoesntPayWithin15MinutesThOrderWillBeCancelAutomaticallyThankYou;
    }

    public String getPleaseAddStartDate() {
        return pleaseAddStartDate;
    }

    public void setPleaseAddStartDate(String pleaseAddStartDate) {
        this.pleaseAddStartDate = pleaseAddStartDate;
    }

    public String getSuccessfullyRefunded() {
        return successfullyRefunded;
    }

    public void setSuccessfullyRefunded(String successfullyRefunded) {
        this.successfullyRefunded = successfullyRefunded;
    }
}